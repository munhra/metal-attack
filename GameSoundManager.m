//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  GameSoundManager.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 2/28/16.
//  Copyright © 2016 Rust Skull. All rights reserved.
//

#import "GameSoundManager.h"
#import "ALSoundSource.h"

@implementation GameSoundManager

AVAudioPlayer *gameAudioPlayer;
NSArray *songsURLArray;
NSMutableArray *songsLocalArray;

int currentSongIndex = 0;
const int numberOfSongs = 6;

@synthesize bandInstrumentsEffectsBuffer;
@synthesize bandInstrumentsSoundSource;
@synthesize soundEffectsALSource;
@synthesize allBandInstruments;
@synthesize isBaseDead;
@synthesize isLeadDead;
@synthesize isVocalDead;
@synthesize isDrummerDead;
@synthesize isBassDead;
@synthesize oneGuySoundBuffer;
@synthesize oneGuySoundSource;
@synthesize isOneGuySoundPlaying;
@synthesize currentGuitarPLaying;
@synthesize vocalName;
@synthesize names;

+(id)sharedInstance
{
    static id master = nil;
    @synchronized(self)
    {
        if (master == nil){
            master = [self new];
            [master preLoadSoundEffects];
            [master resetBandGuysState];
            [master allocateAllinstrumentsDictionary];
            [master allocateEffectsAlSourceArray];
        }
    }
    return master;
}

-(void)allocateEffectsAlSourceArray
{
    self.soundEffectsALSource = [[NSMutableArray alloc] init];
}

-(void)allocateAllinstrumentsDictionary
{
    self.allBandInstruments = [[NSMutableDictionary alloc] init];
}

-(void)resetBandGuysState
{
    [self setIsBaseDead:NO];
    [self setIsBassDead:NO];
    [self setIsLeadDead:NO];
    [self setIsVocalDead:NO];
    [self setIsDrummerDead:NO];
}

-(void)preLoadSoundEffects
{
    NSString *path = [NSString stringWithFormat:@"%@/buttonClick1.wav", [[NSBundle mainBundle] resourcePath]];
    [[OALSimpleAudio sharedInstance] preloadEffect:path];
    path = [NSString stringWithFormat:@"%@/bottleClick1.wav", [[NSBundle mainBundle] resourcePath]];
    [[OALSimpleAudio sharedInstance] preloadEffect:path];
}

-(void)playButtonClick
{
    NSString *path = [NSString stringWithFormat:@"%@/buttonClick1.wav", [[NSBundle mainBundle] resourcePath]];
    [[OALSimpleAudio sharedInstance] playEffect:path];
}

-(void)playStoreSong
{
    NSString *path = [NSString stringWithFormat:@"%@/Music005.caf", [[NSBundle mainBundle] resourcePath]];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playBg:path volume:0.25 pan:0 loop:YES];
}

-(void)playCreditsSong
{
    NSString *path = [NSString stringWithFormat:@"%@/Music004.caf", [[NSBundle mainBundle] resourcePath]];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playBg:path volume:0.25 pan:0 loop:YES];
}

-(void)playLevelSelectionSong
{
    NSString *path = [NSString stringWithFormat:@"%@/Music006.caf", [[NSBundle mainBundle] resourcePath]];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playBg:path volume:0.25 pan:0 loop:YES];
}

-(void)playItemSelectionSong
{
    NSString *path = [NSString stringWithFormat:@"%@/Music008.caf", [[NSBundle mainBundle] resourcePath]];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playBg:path volume:0.25 pan:0 loop:YES];
}

-(void)playGameOverSong
{
    NSString *path = [NSString stringWithFormat:@"%@/Music007.caf", [[NSBundle mainBundle] resourcePath]];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playBg:path volume:0.25 pan:0 loop:YES];
}

-(void)playLevelClearedSong
{
    NSString *path = [NSString stringWithFormat:@"%@/Music002.caf", [[NSBundle mainBundle] resourcePath]];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playBg:path volume:0.4 pan:0 loop:NO];
}
    
-(void)playMainMenuSong
{
    NSString *path = [NSString stringWithFormat:@"%@/Music001.caf", [[NSBundle mainBundle] resourcePath]];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playBg:path volume:0.25 pan:0 loop:YES];
}
    
-(void)playTutorialSong
{
    NSString *path = [NSString stringWithFormat:@"%@/Music003.caf", [[NSBundle mainBundle] resourcePath]];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playBg:path volume:0.3 pan:0 loop:YES];
}

-(void)stopBackgroundSong
{
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio stopBg];
}

-(void)preLoadAllSoundEffects
{
    for (int index = 1; index<=66; index++) {
        NSString *soundFileName;
        if (index < 10) {
            soundFileName = [NSString stringWithFormat:@"%@/_Soundfx00%d.caf",[[NSBundle mainBundle] resourcePath],index];
        }else{
            soundFileName = [NSString stringWithFormat:@"%@/_Soundfx0%d.caf",[[NSBundle mainBundle] resourcePath],index];
        }
        [[OALSimpleAudio sharedInstance] preloadEffect:soundFileName];
    }
}

-(void)playSoundEffectByName:(NSString *)soundName
{
    if ([soundName hasPrefix:@"_Soundfx"]) {
        NSString *path = [NSString stringWithFormat:@"%@/%@.caf", [[NSBundle mainBundle] resourcePath], soundName];
        [[OALSimpleAudio sharedInstance] playEffect:path];
    }
}

-(void)unloadBandGuySounds:(NSArray *)bandGuyNames
{
    for (NSString *name in bandGuyNames) {
        NSString *soundPath  = [NSString stringWithFormat:@"%@/%@Sound.caf", [[NSBundle mainBundle] resourcePath],name];
        [[OALSimpleAudio sharedInstance] unloadEffect:soundPath];
        [allBandInstruments removeObjectForKey:name];
    }
}

-(void)preloadBandGuySounds:(NSArray *)bandGuyNames
{
    for (NSString *name in bandGuyNames) {
        NSString *soundPath  = [NSString stringWithFormat:@"%@/%@Sound.caf", [[NSBundle mainBundle] resourcePath],name];
        [allBandInstruments setObject:[[OALSimpleAudio sharedInstance] preloadEffect:soundPath] forKey:name];
    }
}

-(void)stopBandInstrumentsEffects
{
    for (ALSource *alsource in bandInstrumentsSoundSource) {
        [alsource stop];
    }
    [self unscheduleAllSelectors];
    [bandInstrumentsSoundSource removeAllObjects];
}


-(void)playBandInstrumentsEffectsEnding:(NSArray *)bandGuyNames {
    int bandGuyIndex = 0;
    bandInstrumentsSoundSource = [[NSMutableArray alloc] init];
    for (NSString *name in bandGuyNames) {
        if (bandGuyIndex != 3) {
            ALBuffer *soundBuffer = [allBandInstruments objectForKey:name];
            [bandInstrumentsSoundSource addObject:[[OALSimpleAudio sharedInstance] playBuffer:soundBuffer volume:0.7 pitch:1 pan:0 loop:YES]];
            bandGuyIndex++;
        }
    }
}

-(void)playBandInstrumentsEffects:(NSArray *)bandGuyNames
{
    
    /*
     Array band guys positions
     
     0 - Drummer
     1 - Lead
     2 - Base
     3 - Vocal
     4 - Bass
     
     */
    
    self.currentGuitarPLaying = BASE_CHANNEL;
    int bandGuyIndex = 0;
    bool playWithLoop = true;
    bandInstrumentsSoundSource = [[NSMutableArray alloc] init];
    for (NSString *name in bandGuyNames) {
        playWithLoop = YES;
        [[self names] addObject:name];
        ALBuffer *soundBuffer = [allBandInstruments objectForKey:name];
        float volume = 0.7;
        if (bandGuyIndex == 3) {
            volume = 0.0;
            playWithLoop = NO;
            self.vocalName = name;
        }
        [bandInstrumentsSoundSource addObject:[[OALSimpleAudio sharedInstance] playBuffer:soundBuffer volume:volume pitch:1 pan:0 loop:playWithLoop]];
        bandGuyIndex++;
    }
    [self schedule:@selector(controlSoundSolo) interval:10];
}

-(NSArray *)randomSoundVolume
{
    
    NSMutableArray *bandGuyVolumeArray = [[NSMutableArray alloc] initWithCapacity:5];
    NSArray *volumeArray = [[NSArray alloc] initWithObjects:[[NSNumber alloc] initWithFloat:0.7], [[NSNumber alloc] initWithFloat:0.0],nil];
    
    float total = 0;
    for ( int i = 0 ; i < 5 ; i++) {
        int randomIndex = 1;
        NSNumber *randomVolume = (NSNumber *)[volumeArray objectAtIndex:randomIndex];
        [bandGuyVolumeArray addObject:randomVolume];
        total = total + [(NSNumber *)[volumeArray objectAtIndex:randomIndex] floatValue];
    }
    
    if (total == 0.0) {
        int randomIndex = arc4random() % 5;
        if (randomIndex == 3) {
            randomIndex = 4;
        }
        [bandGuyVolumeArray replaceObjectAtIndex:randomIndex withObject:[[NSNumber alloc] initWithFloat:0.7]];
    }
    
    return bandGuyVolumeArray;
}


-(void)controlSoundSolo
{
    NSLog(@"controlSoundSolo");
    
    /*
    0 - Drummer
    1 - Lead
    2 - Base
    3 - Vocal
    4 - Bass
    */
    
    NSArray *volumeArray = [self randomSoundVolume];
   
    for (int i = 0; i < 5 ; i++) {
        if (i == 0 && !isDrummerDead) {
            [self changeBandGuySound:i volume:volumeArray];
        }
        if (i == 1 && !isLeadDead) {
            [self changeBandGuySound:i volume:volumeArray];
        }
        if (i == 2 && !isBaseDead) {
            [self changeBandGuySound:i volume:volumeArray];
        }
        if (i == 4 && !isBassDead) {
            [self changeBandGuySound:i volume:volumeArray];
        }
    }
    
    int randomVocal = arc4random() % 100;
    if (randomVocal > 20) {
        ALBuffer *soundBuffer = [allBandInstruments objectForKey:vocalName];
        [[OALSimpleAudio sharedInstance] playBuffer:soundBuffer volume:0.7 pitch:1 pan:0 loop:NO];
    }
}

-(void)changeBandGuySound:(int)index volume:(NSArray *)volumeArray
{
    ALSource *source = [bandInstrumentsSoundSource objectAtIndex:index];
    float bandGuyVolume = [(NSNumber *)[volumeArray objectAtIndex:index] floatValue];
    [source setVolume:bandGuyVolume];
}

-(void)playVocalSound
{
    ALBuffer *soundBuffer = [allBandInstruments objectForKey:self.vocalName];
    if ([bandInstrumentsSoundSource count] == 4) {
        [bandInstrumentsSoundSource addObject:[[OALSimpleAudio sharedInstance] playBuffer:soundBuffer volume:0.7 pitch:1 pan:0 loop:NO]];
    }else{
        ALSource *alVocalSource = [bandInstrumentsSoundSource lastObject];
        if ([alVocalSource state] == AL_STOPPED) {
            [alVocalSource play];
        }
    }
}

-(void)playOneBandGuySound:(NSString *)bandGuyName withLoop:(BOOL)loop
{
    oneGuySoundBuffer = [allBandInstruments objectForKey:bandGuyName];
    oneGuySoundSource = [[OALSimpleAudio sharedInstance] playBuffer:oneGuySoundBuffer volume:0 pitch:1 pan:0 loop:loop];
    [oneGuySoundSource fadeTo:1 duration:3 target:nil selector:nil];
    [self setIsOneGuySoundPlaying:YES];
}

-(void)stopOneBandGuySound
{
    [self setIsOneGuySoundPlaying:NO];
    [oneGuySoundSource stop];
}

-(void)stopSound
{
    [oneGuySoundSource stop];
}

//TODO revival power up will bring guys back go play

-(void)muteVocal
{
    /*
    if ([bandInstrumentsSoundSource count] > 0) {
        ALSource *alsource = [bandInstrumentsSoundSource objectAtIndex:VOCAL_CHANNEL];
        alsource.volume = 0.0;
        [self setIsVocalDead:YES];
        NSLog(@"muteVocal");
    }*/
}

-(void)muteBass
{
    
    if ([bandInstrumentsSoundSource count] > 0) {
        ALSource *alsource = [bandInstrumentsSoundSource objectAtIndex:BASS_CHANNEL];
        alsource.volume = 0.0;
        [self setIsBassDead:YES];
        NSLog(@"muteBass");
    }
}

-(void)muteBaseGuitar
{
    
    if ([bandInstrumentsSoundSource count] > 0) {
        ALSource *alsource = [bandInstrumentsSoundSource objectAtIndex:BASE_CHANNEL];
        alsource.volume = 0.0;
        [self setIsBaseDead:YES];
        NSLog(@"muteBaseGuitar");
    }
}

-(void)muteLeadGuitar
{
    if ([bandInstrumentsSoundSource count] > 0) {
        ALSource *alsource = [bandInstrumentsSoundSource objectAtIndex:LEAD_CHANNEL];
        alsource.volume = 0.0;
        [self setIsLeadDead:YES];
        NSLog(@"muteLeadGuitar");
    }
}

-(void)muteDrummer
{
    if ([bandInstrumentsSoundSource count] > 0) {
        ALSource *alsource = [bandInstrumentsSoundSource objectAtIndex:DRUMMER_CHANNEL];
        alsource.volume = 0.0;
        [self setIsDrummerDead:YES];
        NSLog(@"muteDrummer");
    }
}

-(void)preLoadPowerupSoundEffects
{
    for (int index = 1; index<=31; index++) {
        NSString *soundFileName;
        if (index < 10) {
            soundFileName = [NSString stringWithFormat:@"%@/PowerUpFX0%d.caf",[[NSBundle mainBundle] resourcePath],index];
        }else{
            soundFileName = [NSString stringWithFormat:@"%@/PowerUpFX%d.caf",[[NSBundle mainBundle] resourcePath],index];
        }
        [[OALSimpleAudio sharedInstance] preloadEffect:soundFileName];
    }
}

-(void)unloadAllSounds
{
    [[OALSimpleAudio sharedInstance] unloadAllEffects];
}

@end
