//
//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  LevelSelector.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 29/04/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "LevelSelector.h"
#import "LevelSceneController.h"
#import "ItemSelection.h"
#import "GameSoundManager.h"
#import "CCTextureCache.h"

@implementation LevelSelector

@synthesize level1Button;
@synthesize level2Button;
@synthesize level3Button;
@synthesize level4Button;
@synthesize level5Button;
@synthesize levelSlider;
@synthesize levelNumber;
@synthesize selectedLevelNumber;
@synthesize levelsPerContext;
@synthesize firstLevelPerContext;
@synthesize reachedContext;
@synthesize reachedLevel;
@synthesize lockedLevel;
@synthesize buttonArray;

int selectedContext = 1;

-(void)onEnter
{
    [super onEnter];
    [[GameSoundManager sharedInstance] playLevelSelectionSong];
    [self storeButtonsOnArray];
    [self calculateReachedContext];
    [[self levelSlider] setDelegate:self];
    [[CCTextureCache sharedTextureCache] dumpCachedTextureInfo];
    if ([[UniversalInfo sharedInstance] isTesting]) {
        [self setupTestTimer];
    }
}

-(void)setupTestTimer
{
    __weak LevelSelector *weakSelf = self;
    [self scheduleBlock:^(CCTimer *timer) {
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        weakSelf.selectedLevelNumber = [userdef integerForKey:@"Level"] + 1;
        if (weakSelf.selectedLevelNumber == 100) {
            weakSelf.selectedLevelNumber = 1;
        }
        [self doLevelSelection];
    } delay:5];
}


-(void)onExit
{
    [super onExit];
    [[GameSoundManager sharedInstance] stopBackgroundSong];
    [[self levelSlider] setDelegate:nil];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [self removeLevelSelectorTextures];
}

-(void)removeLevelSelectorTextures
{
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/LevelSelection/1 garage full.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/LevelSelection/locked button full.png"];
}

- (void)dealloc
{
    NSLog(@"Dealloc LevelSelector");
    [[CCTextureCache sharedTextureCache] dumpCachedTextureInfo];
}

-(void)storeButtonsOnArray
{
    buttonArray = @[level1Button,level2Button,level3Button,level4Button,level5Button];
}

-(void)calculateReachedContext
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    
    int storedLevel = [userdef integerForKey:@"Level"];
    reachedLevel = storedLevel;
    
    if (storedLevel < 100) {
        reachedLevel++;
    }
    
    //reachedLevel = [userdef integerForKey:@"Level"] + 1;
    
    
    CCSpriteFrame *lockedSpriteFrame = [lockedLevel spriteFrame];
    
    for (NSNumber *maxLevelContextNumber in levelsPerContext) {
        if (reachedLevel <= maxLevelContextNumber.intValue) {
            reachedContext = [levelsPerContext indexOfObject:maxLevelContextNumber];
            break;
        }
    }
    
    for (int i = reachedContext + 1; i < 5; i++) {
        CCButton *levelButton = [buttonArray objectAtIndex:i];
        [levelButton setBackgroundSpriteFrame:lockedSpriteFrame forState:CCControlStateNormal];
        [levelButton setBackgroundSpriteFrame:lockedSpriteFrame forState:CCControlStateDisabled];
        [levelButton setEnabled:NO];
        [levelButton setName:@"locked"];
    }
}

-(void)didLoadFromCCB
{
    levelsPerContext = [[LevelSceneController sharedInstance] levelsPerLevelContext];
    firstLevelPerContext = [[LevelSceneController sharedInstance] firstLevelPerContext];
    lockedLevel = (CCSprite *)[CCBReader load:@"OtherSprites/LockedLevel"];
}

-(void)doMenu
{
    NSLog(@"doMenu");
    [[self levelSlider] setDelegate:nil];
    CCNode *door = [self getChildByName:@"Door" recursively:YES];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
    }];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
}

-(void)doLevelSelection
{
    NSLog(@"doGamePlay");
    [[self levelSlider] setDelegate:nil];
    LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
    [[self levelSlider] setDelegate:nil];
    CCNode *door = [self getChildByName:@"Door" recursively:YES];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[lvcontroller loadLevelScene:selectedLevelNumber - 1 waveNumber:0]];
    }];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
}

-(void)cancelLevelSelection
{
    [[self animationManager] runAnimationsForSequenceNamed:@"HidePunkTimeline"];
    [self enableButtons];
}

-(void)callVanForItems
{
    NSLog(@"Call VAN !!");
    [[self levelSlider] setDelegate:nil];
    CCScene *itemSelectionScence = [CCBReader loadAsScene:@"ItemSelection"];
    [itemSelectionScence getChildByName:@"ItemSelection" recursively:NO];
    ItemSelection *itemSelection = (ItemSelection*)[itemSelectionScence getChildByName:@"ItemSelection" recursively:NO];
    [itemSelection setNextLevel:selectedLevelNumber -1];
    [[CCDirector sharedDirector] replaceScene:itemSelectionScence];
}

-(void)selectLevel:(id)sender
{
    [self playLevelSelectSound];
    [[self animationManager] runAnimationsForSequenceNamed:@"ShowPunkTimeline"];
    CCButton *contextButton = (CCButton*) sender;
    selectedContext = contextButton.name.integerValue;
    
    NSNumber *firstLevel = (NSNumber *) [firstLevelPerContext objectAtIndex:selectedContext - 1];
    
    NSNumber *contextLevels = (NSNumber *) [levelsPerContext objectAtIndex:selectedContext -1];
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    
    //int maxContextLevel = contextLevels.integerValue + firstLevel.integerValue;
    //int maxContextLevel = contextLevels.integerValue - 1;
    int maxContextLevel = contextLevels.integerValue;
    reachedLevel = [userdef integerForKey:@"Level"] + 1;
    
    if (reachedLevel > maxContextLevel) {
        selectedLevelNumber = maxContextLevel;
        //[[self levelSlider] setSliderValue:1];
    }else{
        selectedLevelNumber = reachedLevel;
        //[[self levelSlider] setSliderValue:[[NSNumber alloc] initWithInt:reachedLevel].floatValue/contextLevels.floatValue];
    }
    
    [[self levelSlider] setSliderValue:1];
    
    //selectedLevelNumber = firstLevel.intValue;
    [self updateLevelLabel];
    [self disableButtons];
}

-(void)changeSlideValue:(float)sliderValue
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    reachedLevel = [userdef integerForKey:@"Level"] + 1;
    
    
    NSNumber *contextTotalLevels = (NSNumber *) [levelsPerContext objectAtIndex:selectedContext - 1];
    NSNumber *firstLevel;
    
    if (selectedContext == 1) {
        firstLevel = [[NSNumber alloc] initWithInt:1];
    }else{
        firstLevel = (NSNumber *) [firstLevelPerContext objectAtIndex:selectedContext - 1];
        firstLevel = [[NSNumber alloc] initWithInt:firstLevel.integerValue + 1];
    }
    
    //NSNumber *firstLevel = (NSNumber *) [firstLevelPerContext objectAtIndex:selectedContext - 1];
    
    int maxLevelNumber;
    
    if (reachedLevel > contextTotalLevels.intValue) {
        //maxLevelNumber = contextTotalLevels.intValue - 1;
        maxLevelNumber = contextTotalLevels.intValue;
    }else{
        maxLevelNumber = reachedLevel;
    }

    float floatLevelValue = firstLevel.intValue + (sliderValue * (maxLevelNumber - firstLevel.intValue));
    
    selectedLevelNumber = [[NSNumber numberWithFloat:floatLevelValue] intValue];
    [self updateLevelLabel];
}

-(void)updateLevelLabel
{
    NSString *strLevelNumber = [NSString stringWithFormat:@"%d",selectedLevelNumber];
    strLevelNumber = [[UniversalInfo sharedInstance] addZeroesToNumber:strLevelNumber numberOfZeroes:2];
    [[self levelNumber] setString:strLevelNumber];
}

-(void)addLevel
{
    NSLog(@"addLevel");

    int maxLevelValue = [[self levelsPerContext] objectAtIndex:reachedContext];
    
    if (selectedLevelNumber < maxLevelValue) {
        selectedLevelNumber++;
    }

    [self updateLevelLabel];
}

-(void)subtractLevel
{
    NSLog(@"subtractLevel");
    if (selectedLevelNumber > 1) {
        selectedLevelNumber--;
    }
    [self updateLevelLabel];
}

-(void)disableButtons
{
    for (CCButton *button in buttonArray) {
        if (![[button name] isEqualToString:@"locked"]) {
            [button setEnabled:NO];
        }
    }
}

-(void)enableButtons
{
    for (CCButton *button in buttonArray) {
        if (![[button name] isEqualToString:@"locked"]) {
            [button setEnabled:YES];
        }
    }
}

-(void)playLevelSelectSound
{
    NSLog(@"playLevelSelectSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx001"];
}

-(void)playPunkShowAndHideSound
{
    NSLog(@"playPunkShowSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}

@end
