//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  InfinityBG.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 12/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface InfinityBG : CCNode {
    
    CCSprite *visibleBG;
    CCSprite *leftBG;
    CCSprite *rightBG;
    CCSprite *upperBG;
    CCSprite *lowerBG;
    CCSprite *upperRightBG;
    CCSprite *lowerLeftBG;
    CCSprite *fixedBG;
}

@property (nonatomic,retain) CCSprite *visibleBG;
@property (nonatomic,retain) CCSprite *leftBG;
@property (nonatomic,retain) CCSprite *rightBG;
@property (nonatomic,retain) CCSprite *upperBG;
@property (nonatomic,retain) CCSprite *lowerBG;
@property (nonatomic,retain) CCSprite *upperRightBG;
@property (nonatomic,retain) CCSprite *lowerLeftBG;
@property (nonatomic,retain) CCSprite *fixedBG;

-(void)setBGColor:(CCColor *)color;

@end
