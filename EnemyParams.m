//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  EnemyParams.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 3/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyParams.h"

@implementation EnemyParams

@synthesize enemyName;
@synthesize armor;
@synthesize scorePoints;
@synthesize numberOfFrames;
@synthesize timeToReach;
@synthesize typeOfPowerUp;
@synthesize weaponDamage;
@synthesize atackType;
@synthesize shootStartPosition;
@synthesize shootStyle;
@synthesize upperAction;
@synthesize lowerAction;
@synthesize sideAction;
@synthesize fLowerAction;
@synthesize fSideAction;
@synthesize fUpperAction;
@synthesize shootHit;
@synthesize shootSound;
@synthesize dropCoinHighLevel;
@synthesize dropCoinLowerLevel;

@end
