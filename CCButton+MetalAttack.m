//
//  CCButton+MetalAttack.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 9/28/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCButton+MetalAttack.h"
#import "GameSoundManager.h"

@implementation CCButton (MetalAttack)

-(void)playSimpleButtonClick
{
    NSLog(@"Play simple click sound");
}

-(void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    NSString *clickText = [NSString stringWithFormat:@"Touch began overridden !!!! %@",[self name]];
    NSLog(@"%@", clickText);
 
    if ([self name]) {
        [[GameSoundManager sharedInstance] playSoundEffectByName:[self name]];
    }else{
        [[GameSoundManager sharedInstance] playButtonClick];
    }
    
    [super touchBegan:touch withEvent:event];
}

@end
