//
//  ShieldPowerUp.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 02/11/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "ShieldPowerUp.h"

@implementation ShieldPowerUp

@synthesize showPosition;
@synthesize hidePosition;

static NSMutableArray *shieldPositions;


-(id)initWithShowPosition:(CGPoint)position1 hidePosition:(CGPoint)position2
{
    self = [super init];
    
    if (self){
        
        self.showPosition = position1;
        self.hidePosition = position2;
    }
    
    return self;
}

+(NSMutableArray *)preparePositionArray
{
    
    ShieldPowerUp *shieldPosition1 = [[ShieldPowerUp alloc] initWithShowPosition:showShield1Position hidePosition:hideShield1Position];
    ShieldPowerUp *shieldPosition2 = [[ShieldPowerUp alloc] initWithShowPosition:showShield2Position hidePosition:hideShield2Position];
    ShieldPowerUp *shieldPosition3 = [[ShieldPowerUp alloc] initWithShowPosition:showShield3Position hidePosition:hideShield3Position];
    ShieldPowerUp *shieldPosition4 = [[ShieldPowerUp alloc] initWithShowPosition:showShield4Position hidePosition:hideShield4Position];
    ShieldPowerUp *shieldPosition5 = [[ShieldPowerUp alloc] initWithShowPosition:showShield5Position hidePosition:hideShield5Position];
    ShieldPowerUp *shieldPosition6 = [[ShieldPowerUp alloc] initWithShowPosition:showShield6Position hidePosition:hideShield6Position];
    ShieldPowerUp *shieldPosition7 = [[ShieldPowerUp alloc] initWithShowPosition:showShield7Position hidePosition:hideShield7Position];
    ShieldPowerUp *shieldPosition8 = [[ShieldPowerUp alloc] initWithShowPosition:showShield8Position hidePosition:hideShield8Position];
    ShieldPowerUp *shieldPosition9 = [[ShieldPowerUp alloc] initWithShowPosition:showShield9Position hidePosition:hideShield9Position];
    ShieldPowerUp *shieldPosition10 = [[ShieldPowerUp alloc] initWithShowPosition:showShield10Position hidePosition:hideShield10Position];
    
    shieldPositions = [NSMutableArray arrayWithObjects:shieldPosition1,shieldPosition2,shieldPosition3,shieldPosition4,shieldPosition5,
                       shieldPosition6, shieldPosition7, shieldPosition8, shieldPosition9, shieldPosition10, nil];
    
    for (ShieldPowerUp *shieldPowerup in shieldPositions) {
        CCActionMoveTo *showShieldAction = [CCActionMoveTo actionWithDuration:1 position:shieldPowerup.showPosition];
        CCActionMoveTo *hideShieldAction = [CCActionMoveTo actionWithDuration:1 position:shieldPowerup.hidePosition];
        [shieldPowerup setShowShieldAction:showShieldAction];
        [shieldPowerup setHideShieldAction:hideShieldAction];
    }
    
    return shieldPositions;
}

@end
