//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  UniversalInfo.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 31/03/13.
//
//  This class will be responsible to map all the game coordinates
//  for ipad/inphone universal version. I dont't know if the best
//  way to do this is checking every time for the device or check just
//  once. This class will hadle some game utility

#import "UniversalInfo.h"
#import "CCAnimation.h"
#import "GameColors.h"
#import <AdColony/AdColony.h>

const float screenHideProportion = 0.046875;
CGPoint screenSize;
NSArray *colorArray;

@implementation UniversalInfo

@synthesize isTesting;
@synthesize gotoStore;
@synthesize gotoNextLevel;

+ (UniversalInfo *)sharedInstance
{
    static UniversalInfo *myInstance = nil;
    if (nil == myInstance) {
        myInstance  = [[[self class] alloc] init];
        screenSize = ccp([[CCDirector sharedDirector] viewSize].width,[[CCDirector sharedDirector] viewSize].height);
        [myInstance initializeColorArray];
        [myInstance setIsTesting:NO];
        [myInstance setGotoStore:YES];
        [myInstance setGotoNextLevel:NO];
    }
    return myInstance;
}

-(void)initializeColorArray
{
    colorArray = @[[[GameColors alloc] initWithColors:60 green:188 blue:252],
                   [[GameColors alloc] initWithColors:104 green:136 blue:252],
                   [[GameColors alloc] initWithColors:152 green:120 blue:248],
                   [[GameColors alloc] initWithColors:248 green:120 blue:248],
                   [[GameColors alloc] initWithColors:248 green:88 blue:152],
                   [[GameColors alloc] initWithColors:248 green:120 blue:88],
                   [[GameColors alloc] initWithColors:252 green:160 blue:68],
                   [[GameColors alloc] initWithColors:248 green:184 blue:0],
                   [[GameColors alloc] initWithColors:184 green:248 blue:24],
                   [[GameColors alloc] initWithColors:88 green:216 blue:84],
                   [[GameColors alloc] initWithColors:88 green:248 blue:152],
                   [[GameColors alloc] initWithColors:0 green:232 blue:216],
                   [[GameColors alloc] initWithColors:164 green:228 blue:252],
                   [[GameColors alloc] initWithColors:184 green:184 blue:248],
                   [[GameColors alloc] initWithColors:216 green:184 blue:248],
                   [[GameColors alloc] initWithColors:248 green:184 blue:248],
                   [[GameColors alloc] initWithColors:248 green:164 blue:192],
                   [[GameColors alloc] initWithColors:240 green:208 blue:176],
                   [[GameColors alloc] initWithColors:252 green:224 blue:168],
                   [[GameColors alloc] initWithColors:216 green:248 blue:120],
                   [[GameColors alloc] initWithColors:184 green:248 blue:184],
                   [[GameColors alloc] initWithColors:184 green:248 blue:216],
                   [[GameColors alloc] initWithColors:0 green:252 blue:252],
                   [[GameColors alloc] initWithColors:248 green:216 blue:248]];
}

-(DeviceType)getDeviceType
{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
     
        if (screenSize.y < 568) {
            return IPHONE_4_OR_LESS;
        }
        
        if (screenSize.y == 568){
            return IPHONE_5;
        }
        
        if (screenSize.y == 667) {
            return IPHONE_6;
        }
        
        if (screenSize.y == 736) {
            return IPHONE_6P;
        }
        
    }
    
    return 0;
}

-(BOOL) isDeviceIpad
{
    NSLog(@"isDeviceIpad");
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        return YES;
    }else{
        return NO;
    }
}

-(CGPoint) screenCenter
{
    return ccp(([[CCDirector sharedDirector] viewSize].width * 0.5),
               ([[CCDirector sharedDirector] viewSize].height * 0.5));
}

-(CGPoint)enemyPosition1
{
    NSLog(@"position %f, %f", -screenSize.x * screenHideProportion, -screenSize.y * screenHideProportion);
    return ccpMult(screenSize, -screenHideProportion);
}

-(CGPoint)enemyPosition2
{
    NSLog(@"position %f, %f", -screenSize.x * screenHideProportion, screenSize.y *0.5);
    return ccp(-screenSize.x * screenHideProportion, screenSize.y * 0.5);
}

-(CGPoint)enemyPosition3
{
    NSLog(@"position %f, %f", -screenSize.x * screenHideProportion, screenSize.y *(1+screenHideProportion));
    return ccp(-screenSize.x * screenHideProportion , screenSize.y *(1+screenHideProportion));
}

-(CGPoint)enemyPosition4
{
    NSLog(@"position %f, %f", screenSize.x * 0.5, screenSize.y*(1+screenHideProportion));
    return ccp(screenSize.x * 0.5, screenSize.y*(1+screenHideProportion));
}

-(CGPoint)enemyPosition5
{
    NSLog(@"position %f, %f", screenSize.x * (1+screenHideProportion), screenSize.y*(1+screenHideProportion));
    return ccp(screenSize.x * (1+screenHideProportion), screenSize.y*(1+screenHideProportion));
}

-(CGPoint)enemyPosition6
{
    NSLog(@"position %f, %f", screenSize.x * (1+screenHideProportion), screenSize.y * 0.5);
    return ccp(screenSize.x * (1+screenHideProportion), screenSize.y * 0.5);
}

-(CGPoint)enemyPosition7
{
    NSLog(@"position %f, %f", screenSize.x * (1+screenHideProportion), -screenSize.y * screenHideProportion);
    return ccp(screenSize.x * (1+screenHideProportion), -screenSize.y * screenHideProportion);
}

-(CGPoint)enemyPosition8
{
    NSLog(@"position %f, %f", screenSize.x * 0.5, -screenSize.y * screenHideProportion);
    return ccp(screenSize.x * 0.5, -screenSize.y * screenHideProportion);
}

-(CGPoint)calculateAnchorPoint:(CGPoint)anchorPos width:(int)w height:(int)h
{
    return CGPointMake((1 - anchorPos.x/w), (1 - anchorPos.y/h));
}

-(NSString *)addZeroesToNumber:(NSString*)number numberOfZeroes:(int)zeroes
{
    // max 6 digits
    // default 4
    int zeroesTofill = zeroes - [number length];
    NSString *zeroesStr = @"";
    NSString *completeStr = number;
    
    for (int i = 0; i <= zeroesTofill; i++) {
        zeroesStr = [zeroesStr stringByAppendingString:@"0"];
    }
    
    completeStr = [zeroesStr stringByAppendingString:number];
    
    return completeStr;
}

-(CCSprite *)getSpriteWithCoinAnimation
{
    NSString *coinFrameName = @"coinflip0";
    id coinAnimFrames = [[NSMutableArray alloc] init];
    
    for(int i = 1; i <= 6; i++) {
        NSString *frameName = [coinFrameName stringByAppendingString:@"%d.png"];
        CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:frameName,i]];
        [coinAnimFrames addObject:frame];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:coinAnimFrames delay:0.09f];
    
    id coinAnimation = [CCActionRepeatForever actionWithAction:
                         [CCActionAnimate actionWithAnimation:animation]];
    
    
    CCSprite *coinSprite = [[CCSprite alloc] init];
    [coinSprite runAction:coinAnimation];
    
    return coinSprite;
}

-(UIImage*)screenshot{
    
    [CCDirector sharedDirector].nextDeltaTimeZero = YES;
    CGSize size = [CCDirector sharedDirector].viewSize;
    CCRenderTexture *render = [CCRenderTexture renderTextureWithWidth:size.width
                                                               height:size.height];
    [render begin];
    [[[CCDirector sharedDirector] runningScene] visit];
    [render end];
    return [render getUIImage];
}

- (UIColor *)averageColor:(UIImage *)image {
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), image.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    if(rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }
    else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}

-(CCColor *)generateRandomColor
{
    int randomColorIndex = arc4random_uniform([colorArray count]);
    GameColors *gameColors = [colorArray objectAtIndex:randomColorIndex];
    return [[CCColor alloc] initWithRed:gameColors.red/255 green:gameColors.green/255 blue:gameColors.blue/255];
}

-(void)playInterstitialVideo:(int)probability
{
    int adsChance = arc4random_uniform(100);
    if (adsChance <= probability) {
        [AdColony playVideoAdForZone:@"vz2b7ca1612b934349a8" withDelegate:nil];
    }
}

-(void)playVideoForReward:(int)probability
{
    //interstital zone for video rewards
    int adsChance = arc4random_uniform(100);
    if (adsChance <= probability) {
        [AdColony playVideoAdForZone:@"vz548d673f185e4d4294" withDelegate:nil withV4VCPrePopup:YES andV4VCPostPopup:YES];
    }
}


@end
