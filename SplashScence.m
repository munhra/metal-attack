//
//  SplashScence.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 02/09/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "SplashScence.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation SplashScence

@synthesize moviePlayer;
@synthesize moviePlayerController;

-(void)didLoadFromCCB
{
    NSLog(@"Did load SplashScence");
    
    [self scheduleBlock:^(CCTimer *timer) {
        [self goMenu];
    } delay:1];
    
    
    [self playSplashVideo];
}

-(void)playSplashVideo2
{
    NSLog(@"Play Splash Video 2");
    
    UIView *scenceView = [[CCDirector sharedDirector] view];
    
    
    CGSize sceneSize = [[CCDirector sharedDirector] viewSize];

    
    NSURL *splashURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"splash" ofType:@"mp4"]];

    MPMoviePlayerController *player2 = [[MPMoviePlayerController alloc] initWithContentURL:splashURL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification object:player2];
    
    //CGRect movieRect = CGRectMake(0, 0, sceneSize.width, sceneSize.height);
    CGRect movieRect = CGRectMake(50, 50, 300, 300);
    
    [player2 prepareToPlay];
    player2.view.frame = movieRect;
    
    [scenceView addSubview:player2.view];
    player2.shouldAutoplay = true;
    
    [player2 play];
    
    /*
    MPMoviePlayerController *player2 = [[MPMoviePlayerController alloc]
                                        initWithContentURL:[NSURL fileURLWithPath:url]];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(movieFinishedCallback:)
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:player2];
    
    //---play partial screen---
    player2.view.frame = rScreen;
    // player2.view.frame = CGRectMake(0,0, m_iScreenWidth, m_iScreenHeight);
    anid = [self addSubview:player2.view];
    player2.shouldAutoplay=TRUE;
    //---play movie---
    [player2 play];*/
    
    
}

-(void)playSplashVideo
{
    NSLog(@"Play Splash Video");
    
    UIView *scenceView = [[CCDirector sharedDirector] view];
    
    
    CGSize sceneSize = [[CCDirector sharedDirector] viewSize];
    
    //CGRect movieRect = CGRectMake(0, 0 , 1600, 2339);
    
    CGRect movieRect = CGRectMake(0,0 , sceneSize.width, sceneSize.height);
    
    NSURL *splashURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"splash1136_640" ofType:@"mp4"]];
    
    moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL:splashURL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification
     object:[moviePlayerController moviePlayer]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieReady:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:nil];
    
    moviePlayerController.view.frame = movieRect;

    
    //[scenceView addSubview:moviePlayerController.view];
    
    
    //UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    //[window addSubview:moviePlayerController.view];
    //[window makeKeyAndVisible];
    
    
    //player.moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer = [moviePlayerController moviePlayer];
    
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    
    moviePlayer.scalingMode = MPMovieRepeatModeNone;
    //moviePlayer.shouldAutoplay = true;
    //[moviePlayer play];
    
}

-(void)movieReady:(NSNotificationCenter *) notification
{
    NSLog(@"movieReady");
    UIView *scenceView = [[CCDirector sharedDirector] view];
    [scenceView addSubview:self.moviePlayerController.view];
    
    [self.moviePlayer play];
}

-(void)movieFinishedCallback: (NSNotificationCenter *) notification
{
    NSLog(@"movieFinishedCallback");
    /*
    UIView *playerView = self.moviePlayerController.view;
    
    [playerView removeFromSuperview];
    
    [self goMenu];
     */
}




-(void)goMenu
{
    NSLog(@"Go menu from splash");
    CCScene *mainScence = [CCBReader loadAsScene:@"MainMenu"];
    [[CCDirector sharedDirector] replaceScene:mainScence];
    
}

@end
