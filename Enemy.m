//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  Enemy.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Enemy.h"

@implementation Enemy

@synthesize armor;
@synthesize initialPosition;
@synthesize enemySprite;
@synthesize enemyAction;
@synthesize shootSprite;
@synthesize delegate;
@synthesize removeObject;
@synthesize enemyParticle;
@synthesize scorePoints;
@synthesize frames;
@synthesize name;
@synthesize timeToReach;
@synthesize timeToStart;
@synthesize typeOfPowerUp;
@synthesize weaponDamage;
@synthesize shootPosition;
@synthesize activeShoots;
@synthesize definedPosition;
@synthesize attackType;
@synthesize enemyAttack;
@synthesize isMeeleAttacking;
@synthesize bandsprite;
@synthesize hitRect;
@synthesize hitComponent;
@synthesize isInMovement;
@synthesize shootStartPosition;
#warning remove shootStartPositions
@synthesize shootStyle;
@synthesize shootZIndex;
@synthesize enemyShootTimer;
@synthesize meeleAttackRect;
@synthesize shootHit;
@synthesize shootSound;
@synthesize dropCoinLowerLevel;
@synthesize dropCoinHighLevel;
@synthesize shootInterval;
@synthesize enemyUpdateSeconds;

-(id)initwithStartPosition:(CGPoint)position actionTime:(double)time delegate:(id)scnDelegate enemyParams:(EnemyParams *)params enemyPosition:(EnemyPositions)defPosition shootEndPos:(CGPoint)shootEnd
{
    return nil;
}

-(void)startMovement
{
    
}

-(void)restartMovement
{
    
}

-(int)receiveHeroShoot:(float)damage killNow:(BOOL)value shootRect:(CGRect)rect
{
    return 0;
}

-(void)performDeath;
{
    
}

-(void)performAutoDestruction
{
    
}

-(void)fireWeapon
{
    
}

-(float)autoDestruction
{
    return 0.0;
}

-(void)meeleAttack:(BandSprite *)bandsprite bandComponent:(BandComponents)component hitrect:(CGRect)rect
{
    
}

-(CGRect)meeleRect
{
    return CGRectMake(0, 0, 0, 0);
}

@end
