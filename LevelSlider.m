//
//  LevelSlider.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 5/6/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "LevelSlider.h"
#import "LevelSelector.h"
#import "GameSoundManager.h"

@implementation LevelSlider

@synthesize delegate;

-(void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    NSLog(@"level Slider Value %f",[self sliderValue]);
    [self playSliderSound];
}

-(void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    NSLog(@"level Slider Value %f",[self sliderValue]);
}

-(void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    [super touchMoved:touch withEvent:event];
    NSLog(@"level Slider Value %f",[self sliderValue]);
    [delegate changeSlideValue:[self sliderValue]];
}

-(void)playSliderSound
{
    NSLog(@"playSliderSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}



@end
