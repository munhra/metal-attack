//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  UniversalInfo.h
//  EightbitShooter
//
//  Created by Rafael Munhoz on 31/03/13.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

typedef enum {
    GAME_STARTED,
    WAVE_CLEARED,
    LEVEL_CLEARED,
    GAME_OVER,
    GAME_PAUSED
} GameState;

typedef enum {
    IPHONE_5,
    IPHONE_4_OR_LESS,
    IPHONE_6,
    IPHONE_6P,
    IPAD,
    IPAD_RETINA
} DeviceType;

typedef enum {
    MENU,
    NEXT_LEVEL,
    STORE,
    REPLAY,
} nextScence;

@interface UniversalInfo : NSObject

@property(nonatomic) bool isTesting;
@property(nonatomic) bool gotoStore;
@property(nonatomic) bool gotoNextLevel;

+(UniversalInfo *)sharedInstance;
-(BOOL) isDeviceIpad;
-(CGPoint) screenCenter;

-(CGPoint)enemyPosition1;
-(CGPoint)enemyPosition2;
-(CGPoint)enemyPosition3;
-(CGPoint)enemyPosition4;
-(CGPoint)enemyPosition5;
-(CGPoint)enemyPosition6;
-(CGPoint)enemyPosition7;
-(CGPoint)enemyPosition8;

-(CGPoint)calculateAnchorPoint:(CGPoint)anchorPos width:(int)w height:(int)h;
//-(DeviceType)getDeviceType;
//-(NSString *)addZeroesToNumber:(NSString*)number;
-(NSString *)addZeroesToNumber:(NSString*)number numberOfZeroes:(int)zeroes;
-(CCSprite *)getSpriteWithCoinAnimation;
-(DeviceType)getDeviceType;
-(UIImage*)screenshot;
-(UIColor *)averageColor:(UIImage *)image;
-(CCColor *)generateRandomColor;
-(void)playVideoForReward:(int)probability;
-(void)playInterstitialVideo:(int)probability;

@end
