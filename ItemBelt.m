//
//  ItemBelt.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 10/10/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "ItemBelt.h"
#import "BandStoreItem.h"
#import "BandStoreItemsCtrl.h"
#import "BandVault.h"
#import "Bandvaultitem.h"
#import "LevelScene.h"

@implementation ItemBelt

@synthesize item1Button;
@synthesize item2Button;
@synthesize item3Button;
@synthesize item4Button;
@synthesize item5Button;
@synthesize item6Button;
@synthesize item7Button;
@synthesize item8Button;
@synthesize buttonArray;
@synthesize buttonItemArray;
@synthesize delegate;

-(void)didLoadFromCCB
{
    NSLog(@"didLoadFromCCB");
    self.hidden = YES;
    //[self populateButtonsArray];
    //[self loadSelectedItems];
}

-(void)populateButtonsArray
{
    self.buttonItemArray = [[NSMutableArray alloc] initWithCapacity:8];
    for (int i = 1; i <= 8; i++) {
        CCNode *itemButton = [self getChildByName:[NSString stringWithFormat:@"%d",i] recursively:YES];
        [buttonItemArray addObject:itemButton];
    }
    self.buttonArray = self.buttonItemArray;
}

-(void)loadSelectedItems
{
    NSLog(@"loadSelectedItems");
    
    //buttonArray = [[NSArray alloc] initWithObjects:item1Button,item2Button,item3Button, item4Button,
    //item5Button, item6Button, item7Button, item8Button, nil];
    
    BandVault *vault = [BandVault sharedInstance];
    BandStoreItemsCtrl *bandStoreItemsCtrl = [BandStoreItemsCtrl sharedInstance];
    
    int buttonCount = 0;
    
    
    for (int i = 0 ; i < [buttonArray count]; i++) {
        CCButton *itemButton = [buttonArray objectAtIndex:i];
        [itemButton setBackgroundSpriteFrame:nil forState:CCControlStateNormal];
        [itemButton setUserInteractionEnabled:NO];
        
    }
    
    for (BandVaultItem *vaultItem in [vault selectedItems]) {
        for (BandStoreItem *bandItem in [bandStoreItemsCtrl generalItems]) {
            if ([bandItem.appStoreId isEqualToString:vaultItem.appId]){
                CCButton *itemButton = [buttonArray objectAtIndex:buttonCount];
                [itemButton setBackgroundSpriteFrame:[bandItem.itemSprt spriteFrame] forState:CCControlStateNormal];
                [itemButton setUserObject:vaultItem];
                buttonCount++;
            }
        }
    }
}

-(void)disableUserInteraction
{
    for (int i = 0 ; i < [buttonArray count]; i++) {
        CCButton *itemButton = [buttonArray objectAtIndex:i];
        [itemButton setUserInteractionEnabled:NO];
    }
}


-(void)enableUserInteraction
{
    for (int i = 0 ; i < [buttonArray count]; i++) {
        CCButton *itemButton = [buttonArray objectAtIndex:i];
        [itemButton setUserInteractionEnabled:YES];
    }
}

-(void)activateItem:(CCButton*)sender
{
    NSLog(@"Consume Item %@",[sender name]);
    //[delegate triggerThePowerUp:[sender userObject]];
    
    [[self animationManager] runAnimationsForSequenceNamed:@"hide"];
    //[self disableUserInteraction];
    self.hidden = YES;
}
-(void)touchCancelled:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    NSLog(@"touchcanceled");
}

-(void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    NSLog(@"touchbegan inside button");
}

-(void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    NSLog(@"touchEnded inside button");
}

-(void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    NSLog(@"touchMoved inside buton");
}

@end
