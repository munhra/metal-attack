//
//  ShieldPowerUp.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 02/11/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

static const CGPoint showShield1Position = {0.50f,0.56f};
static const CGPoint showShield2Position = {0.50f,0.66f};
static const CGPoint showShield3Position = {0.50f,0.56f};
static const CGPoint showShield4Position = {0.50f,0.56f};
static const CGPoint showShield5Position = {0.50f,0.56f};
static const CGPoint showShield6Position = {0.50f,0.53f};
static const CGPoint showShield7Position = {0.50f,0.56f};
static const CGPoint showShield8Position = {0.52f,0.54f};
static const CGPoint showShield9Position = {0.50f,0.37f};
static const CGPoint showShield10Position = {0.50f,0.66f};

static const CGPoint hideShield1Position = {0.50f,1.23f};
static const CGPoint hideShield2Position = {0.50f,1.41f};
static const CGPoint hideShield3Position = {0.50f,1.33f};
static const CGPoint hideShield4Position = {0.50f,1.31f};
static const CGPoint hideShield5Position = {0.50f,1.41f};
static const CGPoint hideShield6Position = {0.50f,1.29f};
static const CGPoint hideShield7Position = {0.50f,1.38f};
static const CGPoint hideShield8Position = {0.52f,1.27f};
static const CGPoint hideShield9Position = {0.50f,1.29f};
static const CGPoint hideShield10Position = {0.50f,1.37f};


@interface ShieldPowerUp : NSObject

@property(nonatomic) CGPoint showPosition;
@property(nonatomic) CGPoint hidePosition;
@property(nonatomic,retain) CCActionMoveTo *showShieldAction;
@property(nonatomic,retain) CCActionMoveTo *hideShieldAction;

-(id)initWithShowPosition:(CGPoint)position1 hidePosition:(CGPoint)position2;

+(NSMutableArray *)preparePositionArray;

@end
