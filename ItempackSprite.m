//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  ItempackSprite.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 17/10/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "ItempackSprite.h"
#import "LevelScene.h"

@implementation ItempackSprite

@synthesize clicked;

static NSMutableArray *itemButtons;
static CCNode *menuBarUp;
static CCNode *menuBarRight;


-(void)didLoadFromCCB
{
    NSLog(@"didloadfromccb");
    int intname = [self.name intValue];
    if (intname == 0) {
        self.userInteractionEnabled = true;
    }
}

-(void)loadItemButtonsWithDelegate:(id)delegate
{
    self.delegate = delegate;
    
    itemButtons = [[NSMutableArray alloc] init];
    menuBarRight = [self.delegate menuItemRight];
    menuBarUp = [self.delegate menuItemUp];
    
    [itemButtons addObjectsFromArray:[menuBarRight children]];
    [itemButtons addObjectsFromArray:[menuBarUp children]];
    
    for (ItempackSprite *itemButton in itemButtons) {
        [itemButton setDelegate:self.delegate];
        [itemButton setScale:0.6];
    }
    
    [self loadItemSpriteFrame];
    [self itemBarsStartPosition];
}

-(void)itemBarsStartPosition
{
    [menuBarRight setPosition:ccp(0.12, 0.054)];
    [menuBarRight setScaleX:0];

    [menuBarUp setPosition:ccp(0.103, 0.073)];
    [menuBarUp setScaleY:0];
}

-(void)hideItemAnimationWithDelay
{
    for (ItempackSprite *itemButton in itemButtons) {
        [itemButton setUserInteractionEnabled:NO];
    }
    
    [self hideItemBars];
}

-(void)consumeItemAnimation
{
    CCActionBlink *blinkItemAction = [CCActionBlink actionWithDuration:0.5 blinks:20];
    CCActionCallBlock *blinkItemEndAction = [CCActionCallBlock actionWithBlock:^{
        
        [self.delegate triggerThePowerUp:self.userObject];
        [self hideItemAnimationWithDelay];
        [self removeFromParentAndCleanup:YES];
    }];
    
    CCActionSequence *blinkActionSequence = [CCActionSequence actions:blinkItemAction, blinkItemEndAction, nil];
    [self runAction:blinkActionSequence];
    
}

-(void)hideItemBars
{
    
    CCActionMoveTo *moveTo = [CCActionMoveTo actionWithDuration:0.1 position:ccp(0.12, 0.054)];
    CCActionScaleTo *scaleTo = [CCActionScaleTo actionWithDuration:0.1 scale:0];
    [menuBarRight runAction:moveTo];
    [menuBarRight runAction:scaleTo];
    
    CCActionMoveTo *moveToUp = [CCActionMoveTo actionWithDuration:0.1 position:ccp(0.103, 0.073)];
    CCActionScaleTo *scaleToUp = [CCActionScaleTo actionWithDuration:0.1 scale:0];
    [menuBarUp runAction:moveToUp];
    [menuBarUp runAction:scaleToUp];
}

-(void)showItemBars
{
   
    for (ItempackSprite *itemButton in itemButtons) {
        [itemButton setUserInteractionEnabled:YES];
    }

    CCActionMoveTo *moveRightTo = [CCActionMoveTo actionWithDuration:0.1 position:ccp(0.367, 0.074)];
    CCActionScaleTo *scaleRightTo = [CCActionScaleTo actionWithDuration:0.1 scale:1];
    [menuBarRight runAction:moveRightTo];
    [menuBarRight runAction:scaleRightTo];

    CCActionMoveTo *moveUpTo = [CCActionMoveTo actionWithDuration:0.1 position:ccp(0.103, 0.259)];
    CCActionScaleTo *scaleUpTo = [CCActionScaleTo actionWithDuration:0.1 scale:1];
    [menuBarUp runAction:moveUpTo];
    [menuBarUp runAction:scaleUpTo];
    
    [self scheduleOnce:@selector(hideItemAnimationWithDelay) delay:3];
    
}

-(void)touchCancelled:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    NSLog(@"touchcanceled");
}

-(void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    NSLog(@"touchbegan inside button");
    [self.delegate touchBegan:touch withEvent:event];
        
    int intname = [self.name intValue];
        
    if (intname == 0) {
            
        [self showItemBars];
        [self setScale:1.2];
        [self.delegate openItemBelt];
            
    }else{
        
        [self consumeItemAnimation];
        
        for (ItempackSprite *itemButton in itemButtons) {
            [itemButton setUserInteractionEnabled:NO];
        }
    }
}

-(void)loadItemSpriteFrame{
    
    BandVault *vault = [BandVault sharedInstance];
    BandStoreItemsCtrl *bandStoreItemsCtrl = [BandStoreItemsCtrl sharedInstance];
    
    for (CCSprite *itemSprite in itemButtons) {
        [itemSprite setSpriteFrame:nil];
    }
    
    int buttonCount = 0;
    
    for (BandVaultItem *vaultItem in [vault selectedItems]) {
        for (BandStoreItem *bandItem in [bandStoreItemsCtrl generalItems]) {
            if ([bandItem.appStoreId isEqualToString:vaultItem.appId]){
                
                CCSprite *buttonSprt = [itemButtons objectAtIndex:buttonCount];
                [buttonSprt setUserObject:vaultItem];
                [buttonSprt setSpriteFrame:[bandItem.itemSprt spriteFrame]];
                
                buttonCount++;
            }
        }
    }
}

-(void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    NSLog(@"touchEnded inside button");
    [self.delegate touchEnded:touch withEvent:event];
    int intname = [self.name intValue];
        
    if (intname > 0) {
        [self setScale:0.6];
    }
}

-(void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    NSLog(@"touchMoved inside buton");
    [self.delegate touchMoved:touch withEvent:event];
}

- (void)dealloc
{
    NSLog(@"dealloc itempack sprite");
}

@end
