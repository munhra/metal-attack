//
//  ItemSummary.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 21/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "LevelSlider.h"

@interface ItemSummary : CCSprite {
    
    id delegate;
}

@property(retain,nonatomic) LevelSlider *itemQtdSlider;
@property(retain,nonatomic) id delegate;
@property(retain,nonatomic) CCLabelTTF *itemQtdLabel;
@property(nonatomic) int itemQtd;
@property(nonatomic) int itemUnitPrice;
@property(nonatomic) CGPoint itemImageOriginalPosition;

-(void)setItemText:(NSString *)text;
-(void)setItemTitle:(NSString *)text; // max value 15 chars
-(void)setItemPrice:(NSString *)text;
-(void)setItemImage:(CCSprite *)itemsprt;
-(void)changeSlideValue:(float)sliderValue;
-(void)showItemCost:(bool)visible;

@end
