//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  ItemSelection.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 07/03/15.
//  Copyright 2015 Apportable. All rights reserved.
//

#import "ItemSelection.h"
#import "LevelSceneController.h"
#import "CCTextureCache.h"
#import "GameSoundManager.h"
#import "GameCenterManager.h"

@implementation ItemSelection

@synthesize ownedItems;
@synthesize ownedHorizontalBox1;
@synthesize ownedHorizontalBox2;
@synthesize box1;
@synthesize nextLevel;
@synthesize kombisprt;
@synthesize infinityRoad;
@synthesize littlecarFront;
@synthesize itemShelf;
@synthesize nextItemCounter;
@synthesize maxShelfNumber;
@synthesize itemGirl;
@synthesize message;
@synthesize informationButton;
@synthesize itemInTransition;

bool rightMoved;
bool informationGirl;
bool showingInfoGirl;

-(void)doTest
{
    NSLog(@"doTest");
}

-(void)onEnter {
    [super onEnter];
    [[GameSoundManager sharedInstance] playItemSelectionSong];
}

-(void)onExit {
    [super onExit];
    [[self kombisprt] setDelegate:nil];
    [[GameSoundManager sharedInstance] stopBackgroundSong];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [self removeItemSelectionTextures];
}

-(void)removeItemSelectionTextures
{
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Menu/Van driver.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/road/02_tampa-carrinho-closed.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/road/03_tampa-carrinho-open.png"];
}

-(void)doMenu
{
    NSLog(@"doMenu");
    CCNode *door = [self getChildByName:@"Door" recursively:YES];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
    }];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
}

-(void)doGamePlay
{
    NSLog(@"doGamePlay");
    __weak ItemSelection *weakSelf = self;
    CCNode *door = [self getChildByName:@"Door" recursively:YES];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
        [[CCDirector sharedDirector] replaceScene:[lvcontroller loadLevelScene:[weakSelf nextLevel] waveNumber:0]];
    }];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
}

-(void)doStore
{
    NSLog(@"doStore");
}

-(void)didLoadFromCCB
{
    NSLog(@"didLoadFromCCB ItemSelection");
    rightMoved = NO;
    BandVault *bandVault = [BandVault sharedInstance];
    [bandVault clearSelectedItems];
    ownedItems = [bandVault loadAllOwnedGenericItems];
    
    if ([ownedItems count] % 9 == 0) {
        maxShelfNumber = [ownedItems count] / 9;
    }else{
        maxShelfNumber = [ownedItems count] / 9;
        maxShelfNumber++;
    }
    
    NSLog(@"The size of items array %d",[ownedItems count]);
    itemShelf = [CCBReader load:@"store/ItemsShelf"];
    [self createItemsIconList];
    __weak ItemSelection *weakSelf = self;
    [[self animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [weakSelf kombiArrivedAnimationEnded];
    }];
    
    [[self kombisprt] setDelegate:self];
    
    nextItemCounter = 1;
    
    message = (MessagesSprite *)[CCBReader load:@"Message"];
    showingInfoGirl = false;
    [informationButton setEnabled:NO];
}

-(void)kombiArrivedAnimationEnded{
    // ended animations
    
    NSString *lastComplAni = [[self animationManager] lastCompletedSequenceName];
    CCAnimationManager *scnAnimationManager = [self animationManager];

    if ([lastComplAni isEqualToString:@"Default Timeline"]) {
        [infinityRoad setAccelerate:YES];
        [scnAnimationManager runAnimationsForSequenceNamed:@"accelerate"];
        // run accelerate sequence
    }else if ([lastComplAni isEqualToString:@"accelerate"]){
        NSLog(@"end of accelerate sequence");
        [infinityRoad setStopMoving:YES];
        [scnAnimationManager runAnimationsForSequenceNamed:@"showscenario"];
    }else{
         [[self animationManager] lastCompletedSequenceName];
         NSLog(@"Kombi animation ended");
         CCAnimationManager *kombiAnimManager = [[self kombisprt] animationManager];
         [kombiAnimManager setPaused:YES];
         [kombiAnimManager runAnimationsForSequenceNamed:@"opendoor"];
         __weak ItemSelection *weakSelf = self;
         [kombiAnimManager setCompletedAnimationCallbackBlock:^(id sender) {
             [weakSelf kombiDoorOpened];
         }];
    }
}

-(void)kombiDoorOpened{
    NSLog(@"Kombi door opened show itens");
    [informationButton setEnabled:YES];
    CCNode *ownedItemsGrid = [self getChildByName:@"OwnedItems1" recursively:YES];
    [ownedItemsGrid setVisible:YES];
    CCLayoutBox *verticalColumn = (CCLayoutBox *)[ownedItemsGrid getChildByName:@"VerticalBox" recursively:YES];
    
    for (CCNode *hrzRows in [verticalColumn children]) {
        for (CCNode *itemRect in [hrzRows children]) {
            
            for (CCNode *itemBnt in [itemRect children]){
                if ([itemBnt isKindOfClass:[CCButton class]]) {
                            [(CCButton *)itemBnt setBackgroundOpacity:1 forState:CCControlStateNormal];
                }
            
                if ([itemBnt isKindOfClass:[CCLabelTTF class]]) {
                    [itemBnt setVisible:YES];
                }
            }
        }
    }
}

-(void)createItemsIconList
{
    int itemCounter = 1;
    int itemScreenCounter = 1;
    int rowCount = 1;
    int colCount = 0;
    NSMutableDictionary *itemButtonsDict = [[NSMutableDictionary alloc] init];
    
    CCNode *ownedItemsGrid = [self getChildByName:@"OwnedItems1" recursively:YES];
    CCLayoutBox *verticalColumn = (CCLayoutBox *)[ownedItemsGrid getChildByName:@"VerticalBox" recursively:YES];
    CCLayoutBox *horizontalRow = (CCLayoutBox *)[ownedItemsGrid getChildByName:@"HorizontalBox1" recursively:YES];
    
    BandVault *bandVault = [BandVault sharedInstance];
    
    for (BandVaultItem *ownedItem in ownedItems) {
     
        CCLabelTTF *itemQtdLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", [ownedItem.qtd intValue]] fontName:@"Rockwell-CondensedBold" fontSize:20];

        [itemQtdLabel setVisible:NO];
        
        
        [itemQtdLabel setName:@"qtdlabel"];
    
        NSString *itemButtonName = [NSString stringWithFormat:@"Item%d",itemCounter];
        
        CCNode *colObject = [[horizontalRow children] objectAtIndex:colCount];
        [colObject addChild:itemQtdLabel];
        CCButton *itemButton;
        
        for (int i = 0; i < [ownedItem.qtd integerValue]; i++) {
            
            NSString *itemId = [ownedItem.appId substringFromIndex:4];
            int correctedId = [itemId intValue] + 2;
            itemId = [NSString stringWithFormat:@"%d",correctedId];
            
            itemButton = [itemButtonsDict objectForKey:itemId];
            // itemid should be increased by two
            if (!itemButton){ // if itembutton nil
                itemButton = (CCButton *)[[self itemShelf] getChildByName:itemId recursively:YES];
                [itemButton removeFromParent];
                [itemButtonsDict setObject:itemButton forKey:itemId];
            }else{
                CCSpriteFrame *buttonBg = [itemButton backgroundSpriteFrameForState:CCControlStateNormal];
                itemButton = [[CCButton alloc] initWithTitle:@"" spriteFrame:buttonBg];
                [itemButton setZoomWhenHighlighted:YES];
            }
            
            [itemButton setName:itemButtonName];
            [itemButton setTarget:self selector:@selector(selectItem:)];

            
            [itemButton setScale:0.60];
            
            if (itemScreenCounter == 1){
                [itemButton setBackgroundOpacity:0.0 forState:CCControlStateNormal];
            }
            
            [colObject addChild:itemButton];
            
            switch (rowCount) {
                case 1:
                    [itemButton setPosition:ccp(25,15)]; // 25,25
                    break;
                case 2:
                    [itemButton setPosition:ccp(25,25)]; // 25,25
                    break;
                case 3:
                    [itemButton setPosition:ccp(25,30)]; // 25,25
                    break;
                default:
                    break;
            }
            [ownedItem setSelected:[NSNumber numberWithBool:NO]];
            [itemButton setUserObject:[bandVault copyBandVaultItem:ownedItem]];
        }
        
        colCount++;
        
        if (itemCounter % 9 == 0){
            rowCount = 1;
            itemCounter = 0;
            itemScreenCounter++;
            colCount = 0;
            NSString *childrenName = [NSString stringWithFormat:@"OwnedItems%d",itemScreenCounter];
            ownedItemsGrid = [self getChildByName:childrenName recursively:YES];
            verticalColumn = (CCLayoutBox *)[ownedItemsGrid getChildByName:@"VerticalBox" recursively:YES];
            horizontalRow = (CCLayoutBox *)[ownedItemsGrid getChildByName:@"HorizontalBox1" recursively:YES];
        }else if (itemCounter % 3 == 0){
            rowCount++;
            colCount = 0;
            NSString *rowName = [NSString stringWithFormat:@"HorizontalBox%d",rowCount];
            horizontalRow =  (CCLayoutBox *)[verticalColumn getChildByName:rowName recursively:NO];
        }
        
        itemCounter++;
    }
}

-(void)hideItemGirl{
    
    [self playGirlMoveSound];
    id girlMove = [CCActionMoveTo actionWithDuration:0.5 position:ccp(itemGirl.position.x - 130, itemGirl.position.y)];
    id girlMoveElastic = [CCActionEaseBackInOut actionWithAction:girlMove];

    id endGirlHideAnimation = [CCActionCallBlock actionWithBlock:^{
        showingInfoGirl = false;
    }];
    
    id actionSequence = [CCActionSequence actions:girlMoveElastic, endGirlHideAnimation, nil];
    [[self itemGirl] runAction:actionSequence];
}

-(void)setInformationGirl
{
    if (!showingInfoGirl) {
        NSLog(@"Set information girl");
        NSString *girlInfoMessage;
        if (informationGirl) {
            // show girl saying that she will go away
            informationGirl = false;
            girlInfoMessage = @"Bye, I will take care of other business";
            [[GameCenterManager sharedInstance] achivmentUpdate:@"LectureStopper" withCallBack:^(AchievimentBanner *achievimentBanner) {
                if (achievimentBanner) {
                    [self addChild:achievimentBanner];
                    [achievimentBanner animateMessageInAndOut];
                }
            }];
        }else{
            informationGirl = true;
            // show girl saying that she is here to help
            girlInfoMessage = @"I'm here to help, select an drink and I will tell you what it will cause";
        }
        
        [self showItemGirl:girlInfoMessage infoMessage:YES];
    }
}

-(void)showItemGirl:(NSString*)bandItemDescription infoMessage:(bool)info
{
    [self playGirlMoveSound];
    id girlMove = [CCActionMoveTo actionWithDuration:0.5 position:ccp(itemGirl.position.x + 130, itemGirl.position.y)];
    id girlMoveElastic = [CCActionEaseBackInOut actionWithAction:girlMove];
    

    CCActionCallBlock *endSequence = [CCActionCallBlock actionWithBlock:^{
        
        if (info){
            [self showInfoMessageWith:bandItemDescription];
        }else{
            [self showItemMessageWith:bandItemDescription];
        }
        
        [self scheduleBlock:^(CCTimer *timer) {
            NSLog(@"schedule !");
            [self hideItemMessage];
            [self hideItemGirl];
        } delay:2];
        
        
    }];

    CCActionSequence *girlMoveSequence = [CCActionSequence actions:girlMoveElastic,endSequence, nil];
    
    [[self itemGirl] runAction:girlMoveSequence];
    showingInfoGirl = true;
}

-(void)showInfoMessageWith:(NSString *)infoMessage
{
    NSLog(@"Showinfomessage");
    [self createInfoMessage:infoMessage];
}

-(void)createInfoMessage:(NSString *)text
{
    message = (MessagesSprite *)[CCBReader load:@"Message"];
    CGPoint messagePosition = ccp([[self itemGirl] boundingBox].size.width * 1.4, [[self itemGirl] boundingBox].size.height * 0.95);
    [self.message setPosition:messagePosition];
    [self.message setTextMessage:text];
    [self.message setFontSize:16];
    [self.message setBoxHeight:135];
    [self.message setVisible:YES];
    [self.message.backgroundNine setOpacity:0.8];
    [self.message adjustTextPositionWithFactor:ccp(1.05, 1)];
    [self.message adjustArrowHorizontalPosition:0.05];
    
    
    if ([self.message parent] == nil) {
        float arrowPosition = [[message arrowSprite] position].x + 20;
        [[message arrowSprite] setPosition:ccp(arrowPosition, [[message arrowSprite] position].y)];
    }
    
    [message removeFromParent];
    [itemGirl addChild:message z:-1];
    [message animateShowWithScale];
}

-(void)showItemMessageWith:(NSString*)bandItemDescription
{
    NSLog(@"showItemMessage");
    // pick information of the item
    [self createMessage:bandItemDescription];
}

-(void)hideItemMessage
{
    NSLog(@"hideItemMessage");
    [message animateHideWithScale];
}

-(void)createMessage:(NSString *)text
{
    message = (MessagesSprite *)[CCBReader load:@"Message"];
    CGPoint messagePosition = ccp([[self itemGirl] boundingBox].size.width * 1.4, [[self itemGirl] boundingBox].size.height * 0.95);
    [self.message setPosition:messagePosition];
    [self.message setTextMessage:text];
    [self.message setFontSize:12];
    [self.message setBoxHeight:180];
    [self.message setVisible:YES];
    [self.message.backgroundNine setOpacity:0.8];
    [self.message adjustTextPositionWithFactor:ccp(1.05, 1)];
    [self.message adjustArrowHorizontalPosition:0.05];
   
    
    if ([self.message parent] == nil) {
        float arrowPosition = [[message arrowSprite] position].x + 20;
        [[message arrowSprite] setPosition:ccp(arrowPosition, [[message arrowSprite] position].y)];
    }
    
    [message removeFromParent];
    [itemGirl addChild:message z:-1];
    [message animateShowWithScale];
}

-(void)selectItem:(CCButton *)sender
{
    
    if (![self itemInTransition]){
        [self playBottleClickSound];
        if (!showingInfoGirl){
            //[[GameSoundManager sharedInstance] playBottleClick];
            //check maximum limit of vault
            
            BandVault *bandVault = [BandVault sharedInstance];
            BandVaultItem *ownedItem = [sender userObject];
            
            CCNode *itemBox = sender.parent;
            CCLabelTTF *itemQtdLabel =  (CCLabelTTF* )[itemBox getChildByName:@"qtdlabel" recursively:NO];
            
            if (![[bandVault selectedItems] containsObject:ownedItem]){
                // move to the selected items
                if ([[bandVault selectedItems] count ] < 8) {
                    
                    if (informationGirl){
                        [self showItemGirl:ownedItem.itemDescription infoMessage:NO];
                    }
                    
                    [ownedItem setSelected:[NSNumber numberWithBool:YES]];
                    [ownedItem setItemShelf:[NSNumber numberWithInt:nextItemCounter]];
                    
                    int updatedValue = [self updateItemQtd:ownedItem quantity:-1];
                    [itemQtdLabel setString:[NSString stringWithFormat:@"%d",updatedValue]];
                    
                    ////***** it shall find the free position
                    int selectedPosition = [bandVault addItemSelected:ownedItem];
                    CCNode *owned1;
                    if (selectedPosition <= 3) {
                        owned1 = [[[self ownedHorizontalBox1] children] objectAtIndex:selectedPosition];
                    }else{
                        owned1 = [[[self ownedHorizontalBox2] children] objectAtIndex:selectedPosition-4];
                    }
                    
                    CGPoint ownedPosition = [owned1.parent convertToWorldSpace:owned1.position];
                    CGPoint convertedDestiny = [sender.parent convertToNodeSpace:ownedPosition];
                    
                    convertedDestiny = ccp(convertedDestiny.x + [owned1 boundingBox].size.width/2, convertedDestiny.y+[owned1 boundingBox].size.height/2);
                    CCActionMoveTo *moveTo = [CCActionMoveTo actionWithDuration:0.2 position:convertedDestiny];
                    [moveTo setTag:1];
                    [sender runAction:moveTo];
                    [self setItemInTransition:YES];
                    [[sender animationManager] setCompletedAnimationCallbackBlock:^(id animationManager) {
                        [self setItemInTransition:NO];
                    }];
                    [[self animationManager] runAnimationsForSequenceNamed:@"openDoorLittleCar"];
                    
                }
                
            }else{
                
                [ownedItem setSelected:[NSNumber numberWithBool:NO]];
                int previousCounter = nextItemCounter;
                nextItemCounter = [[ownedItem itemShelf] intValue] - 1;
                [self moreItems:previousCounter];
                
                CCNode *senderParent = [sender parent];
                CGPoint ownedPosition = [senderParent.parent convertToWorldSpace:senderParent.position];
                CGPoint convertedPoint = [sender.parent convertToNodeSpace:ownedPosition];
                CGPoint originalPosition;
                
                if ([[sender.parent.parent name] isEqualToString:@"HorizontalBox1"]) {
                    originalPosition = CGPointMake(25, 15);
                }else if ([[sender.parent.parent name] isEqualToString:@"HorizontalBox2"]){
                    originalPosition = CGPointMake(25, 25);
                }else{
                    originalPosition = CGPointMake(25, 30);
                }
                
                convertedPoint = ccp(convertedPoint.x + originalPosition.x, convertedPoint.y+originalPosition.y);
                CCActionMoveTo *moveToVault = [CCActionMoveTo actionWithDuration:0.2 position:convertedPoint];
                [[bandVault selectedItems] removeObject:ownedItem];
                [[self animationManager] runAnimationsForSequenceNamed:@"openDoorLittleCar"];
                [sender runAction:moveToVault];
                [self setItemInTransition:YES];
                [[sender animationManager] setCompletedAnimationCallbackBlock:^(id animationManager) {
                    [self setItemInTransition:NO];
                }];
                
                int updatedValue = [self updateItemQtd:ownedItem quantity:+1];
                [itemQtdLabel setString:[NSString stringWithFormat:@"%d",updatedValue]];
            }
        }
    }
}

-(void)changeLabelVisibility:(bool)visible withNode:(CCNode *)itemnode
{
    
    for (CCNode *node in [itemnode children]){
        
        if ([node isKindOfClass:[CCLabelTTF class]]) {
            [node setVisible:visible];
        }
    }
    
}

-(void)rearrangeItems
{
    // this method will rearange the itens when a middle item is removed
    // this method should be called to remove the blank spaces when removing itens
    NSLog(@"rearrangeItems");
}

-(void)changeItemsVisibility:(CCNode *)itemGrid visible:(BOOL)visible{
    CCNode *verticalLayout = [itemGrid getChildByName:@"VerticalBox" recursively:YES];
    for (CCNode *node in [verticalLayout children]){
        for (CCNode *itemNode in [node children]){
            //two children one is the button
            for (CCNode *button in [itemNode children]) {
                if ([button isKindOfClass:[CCButton class]]) {
                    if ([(BandVaultItem *)[button userObject] selected] == [NSNumber numberWithBool:NO]){
                        [button setVisible:visible];
                    }
                }
                if ([button isKindOfClass:[CCLabelTTF class]]) {
                    [button setVisible:visible];
                }
            }
        }
    }
}

-(void)moreItems:(int)prvItemCounter
{
    [self playMoreItensSound];
    CCNode *ownedItemsGrid;
    NSString *childrenName;
    int previousItems;
    
    nextItemCounter++;
    
    if (prvItemCounter == -1) {
        previousItems = nextItemCounter - 1;
    }else{
        previousItems = prvItemCounter;
    }
    if (nextItemCounter > maxShelfNumber){
        nextItemCounter = 1;
        childrenName = [NSString stringWithFormat:@"OwnedItems%d",maxShelfNumber];
        ownedItemsGrid = [self getChildByName:childrenName recursively:YES];
        [self changeItemsVisibility:ownedItemsGrid visible:NO];
        ownedItemsGrid = [self getChildByName:@"OwnedItems1" recursively:YES];
        [self changeLabelVisibility:YES withNode:ownedItemsGrid];
        [self changeItemsVisibility:ownedItemsGrid visible:YES];
    }else{
        childrenName = [NSString stringWithFormat:@"OwnedItems%d",previousItems];
        ownedItemsGrid = [self getChildByName:childrenName recursively:YES];
        [self changeItemsVisibility:ownedItemsGrid visible:NO];
        childrenName = [NSString stringWithFormat:@"OwnedItems%d",nextItemCounter];
        ownedItemsGrid = [self getChildByName:childrenName recursively:YES];
        [ownedItemsGrid setVisible:YES];
        [self changeItemsVisibility:ownedItemsGrid visible:YES];
    }
}

-(void)doRight
{
    NSLog(@"doRight");
    if (rightMoved) {
        [[self animationManager] runAnimationsForSequenceNamed:@"MoveLeft"];
        rightMoved = NO;
    }else{
        [[self animationManager] runAnimationsForSequenceNamed:@"MoveRight"];
        rightMoved = YES;
    }
}

-(int)updateItemQtd:(BandVaultItem *)item quantity:(int)qtd{
    for (BandVaultItem *vaultItem in ownedItems) {
        if ([[vaultItem appId] isEqualToString:[item appId]]){
            vaultItem.qtd = @([vaultItem.qtd intValue] + qtd);
            item.qtd = vaultItem.qtd;
            return [vaultItem.qtd intValue];
        }
    }
    return -999;
}

-(void)dealloc
{
    NSLog(@"***** Dealloc item selection *****");
}

-(void)playBottleClickSound
{
    NSLog(@"playBottleClickSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx003"];
}

-(void)playFreezerOpenSound
{
    NSLog(@"playVanMotorSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx008"];
}

-(void)playVanMotorSound
{
    NSLog(@"playVanMotorSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx018"];
}

-(void)playGirlMoveSound
{
    NSLog(@"playGirlMoveSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}

-(void)playMoreItensSound
{
    NSLog(@"playMoreItensSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx017"];
}

@end
