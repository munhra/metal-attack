//
//  StripeEndTestScence.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 7/16/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "StripeEndTestScence.h"
#import "UniversalInfo.h"

@implementation StripeEndTestScence

@synthesize endStripe1;
@synthesize drummerEndSprite;
@synthesize endStripeTest;
@synthesize endStripeContainer;
@synthesize endStripeContainer2;

-(void)didLoadFromCCB
{

    endStripe1 = (EndSprite *)[CCBReader load:@"BandGuys/end/Level1/AceEndStripe"];
    endStripeTest = (CCSprite *)[CCBReader load:@"BandGuys/end/Level1/AceEndSpriteTest"];
    drummerEndSprite = (CCSprite *)[CCBReader load:@"BandGuys/end/Level1/AceEnd"];
}

-(void)onEnter
{
    [super onEnter];
    
    endStripeContainer = [CCNodeColor nodeWithColor:[CCColor yellowColor] width:325 height:97];
    [endStripeContainer setAnchorPoint:ccp(0.5,0.5)];
    [endStripeContainer setPosition:ccpCompMult([[UniversalInfo sharedInstance] screenCenter], ccp(1, 1.5))];
    [self addChild:endStripeContainer];
    
    endStripeContainer2 = [CCNodeColor nodeWithColor:[CCColor cyanColor] width:325 height:97];
    [endStripeContainer2 setAnchorPoint:ccp(0.5,0.5)];
    [endStripeContainer2 setPosition:ccpCompMult([[UniversalInfo sharedInstance] screenCenter], ccp(1, 1))];
    [self addChild:endStripeContainer2];
    
    
    [self myClippingTest];
}

-(void)myClippingTest
{
    
    CCNodeColor *testColorRect = [CCNodeColor nodeWithColor:[CCColor greenColor] width:5000 height:5000];
    [testColorRect setAnchorPoint:ccp(0.5,0.5)];
    [testColorRect setPosition:ccp(0,0)];
    
    [drummerEndSprite setPosition:ccp(0,0)];
    
    CCNodeColor *testScissorRect = [CCNodeColor nodeWithColor:[CCColor clearColor] width:325 height:97];
    [testScissorRect setAnchorPoint:ccp(0.5,0.5)];
    [testScissorRect setPosition:ccp(0,0)];
    
    
    CCClippingNode *scissorTest = [CCClippingNode clippingNodeWithStencil:testScissorRect];
    [scissorTest setColor:[CCColor colorWithUIColor:[UIColor yellowColor]]];
    [scissorTest setContentSize:testScissorRect.contentSize];
    [scissorTest setAnchorPoint:ccp(0.5,0.5)];
    [scissorTest setPosition:ccp(325,97)];
    
    [scissorTest setAlphaThreshold:0.0];
    [endStripeContainer2 addChild:scissorTest];
    [scissorTest addChild:drummerEndSprite];
}

-(void)clippingTestLabel
{
    // Create a colored background (Dark Grey)
    CCNodeColor *background = [CCNodeColor nodeWithColor:[CCColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f]];
    [self addChild:background];
    
    // Hello world
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Chalkduster" fontSize:36.0f];
    label.positionType = CCPositionTypeNormalized;
    label.color = [CCColor redColor];
    label.position = ccp(0.5f, 0.5f); // Middle of screen
    
    // Create Clipping ViewPort
    CCNodeColor *scissorRect = [CCNodeColor nodeWithColor:[CCColor clearColor] width:100 height:50];
    [scissorRect setAnchorPoint:ccp(0.5,0.5f)];
    [scissorRect setPosition:ccp(240,160)];
    
    // Create Clipping Node
    CCClippingNode *scissor = [CCClippingNode clippingNodeWithStencil:scissorRect];
    [scissor setContentSize:self.contentSize];
    [scissor setPositionType:CCPositionTypeNormalized];
    
    [scissor setAlphaThreshold:0.0];
    
    //[scissor setInverted:YES];
    [self addChild:scissor];
    
    // Add nodes to Clipping Node
    [scissor addChild:label];
    

}

-(void)doMenu
{
    NSLog(@"doBack");
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
}


@end
