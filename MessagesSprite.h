//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  MessagesSprite.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 14/08/15.
//  Copyright 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface MessagesSprite : CCNode {
    
    CCLabelTTF *messageText;
    CCSprite9Slice *backgroundNine;

}

@property (nonatomic, retain) CCLabelTTF *messageText;
@property (nonatomic, retain) CCSprite9Slice *backgroundNine;
@property (nonatomic, weak) CCSprite *arrowSprite;

-(void)closeMessage;
-(void)setTextMessage:(NSString *)text;
-(void)setFontSize:(CGFloat)size;
-(void)setBoxHeight:(CGFloat)height;
-(void)setBoxWidth:(CGFloat)width;
-(void)animateMessageIn;
-(void)animateMessageOut;
-(void)animateShowWithScale;
-(void)animateHideWithScale;
-(void)adjustTextPositionWithFactor:(CGPoint)positionfactor;
-(void)adjustTextBoxWithFactor:(CGPoint)positionfactor;
-(void)adjustArrowHorizontalPosition:(CGFloat)factor;
-(void)showMessage;
-(void)hideMessage;

@end
