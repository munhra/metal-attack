//
//  GameColors.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 9/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "GameColors.h"

@implementation GameColors

@synthesize red;
@synthesize green;
@synthesize blue;

-(id)initWithColors:(CGFloat)redValue green:(CGFloat)greenValue blue:(CGFloat)blueValue
{
    self = [super init];
    if (self) {
        self.red = redValue;
        self.green = greenValue;
        self.blue = blueValue;
    }
    return self;
}

@end
