//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//
//  BandStoreItem.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 03/04/13.
//
//

#import "BandStoreItem.h"

@implementation BandStoreItem

@synthesize appStoreId;
@synthesize credits;
@synthesize itemName;
@synthesize value;
@synthesize itemType;
@synthesize description;
@synthesize endingText;
@synthesize duration;
@synthesize assetName;
@synthesize ownedBandItem;
@synthesize selectedBandItem;
@synthesize qtd;
@synthesize itemSprt;
@synthesize bandGuySprite;
@synthesize armor;
@synthesize shootPower;
@synthesize compShoot;
@synthesize level;
@synthesize itemid;
@synthesize compPoster;
@synthesize compLifeImage;
@synthesize compBannerImage;
@synthesize compStorePosterImage;
@synthesize genericItemType;
@synthesize bandGuyEndSprite;
@synthesize frame;
@synthesize bandGuyEndFileName;
@synthesize powerConsumption;
@synthesize powerRestoration;
@synthesize storePosterFileName;
@synthesize posterFileName;

@end
