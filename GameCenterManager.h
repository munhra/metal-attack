//
//  GameCenterManager.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 12/08/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import "AchievimentBanner.h"

@interface GameCenterManager : NSOperation <GKGameCenterControllerDelegate>

@property(nonatomic) BOOL gameCenterAvailable;
@property(nonatomic)BOOL userAuthenticated;
@property(nonatomic,weak) UIViewController *delegateViewController;
@property(nonatomic,retain) NSMutableArray<GKAchievement *> *achivments;
@property(nonatomic,retain) NSArray<GKAchievementDescription *> *achivmentDescriptions;
@property(nonatomic,retain) CCSprite *coinImageSprite;

+(GameCenterManager *)sharedInstance;
-(void)authenticateLocalUser;
-(void)showLeaderBoard:(id)delegate;
-(void)updateLeaderBoard:(NSInteger)newscore;
-(void)achivmentUpdate:(NSString *)achivmentID withCallBack:(void (^)(AchievimentBanner *))callbackBlock;
-(void)loadAchivments;
-(void)resetAchivments;
-(void)downloadAchivmentsDescriptions;
-(void)setAchivmentCompleteByKey:(NSString *)achivmentKey;
-(BOOL)checkAchivmentCompleteByKey:(NSString *)achivmentKey;
-(BOOL)isAllItemsBoughtAchivment;
-(BOOL)isAllBandGuysBought;

@end
