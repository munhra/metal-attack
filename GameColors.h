//
//  GameColors.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 9/13/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameColors : NSObject

@property(nonatomic) CGFloat red;
@property(nonatomic) CGFloat green;
@property(nonatomic) CGFloat blue;

-(id)initWithColors:(CGFloat)redValue green:(CGFloat)greenValue blue:(CGFloat)blueValue;

@end
