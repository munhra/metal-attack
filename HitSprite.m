//
//  HitSprite.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 08/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "HitSprite.h"


@implementation HitSprite

-(void)endHitAnimation
{
    [[self parent] removeChild:self];
}

@end
