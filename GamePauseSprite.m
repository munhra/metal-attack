//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  GamePauseSprite.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 03/12/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "GamePauseSprite.h"
#import "LevelScene.h"
#import "LevelSceneController.h"
#import "ReloadLevel.h"
#import "CCTextureCache.h"


@implementation GamePauseSprite

@synthesize levelDelegate;
@synthesize firstItemColumn;
@synthesize secondtItemColumn;

NSMutableArray *pauseItems;
NSMutableArray *itemLabels;

-(void)doMenu
{
    NSLog(@"doMenu");
    [levelDelegate setGameState:GAME_OVER];
    [[CCDirector sharedDirector] resume];
    [[[levelDelegate door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    //[[[levelDelegate door] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
    //    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
    //}];
    [self removeFromParentAndCleanup:YES];
    [[levelDelegate gameHudSprite] setDelegate:nil];
    [[levelDelegate gameHudSprite] removeFromParent];
    levelDelegate = nil;
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
}

-(void)doRestartLevel
{
    NSLog(@"restartLevel");
    [[CCDirector sharedDirector] resume];
    [levelDelegate setGameState:GAME_OVER];
    [[[levelDelegate door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    [[[levelDelegate door] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
    CCScene *relScene =  [CCBReader loadAsScene:@"ReloadLevel"];
    ReloadLevel *reloadLevel = (ReloadLevel *)[relScene getChildByName:@"ReloadLevel" recursively:YES];
    [reloadLevel setLevelToReload:[levelDelegate levelNumber]];
    [[CCDirector sharedDirector] replaceScene:relScene];
        
        //[[CCDirector sharedDirector] replaceScene:[[LevelSceneController sharedInstance] loadLevelScene:[levelDelegate levelNumber]
        //                                           waveNumber:0]];
    }];
}

-(void)doResumeLevel
{
    NSLog(@"resumeLevel");
    [[CCDirector sharedDirector] resume];
    [[self animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[self levelDelegate] doResumeGame];
    }];
    [[self animationManager] runAnimationsForSequenceNamed:@"RemovePoster"];
}


-(void)loadItemSpriteFrame{
    
    pauseItems = [[NSMutableArray alloc] init];
    itemLabels = [[NSMutableArray alloc] init];
    BandVault *vault = [BandVault sharedInstance];
    BandStoreItemsCtrl *bandStoreItemsCtrl = [BandStoreItemsCtrl sharedInstance];
    
    for (CCNode *itemPause in [secondtItemColumn children]) {
        if (([itemPause isKindOfClass:[CCSprite class]]) &! ([itemPause isKindOfClass:[CCLabelTTF class]])){
            [(CCSprite *) itemPause setSpriteFrame:nil];
            [(CCSprite *) itemPause setSpriteFrame:nil];
            [pauseItems addObject:itemPause];
        }
        if ([itemPause isKindOfClass:[CCLabelTTF class]]) {
            [itemPause setVisible:NO];
            [itemLabels addObject:itemPause];
        }
    }
    
    for (CCNode *itemPause in [firstItemColumn children]) {
        if (([itemPause isKindOfClass:[CCSprite class]]) &! ([itemPause isKindOfClass:[CCLabelTTF class]])){
            [(CCSprite *) itemPause setSpriteFrame:nil];
            [pauseItems addObject:itemPause];
        }
        if ([itemPause isKindOfClass:[CCLabelTTF class]]) {
            [itemPause setVisible:NO];
            [itemLabels addObject:itemPause];
        }
    }
    
    int itemIndex = 0;

    for (BandVaultItem *vaultItem in [vault selectedItems]) {
        for (BandStoreItem *bandItem in [bandStoreItemsCtrl generalItems]) {
            if ([bandItem.appStoreId isEqualToString:vaultItem.appId]){
                
                CCSprite *itemSprt = [pauseItems objectAtIndex:itemIndex];
                [itemSprt setSpriteFrame:[bandItem.itemSprt spriteFrame]];
                [itemSprt setName:vaultItem.appId];
                
                CCLabelTTF *itemLabel = [itemLabels objectAtIndex:itemIndex];
                [itemLabel setName:bandItem.appStoreId];
                [itemLabel setString:bandItem.itemName];
                [itemLabel setVisible:YES];
                itemIndex++;
            }
        }
    }
}

-(void)eraseItem:(NSString *)itemId
{
    for (CCSprite *itemsprt in pauseItems) {
        if (([itemsprt.name isEqualToString:itemId]) &&  (itemsprt.opacity != 0.5)){
            [itemsprt setColor:[CCColor grayColor]];
            [itemsprt setOpacity:0.5];
            break;
        }
    }
}

-(void)dealloc
{
    NSLog(@"Delloc gamepausesprite");
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}

@end
