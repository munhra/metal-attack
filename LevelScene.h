//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  LevelScene.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 01/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "BandSprite.h"
#import "EnemyParams.h"
#import "UniversalInfo.h"
#import "EndGame.h"
#import "Level.h"
#import "GameHudSprite.h"
#import "GamePauseSprite.h"
#import "Enemy.h"
#import "ItemBelt.h"
#import "ItempackSprite.h"
#import "HudButton.h"
#import "InvulnerabilityPowerUP.h"
#import "RevivalPowerUp.h"
#import "CoinMultiplierPowerUp.h"
#import "HealthPowerUp.h"
#import "WaveClearedBanner.h"
#import "RewardVideoDelegate.h"

@interface LevelScene : CCNode <CCBAnimationManagerDelegate> {
    
    CGPoint beginTouch;
    CGPoint endTouch;
    NSMutableArray *activeEnemies;
    NSMutableArray *killedEnemies;
    float rotationAngle;
    BOOL moved;
    int waveEnemiesLeft;
    int levelEnemiesLeft;
    GameState gameState;
    int levelNumber;
    int waveNumber;
    int scoreCount;
    int totalLevel;
    int levelCoins;
    CCTouch *playerTouch;
    Level *currentLevel;
    
    CCSprite *enemyPOS1;
    CCSprite *enemyPOS2;
    CCSprite *enemyPOS3;
    CCSprite *enemyPOS4;
    CCSprite *enemyPOS5;
    CCSprite *enemyPOS6;
    CCSprite *enemyPOS7;
    CCSprite *enemyPOS8;
    
    NSMutableArray *coinAnimFrames;
    
    //items effect
    int coinMultiplier;
    BOOL noCoolDown;
    
    //InvulnerabilityPowerUP *invulnerabilityPowerUP;
    RevivalPowerUp *revivalPowerUp;
    CoinMultiplierPowerUp *coinMultiplierPowerUp;
    HealthPowerUp *healthPowerUp;
    
    WaveClearedBanner *waveClearedSprite;
    
    int levelContext;
    
}


struct ShieldPositions {
    
    CGPoint showPosition;
    CGPoint hidePosition;

};

@property (nonatomic,weak) BandSprite *bandSprite;
@property (nonatomic,weak) CCSprite *shield1Sprite;
@property (nonatomic,weak) CCSprite *shield2Sprite;
@property (nonatomic,weak) CCSprite *shield3Sprite;
@property (nonatomic,weak) CCSprite *shield4Sprite;
@property (nonatomic,weak) CCSprite *shield5Sprite;
@property (nonatomic,weak) CCSprite *shield6Sprite;
@property (nonatomic,weak) CCSprite *shield7Sprite;
@property (nonatomic,weak) CCSprite *shield8Sprite;
@property (nonatomic,weak) CCSprite *shield9Sprite;
@property (nonatomic,weak) CCSprite *shield10Sprite;

@property (nonatomic) int levelCoins;
@property (nonatomic) CGPoint beginTouch;
@property (nonatomic) CGPoint endTouch;
@property (nonatomic) float rotationAngle;
@property (nonatomic,retain) NSMutableArray *activeEnemies;
@property (nonatomic) BOOL moved;
@property (nonatomic) int waveEnemiesLeft;
@property (nonatomic) GameState gameState;
@property (nonatomic) int scoreCount;
@property (nonatomic) int levelNumber;
@property (nonatomic) int waveNumber;
@property (nonatomic) int levelEnemiesLeft;
@property (nonatomic) bool touchEnded;
@property (nonatomic,retain) CCTouch *playerTouch;
@property (nonatomic, retain) NSMutableArray *coinAnimFrames;
@property (nonatomic, retain) CCAction *coinAnimation;
@property (nonatomic, retain) Level *currentLevel;
@property (nonatomic) int totalLevel;
@property (nonatomic, weak) CCSprite *coin;
@property (nonatomic, weak) CCNode *door;
@property (nonatomic, weak) GameHudSprite *gameHudSprite;
@property (nonatomic, weak) GamePauseSprite *gamePause;
@property (nonatomic, weak) CCSprite *frontAmp;
@property (nonatomic, weak) CCSprite *backAmp;
@property (nonatomic, retain) NSMutableArray *killedEnemies;
@property (nonatomic, weak) CCSprite *testRectangle;
@property (nonatomic, weak) CCSprite *testRectangle2;
@property (nonatomic, weak) CCButton *consumeItemButton;
@property (nonatomic, weak) HudButton *itemBeltButton;
@property (nonatomic, weak) ItemBelt *itemBeltMenu;

@property (nonatomic, weak) ItempackSprite *itemPackSprite;
@property (nonatomic) int coinMultiplier;
@property (nonatomic) BOOL noCoolDown;
@property (nonatomic,weak) CCSprite *menuItemUp;
@property (nonatomic,weak) CCSprite *menuItemRight;
@property (nonatomic) int activeShieldType;

@property (nonatomic, retain) NSMutableArray *shieldSpriteArray;
@property (nonatomic, retain) NSMutableArray *shieldPositionArray;
@property (nonatomic, retain) NSMutableArray *jewelsArray;

@property (nonatomic, retain) InvulnerabilityPowerUP *invulnerabilityPowerUP;
@property (nonatomic, retain) CoinMultiplierPowerUp *coinMultiplierPowerUp;
@property (nonatomic, retain) RevivalPowerUp *revivalPowerUp;
@property (nonatomic, retain) HealthPowerUp *healthPowerUp;
@property (nonatomic, retain) WaveClearedBanner *waveClearedSprite;
@property (nonatomic, retain) RewardVideoDelegate *rewardVideoDelegate;

@property (nonatomic) int levelContext;

@property (nonatomic, weak) CCSprite *backgroundSprite;

//Specific Level Context Assets

//Level Context 1

@property (nonatomic, weak) CCSprite *wallBackSprite;
@property (nonatomic, weak) CCSprite *rightWall;
@property (nonatomic, weak) CCSprite *leftWall;
@property (nonatomic, weak) CCSprite *garageDoor;
@property (nonatomic, weak) CCSprite *garagePartsGroup;
@property (nonatomic, weak) CCSprite *garagePartsGroupUp;

@property (nonatomic,weak) CCSprite *sprtLv1AddOn1;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn2;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn3;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn4;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn5;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn6;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn7;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn8;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn9;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn10;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn11;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn12;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn13;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn14;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn15;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn16;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn17;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn18;
@property (nonatomic,weak) CCSprite *sprtLv1AddOn19;

@property (nonatomic,weak) NSArray *sprtLv1AddOnArray;

//Level Context 2

@property (nonatomic,weak) CCSprite *treeSprite;
@property (nonatomic,weak) CCSprite *busSprite;
@property (nonatomic,weak) CCSprite *flagsSprite;
@property (nonatomic,weak) CCSprite *rightBuildPartSprite;
@property (nonatomic,weak) CCNode *SchoolUpGroupParts;
@property (nonatomic,weak) CCNode *schoolFlagsGroup;

@property (nonatomic,weak) CCSprite *sprtLv2AddOn1;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn2;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn3;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn4;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn5;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn6;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn7;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn8;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn9;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn10;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn11;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn12;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn13;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn14;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn15;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn16;
@property (nonatomic,weak) CCSprite *sprtLv2AddOn17;

//Level Context 3

@property (nonatomic,weak) CCSprite *barUpPartsContainer;
@property (nonatomic,weak) CCSprite *barCounterContainer;
@property (nonatomic,weak) CCSprite *barPoolContainer;

@property (nonatomic,weak) CCSprite *sprtLv3AddOn1;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn2;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn3;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn4;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn5;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn6;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn7;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn8;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn9;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn10;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn11;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn12;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn13;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn14;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn15;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn16;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn17;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn18;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn19;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn20;
@property (nonatomic,weak) CCSprite *sprtLv3AddOn21;

//Level Context 4

@property (nonatomic,weak) CCSprite *prisionContainerUp;
@property (nonatomic,weak) CCSprite *prisionContainerLeft;
@property (nonatomic,weak) CCSprite *prisionContainerRight;

@property (nonatomic,weak) CCSprite *sprtLv4AddOn1;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn2;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn3;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn4;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn5;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn6;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn7;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn8;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn9;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn10;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn11;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn12;
@property (nonatomic,weak) CCSprite *sprtLv4AddOn13;

//Level Context 5
@property (nonatomic,weak) CCSprite *gasStationContainerUP;
@property (nonatomic,weak) CCSprite *gasStationContainerRight;
@property (nonatomic,weak) CCSprite *gasStationbackWallSprite;
@property (nonatomic,weak) CCSprite *gasTopLeftSprite;
@property (nonatomic,weak) CCSprite *rightGreenCarSprite;
@property (nonatomic,weak) CCSprite *gasInformationSprite;
@property (nonatomic,weak) CCSprite *gasRightBottom;

@property (nonatomic,weak) CCSprite *sprtLv5AddOn1;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn2;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn3;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn4;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn5;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn6;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn7;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn8;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn9;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn10;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn11;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn12;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn13;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn14;
@property (nonatomic,weak) CCSprite *sprtLv5AddOn15;

//powerup sprites

@property (nonatomic,weak) CCSprite *guita1PowerUp;
@property (nonatomic,weak) CCSprite *guita2PowerUp;
@property (nonatomic,weak) CCSprite *vocalPowerUp;
@property (nonatomic,weak) CCSprite *bassPowerUp;
@property (nonatomic,weak) CCSprite *drummerPowerUp;
@property (nonatomic,weak) CCSprite *allBandPowerUp;

@property (nonatomic,weak) CCSprite *guita1RevivalPowerUp;
@property (nonatomic,weak) CCSprite *guita2RevivalPowerUp;
@property (nonatomic,weak) CCSprite *vocalRevivalPowerUp;
@property (nonatomic,weak) CCSprite *bassRevivalPowerUp;
@property (nonatomic,weak) CCSprite *drummerRevivalPowerUp;

@property (nonatomic,weak) CCTimer *invulVocalSchedule;
@property (nonatomic,weak) CCTimer *invulDrummerSchedule;
@property (nonatomic,weak) CCTimer *invulGuitarSchedule;
@property (nonatomic,weak) CCTimer *invulBassSchedule;
@property (nonatomic,weak) CCTimer *invulAllSchedule;

@property (nonatomic,weak) BandPowerUp *allBandRevivalPowerUp;

//@property(nonatomic, strong) GADInterstitial *interstitial;

-(BOOL)isGameOver;
-(void)doPauseGame;
-(void)doResumeGame;
-(void)removeEnemySprite:(CCSprite *)enemySprite AttackType:(AtackType)type;
-(void)triggerThePowerUp:(BandVaultItem *)item;
-(void)openItemBelt;
-(void)updateGameHudFaceWhenRevival:(BandItemType)bandComponent;

@end
