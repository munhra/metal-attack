//
//  ItemsShelf.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 25/04/15.
//  Copyright 2015 Apportable. All rights reserved.
//

#import "ItemsShelf.h"
#import "CCTextureCache.h"


@implementation ItemsShelf

@synthesize delegate;

-(void)showItemSummary:(CCButton *)sender
{
    NSLog(@"this came from inside item shelf %d",[[sender name] intValue]);
    [[self delegate] showItemSummary:sender];
}

-(void)onExit
{
    [super onExit];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}

- (void)dealloc
{
    NSLog(@"dealloc itemShelf");
}

@end
