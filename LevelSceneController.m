//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  LevelSceneController.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 04/11/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "LevelSceneController.h"
#import "CCTextureCache.h"

@implementation LevelSceneController

@synthesize delegate;
@synthesize loadedLevels;
@synthesize enemyDict;
@synthesize totalLevels;
@synthesize enemyDictMutable;
@synthesize enemyArray;
@synthesize scnDelegate;
@synthesize levelsPerLevelContext;
@synthesize firstLevelPerContext;

+(id)sharedInstance
{
    //Singleton Implementation
    static id master = nil;
    
    @synchronized(self)
    {
        if (master == nil){
            master = [self new];
        }
    }
    return master;
}

-(PowerUp)checkForPowerUp
{
    // calc the random number from 1 to 10
    int randforNoPowerUp = (arc4random() % 10) + 1;
    //  20% of power up to be aquired
    if (randforNoPowerUp <= 8) {
        return NOPOWERUP;
    }else{
        // random again from 1 to 5 to select the power up
        int randForPowerUp = (arc4random() % 5) + 1;
        
        switch (randForPowerUp) {
            case 1:
                return GUNPOWER_1;
                break;
            case 2:
                return GUNPOWER_2;
                break;
            case 3:
                return HEALTHPOWER;
                break;
            case 4:
                return RADIOPOWER;
                break;
            case 5:
                return COIN;
                break;
            default:
                return NOPOWERUP;
                break;
        }
    }
    return NOPOWERUP;
}

-(Level *)loadLevelWave:(int)levelNumber waveNumber:(int)wave delegate:(id)scnDelegate
{
    NSLog(@"Loading level and wave 2");
    
    self.enemyArray = [[NSMutableArray alloc] init];
    Level *loadlevel = [[self loadedLevels] objectAtIndex:levelNumber];
    
    int groupCount = [(NSNumber *)[[loadlevel waves] objectAtIndex:wave] intValue]/8;
    NSLog(@"Group Count %d",groupCount);
    
    self.enemyArray = [self createEnemies:levelNumber waveNumber:wave];
    
    for (RobotBlaster *enemy in self.enemyArray) {
        [enemy setDelegate:self.scnDelegate];
        [self scheduleBlock:^(CCTimer *timer) {
            [enemy startMovement];
        } delay:enemy.timeToStart];
    }

    return loadlevel;
}

-(CCScene *)loadLevelScene:(int)levelNumber waveNumber:(int)wave;
{
    
    NSLog(@"Loading level and wave 1");
    CCScene *gamePlayScene;
    
    self.enemyArray = [[NSMutableArray alloc] init];
    Level *loadlevel = [[self loadedLevels] objectAtIndex:levelNumber];
    
    int groupCount = [(NSNumber *)[[loadlevel waves] objectAtIndex:wave] intValue]/8;
    NSLog(@"Group Count %d",groupCount);

    self.enemyArray = [self createEnemies:levelNumber waveNumber:wave];
    
    int levelContext = [loadlevel levelContext];
    
    switch (levelContext) {
        case 1:
            gamePlayScene = [CCBReader loadAsScene:@"Levels/Level1"];
            break;
        case 2:
            gamePlayScene = [CCBReader loadAsScene:@"Levels/Level2"];
            break;
        case 3:
            gamePlayScene = [CCBReader loadAsScene:@"Levels/Level3"];
            break;
        case 4:
            gamePlayScene = [CCBReader loadAsScene:@"Levels/Level4"];
            break;
        case 5:
            gamePlayScene = [CCBReader loadAsScene:@"Levels/Level5"];
            break;
        default:
            break;
    }
    
    self.scnDelegate = (LevelScene *)[gamePlayScene getChildByName:@"LevelScene" recursively:YES];
    [self.scnDelegate setTotalLevel:self.totalLevels];
    [self.scnDelegate setLevelNumber:levelNumber];
    [self.scnDelegate setLevelContext:levelContext];
    [self.scnDelegate setLevelEnemiesLeft:loadlevel.totalLevelEnemies];
    [self.scnDelegate setWaveEnemiesLeft:[(NSNumber *)[[loadlevel waves] objectAtIndex:wave] intValue]];
    [self.scnDelegate setWaveNumber:wave];
    
    /*
    for (RobotBlaster *enemy in enemyArray) {
        [enemy setDelegate:scnDelegate];
        [self scheduleBlock:^(CCTimer *timer) {
            [enemy startMovement];
        }delay:enemy.timeToStart];
    }*/
    
    return gamePlayScene;
}

-(void)startEnemyMovment
{
    for (RobotBlaster *enemy in self.enemyArray) {
        [enemy setDelegate:self.scnDelegate];
        [self scheduleBlock:^(CCTimer *timer) {
            [enemy startMovement];
        } delay:enemy.timeToStart];
    }
}

-(void)removeEnemiesSpriteSheet:(int)levelContext
{
    switch (levelContext) {
        case 1:
            //[[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"enemySheetNew.plist"];
            [[CCTextureCache sharedTextureCache] removeTextureForKey:@"enemySheetNew.png"];
            break;
        case 2:
            [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"schoolEnemySheet.plist"];
            break;
        case 3:
            [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"barEnemySheet.plist"];
            break;
        case 4:
            [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"prisionEnemySheet.plist"];
            break;
        case 5:
            [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"gasStEnemySheet.plist"];
            break;
        default:
            break;
    }
}

-(NSMutableArray *)createEnemies:(int)levelNumber waveNumber:(int)wave;
{
   
    NSMutableArray *stageEnemyArray;
    Enemy *newEnemy1;
    stageEnemyArray = [[NSMutableArray alloc] init];
    
    int delay = 5;
    Level *loadlevel = [[self loadedLevels] objectAtIndex:levelNumber];
    
    int typeOfEnemies = [[loadlevel avaliableEnemies] count];
    NSLog(@"Type of enemies %d",typeOfEnemies);
    
    int groupCount = [(NSNumber *)[[loadlevel waves] objectAtIndex:wave] intValue]/8;
    NSLog(@"Group Count %d",groupCount);
    
    int rand;
    NSString *enemyName;
    EnemyParams *eparams;
    
    int levelContext = [loadlevel levelContext];
    
    switch (levelContext) {
        case 1:
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"enemySheetNew.plist"];
            break;
        case 2:
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"schoolEnemySheet.plist"];
            [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"enemySheetNew.plist"];
            break;
        case 3:
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"barEnemySheet.plist"];
            [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"schoolEnemySheet.plist"];
            break;
        case 4:
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"prisionEnemySheet.plist"];
            [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"barEnemySheet.plist"];
            break;
        case 5:
            [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"prisionEnemySheet.plist"];
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"gasStEnemySheet.plist"];
            break;
        default:
            break;
    }
    
    for (int i = 0; i<groupCount; i++) {
        
        if (i == 0){
            delay = 1;
        }else{
            delay = 5;
        }
        
        // Position 7
        rand = arc4random() % typeOfEnemies;
        enemyName = [[loadlevel avaliableEnemies] objectAtIndex:rand];
        eparams = [[self enemyDictMutable] objectForKey:enemyName];
        eparams.typeOfPowerUp = [self checkForPowerUp];
        
        newEnemy1 = [[RobotBlaster alloc]
                     initwithStartPosition:[[UniversalInfo sharedInstance]enemyPosition7] actionTime:1.7*i+delay
                     delegate:nil enemyParams:eparams enemyPosition:POS7
                     shootEndPos:[[UniversalInfo sharedInstance]enemyPosition3]];
        
        [stageEnemyArray addObject:newEnemy1];
        // Position 8
        rand = arc4random() % typeOfEnemies;
        enemyName = [[loadlevel avaliableEnemies] objectAtIndex:rand];
        eparams = [[self enemyDictMutable] objectForKey:enemyName];
        eparams.typeOfPowerUp = [self checkForPowerUp];
        
        newEnemy1 = [[RobotBlaster alloc]
                     initwithStartPosition:[[UniversalInfo sharedInstance]enemyPosition8] actionTime:2.9*i+delay
                     delegate:nil enemyParams:eparams enemyPosition:POS8 shootEndPos:[[UniversalInfo sharedInstance]enemyPosition4]];
        
        [stageEnemyArray addObject:newEnemy1];
        // Position 4
        rand = arc4random() % typeOfEnemies;
        enemyName = [[loadlevel avaliableEnemies] objectAtIndex:rand];
        eparams = [[self enemyDictMutable] objectForKey:enemyName];
        eparams.typeOfPowerUp = [self checkForPowerUp];
        
        newEnemy1 = [[RobotBlaster alloc]
                     initwithStartPosition:[[UniversalInfo sharedInstance]enemyPosition4] actionTime:2.5*i+delay
                     delegate:nil enemyParams:eparams enemyPosition:POS4 shootEndPos:[[UniversalInfo sharedInstance]enemyPosition8]];
        
        [stageEnemyArray addObject:newEnemy1];
        // Position 2
        rand = arc4random() % typeOfEnemies;
        enemyName = [[loadlevel avaliableEnemies] objectAtIndex:rand];
        eparams = [[self enemyDictMutable] objectForKey:enemyName];
        eparams.typeOfPowerUp = [self checkForPowerUp];
        
        newEnemy1 = [[RobotBlaster alloc]
                     initwithStartPosition:[[UniversalInfo sharedInstance]enemyPosition2] actionTime:1*i+delay
                     delegate:nil enemyParams:eparams enemyPosition:POS2 shootEndPos:[[UniversalInfo sharedInstance]enemyPosition6]];
        
        [stageEnemyArray addObject:newEnemy1];
        // Position 6
        rand = arc4random() % typeOfEnemies;
        enemyName = [[loadlevel avaliableEnemies] objectAtIndex:rand];
        eparams = [[self enemyDictMutable] objectForKey:enemyName];
        eparams.typeOfPowerUp = [self checkForPowerUp];
        
        newEnemy1 = [[RobotBlaster alloc]
                     initwithStartPosition:[[UniversalInfo sharedInstance]enemyPosition6] actionTime:1.8*i+delay
                     delegate:nil enemyParams:eparams enemyPosition:POS6 shootEndPos:[[UniversalInfo sharedInstance]enemyPosition2]];
        
        [stageEnemyArray addObject:newEnemy1];
        // Position 1
        rand = arc4random() % typeOfEnemies;
        enemyName = [[loadlevel avaliableEnemies] objectAtIndex:rand];
        eparams = [[self enemyDictMutable] objectForKey:enemyName];
        eparams.typeOfPowerUp = [self checkForPowerUp];
        
        newEnemy1 = [[RobotBlaster alloc]
                     initwithStartPosition:[[UniversalInfo sharedInstance]enemyPosition1] actionTime:0.3*i+delay
                     delegate:nil enemyParams:eparams enemyPosition:POS1 shootEndPos:[[UniversalInfo sharedInstance]enemyPosition5]];
        
        [stageEnemyArray addObject:newEnemy1];
        // Position 3
        rand = arc4random() % typeOfEnemies;
        enemyName = [[loadlevel avaliableEnemies] objectAtIndex:rand];
        eparams = [[self enemyDictMutable] objectForKey:enemyName];
        eparams.typeOfPowerUp = [self checkForPowerUp];
        
        newEnemy1 = [[RobotBlaster alloc]
                     initwithStartPosition:[[UniversalInfo sharedInstance]enemyPosition3] actionTime:0.3*i+5+delay
                     delegate:nil enemyParams:eparams enemyPosition:POS3 shootEndPos:[[UniversalInfo sharedInstance]enemyPosition7]];
        
        [stageEnemyArray addObject:newEnemy1];
        // Position 5
        rand = arc4random() % typeOfEnemies;
        enemyName = [[loadlevel avaliableEnemies] objectAtIndex:rand];
        eparams = [[self enemyDictMutable] objectForKey:enemyName];
        eparams.typeOfPowerUp = [self checkForPowerUp];
        
        newEnemy1 = [[RobotBlaster alloc]
                     initwithStartPosition:[[UniversalInfo sharedInstance]enemyPosition5] actionTime:0.3*i+5+delay
                     delegate:nil enemyParams:eparams enemyPosition:POS5 shootEndPos:[[UniversalInfo sharedInstance]enemyPosition1]];
        
        [stageEnemyArray addObject:newEnemy1];
        
    }
    
    return stageEnemyArray;
}


-(void)loadLevelJson
{
    NSLog(@"Loading json");
    
    self.levelsPerLevelContext = [[NSMutableArray alloc] init];
    self.firstLevelPerContext = [[NSMutableArray alloc] init];
    
    NSBundle *appBundle = [NSBundle bundleForClass:[self class]];
    NSString *levelDataPath = [appBundle pathForResource:@"levels" ofType:@"json"];
    NSString *jsonString = [NSString stringWithContentsOfFile:levelDataPath encoding:NSUTF8StringEncoding error:nil];
    
    NSArray *results = [jsonString JSONValue];
    
    self.totalLevels = [results count];
    
    NSString *level;
    NSNumber *numberWaves;
    NSArray *wavesArray;
    
    self.loadedLevels = [[NSMutableArray alloc] init];
    
    int oldContextNumber = 1;
    int levelPerContextCounter = 0;
    int firstLevelOnContext = 1;
    
    int lastLevel = 1;
    
    [firstLevelPerContext addObject:[[NSNumber alloc] initWithInt:firstLevelOnContext]];
    
    for (level in results){
        

        NSDictionary *levelInfo = level;
        
        Level *readLevel = [[Level alloc] init];
        [readLevel setTotalLevelEnemies:0];
        
        numberWaves = [[NSNumber alloc] initWithInt:[[levelInfo valueForKey:@"numberOfWaves"] intValue]];
        wavesArray = [levelInfo valueForKey:@"waves"];
        
        [readLevel setLevelContext:[[levelInfo valueForKey:@"levelContext"] intValue]];
        [readLevel setTotalWaveSPerLevel:[[levelInfo valueForKey:@"numberOfWaves"] intValue]];
        [readLevel setTotalLevelEnemies:[[levelInfo valueForKey:@"levelTotalEnemies"] intValue]];
        [readLevel setLevelNumberName:[[levelInfo valueForKey:@"levelNumber"] intValue]];
        
        int levelContext = [[levelInfo valueForKey:@"levelContext"] intValue];
        
        for ( int i = 0; i < [wavesArray count]; i++) {
            NSString *waveStr = [wavesArray objectAtIndex:i];
            int waveEnemiesInt = [waveStr intValue];
            [[readLevel waves] addObject:[[NSNumber alloc] initWithInt:waveEnemiesInt]];
        }
        
        if (levelContext != oldContextNumber) {
            //context changed
            levelPerContextCounter = [[levelInfo valueForKey:@"levelNumber"] intValue];
            firstLevelOnContext = [[levelInfo valueForKey:@"levelNumber"] intValue] -1;
            [levelsPerLevelContext addObject:[[NSNumber alloc] initWithInt:(levelPerContextCounter - 1)]];
            [firstLevelPerContext addObject:[[NSNumber alloc] initWithInt:firstLevelOnContext]];
            oldContextNumber = readLevel.levelContext;
        }
        
        readLevel.avaliableEnemies = [levelInfo valueForKey:@"enemies"];
        [[self loadedLevels] addObject:readLevel];
        lastLevel = [[levelInfo valueForKey:@"levelNumber"] intValue];
    }
   
    
    
    [levelsPerLevelContext addObject:[[NSNumber alloc] initWithInt:(lastLevel)]];
    [self loadEnemyDictFromJson];
}

-(void)loadEnemyDictFromJson
{
    //load the enemies definitions from a json file
    
    NSBundle *appBundle = [NSBundle bundleForClass:[self class]];
    NSString *levelDataPath = [appBundle pathForResource:@"enemies" ofType:@"json"];
    NSString *jsonString = [NSString stringWithContentsOfFile:levelDataPath encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *results = [jsonString JSONValue];
    
    NSString *enemy;
    
    self.enemyDictMutable = [[NSMutableDictionary alloc] init];
    
    for (enemy in results){
        
        EnemyParams *robot = [[EnemyParams alloc] init];
        NSDictionary *enemyInfo = [results valueForKey:enemy];
        
        robot.enemyName =[enemyInfo valueForKey:@"enemyName"];
        robot.armor = [[enemyInfo valueForKey:@"armor"] floatValue];
        robot.weaponDamage = [[enemyInfo valueForKey:@"weaponDamage"] floatValue];
        robot.scorePoints = [[enemyInfo valueForKey:@"scorePoints"] floatValue];
        robot.numberOfFrames = [[enemyInfo valueForKey:@"numberOfFrames"] intValue];
        robot.timeToReach = [[enemyInfo valueForKey:@"timeToReach"] doubleValue];
        robot.typeOfPowerUp = [[enemyInfo valueForKey:@"typeOfPowerUp"] floatValue];
        robot.atackType = [self getEnemyAtackType:enemyInfo];
        
        robot.lowerAction = ccp([[enemyInfo valueForKey:@"lowerActionX"] floatValue], [[enemyInfo valueForKey:@"lowerActionY"] floatValue]);
        robot.sideAction = ccp([[enemyInfo valueForKey:@"sideActionX"] floatValue], [[enemyInfo valueForKey:@"sideActionY"] floatValue]);
        robot.upperAction = ccp([[enemyInfo valueForKey:@"upperActionX"] floatValue], [[enemyInfo valueForKey:@"upperActionY"] floatValue]);
        
        robot.fLowerAction = ccp([[enemyInfo valueForKey:@"flipLowerActionX"] floatValue], [[enemyInfo valueForKey:@"flipLowerActionY"] floatValue]);
        robot.fSideAction = ccp([[enemyInfo valueForKey:@"flipSideActionX"] floatValue], [[enemyInfo valueForKey:@"flipSideActionY"] floatValue]);
        robot.fUpperAction = ccp([[enemyInfo valueForKey:@"flipUpperActionX"] floatValue], [[enemyInfo valueForKey:@"flipUpperActionY"] floatValue]);
        
        robot.shootSound = [enemyInfo valueForKey:@"shootSound"];
        robot.shootHit = [enemyInfo valueForKey:@"shootHit"];
        
        robot.dropCoinLowerLevel = [[enemyInfo valueForKey:@"dropCoinLowerLevel"] intValue];
        robot.dropCoinHighLevel = [[enemyInfo valueForKey:@"dropCoinHighLevel"] intValue];
        
        if (robot.atackType == WALK_SHOOT) {
            robot.shootStartPosition = ccp([[enemyInfo valueForKey:@"shootStartX"] intValue],
                                           [[enemyInfo valueForKey:@"shootStartY"] intValue]);
            robot.shootStyle = [self convertShootStyle:[[enemyInfo valueForKey:@"shootStyle"] intValue]];
        }

        [[self enemyDictMutable] setValue:robot forKey:robot.enemyName];
        
        NSString *testName = [[[self enemyDictMutable] objectForKey:enemy] enemyName];
        NSLog(@"Enemy name test %@", testName);
        
    }
    
    
    
    NSLog(@"Number of enemies %d",[[self enemyDictMutable] count]);
}

-(ShootStyle)convertShootStyle:(int)style
{
    switch (style) {
        case 0:
            return SINGLE;
            break;
        case 1:
            return ROTATION;
        case 2:
            return JUMP;
            break;
        default:
            return SINGLE;
            break;
    }
}

-(AtackType)getEnemyAtackType:(NSDictionary *)info{
    
    if ([[info valueForKey:@"meele"] boolValue]){
        return MEELEE;
    }else if ([[info valueForKey:@"autoDestruction"] boolValue]){
        return AUTODESTRUCTION;
    }else{
        return WALK_SHOOT;
    }
    
}

-(void)saveLastReachedLevel:(int)levelNumber
{
    int lastLevelReached = [self loadLastReachedLevel];
    if (levelNumber > lastLevelReached) {
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        [userdef setInteger:levelNumber forKey:@"Level"];
    }
}

-(int)loadLastReachedLevel
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    return [userdef integerForKey:@"Level"];
}



@end
