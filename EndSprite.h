//
//  EndSprite.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 7/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCSprite.h"

@interface EndSprite : CCSprite

@property(weak,nonatomic) CCSprite *endingClipMask;
@property(weak,nonatomic) CCClippingNode *endClippingNode;

@end
