//
//  BandShoot.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 09/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface BandShoot : CCSprite
{
    BOOL removeObject;
    int level;
}

@property (nonatomic) BOOL removeObject;
@property(nonatomic,retain) CCSprite *baseShoot;
@property(nonatomic,retain) CCSprite *drummerShoot;
@property(nonatomic,retain) CCSprite *vocalShoot;
@property(nonatomic,retain) CCSprite *bassShoot;
@property(nonatomic,retain) CCSprite *leadShoot;
@property(nonatomic,retain) CCSprite *megaShoot;
@property(nonatomic) int level;

-(id)initWithMaxBandLevel:(int)maxLevel;
-(void)addAllShootComponents;
-(void)enableMegaShootSprite;
-(void)tintShoot;

@end
