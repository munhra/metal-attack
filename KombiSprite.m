//
//  KombiSprite.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 17/04/15.
//  Copyright 2015 Apportable. All rights reserved.
//

#import "KombiSprite.h"
#import "ItemSelection.h"


@implementation KombiSprite

@synthesize delegate;

-(void)moreItems{
    NSLog(@"More Items inside kombi");
    [(ItemSelection *)[self delegate] moreItems:-1];
}

- (void)dealloc
{
    NSLog(@"dealloc kombi");
}

@end
