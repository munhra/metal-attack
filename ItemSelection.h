//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  ItemSelection.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 07/03/15.
//  Copyright 2015 Apportable. All rights reserved.
//infinityRoad

#import <Foundation/Foundation.h>
#import "BandVault.h"
#import "InfinityRoad.h"
#import "KombiSprite.h"
#import "MessagesSprite.h"
#import "cocos2d.h"

@interface ItemSelection : CCNode {
    
    NSMutableArray *ownedItems;
    int nextLevel;
    int nextItemCounter;
    int maxShelfNumber;

}

@property(nonatomic,retain) NSMutableArray *ownedItems;
@property(nonatomic,retain) MessagesSprite *message;
@property(nonatomic,weak) CCLayoutBox *ownedHorizontalBox1;
@property(nonatomic,weak) CCLayoutBox *ownedHorizontalBox2;
@property(nonatomic,weak) CCNode *box1;
@property(nonatomic,weak) KombiSprite *kombisprt;
@property(nonatomic,weak) CCSprite *littlecarFront;
@property(nonatomic,weak) InfinityRoad *infinityRoad;
@property(nonatomic,weak) CCSprite *itemGirl;
@property(nonatomic,weak) CCNode *itemShelf;
@property(nonatomic) int nextLevel;
@property(nonatomic) int nextItemCounter;
@property(nonatomic) int maxShelfNumber;
@property(nonatomic,weak) CCButton *informationButton;
@property(nonatomic) BOOL itemInTransition;

-(void)moreItems:(int)prvItemCounter;

@end
