//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  BandGuySprite.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 01/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "BandGuySprite.h"

@implementation BandGuySprite

@synthesize rightAnchorPoint;
@synthesize rightGuitarPosition;
@synthesize leftAnchorPoint;
@synthesize leftGuitarPosition;

@synthesize rightBackAnchorPoint;
@synthesize rightBackGuitarPosition;
@synthesize leftBackAnchorPoint;
@synthesize leftBackGuitarPosition;

@synthesize vocalLeftAnchorPoint;
@synthesize vocalRightAnchorPoint;
@synthesize leftVocalPosition;
@synthesize rightVocalPosition;
@synthesize vocalRotFactor;
@synthesize guitarRotFactor;

-(id)init
{
    self = [super init];
    
    if (self) {
        NSLog(@"Init BandGuySprite !!!!");
    }
    return self;
}

-(void)didLoadFromCCB
{
    
        CCNode *guitarFront = [self getChildByName:@"GuitarFront" recursively:YES];
        CCNode *guitarBack = [self getChildByName:@"GuitarBack" recursively:YES];
        
        rightGuitarPosition = [guitarFront position];
        rightAnchorPoint = [guitarFront anchorPoint];
        leftGuitarPosition = ccp(1 - rightGuitarPosition.x, rightGuitarPosition.y);
        leftAnchorPoint = ccp(rightAnchorPoint.x,1 - rightAnchorPoint.y);
        
        rightBackGuitarPosition = [guitarBack position];
        rightBackAnchorPoint = [guitarBack anchorPoint];
        
        leftBackGuitarPosition = ccp(1 - rightBackGuitarPosition.x, rightBackGuitarPosition.y);
        leftBackAnchorPoint = ccp(rightBackAnchorPoint.x,1 - rightBackAnchorPoint.y);
    
        leftVocalPosition = [(CCNode *)[[self children] firstObject] position];
        vocalLeftAnchorPoint = [[[self children] firstObject] anchorPoint];
        rightVocalPosition = ccp(1 - [(CCNode *)[[self children] firstObject] position].x, [(CCNode *)[[self children] firstObject] position].y);
        vocalRightAnchorPoint = ccp(1 - [[[self children] firstObject] anchorPoint].x, [[[self children] firstObject] anchorPoint].y);
    [self hideCollisionRect];
}

-(void)hideCollisionRect
{
    int index = 1;
    
    CCSprite *colSprite = (CCSprite *)[self getChildByName:@"ColisionRect" recursively:NO];
    colSprite.opacity = 0;
    
    colSprite = (CCSprite *)[self getChildByName:@"ColisionRect1" recursively:NO];
    
    while (colSprite) {
        colSprite.opacity = 0;
        index++;
        NSString *indexStr = [NSString stringWithFormat:@"ColisionRect%d",index];
        colSprite = (CCSprite *)[self getChildByName:indexStr recursively:NO];
    }
}

-(void)beginDieAnimation
{
    NSLog(@"BeginDieAnimation");
}

-(void)endDieAnimation
{
    NSLog(@"EndDieAnimation");
}

-(void)restoreOriginalState
{
    if (vocalRotFactor == 0) {
        CCSprite *guitarFront = (CCSprite *)[self getChildByName:@"GuitarFront" recursively:YES];
        CCSprite *guitarBack = (CCSprite *)[self getChildByName:@"GuitarBack" recursively:YES];
        CCSprite *body = (CCSprite *)[self getChildByName:@"Body" recursively:NO];
        [guitarFront setAnchorPoint:rightAnchorPoint];
        [guitarBack setAnchorPoint:rightBackAnchorPoint];
        [guitarFront setPosition:rightGuitarPosition];
        [guitarBack setPosition:rightBackGuitarPosition];
        [guitarFront setOpacity:1.0];
        [guitarBack setOpacity:1.0];
        [guitarFront setRotation:0];
        [guitarBack setRotation:0];
        [guitarBack setFlipY:NO];
        [guitarFront setFlipY:NO];
        [body setFlipX:NO];
        [guitarFront setColor:[CCColor whiteColor]];
        [guitarBack setColor:[CCColor whiteColor]];
        [body setColor:[CCColor whiteColor]];
        [self setColor:[CCColor whiteColor]];
    }else{
        // vocalist, as vocalRotFactor can never be  0
        CCSprite *vocalUp =  (CCSprite *)[[self children] firstObject];
        [vocalUp setAnchorPoint:vocalRightAnchorPoint];
        [vocalUp setPosition:rightVocalPosition];
        [vocalUp setAnchorPoint:vocalLeftAnchorPoint];
        [vocalUp setPosition:leftVocalPosition];
        [vocalUp setFlipX:NO];
        [vocalUp setOpacity:1.0];
        [vocalUp setRotation:0];
        [self setFlipX:NO];
        [vocalUp setColor:[CCColor whiteColor]];
        [self setColor:[CCColor whiteColor]];
    }
}

-(void)prepareForRevival
{
    [self restoreOriginalState];
}

-(void)prepareForDeath
{
    if (vocalRotFactor == 0) {
        CCNode *guitarFront = [self getChildByName:@"GuitarFront" recursively:YES];
        CCNode *guitarBack = [self getChildByName:@"GuitarBack" recursively:YES];
        CCSprite *body = (CCSprite *)[self getChildByName:@"Body" recursively:YES];
        [guitarFront setOpacity:0.0f];
        [guitarBack setOpacity:0.0f];
        [guitarFront setRotation:0.0f];
        [guitarBack setRotation:0.0f];
        [body setFlipX:NO];
    }else{
        [(CCSprite *)[[self children] firstObject] setOpacity:0.0f];
        [(CCSprite *)[[self children] firstObject] setRotation:0.0f];
        [self setFlipX:NO];
    }
}

-(void)performTintHit{
    
    CCSprite *guitarFront;
    CCSprite *guitarBack;
    CCSprite *body;
    CCSprite *vocalUp;
    
    CCActionTintTo *tintRedAction = [CCActionTintTo actionWithDuration:0.05 color:[CCColor redColor]];
    CCActionTintTo *tintWhiteAction = [CCActionTintTo actionWithDuration:0.05 color:[CCColor whiteColor]];
    
    if (vocalRotFactor == 0) {
        guitarFront = (CCSprite *)[self getChildByName:@"GuitarFront" recursively:YES];
        guitarBack = (CCSprite *)[self getChildByName:@"GuitarBack" recursively:YES];
        body = (CCSprite *)[self getChildByName:@"Body" recursively:NO];
    }else{
        vocalUp =  (CCSprite *)[[self children] firstObject];
    }
    
    CCActionCallBlock *endVocalUpTintHit = [CCActionCallBlock actionWithBlock:^{
        [vocalUp runAction:tintWhiteAction];
    }];
    
    CCActionSequence *tintUpVocalSequence = [CCActionSequence actions:tintRedAction, endVocalUpTintHit, nil];
    CCActionCallBlock *endGuitarBckTintHit = [CCActionCallBlock actionWithBlock:^{
        [guitarBack runAction:tintWhiteAction];
    }];
    
    CCActionSequence *tintGuitarBackSequence = [CCActionSequence actions:tintRedAction, endGuitarBckTintHit, nil];
    CCActionCallBlock *endTintHit = [CCActionCallBlock actionWithBlock:^{
        
        if (vocalRotFactor == 0){
            [self runAction:tintWhiteAction];
        }else{
            [self runAction:tintWhiteAction];
            [vocalUp runAction:tintUpVocalSequence];
        }
    }];
    
    CCActionCallBlock *endGuitarFntTintHit = [CCActionCallBlock actionWithBlock:^{
        [guitarFront runAction:tintWhiteAction];
        [guitarBack runAction:tintGuitarBackSequence];
        
    }];
    
    CCActionSequence *tintGuitarFrontSequence = [CCActionSequence actions:tintRedAction, endGuitarFntTintHit, nil];
    CCActionCallBlock *endBodyTintHit = [CCActionCallBlock actionWithBlock:^{
        [body runAction:tintWhiteAction];
        [guitarFront runAction:tintGuitarFrontSequence];
        
    }];
    
    CCActionSequence *tintSequence = [CCActionSequence actions:tintRedAction, endTintHit, nil];
    CCActionSequence *tintBodySequence = [CCActionSequence actions:tintRedAction, endBodyTintHit, nil];
    
    [self runAction:tintSequence];
    [body runAction:tintBodySequence];
}


@end
