//
//  WaveClearedBanner.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 4/23/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WaveClearedBanner : CCNode

@property (nonatomic, weak) CCLabelTTF *waveText;
@property (nonatomic, weak) CCLabelTTF *waveNumberText;
@property (nonatomic, weak) CCLabelTTF *waveClearedText;
@property (nonatomic, weak) CCSprite *image;
@property (nonatomic, weak) CCSprite *backgroundNine;
@property (nonatomic) bool hideWithPixelation;

@end
