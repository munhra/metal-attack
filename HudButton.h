//
//  HudButton.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 18/10/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "CCButton.h"

@interface HudButton : CCButton

@property(nonatomic, weak) id delegate;

@end
