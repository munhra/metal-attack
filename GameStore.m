//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  GameStore.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 16/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "GameStore.h"
#import "BandVault.h"
#import "BandStoreItem.h"
#import "ItemSummary.h"
#import "UniversalInfo.h"
#import "MetalAttackAnalytics.h"
#import "GameSoundManager.h"
#import "CCTextureCache.h"
#import "GameCenterManager.h"
#import "AchievimentBanner.h"

@implementation GameStore

@synthesize bandBuyPoster;
@synthesize displayItems;
@synthesize door;
@synthesize itemSummary;
@synthesize selectedItem;
@synthesize bandCoinsLabel;
@synthesize itemsNode1;
@synthesize itemsNode2;
@synthesize price1;
@synthesize price2;
@synthesize price3;
@synthesize price4;
@synthesize price5;
@synthesize itemsShelf;
@synthesize nextItemCounter;
@synthesize itemButtons;
@synthesize drummerButton;
@synthesize vocalButton;
@synthesize bassButton;
@synthesize baseButton;
@synthesize leadButton;
@synthesize punk;
@synthesize moreItems;
@synthesize message;
@synthesize guyInfo;
@synthesize posterInfoGuyPicture;
@synthesize filterDrinksMenu;
@synthesize moreDrinks;
@synthesize activeItemFilter;
@synthesize megaShootItemCounter;
@synthesize shieldItemCounter;
@synthesize bandArmorStarsGroup1;
@synthesize bandArmorStarsGroup2;
@synthesize bandDamageStarsGroup1;
@synthesize bandDamageStarsGroup2;
@synthesize moneyBox;
@synthesize waitForAppleStore;
//@synthesize prequest;
@synthesize purchaseCanceled;
@synthesize coinPack;
@synthesize coinPackPayment;
@synthesize paymentQueue;
@synthesize storeWaitMessage;


int itemCount;
bool nextItems;
bool rightMoved;
bool showingFilter;

bool isMoreItemButtonAnimating;
bool isFilterMenuAnimating;
bool isItemSummaryAnimating;

NSMutableArray *allItems;
NSMutableArray *itemShelfs;
NSSortDescriptor *sort;
NSString *nodePreviousChildrenName;
NSString *nodeChildrenName;

CCSprite *orangeStar;
CCSprite *redStar;
CCSprite *blackStar;

-(id)init{
    self = [super init];
    
    if (self) {
        NSLog(@"StoreScene init called !!!");
        sort = [NSSortDescriptor sortDescriptorWithKey:@"appStoreId" ascending:YES];
        itemShelfs = [[NSMutableArray alloc] init];
        allItems = [NSMutableArray arrayWithArray:[[BandStoreItemsCtrl sharedInstance] coinpacks]];
        [allItems addObjectsFromArray:[[BandStoreItemsCtrl sharedInstance] generalItems]];
        rightMoved = NO;
        showingFilter = NO;
        activeItemFilter = NOFILTER;
        nodeChildrenName = @"ItemsNode0";
        
        isMoreItemButtonAnimating = NO;
        isFilterMenuAnimating = NO;
        isItemSummaryAnimating = NO;
        
        orangeStar = (CCSprite *)[CCBReader load:@"store/OrangeStar"];
        redStar = (CCSprite *)[CCBReader load:@"store/RedStar"];
        blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"dealloc store");
}

-(void)onEnter
{
    [super onEnter];
    [[door animationManager] runAnimationsForSequenceNamed:@"Closed"];
    [[door animationManager] runAnimationsForSequenceNamed:@"OpenDoor"];
    [[MetalAttackAnalytics sharedInstance] registerScreenAnalytics:@"GameStore"];
    //[self playAllBandSound];
    [[GameSoundManager sharedInstance] playStoreSong];
    if ([[UniversalInfo sharedInstance] isTesting]) {
        [self setupTestTimer];
    }
    waitForAppleStore = false;
    [[GameCenterManager sharedInstance] achivmentUpdate:@"BoutiqueKing" withCallBack:^(AchievimentBanner *achievimentBanner) {
        if (achievimentBanner) {
            [self addChild:achievimentBanner];
            [achievimentBanner animateMessageInAndOut];
        }
    }];
}

-(void)setupTestTimer
{
    [self scheduleBlock:^(CCTimer *timer) {
        [self doMenu];
    } delay:5];
}

-(void)onExit
{
    [super onExit];
    [[self itemsShelf] setDelegate:nil];
    [[self bandBuyPoster] setDelegate:nil];
    [[self itemSummary] setDelegate:nil];
    [[GameSoundManager sharedInstance] stopBandInstrumentsEffects];
    [[GameSoundManager sharedInstance] stopOneBandGuySound];
    [[GameSoundManager sharedInstance] stopBackgroundSong];
    [self removeAllChildren];
    [self cleanStoreCachedSprites];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

-(void)cleanStoreCachedSprites
{
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/punk.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/Star-ON-red.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/Star-Off.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/Star-ON-Orange.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/coin.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/bandicons/Shield-Guitar.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/itemsummary/close.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/INFO.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/bandicons/Atack-guitar.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/more itens CLOSED.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/more itens OPENED.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/backText.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/info-page.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/bandArt/lead.png"];
}

-(void)didLoadFromCCB
{
    NSLog(@"didLoadFromCCB");

    [[self bandBuyPoster] setDelegate:self];
    [[self itemSummary] setDelegate:self];
    BandVault *vault = [BandVault sharedInstance];
    NSNumber *bandCoins = [[NSNumber alloc] initWithInt:[vault bandCoins]];

    CCLabelTTF *coinLabelDebug = [CCLabelTTF labelWithString:@"999999999" fontName:@"Rockwell-CondensedBold" fontSize:40];
    NSDictionary *attribDictionary = @{@"fontName" : @"Rockwell-CondensedBold", @"fontSize" : @40 };
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"999999999" attributes:attribDictionary];
    
    NSLog(@"%f",attrString.size.width);
    NSLog(@"%f",attrString.size.height);
    
    CGRect attrBoundingRect = [attrString boundingRectWithSize:[attrString size] options:@[] context:nil];
    
    [coinLabelDebug dimensions];
    [coinLabelDebug setPosition:ccp(139,32)];

    [bandCoinsLabel setString:[[UniversalInfo sharedInstance] addZeroesToNumber:[bandCoins stringValue] numberOfZeroes:4]];
    [[self itemsNode2] setVisible:NO];

    nextItems = YES;
    nextItemCounter = 0;
    megaShootItemCounter = 1;
    shieldItemCounter = 1;
    [[self itemsShelf] setDelegate:self];
    [self createButtonBanner];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [self updatePriceTags:[self itemsNode1]];
    
    message = (MessagesSprite *)[CCBReader load:@"Message"];
    storeWaitMessage = (MessagesSprite *)[CCBReader load:@"Message"];
    
    [self resetArmorStars];
    [self resetDamageStars];
}

-(void)createButtonBanner
{
    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    
    BandStoreItem *itemDrummer = [[storeCtrl drummers] objectAtIndex:vault.drummerIndex];
    BandStoreItem *itemVocal = [[storeCtrl vocals] objectAtIndex:vault.vocalIndex];
    BandStoreItem *itemBase = [[storeCtrl baseGuitars] objectAtIndex:vault.guitar2Index]; //Base
    BandStoreItem *itemLead = [[storeCtrl leadGuitars] objectAtIndex:vault.guitar1Index]; //Lead
    BandStoreItem *itemBass = [[storeCtrl basses] objectAtIndex:vault.bassIndex];
    
    [self.drummerButton setBackgroundSpriteFrame:[itemDrummer.compBannerImage spriteFrame] forState:CCControlStateNormal];
    [self.vocalButton setBackgroundSpriteFrame:[itemVocal.compBannerImage spriteFrame] forState:CCControlStateNormal];
    [self.baseButton setBackgroundSpriteFrame:[itemBase.compBannerImage spriteFrame] forState:CCControlStateNormal];
    [self.bassButton setBackgroundSpriteFrame:[itemBass.compBannerImage spriteFrame] forState:CCControlStateNormal];
    [self.leadButton setBackgroundSpriteFrame:[itemLead.compBannerImage spriteFrame] forState:CCControlStateNormal];
}

-(void)doMenu
{
    if (waitForAppleStore) {
        NSLog(@"waitForAppleStore");
        return;
    }
    
    NSLog(@"doMenu");
    [[[self door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    [[[self door] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
    }];
}

-(void)doRight
{
    NSLog(@"doRight");
    if (rightMoved) {
        
        if (waitForAppleStore) {
            return;
        }
        
        if (showingFilter) {
            
            CGPoint filterMenuPosition = [filterDrinksMenu position];
            CCActionMoveTo *moveFilterMenu = [CCActionMoveTo actionWithDuration:0.5 position:ccp(147,filterMenuPosition.y)];
            CCActionCallBlock *endMoveFilterMenu = [CCActionCallBlock actionWithBlock:^{
                [[self filterDrinksMenu] setVisible:false];
                [[self animationManager] runAnimationsForSequenceNamed:@"MoveLeft"];
            }];
            
            CCActionSequence *hideDrinksFilter = [CCActionSequence actions:moveFilterMenu, endMoveFilterMenu, nil];
            [[self filterDrinksMenu] runAction:hideDrinksFilter];
            showingFilter = NO;
            
        }else{
            [[self animationManager] runAnimationsForSequenceNamed:@"MoveLeft"];
        }
        rightMoved = NO;
        [self disableItemButtons];
        
        
        
    
    }else{
        
        [[GameCenterManager sharedInstance] achivmentUpdate:@"StoreGreenHandPal" withCallBack:^(AchievimentBanner *achievimentBanner) {
            if (achievimentBanner) {
                [self addChild:achievimentBanner];
                [achievimentBanner animateMessageInAndOut];
            }
        }];
        
        [[self animationManager] runAnimationsForSequenceNamed:@"MoveRight"];
        rightMoved = YES;
        [self enableItemButtons];
        
        if ([[GameSoundManager sharedInstance] isOneGuySoundPlaying]) {
            [[GameSoundManager sharedInstance] stopOneBandGuySound];
            //[self playAllBandSound];
        }
    }
}

-(void)doBandSelect:(CCButton *)menuItem
{
    NSLog(@"Band component select");
    [self playBandButtonGuyClick];
    [[self animationManager] runAnimationsForSequenceNamed:@"MoveBandPosterOut"];
    int tag = [menuItem.name intValue];
    [[GameSoundManager sharedInstance] stopBandInstrumentsEffects];
    [[GameSoundManager sharedInstance] stopBackgroundSong];
    switch (tag)
    {
        case 1:
            NSLog(@"show guitarrist 1");
            [bandBuyPoster doShowLeadGuitar];
            break;
        case 2:
            NSLog(@"show guitarrist 2");
            [bandBuyPoster doShowBaseGuitar];
            break;
        case 3:
            NSLog(@"show drummer");
            [bandBuyPoster doShowDrummer:menuItem];
            break;
        case 4:
            NSLog(@"Show bass guy");
            [bandBuyPoster doShowBass:menuItem];
            break;
        case 5:
            NSLog(@"Show vocal");
            [bandBuyPoster doShowVocalist:menuItem];
            break;
        default:
            break;
     
     }
}

-(void)doBuyItem:(int)itemQtd
{

    if (waitForAppleStore) {
        NSLog(@"waitForAppleStore response");
        return;
    }
    
    NSLog(@"DoBuyItem from GameStore");
    BandVault *vault = [BandVault sharedInstance];
    if ([[self selectedItem] itemType] == BAND_COIN_PACK) {
        NSLog(@"Coin pack connect to app store for product %@",[[self selectedItem] appStoreId]);
        NSSet *products = [[NSSet alloc] initWithObjects:[[self selectedItem] appStoreId], nil];
        SKProductsRequest *prequest = [[SKProductsRequest alloc] initWithProductIdentifiers:products];
        prequest.delegate = self;
        waitForAppleStore = true;
        [self waitingAppStoreToRespondMessage];
        [self scheduleAppStoreTimeout];
        [prequest start];
    }else{
        if ([vault bandCoins] >= [[self selectedItem] credits] * itemQtd) {
            NSLog(@"Purchase complete");
            int ownedItemQtd = [vault getAllitemsQtd];
            NSLog(@"Item quantity %d",ownedItemQtd+itemQtd);
            if (itemQtd + ownedItemQtd <= 99) {
                NSString *qtdSTR = [NSString stringWithFormat:@"%d",itemQtd];
                [[MetalAttackAnalytics sharedInstance] registerBandDrinkBuy:[selectedItem itemName] quantity:qtdSTR];
                [vault storeItem:[[self selectedItem] appStoreId] itemQtd:itemQtd];
                [vault updateBandCoins:-[[self selectedItem] credits] * itemQtd];
                NSNumber *bandCoins = [[NSNumber alloc] initWithInt:[vault bandCoins]];
                [bandCoinsLabel setString:[[UniversalInfo sharedInstance] addZeroesToNumber:[bandCoins stringValue] numberOfZeroes:4]];
                [self moveItemSummaryOut];
                NSString *boughtItemFlag = [NSString stringWithFormat:@"Bougth%@",selectedItem.appStoreId];
                [[GameCenterManager sharedInstance] setAchivmentCompleteByKey:boughtItemFlag];
                [self performAllItemsBoughtAchivment];
            } else {
                [self createMessage:@"You do not have room on the truck for more than 99 itens, sorry man !"];
                [self moveItemSummaryOut];
                NSLog(@"Can not buy more than 99 itens of same type");
            }
    
        }else{
            [self createMessage:@"You need more coins to purchase this item !"];
            [self moveItemSummaryOut];
            NSLog(@"You don't have enought coins");
        }
    }
}

-(void)performAllItemsBoughtAchivment
{
    if ([[GameCenterManager sharedInstance] isAllItemsBoughtAchivment]) {
        if (![[GameCenterManager sharedInstance] checkAchivmentCompleteByKey:@"PowerupCapitalist"]) {
            [[GameCenterManager sharedInstance] achivmentUpdate:@"PowerupCapitalist" withCallBack:^(AchievimentBanner *achievimentBanner) {
                if (achievimentBanner) {
                    [self addChild:achievimentBanner];
                    [achievimentBanner animateMessageInAndOut];
                    [[GameCenterManager sharedInstance] setAchivmentCompleteByKey:@"PowerupCapitalist"];
                }
            }];
        }
    }
}

-(void)waitingAppStoreToRespondMessage
{
    [self showWaitingStoreMessage:@"Stay a while and listen and wait..., for the internet connection."];
}

-(void)createMessageCashier:(NSString *)text
{
    CGPoint messagePosition = ccp([self boundingBox].size.width * -1.8,[self boundingBox].size.height * 0.5);
    [self.message setPosition:messagePosition];
    [self.message setTextMessage:text];
    [self.message setFontSize:20];
    [self.message setBoxHeight:150];
    [self.message setVisible:YES];
    
    if ([self.message parent] == nil) {
        float arrowPosition = [[message arrowSprite] position].x;
        [[message arrowSprite] setPosition:ccp(arrowPosition, [[message arrowSprite] position].y)];
    }
    
    [message removeFromParent];
    [self addChild:message];
    [self.message animateMessageIn];
}

-(void)showWaitingStoreMessage:(NSString *)text
{
    CGPoint messagePosition = ccp([self boundingBox].size.width * -2,[self boundingBox].size.height * 0.8);
    
    [self.storeWaitMessage setPosition:messagePosition];
    [self.storeWaitMessage setTextMessage:text];
    [self.storeWaitMessage setFontSize:15];
    [self.storeWaitMessage setBoxHeight:150];
    [self.storeWaitMessage setVisible:YES];
    
    if ([self.storeWaitMessage parent] == nil) {
        float arrowPosition = [[message arrowSprite] position].x + 150;
        [[storeWaitMessage arrowSprite] setPosition:ccp(arrowPosition, [[message arrowSprite] position].y)];
        [[storeWaitMessage arrowSprite] setFlipX:NO];
    }
    
    [storeWaitMessage removeFromParent];
    [self addChild:storeWaitMessage];
    [self.storeWaitMessage showMessage];
    [self showPunk];
}

-(void)hideWaitingStoreMessage
{
    [self.storeWaitMessage hideMessage];
    [self hidePunk];
}

-(void)createMessage:(NSString *)text
{
    CGPoint messagePosition = ccp([self boundingBox].size.width * -2,[self boundingBox].size.height * 0.8);
    [self.message setPosition:messagePosition];
    [self.message setTextMessage:text];
    [self.message setFontSize:15];
    [self.message setBoxHeight:150];
    [self.message setVisible:YES];
    
    if ([self.message parent] == nil) {
        float arrowPosition = [[message arrowSprite] position].x + 150;
        [[message arrowSprite] setPosition:ccp(arrowPosition, [[message arrowSprite] position].y)];
        [[message arrowSprite] setFlipX:NO];
    }
    
    [message removeFromParent];
    [self addChild:message];
    [self.message animateMessageIn];
}

-(void)showPunk
{
    id punkMove = [CCActionMoveTo actionWithDuration:0.5 position:ccp(punk.position.x - 70, punk.position.y)];
    id punkMoveElastic = [CCActionEaseBackInOut actionWithAction:punkMove];
    [[self punk] runAction:punkMoveElastic];
}

-(void)hidePunk
{
    id punkMove = [CCActionMoveTo actionWithDuration:0.5 position:ccp(punk.position.x + 70, punk.position.y)];
    id punkMoveElastic = [CCActionEaseBackInOut actionWithAction:punkMove];
    [[self punk] runAction:punkMoveElastic];
}


-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSLog(@"Product Request %d",[[response products] count]);
    NSLog(@"Invalid identifiers %@",[[response invalidProductIdentifiers] firstObject]);
    
    if ([[response products] count] != 0) {
        
        coinPack = [[response products] objectAtIndex:0];
        NSLog(@"Product identifier %@",[coinPack productIdentifier]);
        coinPackPayment = [SKPayment paymentWithProduct:coinPack];
        paymentQueue = [SKPaymentQueue defaultQueue];
        [paymentQueue addPayment:coinPackPayment];
    }else{
        NSLog(@"Try latter");
    }
}

-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue{
    NSLog(@"paymentQueueRestoreCompletedTransactionsFinished");
}

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    NSLog(@"paymentQueue");
    [self unschedule:@selector(appStoreTimeout)];
    SKPaymentTransaction *transaction = [transactions firstObject];
    
    [transaction transactionState];

    switch ([transaction transactionState]) {
        case SKPaymentTransactionStateDeferred:
            NSLog(@"SKPaymentTransactionStateDeferred");
            waitForAppleStore = false;
            break;
        case SKPaymentTransactionStatePurchased:
            NSLog(@"SKPaymentTransactionStatePurchased");
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            [self persistPurchasedCoins];
            waitForAppleStore = false;
            [self hideWaitingStoreMessage];
            break;
        case SKPaymentTransactionStateFailed:
            NSLog(@"SKPaymentTransactionStateFailed");
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            waitForAppleStore = false;
            [self hideWaitingStoreMessage];
            break;
        case SKPaymentTransactionStateRestored:
            NSLog(@"SKPaymentTransactionStateRestored");
            waitForAppleStore = false;
            break;
        case SKPaymentTransactionStatePurchasing:
            NSLog(@"SKPaymentTransactionStatePurchasing");
        default:
            break;
    }
}

-(void)scheduleAppStoreTimeout
{
    [self scheduleOnce:@selector(appStoreTimeout) delay:10];
}

-(void)appStoreTimeout
{
    waitForAppleStore = false;
    [self hideWaitingStoreMessage];
    [self createMessage:@"Hey Dude, I think you have not paid your internet bill."];
}

-(void)persistPurchasedCoins
{
    [[self selectedItem] value];
    BandVault *vault = [BandVault sharedInstance];
    [vault updateBandCoins:[[self selectedItem] value]];
    NSNumber *bandCoins = [[NSNumber alloc] initWithInt:[vault bandCoins]];
    [bandCoinsLabel setString:[[UniversalInfo sharedInstance] addZeroesToNumber:[bandCoins stringValue] numberOfZeroes:4]];
}

-(void)closeBandPoster
{
    if (waitForAppleStore) {
        return;
    }
    [[self animationManager] runAnimationsForSequenceNamed:@"MoveBandPosterIn"];
    [[GameSoundManager sharedInstance] playStoreSong];
}

-(void)doBuyBandGuy
{
    NSLog(@"DoBuy from GameStore");
    
    BandVault *vault = [BandVault sharedInstance];
    BandStoreItem *bandStoreItem = [[bandBuyPoster itemsToShow] objectAtIndex:itemCount];
    
    if (bandStoreItem.ownedBandItem == YES) {
        NSLog(@"Active the item on the hero !");
        [self setBandGuy:bandStoreItem vaultObj:vault];
        [[MetalAttackAnalytics sharedInstance] registerBandGuySelect:[bandStoreItem itemName]];
        [self resetDamageStars];
        [self resetArmorStars];
        [self performAllGuysBoughtAchivment];
    }else{
        NSLog(@"Buy the item");
        if ([vault bandCoins] >= [bandStoreItem credits]) {
            NSLog(@"Purchase complete");
            [[MetalAttackAnalytics sharedInstance] registerBandGuyBuy:[bandStoreItem itemName]];
            [vault storeBandGuy:[bandStoreItem appStoreId] indexPosition:itemCount];
            [vault updateBandCoins:-bandStoreItem.credits];
            bandStoreItem.ownedBandItem = YES;
            NSNumber *bandCoins = [[NSNumber alloc] initWithInt:[vault bandCoins]];
            [bandCoinsLabel setString:[[UniversalInfo sharedInstance] addZeroesToNumber:[bandCoins stringValue] numberOfZeroes:4]];
            [self setBandGuy:bandStoreItem vaultObj:vault];
            [self resetDamageStars];
            [self resetArmorStars];
            [self performAchivment:bandStoreItem];
            
            NSString *boughtItemFlag;
            
            if (([bandStoreItem itemType] == GUITARRIST_1) || ([bandStoreItem itemType] == GUITARRIST_2)) {
                boughtItemFlag = [NSString stringWithFormat:@"Bougth%d%@",[bandStoreItem itemType],[bandStoreItem appStoreId]];
            }else{
                boughtItemFlag = [NSString stringWithFormat:@"Bougth%@",[bandStoreItem appStoreId]];
                
            }
            
            [[GameCenterManager sharedInstance] setAchivmentCompleteByKey:boughtItemFlag];
            
            [self performAllGuysBoughtAchivment];
        }else{
            NSLog(@"You don't have enought coins");
            [self createMessageCashier:@"You need more coins to hiew this guy !"];
        }
    }
    [[GameSoundManager sharedInstance] stopOneBandGuySound];
    [[self animationManager] runAnimationsForSequenceNamed:@"MoveBandPosterIn"];
    [[GameSoundManager sharedInstance] playStoreSong];
}

-(void)performAllGuysBoughtAchivment
{
    if ([[GameCenterManager sharedInstance] isAllBandGuysBought]) {
        if (![[GameCenterManager sharedInstance] checkAchivmentCompleteByKey:@"BandBigBoss"]) {
            [[GameCenterManager sharedInstance] achivmentUpdate:@"BandBigBoss" withCallBack:^(AchievimentBanner *achievimentBanner) {
                if (achievimentBanner) {
                    [self addChild:achievimentBanner];
                    [achievimentBanner animateMessageInAndOut];
                    [[GameCenterManager sharedInstance] setAchivmentCompleteByKey:@"BandBigBoss"];
                }
            }];
        }
    }
}

-(void)performAchivment:(BandStoreItem *)bandStoreItem
{
    
    NSString *achivment = @"";
    
    switch ([bandStoreItem itemType]) {
        case DRUMMER:
            achivment = @"DrummerEmployer";
            break;
        case BASSGUY:
            achivment = @"BassPlayer";
            break;
        case VOCALIST:
            achivment = @"VocalistFinancier";
            break;
        case GUITARRIST_2:
            achivment = @"LeadGuitarTycoon";
            break;
        case GUITARRIST_1:
            achivment = @"BaseGuitarDealer";
            break;
        default:
            break;
    }
    
    if (![[GameCenterManager sharedInstance] checkAchivmentCompleteByKey:achivment]) {
        [[GameCenterManager sharedInstance] achivmentUpdate:achivment withCallBack:^(AchievimentBanner *achievimentBanner) {
            if (achievimentBanner) {
                [self addChild:achievimentBanner];
                [achievimentBanner animateMessageInAndOut];
                [[GameCenterManager sharedInstance] setAchivmentCompleteByKey:achivment];
            }
        }];
    }
}

-(void)setBandGuy:(BandStoreItem *)bandStoreItem vaultObj:(BandVault *)vault
{
    switch ([bandStoreItem itemType]) {
        case DRUMMER:
        [vault setDrummerId:[[[bandBuyPoster itemsToShow] objectAtIndex:itemCount] appStoreId]];
        [vault setDrummerIndex:itemCount];
        [self.drummerButton setBackgroundSpriteFrame:[bandStoreItem.compBannerImage spriteFrame] forState:CCControlStateNormal];
        break;
        case BASSGUY:
        [vault setBassId:[[[bandBuyPoster itemsToShow] objectAtIndex:itemCount] appStoreId]];
        [vault setBassIndex:itemCount];
        [self.bassButton setBackgroundSpriteFrame:[bandStoreItem.compBannerImage spriteFrame] forState:CCControlStateNormal];
        break;
        case VOCALIST:
        [vault setVocalId:[[[bandBuyPoster itemsToShow] objectAtIndex:itemCount] appStoreId]];
        [vault setVocalIndex:itemCount];
        [self.vocalButton setBackgroundSpriteFrame:[bandStoreItem.compBannerImage spriteFrame] forState:CCControlStateNormal];
        break;
        case GUITARRIST_2:
        [vault setGuitar2Id:[[[bandBuyPoster itemsToShow] objectAtIndex:itemCount] appStoreId]];
        [vault setGuitar2Index:itemCount];
        [self.baseButton setBackgroundSpriteFrame:[bandStoreItem.compBannerImage spriteFrame] forState:CCControlStateNormal];
        break;
        case GUITARRIST_1:
        [vault setGuitar1Id:[[[bandBuyPoster itemsToShow] objectAtIndex:itemCount] appStoreId]];
        [vault setGuitar1Index:itemCount];
        [self.leadButton setBackgroundSpriteFrame:[bandStoreItem.compBannerImage spriteFrame] forState:CCControlStateNormal];
        break;
        default:
        break;
    }
}

-(void)doNextItemDisplay
{
    NSLog(@"doNextItemDisplay");
    [self ShowNextItemsAnimation];
}

-(void)enableItemButtons
{
    NSString *shelfChildrenName;
    switch (self.activeItemFilter) {
        case NOFILTER:
            
            if (nextItemCounter > 0) {
                shelfChildrenName = [NSString stringWithFormat:@"ItemsNode%d",nextItemCounter];
                CCNode *items = [[self itemsShelf] getChildByName:shelfChildrenName recursively:YES];
                for (CCButton *itemButton in [items children]) {
                    [itemButton setEnabled:YES];
                }
            }else{
                for (CCButton *itemButton in [[self itemsNode1] children]) {
                    [itemButton setEnabled:YES];
                }
            }
            
            break;
        case GUITARFILTER:
            shelfChildrenName = @"GuitarItems";
            break;
        case BASSFILTER:
            shelfChildrenName = @"BassItems";
            break;
        case VOCALFILTER:
            shelfChildrenName = @"Vocaltems";
            break;
        case DRUMMEFILTER:
            shelfChildrenName = @"DrumsItems";
            break;
        case COINSFILTER:
            shelfChildrenName = @"CoinMultipliers";
            break;
        case BANDFILTER:
            shelfChildrenName = @"TheBandItems";
            break;
        case COOLDOWNFILTER:
            shelfChildrenName = @"CoolDownItems";
            break;
            
        case HEALINGFILTER:
            shelfChildrenName = @"HealingItems";
            break;
            
        case MEGASHOOTFILTER:
            
            if (megaShootItemCounter == 2) {
                shelfChildrenName = @"MegaShootItems2";
            }else {
                shelfChildrenName = @"MegaShootItems1";
            }
            
            break;
            
        case SHIELDFILTER:
            
            if (shieldItemCounter == 2) {
                shelfChildrenName = @"ShieldItems2";
            }else {
                shelfChildrenName = @"ShieldItems1";
            }
            
        default:
            break;
    }
    
    
    if (shelfChildrenName) {
        CCNode *items = [[self itemsShelf] getChildByName:shelfChildrenName recursively:YES];
        for (CCButton *itemButton in [items children]) {
            [itemButton setEnabled:YES];
        }
    }
}

-(void)disableItemButtons
{
    NSString *shelfChildrenName;
    
    switch (self.activeItemFilter) {
        case NOFILTER:
            
            if (nextItemCounter > 0) {
                shelfChildrenName = [NSString stringWithFormat:@"ItemsNode%d",nextItemCounter];
                CCNode *items = [[self itemsShelf] getChildByName:shelfChildrenName recursively:YES];
                for (CCButton *itemButton in [items children]) {
                    [itemButton setEnabled:NO];
                }
            }else{
                for (CCButton *itemButton in [[self itemsNode1] children]) {
                    [itemButton setEnabled:NO];
                }
            }
            
            break;
        case GUITARFILTER:
            shelfChildrenName = @"GuitarItems";
            break;
        case BASSFILTER:
            shelfChildrenName = @"BassItems";
            break;
        case VOCALFILTER:
            shelfChildrenName = @"Vocaltems";
            break;
        case DRUMMEFILTER:
            shelfChildrenName = @"DrumsItems";
            break;
        case COINSFILTER:
            shelfChildrenName = @"CoinMultipliers";
            break;
        case BANDFILTER:
            shelfChildrenName = @"TheBandItems";
            break;
        case COOLDOWNFILTER:
            shelfChildrenName = @"CoolDownItems";
            break;
            
        case HEALINGFILTER:
            shelfChildrenName = @"HealingItems";
            break;
            
        case MEGASHOOTFILTER:
            
            if (megaShootItemCounter == 2) {
                shelfChildrenName = @"MegaShootItems2";
            }else {
                shelfChildrenName = @"MegaShootItems1";
            }
            break;
        
        case SHIELDFILTER:
            
            if (shieldItemCounter == 2) {
                shelfChildrenName = @"ShieldItems2";
            }else {
                shelfChildrenName = @"ShieldItems1";
            }
            
            break;

            
        default:
            break;
    }
    if (shelfChildrenName) {
        CCNode *items = [[self itemsShelf] getChildByName:shelfChildrenName recursively:YES];
        for (CCButton *itemButton in [items children]) {
            [itemButton setEnabled:NO];
        }
    }
    
}

-(void)moveItemSummaryIn:(BandStoreItem *)item
{
    [self disableItemButtons];
    id actionMove = [CCActionMoveTo actionWithDuration:0.5 position:ccp(itemSummary.position.x, itemSummary.position.y - 480)];
    id actionElastic = [CCActionEaseBackInOut actionWithAction:actionMove];
    
    id punkMove = [CCActionMoveTo actionWithDuration:0.5 position:ccp(punk.position.x + 70, punk.position.y)];
    id punkMoveElastic = [CCActionEaseBackInOut actionWithAction:punkMove];
    
    id moreItemsMove = [CCActionMoveTo actionWithDuration:0.2 position:ccp(moreItems.position.x + 120, moreItems.position.y + 120)];
    id moreItemsElastic = [CCActionEaseBackInOut actionWithAction:moreItemsMove];
    
    if (item.itemType == BAND_COIN_PACK) {
        [[self itemSummary] setItemText:[item description]];
        [[self itemSummary] setItemTitle:[item itemName]];
        [[self itemSummary] setItemImage:item.itemSprt];
        itemSummary.itemQtdLabel.visible = false;
        itemSummary.itemQtdSlider.visible = false;
        itemSummary.itemQtd = 1;
        itemSummary.itemUnitPrice = item.credits;
        [itemSummary showItemCost:NO];
    }else{
        itemSummary.itemQtdLabel.visible = true;
        itemSummary.itemQtdSlider.visible = true;
        [[self itemSummary] setItemText:[item description]];
        [[self itemSummary] setItemTitle:[item itemName]];
        [[self itemSummary] setItemPrice:[NSString stringWithFormat:@"%d.00",[item credits]]];
        [[self itemSummary] setItemImage:item.itemSprt];
        itemSummary.itemQtdLabel.string = @"01";
        itemSummary.itemQtdSlider.sliderValue = 0;
        itemSummary.itemQtd = 1;
        itemSummary.itemUnitPrice = item.credits;
        [itemSummary showItemCost:YES];
    }
    
    [[self itemSummary] runAction:actionElastic];
    [[self punk] runAction:punkMoveElastic];
    [[self moreItems] runAction:moreItemsElastic];
}

-(void)moveItemSummaryOut
{
    
    if (waitForAppleStore) {
        NSLog(@"waitForAppleStore !");
        return;
    }
    
    if (!isItemSummaryAnimating) {
        [self enableItemButtons];
        id actionMove = [CCActionMoveTo actionWithDuration:0.5 position:ccp(itemSummary.position.x, itemSummary.position.y + 480)];
        id actionElastic = [CCActionEaseBackInOut actionWithAction:actionMove];
        
        CCActionCallBlock *endMoveSummaryOut = [CCActionCallBlock actionWithBlock:^{
            isItemSummaryAnimating = NO;
        }];
        
        CCActionSequence *hideItemSummary = [CCActionSequence actions:actionElastic, endMoveSummaryOut, nil];
        
        
        id punkMove = [CCActionMoveTo actionWithDuration:0.5 position:ccp(punk.position.x - 70, punk.position.y)];
        id punkMoveElastic = [CCActionEaseBackInOut actionWithAction:punkMove];
        
        id moreItemsMove = [CCActionMoveTo actionWithDuration:0.2 position:ccp(moreItems.position.x - 120, moreItems.position.y - 120)];
        id moreItemsElastic = [CCActionEaseBackInOut actionWithAction:moreItemsMove];
        
        //[[self itemSummary] runAction:actionElastic];
        [[self itemSummary] runAction:hideItemSummary];
        [[self punk] runAction:punkMoveElastic];
        [[self moreItems] runAction:moreItemsElastic];
        [self ShowMoreDrinksSign];
        
        isItemSummaryAnimating = YES;
        
    }
}


-(void)showItemSummary:(CCButton*)sender
{
    [self playPowerUpBottleClick];
    int tag = [[sender name] intValue];
    //Remove shiled power up from selection
    if (tag < 41 || tag > 50) {
        NSLog(@"showItemSummary %d",tag);
        self.selectedItem = [allItems objectAtIndex:tag];
        self.selectedItem.itemSprt = [CCSprite spriteWithSpriteFrame:[sender backgroundSpriteFrameForState:CCControlStateNormal]];
        [self moveItemSummaryIn:self.selectedItem];
        [self hideMoreDrinksSign];
    }else{
        [self createMessage:@"Item out of stock, come back latter dude !"];
    }
}

-(void)ShowNextItemsAnimation
{
    
    switch (activeItemFilter) {
        case NOFILTER:
            
            nextItemCounter++;
            if (nextItemCounter >= 11 ) {
                nextItemCounter = 0;
            }
            
            if (nextItemCounter == 0) {
                [itemsNode1 setVisible:YES];
                nodePreviousChildrenName = [NSString stringWithFormat:@"ItemsNode%d",10];
                [[[self itemsShelf] getChildByName:nodePreviousChildrenName recursively:NO] setVisible:NO];
                [self updatePriceTags:[self itemsNode1]];
                
            }else{
                [itemsNode1 setVisible:NO];
                nodePreviousChildrenName = [NSString stringWithFormat:@"ItemsNode%d",nextItemCounter-1];
                nodeChildrenName = [NSString stringWithFormat:@"ItemsNode%d",nextItemCounter];
                
                [[[self itemsShelf] getChildByName:nodePreviousChildrenName recursively:NO] setVisible:NO];
                [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
                
                [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
            }
            
            break;
        case GUITARFILTER:
            
            
        
            break;
        case BASSFILTER:
            
            break;
        case VOCALFILTER:
            
            break;
            
        case DRUMMEFILTER:
            break;
      
        case COINSFILTER:
            break;
      
        case BANDFILTER:
            break;
      
        case COOLDOWNFILTER:
            break;
      
        case HEALINGFILTER:

            
            break;
            
        case MEGASHOOTFILTER:
        
            [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:NO];
            if (megaShootItemCounter == 1) {
                megaShootItemCounter++;
            }else {
                megaShootItemCounter--;
            }

            nodeChildrenName = [NSString stringWithFormat:@"MegaShootItems%d",megaShootItemCounter];
            [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
            [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
            break;
            
        case SHIELDFILTER:
            
            [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:NO];
            if (shieldItemCounter == 1) {
                shieldItemCounter++;
            }else {
                shieldItemCounter--;
            }
            
            nodeChildrenName = [NSString stringWithFormat:@"ShieldItems%d",shieldItemCounter];
            [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
            [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
            break;
            
        default:
            break;
    }
    
}

-(void)updatePriceTags:(CCNode *)items
{
    NSMutableArray *itemPriceList = [[NSMutableArray alloc] init];

    for (CCNode *item in [items children]) {
        NSString *itemPrice = @"";
        if ([[item name] intValue] > 2) {
            itemPrice = [NSString stringWithFormat:@"%d",[[allItems objectAtIndex:[[item name] intValue]] credits]];
        }
        [itemPriceList addObject:itemPrice];
    }
    
    NSArray *priceTags = @[price1,price2,price3,price4,price5];
    
    int priceListCount = 0;
    for (CCLabelTTF *priceTag in priceTags) {
        [[priceTag parent] setVisible:YES];
        if ([itemPriceList[priceListCount] length] == 0) {
            [[priceTag parent] setVisible:NO];
        }else{
            [priceTag setString:itemPriceList[priceListCount]];
        }
    
        priceListCount++;
    }
}

-(void)doFilterDrinks
{
    NSLog(@"doFilterDrinks");
    [self showFilterDrinkAnimation];
    [self hideMoreDrinksSign];
    [[self moreItems] setEnabled:NO];
}

-(void)showFilterDrinkAnimation
{
    [self playFilterPosterShowSound];
    if (!isFilterMenuAnimating) {
        [[self filterDrinksMenu] setVisible:true];
        CGPoint filterMenuPosition = [filterDrinksMenu position];
        CCActionMoveTo *moveFilterMenu = [CCActionMoveTo actionWithDuration:0.5 position:ccp(515,filterMenuPosition.y)];
        CCActionCallBlock *endMoveFilterMenu = [CCActionCallBlock actionWithBlock:^{
            isFilterMenuAnimating = NO;
        }];
        isFilterMenuAnimating = YES;
        CCActionSequence *showDrinksFilter = [CCActionSequence actions:moveFilterMenu, endMoveFilterMenu, nil];
        [[self filterDrinksMenu] runAction:showDrinksFilter];
        [self disableItemButtons];
        showingFilter = YES;
    }
}

-(void)hideFilterDrinkAnimation
{
    if (!isFilterMenuAnimating) {
        CGPoint filterMenuPosition = [filterDrinksMenu position];
        CCActionMoveTo *moveFilterMenu = [CCActionMoveTo actionWithDuration:0.5 position:ccp(147,filterMenuPosition.y)];
        CCActionCallBlock *endMoveFilterMenu = [CCActionCallBlock actionWithBlock:^{
            [[self filterDrinksMenu] setVisible:false];
            isFilterMenuAnimating = NO;
        }];
        isFilterMenuAnimating = YES;
        CCActionSequence *hideDrinksFilter = [CCActionSequence actions:moveFilterMenu, endMoveFilterMenu, nil];
        [[self filterDrinksMenu] runAction:hideDrinksFilter];
        [self enableItemButtons];
        showingFilter = NO;
        [self enableItemButtons];
    }
}

-(void)hideMoreDrinksSign
{
    CGPoint moreDrinksPosition = self.moreDrinks.position;
    CCActionMoveTo *hideMoreDrinks = [CCActionMoveTo actionWithDuration:0.5 position:ccp(moreDrinksPosition.x, 666)];
    [[self moreDrinks] runAction:hideMoreDrinks];
}

-(void)ShowMoreDrinksSign
{
    CGPoint moreDrinksPosition = self.moreDrinks.position;
    CCActionMoveTo *showMoreDrinks = [CCActionMoveTo actionWithDuration:0.5 position:ccp(moreDrinksPosition.x, 550)];
    [[self moreDrinks] runAction:showMoreDrinks];
}

-(void)doNoFilter
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doNoFilter");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = NOFILTER;
        nodeChildrenName = @"itemsToShow";
        [self hideAllShelfs];
        [itemsNode1 setVisible:YES];
        [self updatePriceTags:[self itemsNode1]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doFilterBass
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doFilterBass");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = BASSFILTER;
        [self hideAllShelfs];
        nodeChildrenName = @"BassItems";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doGuitarFilter
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doGuitarFilter");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = GUITARFILTER;
        [self hideAllShelfs];
        nodeChildrenName = @"GuitarItems";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doVocalFilterButton
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doVocalFilterButton");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = VOCALFILTER;
        [self hideAllShelfs];
        nodeChildrenName = @"Vocaltems";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doDrumsFilter
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doDrumsFilter");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = DRUMMEFILTER;
        [self hideAllShelfs];
        nodeChildrenName = @"DrumsItems";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doCoinsFilter
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doCoinsFilter");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = COINSFILTER;
        [self hideAllShelfs];
        nodeChildrenName = @"CoinMultipliers";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doBandFilter
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doBandFilter");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = BANDFILTER;
        [self hideAllShelfs];
        nodeChildrenName = @"TheBandItems";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doCoolDownFilter
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doCoolDownFilter");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = COOLDOWNFILTER;
        [self hideAllShelfs];
        nodeChildrenName = @"CoolDownItems";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doHealingFilter
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doHealingFilter");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = HEALINGFILTER;
        [self hideAllShelfs];
        nodeChildrenName = @"HealingItems";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doShieldFilter
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doShieldFilter");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = SHIELDFILTER;
        shieldItemCounter = 1;
        [self hideAllShelfs];
        nodeChildrenName = @"ShieldItems1";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)doMegaShootFilter
{
    [self playFilterMenuClick];
    if (!isFilterMenuAnimating) {
        NSLog(@"doMegaShootFilter");
        [self hideFilterDrinkAnimation];
        [self ShowMoreDrinksSign];
        self.activeItemFilter = MEGASHOOTFILTER;
        megaShootItemCounter = 1;
        [self hideAllShelfs];
        nodeChildrenName = @"MegaShootItems1";
        [[[self itemsShelf] getChildByName:nodeChildrenName recursively:NO] setVisible:YES];
        [self updatePriceTags:[[self itemsShelf] getChildByName:nodeChildrenName recursively:YES]];
        [[self moreItems] setEnabled:YES];
    }
}

-(void)hideAllShelfs
{
    [itemsNode1 setVisible:NO];
    for (CCNode *node in [itemsShelf children]) {
        [node setVisible:NO];
    }
}

-(int)bandTotalDamage
{
    
    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    
    BandStoreItem *itemDrummer = [[storeCtrl drummers] objectAtIndex:vault.drummerIndex];
    BandStoreItem *itemVocal = [[storeCtrl vocals] objectAtIndex:vault.vocalIndex];
    BandStoreItem *itemBase = [[storeCtrl baseGuitars] objectAtIndex:vault.guitar2Index];
    BandStoreItem *itemLead = [[storeCtrl leadGuitars] objectAtIndex:vault.guitar1Index];
    BandStoreItem *itemBass = [[storeCtrl basses] objectAtIndex:vault.bassIndex];
    
    float totalDamage = itemDrummer.shootPower + itemVocal.shootPower + itemBase.shootPower + itemLead.shootPower + itemBass.shootPower;
    return roundf((totalDamage)/65);
}

-(int)bandTotalArmor
{
    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    
    BandStoreItem *itemDrummer = [[storeCtrl drummers] objectAtIndex:vault.drummerIndex];
    BandStoreItem *itemVocal = [[storeCtrl vocals] objectAtIndex:vault.vocalIndex];
    BandStoreItem *itemBase = [[storeCtrl baseGuitars] objectAtIndex:vault.guitar2Index];
    BandStoreItem *itemLead = [[storeCtrl leadGuitars] objectAtIndex:vault.guitar1Index];
    BandStoreItem *itemBass = [[storeCtrl basses] objectAtIndex:vault.bassIndex];
    
    float totalArmor = itemDrummer.armor + itemVocal.armor + itemBase.armor + itemLead.armor + itemBass.armor;
    return roundf((totalArmor)/65);
}

-(void)resetArmorStars
{
    //MAX 500 armor
    
    CCSprite *blackStar;
    
    int armorPoints = [self bandTotalArmor];
    
    [bandArmorStarsGroup1 removeAllChildren];
    [bandArmorStarsGroup2 removeAllChildren];
    
    
    for (int i = 0; i < armorPoints ; i++) {
        if (i < 5){
            orangeStar = (CCSprite *)[CCBReader load:@"store/OrangeStar"];
            [bandArmorStarsGroup1 addChild:orangeStar];
        }else{
            orangeStar = (CCSprite *)[CCBReader load:@"store/OrangeStar"];
            [bandArmorStarsGroup2 addChild:orangeStar];
        }
    }
    
    int blackCount = 0;
    
    if ([[bandArmorStarsGroup1 children] count] < 5) {
        //complete first 1 with black stars
        blackCount = 5 - armorPoints;
        for (int i = 0; i < blackCount ; i++) {
            blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [bandArmorStarsGroup1 addChild:blackStar];
        }
        for (int i = 0; i < 5 ; i++) {
            blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [bandArmorStarsGroup2 addChild:blackStar];
        }
    }else{
        //First one is complete
        blackCount = 10 - armorPoints;
        for (int i = 0; i < blackCount ; i++) {
            blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [bandArmorStarsGroup2 addChild:blackStar];
        }
    }
    
}

/*
-(void)playAllBandSound
{
    
    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    
    BandStoreItem *itemDrummer = [[storeCtrl drummers] objectAtIndex:vault.drummerIndex];
    BandStoreItem *itemVocal = [[storeCtrl vocals] objectAtIndex:vault.vocalIndex];
    BandStoreItem *itemBase = [[storeCtrl leadGuitars] objectAtIndex:vault.guitar2Index];
    BandStoreItem *itemLead = [[storeCtrl baseGuitars] objectAtIndex:vault.guitar1Index];
    BandStoreItem *itemBass = [[storeCtrl basses] objectAtIndex:vault.bassIndex];
 
    //[[GameSoundManager sharedInstance] playBandInstrumentsEffects:@[itemDrummer.itemName,itemLead.itemName, itemBase.itemName, itemVocal.itemName, itemBass.itemName]];
 
}*/

-(void)resetDamageStars
{
    
    CCSprite *blackStar;
    
    int damagePoints = [self bandTotalDamage];
    
    [bandDamageStarsGroup1 removeAllChildren];
    [bandDamageStarsGroup2 removeAllChildren];
    
    for (int i = 0; i < damagePoints ; i++) {
        if (i < 5){
            redStar = (CCSprite *)[CCBReader load:@"store/RedStar"];
            [bandDamageStarsGroup1 addChild:redStar];
        }else{
            redStar = (CCSprite *)[CCBReader load:@"store/RedStar"];
            [bandDamageStarsGroup2 addChild:redStar];
        }
    }
    
    int blackCount = 0;
    
    if ([[bandDamageStarsGroup1 children] count] < 5) {
        //complete first 1 with black stars
        blackCount = 5 - damagePoints;
        for (int i = 0; i < blackCount ; i++) {
            blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [bandDamageStarsGroup1 addChild:blackStar];
        }
        for (int i = 0; i < 5 ; i++) {
            blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [bandDamageStarsGroup2 addChild:blackStar];
        }
    }else{
        //First one is complete
        blackCount = 10 - damagePoints;
        for (int i = 0; i < blackCount ; i++) {
            blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [bandDamageStarsGroup2 addChild:blackStar];
        }
    }
}

-(void)playMenuTransitionSound
{
    NSLog(@"playMenuTransitionSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx041"];
}

-(void)playBandPosterAnimationSound
{
    NSLog(@"playBandPosterAnimationSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx005"];
}

-(void)playPowerUpBottleClick
{
    NSLog(@"playPowerUpBottleClick");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx043"];
}

-(void)playBandButtonGuyClick
{
    NSLog(@"playBandButtonGuyClick");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx006"];
}

-(void)playFilterPosterShowSound
{
    NSLog(@"playFilterPosterShowSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}

-(void)playFilterMenuClick
{
    NSLog(@"playFilterMenuClick");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx006"];
}

-(void)playItemSumarrySound
{
    NSLog(@"playItemSumarrySound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx030"];
}

-(void)playPunkMovmentSound
{
    NSLog(@"playItemSumarrySound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}

-(void)showCachier
{
    NSLog(@"showCachier");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx073"];
}

-(void)showPunkStore
{
    NSLog(@"showPunkStore");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx071"];
}

@end
