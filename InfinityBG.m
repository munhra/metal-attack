
//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//
//  InfinityBG.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 12/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "InfinityBG.h"


@implementation InfinityBG

@synthesize leftBG;
@synthesize visibleBG;
@synthesize rightBG;
@synthesize upperBG;
@synthesize lowerBG;
@synthesize upperRightBG;
@synthesize lowerLeftBG;
@synthesize fixedBG;

-(void)update:(CCTime)delta
{
    [self moveBgToUpperRight];
}

-(void)moveBgToUpperRight
{
    if ((lowerLeftBG.position.x >= 0.5) && (lowerLeftBG.position.y  >= 0.5)) {
        id bgaux = visibleBG;
        visibleBG = lowerLeftBG;
        lowerLeftBG = bgaux;
        [visibleBG setPosition:ccp(0.5, 0.5)];
        [lowerLeftBG setPosition:ccp(-0.776,-0.65)];
        [leftBG setPosition:ccp(-0.776, 0.5)];
        [lowerBG setPosition:ccp(0.5,-0.65)];
    }
    //0.02002, 0.01801
    [visibleBG setPosition:ccpAdd(visibleBG.position, ccp(0.01001, 0.009005))];
    [lowerLeftBG setPosition:ccpAdd(lowerLeftBG.position, ccp(0.01001, 0.009005))];
    [leftBG setPosition:ccpAdd(leftBG.position, ccp(0.01001, 0.009005))];
    [lowerBG setPosition:ccpAdd(lowerBG.position, ccp(0.01001, 0.009005))];

}

-(void)setBGColor:(CCColor *)color
{
    [leftBG setColor:color];
    [visibleBG setColor:color];
    [rightBG setColor:color];
    [upperBG setColor:color];
    [lowerBG setColor:color];
    [upperRightBG setColor:color];
    [lowerLeftBG setColor:color];
    [fixedBG setColor:color];
}

@end
