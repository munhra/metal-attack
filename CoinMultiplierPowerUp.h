//
//  CoinMultiplierPowerUp.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 02/01/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoinMultiplierPowerUp : CCNode
{
    CCSprite *coinMultSprite1;
    CCSprite *coinMultSprite2;
    CCSprite *coinMultSprite3;
    CCSprite *coinMultSprite4;
}

@property(nonatomic,retain) CCSprite *coinMultSprite1;
@property(nonatomic,retain) CCSprite *coinMultSprite2;
@property(nonatomic,retain) CCSprite *coinMultSprite3;
@property(nonatomic,retain) CCSprite *coinMultSprite4;
@property(nonatomic,weak) id levelDelegate;

//+(id)sharedInstance;
-(void)preparePowerUps:(CGPoint)bandposition;
-(void)activateCoinPowerUp:(int)coinMultiplierId;
-(void)removePowerUps;

@end
