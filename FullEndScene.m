//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  FullEndScene.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 6/25/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "FullEndScene.h"
#import "UniversalInfo.h"
#import "GameSoundManager.h"
#import "GameCenterManager.h"

@implementation FullEndScene

@synthesize drummerEndSprite;
@synthesize drummerEndSpriteEffect;
@synthesize baseEndSprite;
@synthesize bassEndSprite;
@synthesize leadEndSprite;
@synthesize vocalEndSprite;

@synthesize baseThinEndSprite;
@synthesize bassThinEndSprite;
@synthesize leadThinEndSprite;
@synthesize vocalThinEndSprite;
@synthesize drummerThinEndSprite;

@synthesize endStripe1;
@synthesize endStripe2;
@synthesize endStripe3;
@synthesize endStripe4;
@synthesize endStripe5;
@synthesize endAreaNode;
@synthesize endSpritesArray;
@synthesize endThinSpriteArray;
@synthesize endIndex;
@synthesize logo;
@synthesize endInfinityBG;
@synthesize message;
@synthesize updateEnd;
@synthesize currentPixelBlockSize;
@synthesize previousPixelBlockSize;
@synthesize endStripeArray;
@synthesize endMessageSprite;
@synthesize logoWater;
@synthesize punkSprite;
@synthesize stripeBorder;
@synthesize bandGuysItem;
@synthesize bandGuysEndingText;

- (instancetype)init
{
    self = [super init];
    if (self) {
        message = (MessagesSprite *)[CCBReader load:@"Message"];
        endIndex = -1;
        //endIndex = 4; // show stripes
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"dealloc full end scene");
}

-(void)didLoadFromCCB
{
    NSLog(@"FullEndScence did load");
    stripeBorder = (CCSprite *)[CCBReader load:@"BandGuys/end/StripeBorder"];
    endStripeArray = [NSArray arrayWithObjects:endStripe1, endStripe2, endStripe3, endStripe4, endStripe5, nil];
    [endStripe1 setOpacity:0.0];
}

-(void)update:(CCTime)delta
{
    if (updateEnd) {
        [self doPixelate];
    }
}

-(void)prepareEndSpriteStripes
{
    int endStripeIndex = 0;
    
    for (CCSprite *endSprite in endThinSpriteArray) {
        
        CCSprite *stripeFrame = [[CCSprite alloc] initWithSpriteFrame:[[self stripeBorder] spriteFrame]];
        
        
        [endSprite setPosition:ccp(0,0)];
        
        //CCNodeColor *testScissorRect = [CCNodeColor nodeWithColor:[CCColor clearColor] width:300 height:90];
        CCNodeColor *testScissorRect = [CCNodeColor nodeWithColor:[CCColor clearColor] width:295 height:90];
        [testScissorRect setAnchorPoint:ccp(0.5,0.5)];
        [testScissorRect setPosition:ccp(0,0)];
        
        CCClippingNode *scissorTest = [CCClippingNode clippingNodeWithStencil:testScissorRect];
        [scissorTest setColor:[CCColor colorWithUIColor:[UIColor yellowColor]]];
        [scissorTest setContentSize:testScissorRect.contentSize];
        [scissorTest setAnchorPoint:ccp(0.5,0.5)];
        //[scissorTest setPosition:ccp(300,90)];
        [scissorTest setPosition:ccp(295,90)];
        
        [scissorTest setAlphaThreshold:0.0];
        //[scissorTest setOpacity:0.0];
        
        [scissorTest addChild:endSprite];
        //[scissorTest addChild:stripeFrame];
        //[endSprite setScale:0.2];
        [[endStripeArray objectAtIndex:endStripeIndex] addChild:scissorTest];
        [[endStripeArray objectAtIndex:endStripeIndex] addChild:stripeFrame];
        CCSprite *colorSpriteFrame = (CCSprite *)[endStripeArray objectAtIndex:endStripeIndex];
        [colorSpriteFrame setOpacity:0.0];
        [stripeFrame setAnchorPoint:ccp(0,0)];
        [stripeFrame setPosition:CGPointZero];
        endStripeIndex++;
    }
    
}

-(void)preloadBandGuysSound
{
    [[GameSoundManager sharedInstance] preloadBandGuySounds:bandGuysItem];
}

-(void)unloadBandGuysSound
{
    [[GameSoundManager sharedInstance] unloadBandGuySounds:bandGuysItem];
}

-(void)prepareEndEffect
{
    CCEffectPixellate *pixellateEffect;
    for (CCSprite *endNode in endSpritesArray) {
        pixellateEffect = [[CCEffectPixellate alloc] initWithBlockSize:currentPixelBlockSize];
        endNode.effect = pixellateEffect;
        for (CCSprite *childNode in [endNode children]) {
            childNode.effect = pixellateEffect;
        }
    }
}

-(void)updateEndNodes
{
    CCEffectNode *currentNode = [endSpritesArray objectAtIndex:endIndex];
    CCEffectPixellate *currrentPixelate = (CCEffectPixellate *) currentNode.effect;
    [currrentPixelate setBlockSize:currentPixelBlockSize];
    for (CCSprite *childNode in [currentNode children]) {
         CCEffectPixellate *childrenPixelate = (CCEffectPixellate *) childNode.effect;
        [childrenPixelate setBlockSize:currentPixelBlockSize];
    }
}


-(void)doPixelate
{
    CCEffectNode *currentNode = [endSpritesArray objectAtIndex:endIndex];
    
    if (currentPixelBlockSize < 20) {
        currentPixelBlockSize+=0.5;
        [self updateEndNodes];
    }else{
        currentPixelBlockSize = 0;
        currentNode.visible = NO;
        updateEnd = NO;
        [message animateHideWithScale];
        [self showEnding];
    }
}

-(void)onEnter {
    [super onEnter];
    [self prepareEndEffect];
    [[self animationManager] setDelegate:self];
    [self prepareEndSpriteStripes];
    [self preloadBandGuysSound];
    [endInfinityBG setBGColor:[CCColor redColor]];
    [self performAchivment];
}

-(void)performAchivment
{
    if (![[GameCenterManager sharedInstance] checkAchivmentCompleteByKey:@"BrickOctan"]) {
        [[GameCenterManager sharedInstance] achivmentUpdate:@"BrickOctan" withCallBack:^(AchievimentBanner *achievimentBanner) {
            if (achievimentBanner) {
                [self addChild:achievimentBanner];
                [achievimentBanner animateMessageInAndOut];
                [[GameCenterManager sharedInstance] setAchivmentCompleteByKey:@"BrickOctan"];
            }
        }];
    }
}

-(void)onExit {
    [super onExit];
    [[GameSoundManager sharedInstance] stopBandInstrumentsEffects];
    [self unloadBandGuysSound];
    //[[GameSoundManager sharedInstance] stopBandInstrumentsEffects];
}

-(void)doMenu
{
    NSLog(@"doBack");
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
}

-(void)showEnding
{
    if (endIndex >= 0) {
        CCSprite *endToBeRemoved = [endSpritesArray objectAtIndex:endIndex];
        [endToBeRemoved removeFromParent];
    }

    endIndex++;
    
    if (endIndex < 5) {
        CCSprite *endToBeShow = [endSpritesArray objectAtIndex:endIndex];
        
        NSString *bandGuyName = [bandGuysItem objectAtIndex:endIndex];
        NSString *bandGuyEndingText = [bandGuysEndingText objectAtIndex:endIndex];
        
        [[GameSoundManager sharedInstance] stopOneBandGuySound];
        
        if (endIndex == 4) {
            [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:NO];
        }else{
            [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:YES];
        }
    
        [self tintBackGround:endToBeShow];
        [endToBeShow setPosition:ccpCompMult([[UniversalInfo sharedInstance] screenCenter], ccp(1, 0.95))];
        [[endToBeShow animationManager] setDelegate:self];
        
        //NSString *endText = [NSString stringWithFormat:@"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis %d",endIndex];
        [self performEndingAchivment:bandGuyName];
        [self createInfoMessage:bandGuyEndingText];
        [super addChild:endToBeShow];
    }else{
        [[GameSoundManager sharedInstance] stopOneBandGuySound];
        [self showEndFiveStripes];
    }
}

-(void)performEndingAchivment:(NSString *)bandGuyName
{
    NSString *achivmentName;
    
    if ([bandGuyName isEqualToString:@"Scotch"]) {
        achivmentName = @"ScotchFirst";
    }else{
        achivmentName = [NSString stringWithFormat:@"%@Founder",bandGuyName];
    }
    
    if (![[GameCenterManager sharedInstance] checkAchivmentCompleteByKey:achivmentName]) {
        [[GameCenterManager sharedInstance] achivmentUpdate:achivmentName withCallBack:^(AchievimentBanner *achievimentBanner) {
            if (achievimentBanner) {
                [self addChild:achievimentBanner];
                [achievimentBanner animateMessageInAndOut];
                [[GameCenterManager sharedInstance] setAchivmentCompleteByKey:achivmentName];
            }
        }];
    }
}

-(void)showEndFiveStripes
{
    [endInfinityBG setBGColor:[CCColor redColor]];
    [message animateHideWithScale];
    [[[self drummerThinEndSprite] animationManager] runAnimationsForSequenceNamed:@"STL"];
    [[[self baseThinEndSprite] animationManager] runAnimationsForSequenceNamed:@"STL"];
    [[[self vocalThinEndSprite] animationManager] runAnimationsForSequenceNamed:@"STL"];
    [[[self leadThinEndSprite] animationManager] runAnimationsForSequenceNamed:@"STL"];
    [[[self baseThinEndSprite] animationManager] runAnimationsForSequenceNamed:@"STL"];
    [[self animationManager] runAnimationsForSequenceNamed:@"EndScenceStripes"];
    
    // Sound problem here
    [[GameSoundManager sharedInstance] stopBandInstrumentsEffects];
    [[GameSoundManager sharedInstance] playBandInstrumentsEffectsEnding:bandGuysItem];
    //[[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}

-(void)nextEndingAnimation
{
    [[self message] animateHideWithScale];
}

- (void)completedAnimationSequenceNamed:(NSString*)name;
{
    if ([name isEqualToString:@"EndScenceAnimation"]) {
        [self showEnding];
        
    }else if ([name isEqualToString:@"EndScenceStripes"]) {
        self.updateEnd = NO;
    }else{
        self.updateEnd = YES;
    }
}

-(void)createInfoMessage:(NSString *)text
{
    message = (MessagesSprite *)[CCBReader load:@"Message"];
    
    CGPoint messagePosition = ccpCompMult([[UniversalInfo sharedInstance] screenCenter], ccp(0.90, 0.3));
    
    [self.message setPosition:messagePosition];
    [self.message setTextMessage:text];
    [self.message setFontSize:14];
    [self.message setBoxHeight:135];
    [self.message setBoxWidth:310];
    [self.message setVisible:YES];
    [self.message.backgroundNine setOpacity:0.8];
    [self.message adjustTextPositionWithFactor:ccp(1, 1)];
    [self.message adjustArrowHorizontalPosition:0.05];
    

    if ([self.message parent] == nil) {
        [[message arrowSprite] setVisible:false];
    }
    
    [message removeFromParent];
    [super addChild:message];
    [message animateShowWithScale];

}
     
-(void)tintBackGround:(CCSprite *)sprite {
    UIImage *spriteImage = [self renderUIImageFromSprite:sprite];
    UIColor *bgColor = [[UniversalInfo sharedInstance] averageColor:spriteImage];
    CCColor *bgccColor = [CCColor colorWithUIColor:bgColor];
    [[self endInfinityBG] setBGColor:bgccColor];
}

-(void)playStripeSound
{
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}

- (UIImage *)renderUIImageFromSprite:(CCSprite *)sprite {
    
    int tx = sprite.contentSize.width;
    int ty = sprite.contentSize.height;
    
    CCRenderTexture *renderer    = [CCRenderTexture renderTextureWithWidth:tx height:ty];
    
    const CGPoint ANCHORBEFORE = sprite.anchorPoint;
    self.anchorPoint = CGPointZero;
    
    sprite.anchorPoint  = CGPointZero;
    
    [renderer begin];
    [sprite visit];
    [renderer end];
    
    sprite.anchorPoint = ANCHORBEFORE;
    
    return [renderer getUIImage];
}

- (UIColor *)averageColor:(UIImage *)image {
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), image.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    if(rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }
    else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}

@end
