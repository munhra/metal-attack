//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  Credits.m
//  HelloWorldCocos
//
//  Created by Rafael Munhoz on 23/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Credits.h"
#import "GameSoundManager.h"
#import "CCTextureCache.h"
#import "GameCenterManager.h"

@implementation Credits

@synthesize lifeBar;
@synthesize door;
@synthesize leadProgrammerEffects;
@synthesize shirtStamps;
@synthesize artDesignEffectsNode;
@synthesize thanksEffectsNode;
@synthesize currentStampIndex;
@synthesize previousStampIndex;
@synthesize previousPixelBlockSize;
@synthesize currentPixelBlockSize;
@synthesize updateStamp;
@synthesize achivmentShowed;
@synthesize friend1;
@synthesize friend2;
@synthesize friend3;
@synthesize friend4;
@synthesize friend5;
@synthesize friend6;

-(void)didLoadFromCCB
{
    NSLog(@"Credits did load");
}

-(void)prepareStampEffect
{
    CCEffectPixellate *pixellateEffect;
    for (CCEffectNode *stampNode in shirtStamps) {
        pixellateEffect = [[CCEffectPixellate alloc] initWithBlockSize:currentPixelBlockSize];
        stampNode.effect = pixellateEffect;
        stampNode.visible = NO;
    }
}

-(void)update:(CCTime)delta
{
    if (updateStamp) {
        [self doPixelate];
    }
}

-(void)doPixelate
{
    CCEffectNode *currentNode = [shirtStamps objectAtIndex:currentStampIndex];
    CCEffectNode *previousNode = [shirtStamps objectAtIndex:previousStampIndex];
    
    if (currentPixelBlockSize < 20) {
        currentPixelBlockSize+=0.5;
        [self updateStampNodes];
    }else if (previousPixelBlockSize > 1){
        currentNode.visible = NO;
        previousNode.visible = YES;
        previousPixelBlockSize-=0.5;
        [self updateStampNodes];
    }else{
        updateStamp = NO;
        [self updateStampIndex];
    }
}

-(void)updateStampNodes
{
    CCEffectNode *currentNode = [shirtStamps objectAtIndex:currentStampIndex];
    CCEffectNode *previousNode = [shirtStamps objectAtIndex:previousStampIndex];
    CCEffectPixellate *currrentPixelate = (CCEffectPixellate *) currentNode.effect;
    CCEffectPixellate *previousPixelate = (CCEffectPixellate *) previousNode.effect;
    [currrentPixelate setBlockSize:currentPixelBlockSize];
    [previousPixelate setBlockSize:previousPixelBlockSize];
}

-(void)updateStampIndex
{
    
    if (currentStampIndex == 2) {
        currentStampIndex = 0;
        previousStampIndex = 2;
        if (!achivmentShowed) {
            [[GameCenterManager sharedInstance] achivmentUpdate:@"CreditsDone" withCallBack:^(AchievimentBanner *achievimentBanner) {
                if (achievimentBanner) {
                    [self addChild:achievimentBanner];
                    [achievimentBanner animateMessageInAndOut];
                    achivmentShowed = true;
                }
            }];
        }
    }else{
        [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx006"];
        previousStampIndex = currentStampIndex;
        currentStampIndex++;
    }
    
    currentPixelBlockSize = 1;
    previousPixelBlockSize = 20;
}

-(void)updateCreditStamp
{
    [self updateStampIndex];
    updateStamp = YES;
}

-(void)onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    achivmentShowed = false;
    NSLog(@"Credits onEnterTransitionDidFinish");
}

-(void)onEnter
{
    [super onEnter];
    NSLog(@"Credits onEnter");
    [[[self door] animationManager] runAnimationsForSequenceNamed:@"Closed"];
    [[[self door] animationManager] runAnimationsForSequenceNamed:@"OpenDoor"];
    currentPixelBlockSize = 1;
    previousPixelBlockSize = 20;
    updateStamp = NO;
    
    int aleatoryIndex = arc4random() % 3;
    
    switch (aleatoryIndex) {
        case 1:
            shirtStamps = @[thanksEffectsNode, leadProgrammerEffects, artDesignEffectsNode];
            break;
        case 2:
            shirtStamps = @[friend6, friend5, friend4];
            break;
        case 3:
            shirtStamps = @[friend3, friend2, friend1];
            break;
        default:
            break;
    }
    
    [self prepareStampEffect];
    currentStampIndex = 0;
    previousStampIndex = 2;
    [self schedule:@selector(updateCreditStamp) interval:5 repeat:NSIntegerMax delay:1];
    [[GameSoundManager sharedInstance] playCreditsSong];
}

-(void)onExit
{
    [super onExit];
    [self unscheduleAllSelectors];
    [[GameSoundManager sharedInstance] stopBackgroundSong];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [self removeCreditsTextures];
}

-(void)removeCreditsTextures
{
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/creditsArtdesigner.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/CreditsGuy.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/head.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/CreditsBG.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/CreditsButt.png"];
    
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/CreditsTela01.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/CreditsTela02.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/CreditsTela03.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/CreditsTela04.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/CreditsTela05.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/CreditsTela06.png"];

    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/credits/creditsDevelopment.png"];
}
    
-(void)doMenu
{
    NSLog(@"doMenu");
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
}

-(void)dealloc
{
    NSLog(@"dealloc credits");
}

@end
