//
//  StripeEndTestScence.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 7/16/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCScene.h"
#import "EndSprite.h"

@interface StripeEndTestScence : CCScene

@property (nonatomic, retain) EndSprite *endStripe1;
@property (nonatomic, retain) CCSprite *drummerEndSprite;
@property (nonatomic, retain) CCSprite *endStripeTest;
@property (nonatomic, retain) CCNodeColor *endStripeContainer;
@property (nonatomic, retain) CCNodeColor *endStripeContainer2;

@end
