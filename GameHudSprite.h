//
//  GameHudSprite.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 08/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "BandSprite.h"

@interface GameHudSprite : CCSprite {
    
    CCButton *pauseButton;
    CCSprite *drummerPic;
    CCSprite *leadGuitarPic;
    CCSprite *vocalPic;
    CCSprite *baseGuitarPic;
    CCSprite *bassPic;
    
    CCSprite *drummerLife;
    CCSprite *leadGuitarLife;
    CCSprite *vocalLife;
    CCSprite *baseGuitarLife;
    CCSprite *bassLife;
    
    CCProgressNode *drummerProgress;
    CCProgressNode *leadGuitarProgress;
    CCProgressNode *vocalLifeProgress;
    CCProgressNode *baseGuitarLifeProgress;
    CCProgressNode *bassLifeProgress;
    
    CCProgressNode *snake;
    CCLabelTTF *coinsLabel;
    
    float totalPowerConsumption;
    float totalPowerRestoration;
    
    int totalEnemies;
    
}

@property(weak,nonatomic) id delegate;

@property(retain,nonatomic) CCSprite *drummerPic;
@property(retain,nonatomic) CCSprite *leadGuitarPic;
@property(retain,nonatomic) CCSprite *vocalPic;
@property(retain,nonatomic) CCSprite *baseGuitarPic;
@property(retain,nonatomic) CCSprite *bassPic;

@property(retain,nonatomic) CCSprite *drummerLife;
@property(retain,nonatomic) CCSprite *leadGuitarLife;
@property(retain,nonatomic) CCSprite *vocalLife;
@property(retain,nonatomic) CCSprite *baseGuitarLife;
@property(retain,nonatomic) CCSprite *bassLife;

@property(weak,nonatomic) CCSprite *drummerFig;
@property(weak,nonatomic) CCSprite *leadGuitarFig;
@property(weak,nonatomic) CCSprite *vocalFig;
@property(weak,nonatomic) CCSprite *baseGuitarFig;
@property(weak,nonatomic) CCSprite *bassFig;

@property(retain,nonatomic) CCSprite *guitarGaugeSprite;
@property(retain,nonatomic) CCSprite *snakeGaugeSprite;

@property(retain,nonatomic) CCProgressNode *drummerProgress;
@property(retain,nonatomic) CCProgressNode *leadGuitarProgress;
@property(retain,nonatomic) CCProgressNode *vocalLifeProgress;
@property(retain,nonatomic) CCProgressNode *baseGuitarLifeProgress;
@property(retain,nonatomic) CCProgressNode *bassLifeProgress;
@property(retain,nonatomic) CCProgressNode *guitarGaugeProgress;
@property(retain,nonatomic) CCProgressNode *snakeBonesGaugeProgress;

@property(retain,nonatomic) CCLayoutBox *lifeIconsBox;
@property(retain,nonatomic) CCButton *pauseButton;

@property(retain,nonatomic) CCSprite *ice1Powerup;
@property(retain,nonatomic) CCSprite *ice2Powerup;
@property(retain,nonatomic) CCSprite *ice3Powerup;
@property(retain,nonatomic) CCSprite *ice4Powerup;

@property(retain,nonatomic) CCLabelTTF *coinsLabel;
@property(weak,nonatomic) CCSprite *snakeHeadSprite;
@property(nonatomic) int totalEnemies;

@property(nonatomic) float totalPowerConsumption;
@property(nonatomic) float totalPowerRestoration;


-(void)updateCoinLabel:(NSNumber *)coinValue;
-(void)updateBandLife:(BandSprite *)bandSprite;
-(void)raiseGuitarCooldownBar;
-(void)lowerGuitarCooldownBar;
-(void)prepareHudIcons;
-(void)updateBandFaceState:(BandComponents)type isDead:(BOOL)dead isRevival:(BOOL)revival withBandSprite:(BandSprite*)bandSprite;
-(void)updateSnakeBonesProgress:(float)leftEnemies;
-(void)stopSnakeAnimation;


@end
