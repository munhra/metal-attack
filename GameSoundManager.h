//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  GameSoundManager.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 2/28/16.
//  Copyright © 2016 Rust Skull. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameSoundManager : CCNode  <AVAudioPlayerDelegate>

typedef enum {
    
    BASS_CHANNEL = 3,
    DRUMMER_CHANNEL = 0,
    BASE_CHANNEL = 2,
    LEAD_CHANNEL = 1,
    VOCAL_CHANNEL = 4
    
} BandChannels;

@property (nonatomic,retain) NSMutableArray *bandInstrumentsEffectsBuffer;
@property (nonatomic,retain) NSMutableArray *bandInstrumentsSoundSource;
@property (nonatomic,retain) NSMutableDictionary *allBandInstruments;
@property (nonatomic,retain) NSMutableArray *soundEffectsALSource;
@property (nonatomic,retain) NSMutableArray *names;

@property (nonatomic,retain) ALBuffer *oneGuySoundBuffer;
@property (nonatomic,retain) ALSource *oneGuySoundSource;
@property (nonatomic) bool isOneGuySoundPlaying;

@property (nonatomic) bool isVocalDead;
@property (nonatomic) bool isBassDead;
@property (nonatomic) bool isLeadDead;
@property (nonatomic) bool isBaseDead;
@property (nonatomic) bool isDrummerDead;

@property (nonatomic) BandChannels currentGuitarPLaying;
@property (nonatomic) NSString *vocalName;


+(id)sharedInstance;
-(void)playSoundEffectByName:(NSString *)soundName;
-(void)preLoadAllSoundEffects;
-(void)playButtonClick;

-(void)stopBandInstrumentsEffects;
-(void)playBandInstrumentsEffectsEnding:(NSArray *)bandGuyNames;
-(void)playBandInstrumentsEffects:(NSArray *)bandGuyNames;
-(void)preloadBandGuySounds:(NSArray *)bandGuyNames;
-(void)unloadBandGuySounds:(NSArray *)bandGuyNames;

-(void)resetBandGuysState;

-(void)muteVocal;
-(void)muteBass;
-(void)muteBaseGuitar;
-(void)muteLeadGuitar;
-(void)muteDrummer;

-(void)playOneBandGuySound:(NSString *)bandGuyName withLoop:(BOOL)loop;
-(void)stopOneBandGuySound;
    
-(void)playTutorialSong;
-(void)playMainMenuSong;
-(void)stopBackgroundSong;
-(void)playStoreSong;
-(void)playCreditsSong;
-(void)playLevelClearedSong;
-(void)playLevelSelectionSong;
-(void)playItemSelectionSong;
-(void)playGameOverSong;
-(void)playVocalSound;

-(void)unloadAllSounds;

@end
