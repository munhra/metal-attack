//
//  GameCenterManager.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 12/08/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

#import "GameCenterManager.h"

static NSString *const MAIN_LEADER_BOARD_ID = @"metal_leader_board";

@implementation GameCenterManager

@synthesize delegateViewController;
@synthesize achivments;
@synthesize achivmentDescriptions;
@synthesize coinImageSprite;


//Buy all band guys

const NSString *BOUGHT_DRUMMER_1 = @"Bougthdrummer1", *BOUGHT_DRUMMER_2 = @"Bougthdrummer2", *BOUGHT_DRUMMER_3 = @"Bougthdrummer3", *BOUGHT_DRUMMER_4 = @"Bougthdrummer4", *BOUGHT_DRUMMER_5 = @"Bougthdrummer5";

const NSString *BOUGHT_BASS_1 = @"Bougthbasses1", *BOUGHT_BASS_2 = @"Bougthbasses2", *BOUGHT_BASS_3 = @"Bougthbasses3", *BOUGHT_BASS_4 = @"Bougthbasses4", *BOUGHT_BASS_5 = @"Bougthbasses5";

const NSString *BOUGHT_VOCAL_1 = @"Bougthvocal1", *BOUGHT_VOCAL_2 = @"Bougthvocal2", *BOUGHT_VOCAL_3 = @"Bougthvocal3", *BOUGHT_VOCAL_4 = @"Bougthvocal4", *BOUGHT_VOCAL_5 = @"Bougthvocal5";

const NSString *BOUGHT_GUITAR_1 = @"Bougth1guitarrist1", *BOUGHT_GUITAR_2 = @"Bougth1guitarrist2", *BOUGHT_GUITAR_3 = @"Bougth1guitarrist3", *BOUGHT_GUITAR_4 = @"Bougth1guitarrist4", *BOUGHT_GUITAR_5 = @"Bougth1guitarrist5";

const NSString *BOUGHT_LEAD_1 = @"Bougth2guitarrist1", *BOUGHT_LEAD_2 = @"Bougth2guitarrist2", *BOUGHT_LEAD_3 = @"Bougth2guitarrist3", *BOUGHT_LEAD_4 = @"Bougth2guitarrist4", *BOUGHT_LEAD_5 = @"Bougth2guitarrist5";


//Buy all items achivment keys

const NSString *BOUGHT_ITEM_1 = @"Bougthitem1", *BOUGHT_ITEM_2 = @"Bougthitem2", *BOUGHT_ITEM_3 = @"Bougthitem3", *BOUGHT_ITEM_4 = @"Bougthitem4";
const NSString *BOUGHT_ITEM_5 = @"Bougthitem5", *BOUGHT_ITEM_6 = @"Bougthitem6", *BOUGHT_ITEM_7 = @"Bougthitem7", *BOUGHT_ITEM_8 = @"Bougthitem8";
const NSString *BOUGHT_ITEM_9 = @"Bougthitem9", *BOUGHT_ITEM_10 = @"Bougthitem10", *BOUGHT_ITEM_11 = @"Bougthitem11", *BOUGHT_ITEM_12 = @"Bougthitem12";
const NSString *BOUGHT_ITEM_13 = @"Bougthitem13", *BOUGHT_ITEM_14 = @"Bougthitem14", *BOUGHT_ITEM_15 = @"Bougthitem15", *BOUGHT_ITEM_16 = @"Bougthitem16";

const NSString *BOUGHT_ITEM_17 = @"Bougthitem17", *BOUGHT_ITEM_18 = @"Bougthitem18", *BOUGHT_ITEM_19 = @"Bougthitem19", *BOUGHT_ITEM_20 = @"Bougthitem20";
const NSString *BOUGHT_ITEM_21 = @"Bougthitem21", *BOUGHT_ITEM_22 = @"Bougthitem22", *BOUGHT_ITEM_23 = @"Bougthitem23", *BOUGHT_ITEM_24 = @"Bougthitem24";
const NSString *BOUGHT_ITEM_25 = @"Bougthitem25", *BOUGHT_ITEM_26 = @"Bougthitem26", *BOUGHT_ITEM_27 = @"Bougthitem27", *BOUGHT_ITEM_28 = @"Bougthitem28";
const NSString *BOUGHT_ITEM_29 = @"Bougthitem29", *BOUGHT_ITEM_30 = @"Bougthitem30", *BOUGHT_ITEM_31 = @"Bougthitem31", *BOUGHT_ITEM_32 = @"Bougthitem32";

const NSString *BOUGHT_ITEM_33 = @"Bougthitem33", *BOUGHT_ITEM_34 = @"Bougthitem34", *BOUGHT_ITEM_35 = @"Bougthitem35", *BOUGHT_ITEM_36 = @"Bougthitem36";
const NSString *BOUGHT_ITEM_37 = @"Bougthitem37", *BOUGHT_ITEM_38 = @"Bougthitem38", *BOUGHT_ITEM_49 = @"Bougthitem49", *BOUGHT_ITEM_50 = @"Bougthitem50";
const NSString *BOUGHT_ITEM_51 = @"Bougthitem51", *BOUGHT_ITEM_52 = @"Bougthitem52";


//First finish stage with all guys left
const NSString *FIRST_FINISH_ALL_GUYS_LEFT_LEVEL1 = @"FIRST_FINISH_ALL_GUYS_LEFT_LEVEL1";
const NSString *FIRST_FINISH_ALL_GUYS_LEFT_LEVEL2 = @"FIRST_FINISH_ALL_GUYS_LEFT_LEVEL2";
const NSString *FIRST_FINISH_ALL_GUYS_LEFT_LEVEL3 = @"FIRST_FINISH_ALL_GUYS_LEFT_LEVEL3";
const NSString *FIRST_FINISH_ALL_GUYS_LEFT_LEVEL4 = @"FIRST_FINISH_ALL_GUYS_LEFT_LEVEL4";
const NSString *FIRST_FINISH_ALL_GUYS_LEFT_LEVEL5 = @"FIRST_FINISH_ALL_GUYS_LEFT_LEVEL5";

//First finish stage achivments
const NSString *FIRST_FINISH_LEVEL1 = @"FIRST_FINISH_LEVEL1";
const NSString *FIRST_FINISH_LEVEL2 = @"FIRST_FINISH_LEVEL2";
const NSString *FIRST_FINISH_LEVEL3 = @"FIRST_FINISH_LEVEL3";
const NSString *FIRST_FINISH_LEVEL4 = @"FIRST_FINISH_LEVEL4";
const NSString *FIRST_FINISH_LEVEL5 = @"FIRST_FINISH_LEVEL5";

//First buy bandguy component
const NSString *FIRST_BUY_VOCAL = @"FIRST_BUY_VOCAL";
const NSString *FIRST_BUY_BASEGUITAR = @"FIRST_BUY_BASEGUITAR";
const NSString *FIRST_BUY_LEADGUITAR = @"FIRST_BUY_LEADGUITAR";
const NSString *FIRST_BUY_LEVEL4 = @"FIRST_BUY_BASS";
const NSString *FIRST_BUY_LEVEL5 = @"FIRST_BUY_DRUMMER";

+(GameCenterManager *)sharedInstance
{
    static GameCenterManager *myInstance = nil;
    if (nil == myInstance) {
        myInstance  = [[[self class] alloc] init];
    }
    return myInstance;
}

- (id)init {
    
    NSLog(@"init game center handler");
    if ((self = [super init])) {
        self.achivments = [[NSMutableArray alloc] init];
        self.gameCenterAvailable = [self isGameCenterAvailable];
        if (self.gameCenterAvailable) {
            NSNotificationCenter *nc =
            [NSNotificationCenter defaultCenter];
            [nc addObserver:self
                   selector:@selector(authenticationChanged)
                       name:GKPlayerAuthenticationDidChangeNotificationName
                     object:nil];
        }
    }
    return self;
}

- (BOOL)isGameCenterAvailable {
    
    Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    NSString *reqSysVer = @"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer
                                           options:NSNumericSearch] != NSOrderedAscending);
    return (gcClass && osVersionSupported);
}

-(void)authenticateLocalUser;
{
    if (!self.gameCenterAvailable) return;
    NSLog(@"Authenticating local user...");
    if ([GKLocalPlayer localPlayer].authenticated == NO) {
        [[GKLocalPlayer localPlayer] authenticateWithCompletionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Error description %@",[error description]);
            }
            [self authenticationChanged];
        }];
        
    } else {
        NSLog(@"Already authenticated!");
    }
}

- (void)authenticationChanged {
    
    NSLog(@"Authentication changed !");
    if ([GKLocalPlayer localPlayer].isAuthenticated && !self.userAuthenticated) {
        NSLog(@"Authentication changed: player authenticated.");
        self.userAuthenticated = TRUE;
        //[self resetAchivments];
        [self loadAchivments];
    } else if (![GKLocalPlayer localPlayer].isAuthenticated && self.userAuthenticated) {
        NSLog(@"Authentication changed: player not authenticated");
        self.userAuthenticated = FALSE;
    }
}

-(void)showLeaderBoard:(id)delegate;
{
    GKGameCenterViewController *leaderboardViewController = [[GKGameCenterViewController alloc] init];
    leaderboardViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
    leaderboardViewController.leaderboardIdentifier = MAIN_LEADER_BOARD_ID;
    leaderboardViewController.gameCenterDelegate = self;
    [delegate presentModalViewController:leaderboardViewController animated:YES];
    self.delegateViewController = delegate;
}

-(void)updateLeaderBoard:(NSInteger)newscore
{
    if ([GKLocalPlayer localPlayer].isAuthenticated) {
        //NSLog(@"update score !");
        GKScore* score = [[GKScore alloc] initWithLeaderboardIdentifier:MAIN_LEADER_BOARD_ID];
        score.value = newscore;
        [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"error: %@", error);
            }else{
                NSLog(@"Score succesfully update!");
            }
        }];
    }
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    NSLog(@"did finish load");
    [self.delegateViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)resetAchivments
{
#warning remove this in release
    [GKAchievement resetAchievementsWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error %@",error.description);
        }
        NSLog(@"Achivments reseted");
    }];
}

-(void)loadAchivments
{
    [GKAchievement loadAchievementsWithCompletionHandler:^(NSArray<GKAchievement *> * _Nullable gameCenterAchivments,
                    NSError * _Nullable error) {
        if (error) {
            NSLog(@"loadachivments error %@",error.description);
        }else{
            NSLog(@"Achivments loaded count %lu",[gameCenterAchivments count]);
            //the array gameCenterAchivments will only have achivments that were previous partialy or completed
            [self setAchivments:gameCenterAchivments];
        }
    }];
}

-(void)downloadAchivmentsDescriptions
{
    [GKAchievementDescription loadAchievementDescriptionsWithCompletionHandler:^(NSArray<GKAchievementDescription *> * _Nullable descriptions, NSError * _Nullable error) {
        if (error) {
            return;
        }
        achivmentDescriptions = descriptions;
        for (GKAchievementDescription *achievementDescription in descriptions) {
            NSLog(@"Achievment description %@",[achievementDescription description]);
        }
    }];
}

-(void)achivmentUpdate:(NSString *)achivmentID withCallBack:(void (^)(AchievimentBanner *))callbackBlock
{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier = %@",achivmentID];
    GKAchievement *checkAchivment = [[achivments filteredArrayUsingPredicate:predicate] firstObject];
    
    if (!(checkAchivment == nil) || [checkAchivment isCompleted]) {
        callbackBlock(nil);
        return;
    }
    
    GKAchievement *achivment = [[GKAchievement alloc] initWithIdentifier:achivmentID];
    achivment.percentComplete = 100;
    
    [GKAchievement reportAchievements:@[achivment] withCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error %@s",error.description);
        }
        GKAchievementDescription *achivmentDescription = [[achivmentDescriptions filteredArrayUsingPredicate:predicate] firstObject];
        NSLog(@"Achivment Description %@",[achivmentDescription achievedDescription]);
        
        if ([achivmentDescription achievedDescription] != nil) {
            AchievimentBanner*  achievimentBanner = (AchievimentBanner *)[CCBReader load:@"AchivmentBanner"];
            NSString *achivementImageFile = [NSString stringWithFormat:@"Achivments/%@",achivmentID];
            
            coinImageSprite = (CCSprite *)[CCBReader load:achivementImageFile];
            [achievimentBanner.achievimentCoin setSpriteFrame:[coinImageSprite spriteFrame]];
            
            [[achievimentBanner messageText] setString:[achivmentDescription achievedDescription]];
            
            [achievimentBanner setBannerPosition];
            callbackBlock(achievimentBanner);

        }else{
            callbackBlock(nil);
        }
    
    }];
}

-(BOOL)isAllItemsBoughtAchivment
{
    for ( int i = 1 ; i < 53 ; i ++) {
        if (!(i > 38 && i < 49)) {
            NSString *itemKey = [NSString stringWithFormat:@"Bougthitem%d",i];
            bool itemBought = [self checkAchivmentCompleteByKey:itemKey];
            if (!(itemBought)) {
                return NO;
            }
        }
    }
    return YES;
}

-(BOOL)isAllBandGuysBought
{
    for ( int i = 2 ; i < 6 ; i ++) {
        
        NSString *drummerKey = [NSString stringWithFormat:@"Bougthdrummer%d",i];
        NSString *bassKey = [NSString stringWithFormat:@"Bougthbasses%d",i];
        NSString *vocalKey = [NSString stringWithFormat:@"Bougthvocal%d",i];
        NSString *leadKey = [NSString stringWithFormat:@"Bougth1guitarrist%d",i];
        NSString *baseKey = [NSString stringWithFormat:@"Bougth2guitarrist%d",i];
        
        bool drummerKeyBought = [self checkAchivmentCompleteByKey:drummerKey];
        bool bassKeyBought = [self checkAchivmentCompleteByKey:bassKey];
        bool vocalKeyBought = [self checkAchivmentCompleteByKey:vocalKey];
        bool leadKeyBought = [self checkAchivmentCompleteByKey:leadKey];
        bool baseKeyBought = [self checkAchivmentCompleteByKey:baseKey];
        
        if (!(drummerKeyBought)) {
            return NO;
        }
        if (!(bassKeyBought)) {
            return NO;
        }
        if (!(vocalKeyBought)) {
            return NO;
        }
        if (!(leadKeyBought)) {
            return NO;
        }
        if (!(baseKeyBought)) {
            return NO;
        }
    
    }
    return YES;
}


-(void)setAchivmentCompleteByKey:(NSString *)achivmentKey
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    [userdef setBool:YES forKey:achivmentKey];
}

-(BOOL)checkAchivmentCompleteByKey:(NSString *)achivmentKey
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    return [userdef boolForKey:achivmentKey];
}

@end
