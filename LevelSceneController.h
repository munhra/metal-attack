//
//  LevelSceneController.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 04/11/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSON.h"
#import "RobotBlaster.h"
#import "Level.h"
#import "LevelScene.h"


@interface LevelSceneController : CCNode
{
    id delegate;
    NSMutableArray *loadedLevels;
    NSDictionary *enemyDict;
    NSMutableDictionary *enemyDictMutable;
    NSMutableArray *enemyArray;
    LevelScene *scnDelegate;
    int totalLevels;
    NSMutableArray *levelsPerLevelContext;
}

@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) NSMutableArray *loadedLevels;
@property (nonatomic, retain) NSDictionary *enemyDict;
@property (nonatomic) int totalLevels;
@property (nonatomic, retain) NSMutableDictionary *enemyDictMutable;
@property (nonatomic, retain) NSMutableArray *enemyArray;
@property (nonatomic, retain) LevelScene *scnDelegate;
@property (nonatomic, retain) NSMutableArray *levelsPerLevelContext;
@property (nonatomic, retain) NSMutableArray *firstLevelPerContext;

+(id)sharedInstance;
-(void)loadLevelJson;
-(void)saveLastReachedLevel:(int)levelNumber;
-(int)loadLastReachedLevel;
-(CCScene *)loadLevelScene:(int)levelNumber waveNumber:(int)wave;
-(Level *)loadLevelWave:(int)levelNumber waveNumber:(int)wave delegate:(id)scnDelegate;
-(void)startEnemyMovment;
-(void)removeEnemiesSpriteSheet:(int)levelContext;

@end
