//
//  HeroShoot.h
//  EightbitShooter
//
//  Created by Rafael Munhoz on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface HeroShoot : CCSprite
{
    BOOL removeObject;
}

@property (nonatomic) BOOL removeObject;

@end
