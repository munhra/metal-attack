//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//
//
//  BandStoreItemsCtrl.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 03/04/13.
//
//

#import "BandStoreItemsCtrl.h"
#import "BandVaultItem.h" // remove it just for test
#import "BandVault.h"
#import "GameSoundManager.h"

@implementation BandStoreItemsCtrl

@synthesize itemsDict;
@synthesize drummers;
@synthesize vocals;
@synthesize basses;
@synthesize coinpacks;
@synthesize coinpacksByid;
@synthesize generalItems;
@synthesize bandCoins;
@synthesize baseGuitars;
@synthesize leadGuitars;
@synthesize genericItemsId;

NSDictionary *bandItemsDict;
NSSortDescriptor *sort;
NSSortDescriptor *sortGenericId;

+(id)sharedInstance
{
    static id master = nil;
	@synchronized(self)
	{
		if (master == nil){
            master = [self new];
        }
	}
    return master;
}

-(void)loadOwnedItems
{
    // set here the selected band guys
    
    BandVault *bandVault = [BandVault sharedInstance];
    BandVaultItem *vaultItem;
    
    for (int i = 0; i < [[self drummers] count]; i++) {
        vaultItem = [bandVault getBandVaultByItemId:[[[self drummers] objectAtIndex:i] appStoreId]];
        [[drummers objectAtIndex:i] setOwnedBandItem:vaultItem.ownedVaultItem];
        [[drummers objectAtIndex:i] setSelectedBandItem:vaultItem.selectedVaultItem];
        if (vaultItem.selectedVaultItem) {
            [bandVault setDrummerIndex:i];
        }
    }
    
    for (int i = 0; i < [[self vocals] count]; i++) {
        vaultItem = [bandVault getBandVaultByItemId:[[[self vocals] objectAtIndex:i] appStoreId]];
        [[vocals objectAtIndex:i] setOwnedBandItem:vaultItem.ownedVaultItem];
        [[vocals objectAtIndex:i] setSelectedBandItem:vaultItem.selectedVaultItem];
        if (vaultItem.selectedVaultItem) {
            [bandVault setVocalIndex:i];
        }
    }
    
    for (int i = 0; i < [[self leadGuitars] count]; i++) {
        vaultItem = [bandVault getBandVaultByItemId:[[[self leadGuitars] objectAtIndex:i] appStoreId]];
        [[leadGuitars objectAtIndex:i] setOwnedBandItem:vaultItem.ownedVaultItem];
        [[leadGuitars objectAtIndex:i] setSelectedBandItem:vaultItem.selectedVaultItem];
        if (vaultItem.selectedVaultItem) {
            [bandVault setGuitar1Index:i];
        }
    }
    
    for (int i = 0; i < [[self baseGuitars] count]; i++) {
        vaultItem = [bandVault getBandVaultByItemId:[[[self baseGuitars] objectAtIndex:i] appStoreId]];
        [[baseGuitars objectAtIndex:i] setOwnedBandItem:vaultItem.ownedVaultItem];
        [[baseGuitars objectAtIndex:i] setSelectedBandItem:vaultItem.selectedVaultItem];
        if (vaultItem.selectedVaultItem) {
            [bandVault setGuitar2Index:i];
        }
    }
    
    for (int i = 0; i < [[self basses] count]; i++) {
        vaultItem = [bandVault getBandVaultByItemId:[[[self basses] objectAtIndex:i] appStoreId]];
        [[basses objectAtIndex:i] setOwnedBandItem:vaultItem.ownedVaultItem];
        [[basses objectAtIndex:i] setSelectedBandItem:vaultItem.selectedVaultItem];
        if (vaultItem.selectedVaultItem) {
            [bandVault setBassIndex:i];
        }
    }

}


-(void)savePurchaseditem:(NSString *)itemid
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    [userdef setValue:@"YES" forKey:itemid];
}

-(void)populateDrummerData
{
    self.drummers = [[NSMutableArray alloc] init];
    NSDictionary *drummersDict = [bandItemsDict valueForKey:@"DRUMMERS"];
    NSString *drummer;
    BandStoreItem *item;
    
    for (drummer in drummersDict){
        
        NSDictionary *drummerInfo = [drummersDict valueForKey:drummer];
        // drummer info is null create it from JSON and store
        item = [[BandStoreItem alloc] init];
        item.appStoreId = [drummerInfo valueForKey:@"appStoreId"];
        item.itemName = [drummerInfo valueForKey:@"name"];
        item.description = [drummerInfo valueForKey:@"description"];
        item.endingText = [drummerInfo valueForKey:@"endingText"];
        item.credits = [[drummerInfo valueForKey:@"credits"] intValue];
        item.value = [[drummerInfo valueForKey:@"value"] intValue];
        item.duration = [[drummerInfo valueForKey:@"duration"] intValue];
        item.armor = [[drummerInfo valueForKey:@"armor"] intValue];
        item.shootPower = [[drummerInfo valueForKey:@"shootPower"] floatValue];
        item.ownedBandItem = [[drummerInfo valueForKey:@"owner"] boolValue];
        item.level = [[drummerInfo valueForKey:@"level"] intValue];
        item.itemType = DRUMMER;
        NSString *spriteFile = [NSString stringWithFormat:@"BandGuys/%@",item.itemName];
        item.bandGuySprite = (BandGuySprite *)[CCBReader load:spriteFile];
        NSString *shootFile = [NSString stringWithFormat:@"BandGuys/shoot/Level%d/%@Shoot",item.level,item.itemName];
        item.compShoot = (CCSprite *)[CCBReader load:shootFile];
        NSString *posterFile = [NSString stringWithFormat:@"BandGuys/poster/Level%d/%@Poster",item.level,item.itemName];
        item.posterFileName = posterFile;
        //item.compPoster = (CCSprite *)[CCBReader load:posterFile];
        NSString *lifeFile = [NSString stringWithFormat:@"BandGuys/hudface/Level%d/%@Life",item.level,item.itemName];
        item.compLifeImage = (CCSprite *)[CCBReader load:lifeFile];
        NSString *bannerFile = [NSString stringWithFormat:@"BandGuys/banner/Level%d/%@Banner",item.level,item.itemName];
        item.compBannerImage = (CCSprite *)[CCBReader load:bannerFile];
        NSString *storePosterFile = [NSString stringWithFormat:@"BandGuys/storePoster/Level%d/%@StrPoster",item.level,item.itemName];
        //item.compStorePosterImage = (CCSprite *)[CCBReader load:storePosterFile];
        item.storePosterFileName = storePosterFile;
        NSString *bandGuyEndSpriteFile = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",item.level,item.itemName];
        //item.bandGuyEndSprite = (CCSprite *)[CCBReader load:bandGuyEndSpriteFile];
        item.bandGuyEndFileName = bandGuyEndSpriteFile;
        item.powerConsumption = [[drummerInfo valueForKey:@"powerConsumption"] floatValue];
        item.powerRestoration = [[drummerInfo valueForKey:@"powerRestoration"] floatValue];
        //[[GameSoundManager sharedInstance] preloadBandGuySound:item.itemName];
        [drummers addObject:item];
    }
    
    self.drummers = [[NSMutableArray alloc] initWithArray:[drummers sortedArrayUsingDescriptors:@[sort]]];
    
    BandVault *bandVault = [BandVault sharedInstance];
    BandVaultItem *vaultItem = [bandVault getBandVaultByItemId:[[drummers firstObject] appStoreId]];
    
    //default values
    if (!vaultItem) {
        [bandVault storeBandGuy:[[drummers firstObject] appStoreId] indexPosition:0];
        [bandVault selectBandGuy:[[drummers firstObject] appStoreId] bandGuyType:DRUMMER oldSelectedGuy:nil];
    }
    
    NSLog(@"Drummer data size %lu",(unsigned long)[drummers count]);
}

-(void)populateVocalData
{
    self.vocals = [[NSMutableArray alloc] init];
    NSDictionary *vocalsDict = [bandItemsDict valueForKey:@"VOCALS"];
    NSString *vocal;
    for (vocal in vocalsDict){
        BandStoreItem *item = [[BandStoreItem alloc] init];
        NSDictionary *vocalInfo = [vocalsDict valueForKey:vocal];
        item.appStoreId = [vocalInfo valueForKey:@"appStoreId"];
        item.itemName = [vocalInfo valueForKey:@"name"];
        item.description = [vocalInfo valueForKey:@"description"];
        item.endingText = [vocalInfo valueForKey:@"endingText"];
        item.credits = [[vocalInfo valueForKey:@"credits"] intValue];
        item.value = [[vocalInfo valueForKey:@"value"] intValue];
        item.duration = [[vocalInfo valueForKey:@"duration"] intValue];
        item.armor = [[vocalInfo valueForKey:@"armor"] floatValue];
        item.shootPower = [[vocalInfo valueForKey:@"shootPower"] floatValue];
        item.ownedBandItem = [[vocalInfo valueForKey:@"owner"] boolValue];
        item.level = [[vocalInfo valueForKey:@"level"] intValue];
        item.itemType = VOCALIST;
        NSString *spriteFile = [NSString stringWithFormat:@"BandGuys/%@",item.itemName];
        item.bandGuySprite = (BandGuySprite *)[CCBReader load:spriteFile];
        NSString *shootFile = [NSString stringWithFormat:@"BandGuys/shoot/Level%d/%@Shoot",item.level,item.itemName];
        item.compShoot = (CCSprite *)[CCBReader load:shootFile];
        NSString *posterFile = [NSString stringWithFormat:@"BandGuys/poster/Level%d/%@Poster",item.level,item.itemName];
        item.posterFileName = posterFile;
        //item.compPoster = (CCSprite *)[CCBReader load:posterFile];
        NSString *lifeFile = [NSString stringWithFormat:@"BandGuys/hudface/Level%d/%@Life",item.level,item.itemName];
        item.compLifeImage = (CCSprite *)[CCBReader load:lifeFile];
        NSString *bannerFile = [NSString stringWithFormat:@"BandGuys/banner/Level%d/%@Banner",item.level,item.itemName];
        item.compBannerImage = (CCSprite *)[CCBReader load:bannerFile];
        NSString *storePosterFile = [NSString stringWithFormat:@"BandGuys/storePoster/Level%d/%@StrPoster",item.level,item.itemName];
        item.storePosterFileName = storePosterFile;
        //item.compStorePosterImage = (CCSprite *)[CCBReader load:storePosterFile];
        item.storePosterFileName = storePosterFile;
        NSString *bandGuyEndSpriteFile = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",item.level,item.itemName];
        //item.bandGuyEndSprite = (CCSprite *)[CCBReader load:bandGuyEndSpriteFile];
        item.bandGuyEndFileName = bandGuyEndSpriteFile;
        item.powerConsumption = [[vocalInfo valueForKey:@"powerConsumption"] floatValue];
        item.powerRestoration = [[vocalInfo valueForKey:@"powerRestoration"] floatValue];
        //[[GameSoundManager sharedInstance] preloadBandGuySound:item.itemName];
        [vocals addObject:item];
    }
    self.vocals = [[NSMutableArray alloc] initWithArray:[vocals sortedArrayUsingDescriptors:@[sort]]];
    
    BandVault *bandVault = [BandVault sharedInstance];
    BandVaultItem *vaultItem = [bandVault getBandVaultByItemId:[[vocals firstObject] appStoreId]];
    
    //default values
    if (!vaultItem) {
        [bandVault storeBandGuy:[[vocals firstObject] appStoreId] indexPosition:0];
        [bandVault selectBandGuy:[[vocals firstObject] appStoreId] bandGuyType:VOCALIST oldSelectedGuy:nil];
    }

}

-(void)populateLeadGuitarData
{
    self.leadGuitars = [[NSMutableArray alloc] init];
    
    NSDictionary *guitarristsDict = [bandItemsDict valueForKey:@"LEADGUITAR"];
    NSString *guitarrist;
    for (guitarrist in guitarristsDict){
        BandStoreItem *item = [[BandStoreItem alloc] init];
        NSDictionary *guitarristInfo = [guitarristsDict valueForKey:guitarrist];
        item.appStoreId = [guitarristInfo valueForKey:@"appStoreId"];
        item.itemName = [guitarristInfo valueForKey:@"name"];
        item.description = [guitarristInfo valueForKey:@"description"];
        item.endingText = [guitarristInfo valueForKey:@"endingText"];
        item.credits = [[guitarristInfo valueForKey:@"credits"] intValue];
        item.value = [[guitarristInfo valueForKey:@"value"] intValue];
        item.duration = [[guitarristInfo valueForKey:@"duration"] intValue];
        item.armor = [[guitarristInfo valueForKey:@"armor"] floatValue];
        item.shootPower = [[guitarristInfo valueForKey:@"shootPower"] floatValue];
        item.ownedBandItem = [[guitarristInfo valueForKey:@"owner"] boolValue];
        item.level = [[guitarristInfo valueForKey:@"level"] intValue];
        item.itemType = GUITARRIST_1;
        NSString *spriteFile = [NSString stringWithFormat:@"BandGuys/%@",item.itemName];
        item.bandGuySprite = (BandGuySprite *)[CCBReader load:spriteFile];
        NSString *shootFile = [NSString stringWithFormat:@"BandGuys/shoot/Level%d/%@Shoot",item.level,item.itemName];
        item.compShoot = (CCSprite *)[CCBReader load:shootFile];
        NSString *posterFile = [NSString stringWithFormat:@"BandGuys/poster/Level%d/%@Poster",item.level,item.itemName];
        item.posterFileName = posterFile;
        //item.compPoster = (CCSprite *)[CCBReader load:posterFile];
        NSString *lifeFile = [NSString stringWithFormat:@"BandGuys/hudface/Level%d/%@Life",item.level,item.itemName];
        item.compLifeImage = (CCSprite *)[CCBReader load:lifeFile];
        NSString *bannerFile = [NSString stringWithFormat:@"BandGuys/banner/Level%d/%@Banner",item.level,item.itemName];
        item.compBannerImage = (CCSprite *)[CCBReader load:bannerFile];
        NSString *storePosterFile = [NSString stringWithFormat:@"BandGuys/storePoster/Level%d/%@StrPoster",item.level,item.itemName];
        item.storePosterFileName = storePosterFile;
        //item.compStorePosterImage = (CCSprite *)[CCBReader load:storePosterFile];
        NSString *bandGuyEndSpriteFile = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",item.level,item.itemName];
        //item.bandGuyEndSprite = (CCSprite *)[CCBReader load:bandGuyEndSpriteFile];
        item.bandGuyEndFileName = bandGuyEndSpriteFile;
        item.powerConsumption = [[guitarristInfo valueForKey:@"powerConsumption"] floatValue];
        item.powerRestoration = [[guitarristInfo valueForKey:@"powerRestoration"] floatValue];
        //[[GameSoundManager sharedInstance] preloadBandGuySound:item.itemName];
        [leadGuitars addObject:item];
    }
    
    self.leadGuitars = [[NSMutableArray alloc] initWithArray:[leadGuitars sortedArrayUsingDescriptors:@[sort]]];
    
    BandVault *bandVault = [BandVault sharedInstance];
    BandVaultItem *vaultItem = [bandVault getBandVaultByItemId:[[leadGuitars firstObject] appStoreId]];
    
    //default values
    if (!vaultItem) {
        [bandVault storeBandGuy:[[leadGuitars firstObject] appStoreId] indexPosition:0];
        [bandVault selectBandGuy:[[leadGuitars firstObject] appStoreId] bandGuyType:GUITARRIST_1 oldSelectedGuy:nil];
    }
    
}

-(void)populateBaseGuitarData
{
    
    self.baseGuitars = [[NSMutableArray alloc] init];
    
    NSDictionary *guitarristsDict = [bandItemsDict valueForKey:@"BASEGUITAR"];
    NSString *guitarrist;
    for (guitarrist in guitarristsDict){
        BandStoreItem *item = [[BandStoreItem alloc] init];
        NSDictionary *guitarristInfo = [guitarristsDict valueForKey:guitarrist];
        item.appStoreId = [guitarristInfo valueForKey:@"appStoreId"];
        item.itemName = [guitarristInfo valueForKey:@"name"];
        item.description = [guitarristInfo valueForKey:@"description"];
        item.endingText = [guitarristInfo valueForKey:@"endingText"];
        item.credits = [[guitarristInfo valueForKey:@"credits"] intValue];
        item.value = [[guitarristInfo valueForKey:@"value"] intValue];
        item.duration = [[guitarristInfo valueForKey:@"duration"] intValue];
        item.armor = [[guitarristInfo valueForKey:@"armor"] floatValue];
        item.shootPower = [[guitarristInfo valueForKey:@"shootPower"] floatValue];
        item.ownedBandItem = [[guitarristInfo valueForKey:@"owner"] boolValue];
        item.level = [[guitarristInfo valueForKey:@"level"] intValue];
        item.itemType = GUITARRIST_2;
        NSString *spriteFile = [NSString stringWithFormat:@"BandGuys/%@",item.itemName];
        item.bandGuySprite = (BandGuySprite *)[CCBReader load:spriteFile];
        NSString *shootFile = [NSString stringWithFormat:@"BandGuys/shoot/Level%d/%@Shoot",item.level,item.itemName];
        item.compShoot = (CCSprite *)[CCBReader load:shootFile];
        NSString *posterFile = [NSString stringWithFormat:@"BandGuys/poster/Level%d/%@Poster",item.level,item.itemName];
        item.posterFileName = posterFile;
        //item.compPoster = (CCSprite *)[CCBReader load:posterFile];
        NSString *lifeFile = [NSString stringWithFormat:@"BandGuys/hudface/Level%d/%@Life",item.level,item.itemName];
        item.compLifeImage = (CCSprite *)[CCBReader load:lifeFile];
        NSString *bannerFile = [NSString stringWithFormat:@"BandGuys/banner/Level%d/%@Banner",item.level,item.itemName];
        item.compBannerImage = (CCSprite *)[CCBReader load:bannerFile];
        NSString *storePosterFile = [NSString stringWithFormat:@"BandGuys/storePoster/Level%d/%@StrPoster",item.level,item.itemName];
        item.storePosterFileName = storePosterFile;
        //item.compStorePosterImage = (CCSprite *)[CCBReader load:storePosterFile];
        NSString *bandGuyEndSpriteFile = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",item.level,item.itemName];
        //item.bandGuyEndSprite = (CCSprite *)[CCBReader load:bandGuyEndSpriteFile];
        item.bandGuyEndFileName = bandGuyEndSpriteFile;
        item.powerConsumption = [[guitarristInfo valueForKey:@"powerConsumption"] floatValue];
        item.powerRestoration = [[guitarristInfo valueForKey:@"powerRestoration"] floatValue];
        //[[GameSoundManager sharedInstance] preloadBandGuySound:item.itemName];
        [baseGuitars addObject:item];
    }
    
    self.baseGuitars = [[NSMutableArray alloc] initWithArray:[baseGuitars sortedArrayUsingDescriptors:@[sort]]];
    
    BandVault *bandVault = [BandVault sharedInstance];
    BandVaultItem *vaultItem = [bandVault getBandVaultByItemId:[[baseGuitars firstObject] appStoreId]];
    
    //default values
    if (!vaultItem) {
        [bandVault storeBandGuy:[[baseGuitars firstObject] appStoreId] indexPosition:0];
        [bandVault selectBandGuy:[[baseGuitars firstObject] appStoreId] bandGuyType:GUITARRIST_2 oldSelectedGuy:nil];
    }
    
}

-(void)populateBassData
{
    self.basses = [[NSMutableArray alloc] init];
    NSDictionary *bassesDict = [bandItemsDict valueForKey:@"BASSES"];
    NSString *bass;
    for (bass in bassesDict){
        BandStoreItem *item = [[BandStoreItem alloc] init];
        NSDictionary *bassistInfo = [bassesDict valueForKey:bass];
        item.appStoreId = [bassistInfo valueForKey:@"appStoreId"];
        item.itemName = [bassistInfo valueForKey:@"name"];
        item.description = [bassistInfo valueForKey:@"description"];
        item.endingText = [bassistInfo valueForKey:@"endingText"];
        item.credits = [[bassistInfo valueForKey:@"credits"] intValue];
        item.value = [[bassistInfo valueForKey:@"value"] intValue];
        item.duration = [[bassistInfo valueForKey:@"duration"] intValue];
        item.armor = [[bassistInfo valueForKey:@"armor"] floatValue];
        item.shootPower = [[bassistInfo valueForKey:@"shootPower"] floatValue];
        item.ownedBandItem = [[bassistInfo valueForKey:@"owner"] boolValue];
        item.level = [[bassistInfo valueForKey:@"level"] intValue];
        item.itemType = BASSGUY;
        NSString *spriteFile = [NSString stringWithFormat:@"BandGuys/%@",item.itemName];
        item.bandGuySprite = (BandGuySprite *)[CCBReader load:spriteFile];
        NSString *shootFile = [NSString stringWithFormat:@"BandGuys/shoot/Level%d/%@Shoot",item.level,item.itemName];
        item.compShoot = (CCSprite *)[CCBReader load:shootFile];
        NSString *posterFile = [NSString stringWithFormat:@"BandGuys/poster/Level%d/%@Poster",item.level,item.itemName];
        item.posterFileName = posterFile;
        //item.compPoster = (CCSprite *)[CCBReader load:posterFile];
        NSString *lifeFile = [NSString stringWithFormat:@"BandGuys/hudface/Level%d/%@Life",item.level,item.itemName];
        item.compLifeImage  = (CCSprite *)[CCBReader load:lifeFile];
        NSString *bannerFile = [NSString stringWithFormat:@"BandGuys/banner/Level%d/%@Banner",item.level,item.itemName];
        item.compBannerImage = (CCSprite *)[CCBReader load:bannerFile];
        NSString *storePosterFile = [NSString stringWithFormat:@"BandGuys/storePoster/Level%d/%@StrPoster",item.level,item.itemName];
        //item.compStorePosterImage = (CCSprite *)[CCBReader load:storePosterFile];
        item.storePosterFileName = storePosterFile;
        NSString *bandGuyEndSpriteFile = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",item.level,item.itemName];
        //item.bandGuyEndSprite = (CCSprite *)[CCBReader load:bandGuyEndSpriteFile];
        item.bandGuyEndFileName = bandGuyEndSpriteFile;
        item.powerConsumption = [[bassistInfo valueForKey:@"powerConsumption"] floatValue];
        item.powerRestoration = [[bassistInfo valueForKey:@"powerRestoration"] floatValue];
        //[[GameSoundManager sharedInstance] preloadBandGuySound:item.itemName];
        [basses addObject:item];
    }
    self.basses = [[NSMutableArray alloc] initWithArray:[basses sortedArrayUsingDescriptors:@[sort]]];
    
    BandVault *bandVault = [BandVault sharedInstance];
    BandVaultItem *vaultItem = [bandVault getBandVaultByItemId:[[basses firstObject] appStoreId]];
    
    //default values
    if (!vaultItem) {
        [bandVault storeBandGuy:[[basses firstObject] appStoreId] indexPosition:0];
        [bandVault selectBandGuy:[[basses firstObject] appStoreId] bandGuyType:BASSGUY oldSelectedGuy:nil];
    }
}

-(void)populateCoinPackData
{
    self.coinpacks = [[NSMutableArray alloc] init];
    self.coinpacksByid = [[NSMutableArray alloc] init];
    
    NSDictionary *coinDict = [bandItemsDict valueForKey:@"COINPACK"];
    NSString *pack;
    for (pack in coinDict){
        BandStoreItem *item = [[BandStoreItem alloc] init];
        NSDictionary *coinpackInfo = [coinDict valueForKey:pack];
        item.itemid = [[coinpackInfo valueForKey:@"id"] intValue];
        item.appStoreId = [coinpackInfo valueForKey:@"appStoreId"];
        item.itemName = [coinpackInfo valueForKey:@"name"];
        item.description = [coinpackInfo valueForKey:@"description"];
        item.credits = [[coinpackInfo valueForKey:@"credits"] intValue];
        item.value = [[coinpackInfo valueForKey:@"value"] intValue];
        item.duration = [[coinpackInfo valueForKey:@"duration"] intValue];
        item.itemType = BAND_COIN_PACK;
        [coinpacks addObject:item];
    }
    
    self.coinpacks = [[NSMutableArray alloc] initWithArray:[coinpacks sortedArrayUsingDescriptors:@[sortGenericId]]];
    for (BandStoreItem *bandStoreItem in self.coinpacks) {
        [self.coinpacksByid addObject:bandStoreItem.appStoreId];
    }

}

-(void)populateGenericItemData
{
    self.generalItems =[[NSMutableArray alloc] init];
    self.genericItemsId = [[NSMutableArray alloc] init];
    
    NSDictionary *genericItemDict = [bandItemsDict valueForKey:@"GENERICITEM"];
    NSString *genericItem;
    
    for (genericItem in genericItemDict) {
        BandStoreItem *item = [[BandStoreItem alloc] init];
        NSDictionary *genericItemInfo = [genericItemDict valueForKey:genericItem];
        item.itemid = [[genericItemInfo valueForKey:@"id"] intValue];
        item.appStoreId = [genericItemInfo valueForKey:@"appStoreId"];
        item.itemName = [genericItemInfo valueForKey:@"name"];
        item.description = [genericItemInfo valueForKey:@"description"];
        item.credits = [[genericItemInfo valueForKey:@"credits"] intValue];
        item.value = [[genericItemInfo valueForKey:@"value"] intValue];
        item.ownedBandItem = [[genericItemInfo valueForKey:@"owner"] boolValue];
        item.duration = [[genericItemInfo valueForKey:@"duration"] intValue];
        item.frame = [[genericItemInfo valueForKey:@"frames"] intValue];
        //item.itemType = GENERIC_ITEM;
        item.itemType = [[genericItemInfo valueForKey:@"itemType"] intValue];
        item.genericItemType = [[genericItemInfo valueForKey:@"genericType"] intValue];
        NSString *itemFile = [NSString stringWithFormat:@"store/items/%@",item.appStoreId];
        item.itemSprt = (CCSprite *)[CCBReader load:itemFile];
        [self.generalItems addObject:item];
    
    }
    
    self.generalItems = [[NSMutableArray alloc] initWithArray:[generalItems sortedArrayUsingDescriptors:@[sortGenericId]]];
    
    for (BandStoreItem *bandStoreItem in self.generalItems) {
        [self.genericItemsId addObject:bandStoreItem.appStoreId];
    }
    
}

/*
-(void)mapGenericItemType(int value){
    
}*/

-(void)loadBandItemsFromJson
{
    //load the band members definitions from a json file
    NSBundle *appBundle = [NSBundle bundleForClass:[self class]];
    NSString *levelDataPath = [appBundle pathForResource:@"bandItemsBasic" ofType:@"json"];
    NSString *jsonString = [NSString stringWithContentsOfFile:levelDataPath encoding:NSUTF8StringEncoding error:nil];
    bandItemsDict = [jsonString JSONValue];
    sort = [NSSortDescriptor sortDescriptorWithKey:@"appStoreId" ascending:YES];
    sortGenericId = [NSSortDescriptor sortDescriptorWithKey:@"itemid" ascending:YES];
    
    [self populateDrummerData];
    [self populateLeadGuitarData];
    [self populateBaseGuitarData];
    [self populateVocalData];
    [self populateBassData];
    [self populateCoinPackData];
    [self populateGenericItemData];
    [self loadOwnedItems];
    NSLog(@"Items loaded !");
}

-(void)restoreBandGuys
{
    for (BandStoreItem *bandItem in [self leadGuitars]) {
        [bandItem.bandGuySprite restoreOriginalState];
    }
    
    for (BandStoreItem *bandItem in [self baseGuitars]) {
        [bandItem.bandGuySprite restoreOriginalState];
    }
    
    for (BandStoreItem *bandItem in [self vocals]) {
        [bandItem.bandGuySprite restoreOriginalState];
    }
    
    for (BandStoreItem *bandItem in [self basses]) {
        [bandItem.bandGuySprite restoreOriginalState];
    }
    
}

@end
