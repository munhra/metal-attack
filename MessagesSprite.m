//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  MessagesSprite.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 14/08/15.
//  Copyright 2015 Apportable. All rights reserved.
//

#import "MessagesSprite.h"


@implementation MessagesSprite

@synthesize messageText;
@synthesize backgroundNine;

-(void)closeMessage;
{
    NSLog(@"Close message box");
}

-(void)setTextMessage:(NSString *)text
{
    [self.messageText setString:text];
}

-(void)adjustTextPositionWithFactor:(CGPoint)positionfactor
{
    CGPoint adjustedPoint = ccp(self.messageText.position.x * positionfactor.x, self.messageText.position.y *positionfactor.y);
    [self.messageText setPosition:adjustedPoint];
}

-(void)adjustArrowHorizontalPosition:(CGFloat)factor {
    
    CGPoint adjustedPosition = ccp(self.arrowSprite.position.x * factor, self.arrowSprite.position.y);
    [self.arrowSprite setPosition:adjustedPosition];
}


-(void)adjustTextBoxWithFactor:(CGPoint)positionfactor
{
    CGSize adjustedSize = CGSizeMake(self.messageText.contentSize.width * positionfactor.x, self.messageText.contentSize.height * positionfactor.y);
    [self.messageText setContentSize:adjustedSize];
}

-(void)setFontSize:(CGFloat)size
{
    [[self messageText] setFontSize:size];
}

-(void)setBoxWidth:(CGFloat)width
{
    CGSize bgSize = CGSizeMake(width, self.backgroundNine.contentSize.height);
    [self.backgroundNine setContentSize:bgSize];
}

-(void)setBoxHeight:(CGFloat)height
{
    CGSize bgSize = CGSizeMake(self.backgroundNine.contentSize.width, height);
    [self.backgroundNine setContentSize:bgSize];
}

-(void)animateShowWithScale
{
    [[backgroundNine animationManager] runAnimationsForSequenceNamed:@"ShowMessage"];
}

-(void)animateHideWithScale
{
    [[backgroundNine animationManager] runAnimationsForSequenceNamed:@"HideMessage"];
}

-(void)animateMessageIn
{
    CCActionMoveTo *moveMessageOut = [CCActionMoveTo actionWithDuration:0.5 position:ccp(self.parent.boundingBox.size.width * -2,  self.position.y)];
    CCActionMoveTo *moveMessageIn = [CCActionMoveTo actionWithDuration:0.5 position:ccp(self.parent.boundingBox.size.width/2, self.position.y)];
    CCActionEaseBackInOut *moveMessageOutEase = [CCActionEaseBackInOut actionWithAction:moveMessageOut];
    CCActionEaseBackInOut *moveMessageInEase = [CCActionEaseBackInOut actionWithAction:moveMessageIn];
    CCActionBlink *blinkAction = [CCActionBlink actionWithDuration:0.5 blinks:10];
    CCActionCallBlock *endSequence = [CCActionCallBlock actionWithBlock:^{
        NSLog(@"endBlikAction");
        [self scheduleBlock:^(CCTimer *timer) {
            NSLog(@"schedule !");
            [self runAction:moveMessageOutEase];
        } delay:2];
    }];
    CCActionSequence *moveSequence = [CCActionSequence actions:moveMessageInEase,endSequence, nil];
    [self runAction:moveSequence];
}

-(void)animateMessageOut
{
    
}

-(void)showMessage
{
    CCActionMoveTo *moveMessageOut = [CCActionMoveTo actionWithDuration:0.5 position:ccp(self.parent.boundingBox.size.width * -2,  self.position.y)];
    CCActionMoveTo *moveMessageIn = [CCActionMoveTo actionWithDuration:0.5 position:ccp(self.parent.boundingBox.size.width/2, self.position.y)];
    CCActionEaseBackInOut *moveMessageOutEase = [CCActionEaseBackInOut actionWithAction:moveMessageOut];
    CCActionEaseBackInOut *moveMessageInEase = [CCActionEaseBackInOut actionWithAction:moveMessageIn];
    CCActionBlink *blinkAction = [CCActionBlink actionWithDuration:0.5 blinks:10];
    CCActionCallBlock *endSequence = [CCActionCallBlock actionWithBlock:^{
        NSLog(@"endBlikAction");
        [self scheduleBlock:^(CCTimer *timer) {
            NSLog(@"schedule !");
            [self runAction:moveMessageOutEase];
        } delay:2];
    }];
    //CCActionSequence *moveSequence = [CCActionSequence actions:moveMessageInEase,endSequence, nil];
    CCActionSequence *moveSequence = [CCActionSequence actions:moveMessageInEase, nil];
    [self runAction:moveSequence];
}

-(void)hideMessage
{
    CCActionMoveTo *moveMessageOut = [CCActionMoveTo actionWithDuration:0.5 position:ccp(self.parent.boundingBox.size.width * -2,  self.position.y)];
    CCActionMoveTo *moveMessageIn = [CCActionMoveTo actionWithDuration:0.5 position:ccp(self.parent.boundingBox.size.width/2, self.position.y)];
    CCActionEaseBackInOut *moveMessageOutEase = [CCActionEaseBackInOut actionWithAction:moveMessageOut];
    CCActionEaseBackInOut *moveMessageInEase = [CCActionEaseBackInOut actionWithAction:moveMessageIn];
    CCActionBlink *blinkAction = [CCActionBlink actionWithDuration:0.5 blinks:10];
    CCActionCallBlock *endSequence = [CCActionCallBlock actionWithBlock:^{
        NSLog(@"endBlikAction");
        [self scheduleBlock:^(CCTimer *timer) {
            NSLog(@"schedule !");
            [self runAction:moveMessageOutEase];
        } delay:2];
    }];
    //CCActionSequence *moveSequence = [CCActionSequence actions:moveMessageInEase,endSequence, nil];
    CCActionSequence *moveSequence = [CCActionSequence actions:moveMessageOutEase, nil];
    [self runAction:moveSequence];
}

@end
