//
//  CoinMultiplierPowerUp.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 02/01/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CoinMultiplierPowerUp.h"
#import "GameSoundManager.h"
#import "BandVaultItem.h"
#import "BandVault.h"
#import "CCTextureCache.h"

@implementation CoinMultiplierPowerUp

@synthesize coinMultSprite1;
@synthesize coinMultSprite2;
@synthesize coinMultSprite3;
@synthesize coinMultSprite4;
@synthesize levelDelegate;

CGSize screenSize;
CGPoint bandPosition;
//const int powerUPZorder = 90;

/*
+(id)sharedInstance
{
    static id master = nil;
    @synchronized(self)
    {
        if (master == nil){
            master = [self new];
        }
    }
    return master;
}*/

-(void)removePowerUps
{
    //[[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/revival/bass.png"];
    //[[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/revival/drummer.png"];
    //[[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/revival/vocal.png"];
}

-(void)preparePowerUps:(CGPoint)bandposition
{
    BandVault *vault = [BandVault sharedInstance];
    for (BandVaultItem *vaultItem in [vault selectedItems]) {
        NSString *itemId = [vaultItem.appId substringFromIndex:4];
        switch ([itemId integerValue]) {
            case 5:
                coinMultSprite1 = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult1"];
                break;
            case 6:
                coinMultSprite2 = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult2"];
                break;
            case 7:
                coinMultSprite3 = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult3"];
                break;
            case 8:
                coinMultSprite4 = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult4"];
                break;
            default:
                break;
        }
    }
    
    /*
    coinMultSprite1 = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult1"];
    coinMultSprite2 = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult2"];
    coinMultSprite3 = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult3"];
    coinMultSprite4 = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult4"];
    */
    bandPosition = bandposition;
    screenSize = [[CCDirector sharedDirector] viewSize];
}

-(void)activateCoinPowerUp:(int)coinMultiplierId
{
    GameSoundManager *gameSoundManager = [GameSoundManager sharedInstance];
    
    switch (coinMultiplierId) {
        
        case 5:
        [gameSoundManager playSoundEffectByName:@"PowerUpFX01"];
        [self performCoinMultiplier1];
        break;
        
        case 6:
        [gameSoundManager playSoundEffectByName:@"PowerUpFX02"];
        [self performCoinMultiplier2];
        break;
        
        case 7:
        [gameSoundManager playSoundEffectByName:@"PowerUpFX03"];
        [self performCoinMultiplier3];
        break;
        
        case 8:
        [gameSoundManager playSoundEffectByName:@"PowerUpFX04"];
        [self performCoinMultiplier4];
        break;
        
        default:
        break;
    }
}

-(void)performCoinMultiplier1
{
    
    int inversor = 1;
    
    CCSprite *moneyPigSprite;
    CCActionJumpTo *jumpMoneyPigAction;
    CCActionJumpTo *revJumpMoneyPigAction;
    CCActionSequence *sequenceAction;
    CCActionScaleTo *scaleToFlip;
    CCActionScaleTo *scaleToUnflip;
    CCActionBlink *pigBlink;
    CCActionCallBlock *endAnimation;

    
    float positionFactor = screenSize.height/10;
    float partialPosition = positionFactor;
    
    
    for (int i = 1; i <= 8; i++) {
        
        if (i % 2 == 0) {
            inversor = -1;
        }else{
            inversor = 1;
        }
        
        float randJumpHeight = arc4random() % 11 * 0.1;
        moneyPigSprite = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult1"];
        [moneyPigSprite setPosition:ccp(0, partialPosition)];

        [self.levelDelegate addChild:moneyPigSprite z:90];
        
        jumpMoneyPigAction = [CCActionJumpTo actionWithDuration:1.5 position:ccp(screenSize.width,partialPosition) height:50*inversor*randJumpHeight jumps:2];
        
        revJumpMoneyPigAction = [CCActionJumpTo actionWithDuration:1.5 position:ccp(0,partialPosition) height:50*inversor*randJumpHeight jumps:2];
        scaleToFlip = [CCActionScaleTo actionWithDuration:0.2 scaleX:-1 scaleY:1];
        scaleToUnflip = [CCActionScaleTo actionWithDuration:0.2 scaleX:1 scaleY:1];
        pigBlink = [CCActionBlink actionWithDuration:0.5 blinks:10];
        
        endAnimation = [CCActionCallBlock actionWithBlock:^{
            [moneyPigSprite removeFromParent];
        }];
        
        sequenceAction = [CCActionSequence actions:jumpMoneyPigAction, scaleToFlip, revJumpMoneyPigAction, scaleToUnflip, pigBlink, endAnimation, nil];
        
        [self.levelDelegate scheduleBlock:^(CCTimer *timer) {
            [moneyPigSprite runAction:sequenceAction];
        } delay:2 * randJumpHeight];

        partialPosition += positionFactor;
    }
    
    
}

-(void)performCoinMultiplier2
{
    int inversor = 1;
    
    CCSprite *moneyPigSprite;
    CCActionJumpTo *jumpMoneyPigAction;
    CCActionJumpTo *revJumpMoneyPigAction;
    CCActionSequence *sequenceAction;
    CCActionScaleTo *scaleToFlip;
    CCActionScaleTo *scaleToUnflip;
    CCActionBlink *pigBlink;
    CCActionCallBlock *endAnimation;
    
    
    float positionFactor = screenSize.height/10;
    float partialPosition = positionFactor;
    
    
    for (int i = 1; i <= 8; i++) {
        
        if (i % 2 == 0) {
            inversor = -1;
        }else{
            inversor = 1;
        }
        
        float randJumpHeight = arc4random() % 11 * 0.1;
        moneyPigSprite = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult2"];
        
        [moneyPigSprite setPosition:ccp(screenSize.width, partialPosition)];
        
        [self.levelDelegate addChild:moneyPigSprite z:90];
        
        
        jumpMoneyPigAction = [CCActionJumpTo actionWithDuration:2 position:ccp(0,partialPosition) height:50*inversor*randJumpHeight jumps:2];
        
        
        
        revJumpMoneyPigAction = [CCActionJumpTo actionWithDuration:2 position:ccp(screenSize.width,partialPosition) height:60*inversor*randJumpHeight jumps:2];
        
        
        scaleToFlip = [CCActionScaleTo actionWithDuration:0.2 scaleX:-1 scaleY:1];
        scaleToUnflip = [CCActionScaleTo actionWithDuration:0.2 scaleX:1 scaleY:1];
        pigBlink = [CCActionBlink actionWithDuration:0.5 blinks:10];
        
        endAnimation = [CCActionCallBlock actionWithBlock:^{
            [moneyPigSprite removeFromParent];
        }];
        
        sequenceAction = [CCActionSequence actions:jumpMoneyPigAction, scaleToFlip, revJumpMoneyPigAction, scaleToUnflip, pigBlink, endAnimation, nil];
        
        [self.levelDelegate scheduleBlock:^(CCTimer *timer) {
            [moneyPigSprite runAction:sequenceAction];
        } delay:2 * randJumpHeight];
        
        partialPosition += positionFactor;
    }

}

-(void)performCoinMultiplier3
{
    // raining diamonds to the center of the band, spaw aleatory off  screen, transition to the x position of the band
    // keeps spwaning for 10 seconds, 20 diamonds
    [self schedule:@selector(createDiamond) interval:0.5 repeat:30 delay:0.5];
    [self schedule:@selector(createDiamond) interval:0.2 repeat:20 delay:0.8];
    [self schedule:@selector(createDiamond) interval:0.1 repeat:40 delay:0.1];
    
}

-(void)createDiamond {
    
    float randomX = arc4random() % (int)screenSize.width;
    float randomY = 50 + arc4random() % (150 - 50);
    
    CCSprite *diamond = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult3"];
    [diamond setScale:0.75];
    diamond.position = ccp(randomX,screenSize.height + randomY);
    
    [self.levelDelegate addChild:diamond z:90];
    
    CCActionMoveTo *diamondMove = [CCActionMoveTo actionWithDuration:0.7 position:ccp(randomX,bandPosition.y * screenSize.height -150)];
    CCActionCallBlock *endAnimation = [CCActionCallBlock actionWithBlock:^{
        
        [[diamond animationManager] runAnimationsForSequenceNamed:@"BrokeDiamond"];
        [[diamond animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
            [diamond removeFromParent];
        }];
        
        
    }];
    
    CCActionSequence *sequenceAction = [CCActionSequence actions:diamondMove, endAnimation, nil];
    [diamond runAction:sequenceAction];
    
}

-(void)performCoinMultiplier4
{
    CCSprite *moneyGods = (CCSprite *) [CCBReader load:@"CoinMultiplier/CoinMult4"];
    [moneyGods setPosition:ccp(screenSize.width*0.25,screenSize.height/2)];
    [moneyGods setScale:0.5];
    [self.levelDelegate addChild:moneyGods z:90];
}



@end
