//
//  CCButton+MetalAttack.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 9/28/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CCButton (MetalAttack)

-(void)playSimpleButtonClick;


@end
