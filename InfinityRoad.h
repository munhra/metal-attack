//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  InfinityRoad.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 11/04/15.
//  Copyright 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface InfinityRoad : CCNode {

    CCSprite *visibleBG;
    CCSprite *visibleRoad;
    CCSprite *leftBG;
    CCSprite *leftRoad;
    CCSprite *rightBG;
    CCSprite *rightRoad;
    
    BOOL stopMoving;
    BOOL accelerate;
    float speedFactor;
    
}

@property (nonatomic,retain) CCSprite *visibleBG;
@property (nonatomic,retain) CCSprite *visibleRoad;
@property (nonatomic,retain) CCSprite *leftBG;
@property (nonatomic,retain) CCSprite *leftRoad;
@property (nonatomic,retain) CCSprite *rightBG;
@property (nonatomic,retain) CCSprite *rightRoad;
@property (nonatomic) BOOL stopMoving;
@property (nonatomic) BOOL accelerate;
@property (nonatomic) float speedFactor;

-(void)deaccelerate;


@end
