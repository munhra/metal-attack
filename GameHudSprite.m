//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  GameHudSprite.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 08/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "GameHudSprite.h"
#import "UniversalInfo.h"
#import "CCTextureCache.h"


@implementation GameHudSprite

@synthesize drummerPic;
@synthesize leadGuitarPic;
@synthesize vocalPic;
@synthesize baseGuitarPic;
@synthesize bassPic;

@synthesize drummerLife;
@synthesize leadGuitarLife;
@synthesize vocalLife;
@synthesize baseGuitarLife;
@synthesize bassLife;

@synthesize drummerFig;
@synthesize baseGuitarFig;
@synthesize bassFig;
@synthesize vocalFig;
@synthesize leadGuitarFig;

@synthesize drummerProgress;
@synthesize leadGuitarProgress;
@synthesize vocalLifeProgress;
@synthesize baseGuitarLifeProgress;
@synthesize bassLifeProgress;
@synthesize guitarGaugeProgress;
@synthesize snakeBonesGaugeProgress;

@synthesize guitarGaugeSprite;
@synthesize snakeGaugeSprite;
@synthesize lifeIconsBox;

@synthesize coinsLabel;
@synthesize delegate;

@synthesize pauseButton;

@synthesize ice1Powerup;
@synthesize ice2Powerup;
@synthesize ice3Powerup;
@synthesize ice4Powerup;

@synthesize snakeHeadSprite;
@synthesize totalEnemies;

@synthesize totalPowerConsumption;
@synthesize totalPowerRestoration;

-(void)onEnter{
    [super onEnter];
    [self prepareHudIcons];
    [[self animationManager] runAnimationsForSequenceNamed:@"AnimateSnake"];
}

-(void)didLoadFromCCB
{
    NSLog(@"GameHudSprite !!");
    [self createGuitarGauge];
    [self createSnakeBonesGauge];
    [self defineSpriteZorder];
}

-(void)prepareHudIcons
{
    [self createLifeIcons];
    [self createLifeGauges];
}

-(void)createLifeIcons
{
    CGPoint drummerPosition = ccp(self.drummerPic.boundingBox.size.width/2, self.drummerPic.boundingBox.size.height/2);
    CGPoint vocalPosition = ccp(self.vocalPic.boundingBox.size.width/2, self.vocalPic.boundingBox.size.height/2);
    CGPoint leadPosition = ccp(self.leadGuitarPic.boundingBox.size.width/2, self.leadGuitarPic.boundingBox.size.height/2);
    CGPoint basePosition = ccp(self.baseGuitarPic.boundingBox.size.width/2, self.baseGuitarPic.boundingBox.size.height/2);
    CGPoint bassPosition = ccp(self.bassPic.boundingBox.size.width/2, self.bassPic.boundingBox.size.height/2);

    [drummerFig setPosition:drummerPosition];
    [vocalFig setPosition:vocalPosition];
    [baseGuitarFig setPosition:basePosition];
    [leadGuitarFig setPosition:leadPosition];
    [bassFig setPosition:bassPosition];
    
    [self.drummerPic addChild:drummerFig];
    [self.vocalPic addChild:vocalFig];
    [self.baseGuitarPic addChild:baseGuitarFig];
    [self.leadGuitarPic addChild:leadGuitarFig];
    [self.bassPic addChild:bassFig];
    
    [[leadGuitarFig animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
    [[baseGuitarFig animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
    [[bassFig animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
    [[vocalFig animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
    [[drummerFig animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
    
}

-(void)stopSnakeAnimation
{
    [[self animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
}

-(void)defineSpriteZorder
{
    [guitarGaugeSprite setZOrder:1];
    [lifeIconsBox setZOrder:5];
    [pauseButton setZOrder:5];
    [snakeGaugeSprite setZOrder:4];
    [snakeHeadSprite setZOrder:5];
    [ice4Powerup setZOrder:3];
    [ice3Powerup setZOrder:3];
    [ice2Powerup setZOrder:3];
    [ice1Powerup setZOrder:3];
}

-(void)createSnakeBonesGauge
{
    CCSprite *snakeBonesSprite = (CCSprite *)[CCBReader load:@"snakeBonesSprite"];
    CGPoint snakeGaugePosition = [snakeGaugeSprite position];
    CCNode *parent = [snakeGaugeSprite parent];
    snakeBonesGaugeProgress = [[CCProgressNode alloc] initWithSprite:snakeBonesSprite];
    [snakeBonesGaugeProgress setType:CCProgressNodeTypeBar];
    [snakeBonesGaugeProgress setBarChangeRate:ccp(1,0)];
    [snakeBonesGaugeProgress setMidpoint:ccp(0,0)];
    [snakeBonesGaugeProgress setPercentage:0];
    snakeBonesGaugeProgress.position = snakeGaugePosition;
    [snakeBonesGaugeProgress setZOrder:10];
    [parent addChild:snakeBonesGaugeProgress];
    
    //[self schedule:@selector(raiseGuitarCooldownBar) interval:time];
}

-(void)createGuitarGauge
{
    CCSprite *guitarColorSprite = (CCSprite *)[CCBReader load:@"guitarColorSprite"];
    CGPoint guitarGaugePosition = [guitarGaugeSprite position];
    CCNode *parent = [guitarGaugeSprite parent];
    guitarGaugeProgress = [[CCProgressNode alloc] initWithSprite:guitarColorSprite];
    [guitarGaugeProgress setType:CCProgressNodeTypeBar];
    [guitarGaugeProgress setBarChangeRate:ccp(1,0)];
    [guitarGaugeProgress setMidpoint:ccp(0,0)];
    [guitarGaugeProgress setPercentage:30];
    guitarGaugeProgress.position = guitarGaugePosition;
    [guitarGaugeProgress setZOrder:2];
    [parent addChild:guitarGaugeProgress];
    //[self schedule:@selector(raiseGuitarCooldownBar) interval:time];
}

-(void)createLifeGauges
{
    CGPoint lifePoint;
    CCNode *parent;
    CCSprite *lifeBar = (CCSprite *)[CCBReader load:@"lifeBarSprite"];

    lifePoint = drummerLife.position;
    parent = [drummerLife parent];
    drummerProgress =  [[CCProgressNode alloc] initWithSprite:lifeBar];
    [drummerProgress setType:CCProgressNodeTypeBar];
    [drummerProgress setBarChangeRate:ccp(1,0)];
    [drummerProgress setMidpoint:ccp(0,0)];
    [drummerProgress setPercentage:100];
    drummerProgress.position = lifePoint;
    [parent addChild:drummerProgress];

    lifePoint = leadGuitarLife.position;
    parent = [leadGuitarLife parent];
    leadGuitarProgress =  [[CCProgressNode alloc] initWithSprite:lifeBar];
    [leadGuitarProgress setType:CCProgressNodeTypeBar];
    [leadGuitarProgress setBarChangeRate:ccp(1,0)];
    [leadGuitarProgress setMidpoint:ccp(0,0)];
    [leadGuitarProgress setPercentage:100];
    leadGuitarProgress.position = lifePoint;
    [parent addChild:leadGuitarProgress];
    
    lifePoint = baseGuitarLife.position;
    parent = [baseGuitarLife parent];
    baseGuitarLifeProgress =  [[CCProgressNode alloc] initWithSprite:lifeBar];
    [baseGuitarLifeProgress setType:CCProgressNodeTypeBar];
    [baseGuitarLifeProgress setBarChangeRate:ccp(1,0)];
    [baseGuitarLifeProgress setMidpoint:ccp(0,0)];
    [baseGuitarLifeProgress setPercentage:100];
    baseGuitarLifeProgress.position = lifePoint;
    [parent addChild:baseGuitarLifeProgress];
    
    lifePoint = vocalLife.position;
    parent = [vocalLife parent];
    vocalLifeProgress =  [[CCProgressNode alloc] initWithSprite:lifeBar];
    [vocalLifeProgress setType:CCProgressNodeTypeBar];
    [vocalLifeProgress setBarChangeRate:ccp(1,0)];
    [vocalLifeProgress setMidpoint:ccp(0,0)];
    [vocalLifeProgress setPercentage:100];
    vocalLifeProgress.position = lifePoint;
    [parent addChild:vocalLifeProgress];
    
    lifePoint = bassLife.position;
    parent = [bassLife parent];
    bassLifeProgress =  [[CCProgressNode alloc] initWithSprite:lifeBar];
    [bassLifeProgress setType:CCProgressNodeTypeBar];
    [bassLifeProgress setBarChangeRate:ccp(1,0)];
    [bassLifeProgress setMidpoint:ccp(0,0)];
    [bassLifeProgress setPercentage:100];
    bassLifeProgress.position = lifePoint;
    [parent addChild:bassLifeProgress];

}

-(void)doPauseGame
{
    NSLog(@"doPauseGame");
    [[self delegate] doPauseGame];
}

-(void)updateCoinLabel:(NSNumber *)coinValue;
{
    [[self coinsLabel] setString:[[UniversalInfo sharedInstance] addZeroesToNumber:[coinValue stringValue] numberOfZeroes:4]];
    
}

-(void)updateBandFaceState:(BandComponents)type isDead:(BOOL)dead isRevival:(BOOL)revival withBandSprite:(BandSprite*)bandSprite;
{
    
    NSString *animationType = @"";
    
    if (dead){
        animationType = @"Die";
    }else{
        animationType = @"Hit";
    }
    
    if (revival) {
        animationType = @"Default Timeline";
    }
    
    if (!bandSprite.invulnerability) {
        switch (type) {
            case GUITARRIST1_TYPE:
            if (![[[[self leadGuitarFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.guitarBothInvulnerabiliy){
                [[leadGuitarFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            break;
            case GUITARRIST2_TYPE:
            if (![[[[self baseGuitarFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.guitarBothInvulnerabiliy){
                [[baseGuitarFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            break;
            case BASS_TYPE:
            if (![[[[self bassFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.bassInvulnerabiliy){
                [[bassFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            break;
            case VOCALS_TYPE:
            if (![[[[self vocalFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.vocalInvulnerabiliy){
                [[vocalFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            break;
            case DRUMMER_TYPE:
            if (![[[[self drummerFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.drummerInvulnerabiliy){
                [[drummerFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            break;
            case ALL_GUYS:
            
            if (![[[[self leadGuitarFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.guitarBothInvulnerabiliy){
                [[leadGuitarFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            
            if (![[[[self baseGuitarFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.guitarBothInvulnerabiliy){
                [[baseGuitarFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            
            if (![[[[self bassFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.bassInvulnerabiliy){
                [[bassFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            
            if (![[[[self vocalFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.vocalInvulnerabiliy){
                [[vocalFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            
            if (![[[[self drummerFig] animationManager] runningSequenceName] isEqualToString:animationType] && !bandSprite.drummerInvulnerabiliy){
                [[drummerFig animationManager] runAnimationsForSequenceNamed:animationType];
            }
            
            break;
            
            default:
            break;
        }

    }
    
}

-(void)updateBandLife:(BandSprite *)bandSprite
{
    float lifeValue;
    
    lifeValue = ([bandSprite drummerArmor] / [bandSprite totalDrummerArmor]) * 100;
    //NSLog(@"drummer life %f",lifeValue);
    [drummerProgress setPercentage:lifeValue];
    /*
    if (lifeValue == 0) {
        [[drummerFig animationManager] runAnimationsForSequenceNamed:@"Die"];
    }else{
        [[drummerFig animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
    }*/
    
    lifeValue = ([bandSprite guitar1Armor] / [bandSprite totalGuitar1Armor]) * 100;
    //NSLog(@"lead life %f",lifeValue);
    [leadGuitarProgress setPercentage:lifeValue];

    lifeValue = ([bandSprite guitar2Armor] / [bandSprite totalGuitar2Armor]) * 100;
    //NSLog(@"base life %f",lifeValue);
    [baseGuitarLifeProgress setPercentage:lifeValue];

    lifeValue = ([bandSprite vocalArmor] / [bandSprite totalVocalArmor]) * 100;
    //NSLog(@"vocal life %f",lifeValue);
    [vocalLifeProgress setPercentage:lifeValue];
    
    lifeValue = ([bandSprite bassArmor] / [bandSprite totalBassArmor]) * 100;
    //NSLog(@"bass life %f",lifeValue);
    [bassLifeProgress setPercentage:lifeValue];
}

-(void)updateSnakeBonesProgress:(float)leftEnemies;
{
    float levelProgress = (1 - (leftEnemies/totalEnemies))*100;
    [snakeBonesGaugeProgress setPercentage:levelProgress];
}

-(void)raiseGuitarCooldownBar
{
#warning this could be a factor that improves when the band is on a improved level
    //sum of all and guys cooldown factor
    
    guitarGaugeProgress.percentage = guitarGaugeProgress.percentage + totalPowerRestoration; // 0.7
}

-(void)lowerGuitarCooldownBar
{
    guitarGaugeProgress.percentage = guitarGaugeProgress.percentage - totalPowerConsumption; // -1 the most the worst
}

-(void)dealloc
{
    NSLog(@"GameHud");
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}



@end
