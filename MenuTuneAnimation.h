//
//  MenuTuneAnimation.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 11/3/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "CCSprite.h"

@interface MenuTuneAnimation : CCSprite

//@property(class) bool playFirstTime;
@property bool playFirstTime;

@end
