//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  InfinityRoad.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 11/04/15.
//  Copyright 2015 Apportable. All rights reserved.
//

#import "InfinityRoad.h"


@implementation InfinityRoad

@synthesize visibleBG;
@synthesize visibleRoad;
@synthesize leftBG;
@synthesize leftRoad;
@synthesize rightBG;
@synthesize rightRoad;
@synthesize stopMoving;
@synthesize accelerate;
@synthesize speedFactor;



-(id)init
{
    speedFactor = 1;
    return [super init];
}

-(void)update:(CCTime)delta
{
    if (speedFactor >= 0) {
        if (stopMoving){
            accelerate = NO;
            [self deaccelerate];
        }
        
        [self moveBgToRight];
    }
}

-(void)moveBgToRight
{    
    if (rightBG.position.x <= 0.27) {
        id bgaux = visibleBG;
        id roadbgaux = visibleRoad;
        
        visibleBG = rightBG;
        visibleRoad = rightRoad;
        
        rightBG = bgaux;
        rightRoad = roadbgaux;
        [rightBG setPosition:ccp(1 + visibleBG.position.x,0.512)];
        [rightRoad setPosition:ccp(1 + visibleRoad.position.x,0.025)];
    }
    
    if (accelerate) {
        speedFactor = 2;
    }
    
    [visibleBG setPosition:ccpAdd(visibleBG.position, ccp(-0.02001*speedFactor, 0))];
    [rightBG setPosition:ccpAdd(rightBG.position, ccp(-0.0201*speedFactor, 0))];
   
    [visibleRoad setPosition:ccpAdd(visibleRoad.position, ccp(-0.02001*speedFactor, 0))];
    [rightRoad setPosition:ccpAdd(rightRoad.position, ccp(-0.0201*speedFactor, 0))];
}

-(void)doBack
{
    NSLog(@"doBack");
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
}

-(void)deaccelerate
{
    [self scheduleBlock:^(CCTimer *timer) {
        speedFactor = speedFactor -0.05;
    } delay:0.05];
}

- (void)dealloc
{
    NSLog(@"dealloc infitnity road");
}



@end
