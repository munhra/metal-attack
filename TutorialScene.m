//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  TutorialScene.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 8/6/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "TutorialScene.h"
#import "LevelSceneController.h"
#import "CCTextureCache.h"
#import "GameSoundManager.h"
#import "GameCenterManager.h"

@implementation TutorialScene

@synthesize tutorialAnimationIndex;
@synthesize calledFromGamePlay;

-(void)onEnter {
    [super onEnter];
    [[self animationManager] setDelegate:self];
    tutorialAnimationIndex = 1;
    [self saveTutorialPlayed];
    [[GameSoundManager sharedInstance] playTutorialSong];
}
    
-(void)onExit {
    [super onExit];
    [[GameSoundManager sharedInstance] stopBackgroundSong];
}
    
-(void)doCloseTutorial
{
    NSLog(@"doCloseTutorial");
    if (calledFromGamePlay) {
        LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
        [[CCDirector sharedDirector] replaceScene:[lvcontroller loadLevelScene:0 waveNumber:0] withTransition:[CCTransition transitionCrossFadeWithDuration:0.3]];
        [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    }else{
        [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
    }
}

- (void)completedAnimationSequenceNamed:(NSString*)name
{
    tutorialAnimationIndex++;
    
    if (tutorialAnimationIndex == 7) {
        [[GameCenterManager sharedInstance] achivmentUpdate:@"TutorialWatchers" withCallBack:^(AchievimentBanner *achievimentBanner) {
            if (achievimentBanner) {
                [self addChild:achievimentBanner];
                [achievimentBanner animateMessageInAndOut];
            }
        }];
    }
    
    if (tutorialAnimationIndex <= 8) {
        NSString *sequenceName = [NSString stringWithFormat:@"Tutorial%d",tutorialAnimationIndex];
        [[self animationManager] runAnimationsForSequenceNamed:sequenceName];
    }else{
        if (calledFromGamePlay) {
            LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
            [[CCDirector sharedDirector] replaceScene:[lvcontroller loadLevelScene:0 waveNumber:0] withTransition:[CCTransition transitionCrossFadeWithDuration:0.3]];
            [[CCTextureCache sharedTextureCache] removeUnusedTextures];
        }else{
            [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
        }
    }
}

-(void)saveTutorialPlayed
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    [userdef setBool:true forKey:@"TUTORIAL_PLAYED"];
}
    
-(void)playEnterTextAndArrowSound
{
    NSLog(@"playEnterTextAndArrowSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx006"];
}

-(void)playExitTextAndArrowSound
{
    NSLog(@"playExitTextAndArrowSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx014"];
}
    
-(void)playEnemyEnterSound
{
    NSLog(@"playEnemyEnterSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx023"];
}

-(void)playEnemyExitSound
{
    NSLog(@"playEnemyExitSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx011"];
}
    
-(void)playBandEnterSound
{
    NSLog(@"playBandEnterSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}
    
-(void)playHandEnterSound
{
    NSLog(@"playHandEnterSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx038"];
}

-(void)playShootSound
{
    NSLog(@"playShootSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx007"];
}

-(void)playHudAppearSound
{
    NSLog(@"playHudAppearSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx031"];
}

-(void)playCoinShowSound
{
    NSLog(@"playCoinShowSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx046"];
}

-(void)playPowerUpAndStoreBandShowSound
{
    NSLog(@"playPowerUpShowSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx006"];
}
    



@end
