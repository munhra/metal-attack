//
//  MetalPoint.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 27/03/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MetalPoint : NSObject

@property(nonatomic) CGPoint point;

-(id)initWithPoint:(CGPoint)newPoint;

@end
