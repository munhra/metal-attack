//
//  MenuTuneAnimation.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 11/3/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "MenuTuneAnimation.h"
#import "GameSoundManager.h"

@implementation MenuTuneAnimation
static bool _playFirstTime;

+(bool)playFirstTime {
    return _playFirstTime;
}

+(void)setPlayFirstTime:(bool)playFirstTime
{
    _playFirstTime = playFirstTime;
}

-(void)onEnter
{
    [super onEnter];
    _playFirstTime = YES;
}

-(void)playMenuTune
{
    //play it aleatory ???
    //check how to do that
    
    //if (_playFirstTime) {
    //    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx069"];
    //    _playFirstTime = NO;
    //}else{
        int random = arc4random_uniform(100);
        if (random > 97) {
            NSLog(@"playMenuTune");
            [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx069"];
        }
    //}
}

@end
