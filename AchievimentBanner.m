//
//  AchievimentBanner.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 08/09/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

#import "AchievimentBanner.h"

@implementation AchievimentBanner

@synthesize messageText;
@synthesize achievimentCoin;
@synthesize banner;

/*
+(id)sharedInstance
{
    static id master = nil;
    @synchronized(self)
    {
        if (master == nil){
            master = [self new];
        }
    }
    return master;
}*/

/*
-(AchievimentBanner *)createAchievmentBanner
{
    AchievimentBanner*  achievimentBanner = (AchievimentBanner *)[CCBReader load:@"AchivmentBanner"];
    AchievimentBanner*  achievimentBanner = [[achievimentBanner alloc] init];
    CGPoint messagePosition = ccp([self boundingBox].size.width * -1.8,[self boundingBox].size.height * 0.5);
    [self setPosition:[self bannerPosition]];
    [banner setPosition:[self bannerPosition]];
    return self;
}*/

-(void)setBannerPosition
{
    [self setPosition:ccp(([[CCDirector sharedDirector] viewSize].width * 0.5),
                          ([[CCDirector sharedDirector] viewSize].height * 1.3))];
    
    //return ccp(([[CCDirector sharedDirector] viewSize].width * 0.5),
    //           ([[CCDirector sharedDirector] viewSize].height * 0.7));
}

-(void)setTextMessage:(NSString *)text
{
    
}

-(void)setCoinSpriteFrame:(CCSpriteFrame *)spriteFrame
{
    
}

-(void)animateMessageInAndOut
{
    CCActionMoveTo *moveMessageOut = [CCActionMoveTo actionWithDuration:0.5 position:ccp(self.position.x, self.parent.boundingBox.size.height * 2 )];
    CCActionMoveTo *moveMessageIn = [CCActionMoveTo actionWithDuration:0.5 position:ccp(self.position.x,  self.parent.boundingBox.size.height * 0.9)];
    CCActionEaseBackInOut *moveMessageOutEase = [CCActionEaseBackInOut actionWithAction:moveMessageOut];
    CCActionEaseBackInOut *moveMessageInEase = [CCActionEaseBackInOut actionWithAction:moveMessageIn];
    CCActionCallBlock *endSequence = [CCActionCallBlock actionWithBlock:^{
        NSLog(@"endBlikAction");
        [self scheduleBlock:^(CCTimer *timer) {
            NSLog(@"schedule !");
            [self runAction:moveMessageOut];
        } delay:4];
    }];
    CCActionSequence *moveSequence = [CCActionSequence actions:moveMessageInEase,endSequence, nil];
    [self runAction:moveSequence];
}

@end
