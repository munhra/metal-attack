//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  ItemSummary.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 21/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "ItemSummary.h"
#import "GameStore.h"
#import "UniversalInfo.h"
#import "CCTextureCache.h"

@implementation ItemSummary

@synthesize delegate;
@synthesize itemQtdSlider;
@synthesize itemQtdLabel;
@synthesize itemUnitPrice;
@synthesize itemQtd;
@synthesize itemImageOriginalPosition;

-(void)onEnter
{
    [super onEnter];
    [[self itemQtdSlider] setDelegate:self];
    [[self itemQtdSlider] setSliderValue:0.0];
    CCSprite *imageSprt = (CCSprite *)[self getChildByName:@"itemImage" recursively:YES];
    itemImageOriginalPosition = imageSprt.position;
}

-(void)onExit
{
    [super onExit];
    [[self itemQtdSlider] setDelegate:nil];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}

-(void)setItemText:(NSString *)text
{
    CCLabelTTF *itemText = (CCLabelTTF *)[self getChildByName:@"itemText" recursively:YES];
    [itemText setString:text];
}

-(void)setItemTitle:(NSString *)text
{
    CCLabelTTF *itemTitle = (CCLabelTTF *)[self getChildByName:@"itemTitle" recursively:YES];
    [itemTitle setString:text];
}

-(void)setItemPrice:(NSString *)text
{
    CCLabelTTF *itemPrice = (CCLabelTTF *)[self getChildByName:@"itemPrice" recursively:YES];
    [itemPrice setString:text];
}

-(void)setItemImage:(CCSprite *)itemsprt
{
    CCSprite *imageSprt = (CCSprite *)[self getChildByName:@"itemImage" recursively:YES];
    itemsprt.position = imageSprt.position;
    [itemsprt setName:@"itemImage"];
    [imageSprt removeFromParent];
    [self addChild:itemsprt];
}

-(void)doBuy
{
    NSLog(@"delegate item doBuy");
    [[self delegate] doBuyItem:itemQtd];
}

-(void)doClose
{
    NSLog(@"doClose");
    [[self delegate] moveItemSummaryOut];
}

-(void)showItemCost:(bool)visible;
{
    CCNode *itemCostLabel = (CCNode *)[self getChildByName:@"itemCostLabel" recursively:YES];
    CCNode *itemPrice = (CCNode *)[self getChildByName:@"itemPrice" recursively:YES];
    CCSprite *imageSprt = (CCSprite *)[self getChildByName:@"itemImage" recursively:YES];
    
    imageSprt.position = itemImageOriginalPosition;
    
    if (!visible) {
        CGPoint adjustPosition = ccp(40,0);
        imageSprt.position = ccpAdd(imageSprt.position, adjustPosition);
    }
    
    itemCostLabel.visible = visible;
    itemPrice.visible = visible;
}

-(void)changeSlideValue:(float)sliderValue
{
    NSLog(@"Change Slider Value");
    CCLabelTTF *itemPrice = (CCLabelTTF *)[self getChildByName:@"itemPrice" recursively:YES];
    float itemQtdValueFloat =  1 + (sliderValue * 9);
    itemQtd = [[NSNumber numberWithFloat:itemQtdValueFloat] intValue];
    NSString *itemStrQtd = [NSString stringWithFormat:@"%d",itemQtd];
    NSString *strLevelNumber = [[UniversalInfo sharedInstance] addZeroesToNumber:itemStrQtd numberOfZeroes:1];
    [[self itemQtdLabel] setString:strLevelNumber];
    int totalValue = itemQtd * itemUnitPrice;
    [NSString stringWithFormat:@"%d",totalValue];
    [itemPrice setString:[NSString stringWithFormat:@"%d.00",totalValue]];
}

- (void)dealloc
{
    NSLog(@"dealloc item Summary");
}

@end
