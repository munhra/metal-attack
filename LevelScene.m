//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  LevelScene.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 01/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "LevelScene.h"
#import "CCAnimation.h"
#import "LevelSceneController.h"
#import "BandShoot.h"
#import "LevelCleared.h"
#import "ShieldPowerUp.h"
#import "GameOverScence.h"
#import "CCTextureCache.h"
#import "MetalAttackAnalytics.h"
#import "FullEndScene.h"
#import "GameSoundManager.h"


@implementation LevelScene

#warning this must be used when parametrizing the max angle
// ** information about angle limit for each band guy
// 360
// 180
// it seams that the solution will have to be angle limits for all guitarrists and basses.
// use Y = A + B*X

// let say that the limit angle is 60 for the guitarrist
// 0 - 60
// 90 - 90
// 180 - 120
// example with 60 degrees of angle limit, the required here is to
// find a faster way to calculate the equation parameters
// not flipped
// Y = 60 + 0.3333 * X
// flipped
// Y = 180 + 0.3333 * X

@synthesize bandSprite;
@synthesize beginTouch;
@synthesize endTouch;
@synthesize rotationAngle;
@synthesize moved;
@synthesize activeEnemies;
@synthesize waveEnemiesLeft;
@synthesize levelEnemiesLeft;
@synthesize gameState;
@synthesize scoreCount;
@synthesize waveNumber;
@synthesize levelNumber;
@synthesize touchEnded;
@synthesize playerTouch;
@synthesize coinAnimFrames;
@synthesize coinAnimation;
@synthesize currentLevel;
@synthesize coin;
@synthesize totalLevel;
@synthesize door;
@synthesize gameHudSprite;
@synthesize gamePause;
@synthesize frontAmp;
@synthesize backAmp;
@synthesize leftWall;
@synthesize rightWall;
@synthesize garageDoor;
@synthesize killedEnemies;
@synthesize testRectangle;
@synthesize testRectangle2;
@synthesize consumeItemButton;
@synthesize coinMultiplier;
@synthesize noCoolDown;
@synthesize shield1Sprite;
@synthesize shield2Sprite;
@synthesize shield3Sprite;
@synthesize shield4Sprite;
@synthesize shield5Sprite;
@synthesize shield6Sprite;
@synthesize shield7Sprite;
@synthesize shield8Sprite;
@synthesize shield9Sprite;
@synthesize shield10Sprite;
@synthesize itemBeltButton;
@synthesize itemBeltMenu;
@synthesize itemPackSprite;
@synthesize menuItemRight;
@synthesize menuItemUp;
@synthesize activeShieldType;
@synthesize shieldSpriteArray;
@synthesize shieldPositionArray;
@synthesize revivalPowerUp;
@synthesize healthPowerUp;
@synthesize waveClearedSprite;
@synthesize levelContext;
@synthesize treeSprite;
@synthesize backgroundSprite;
@synthesize rewardVideoDelegate;
@synthesize levelCoins;
@synthesize jewelsArray;

//Level 1
@synthesize garagePartsGroup;
@synthesize garagePartsGroupUp;

//Level 2
@synthesize schoolFlagsGroup;
@synthesize SchoolUpGroupParts;

//Level 3
@synthesize barUpPartsContainer;
@synthesize barPoolContainer;
@synthesize barCounterContainer;

//Level 4
@synthesize prisionContainerUp;
@synthesize prisionContainerLeft;
@synthesize prisionContainerRight;

//Level 5
@synthesize gasStationContainerUP;
@synthesize gasStationContainerRight;


//powerups
@synthesize guita1PowerUp;
@synthesize guita2PowerUp;
@synthesize vocalPowerUp;
@synthesize bassPowerUp;
@synthesize drummerPowerUp;
@synthesize allBandPowerUp;
@synthesize invulnerabilityPowerUP;


@synthesize guita1RevivalPowerUp;
@synthesize guita2RevivalPowerUp;
@synthesize bassRevivalPowerUp;
@synthesize vocalRevivalPowerUp;
@synthesize drummerRevivalPowerUp;

@synthesize invulAllSchedule;
@synthesize invulBassSchedule;
@synthesize invulVocalSchedule;
@synthesize invulGuitarSchedule;
@synthesize invulDrummerSchedule;

//@synthesize interstitial;

// General z-index
const int bandShootZindex = 44;
const int bandSpriteZorder = 45; // 40

const int gameHudZindex = 80;
const int waveClearedZindex = 80;
const int frontAmpZindex = 41;
const int backAmpZindex = 24;

//Context 1 z-index constatns
const int garagePartsGroupZindex = 42;
const int garagePartsGroupUpZindex = 79;

//Context 2 z-index constatns
const int schoolFlagsGroupZindex = 90;
const int SchoolUpGroupPartsZindex = 79;

//Context 3 z-index constatns
const int barUpPartsContainerZindex = 79;
const int barPoolContainerZindex = 42;
const int barCounterContainerZindex = 20;

//Context 4 z-index constatns
const int prisionContainerUpZindex = 79;
const int prisionConteinerLeftZindex = 42;
const int prisionContainerRightZindex = 23;

//Context 5 z-index constatns
const int gasStationContainerRightZindex = 42;
const int gasStationContainerUpZindex = 79;

const int gasStationbackWallSpriteZindex = 79;
const int gasTopLeftSpriteZindex = 79;
const int rightGreenCarSpriteZindex = 42;
const int gasInformationSpriteZindex = 42;
const int gasRightBottomZindex = 79;

const int colisionOffSet = -25;
const int shieldZindex = 40;

CGSize screenSize;

CCSprite *selectedPowerUp1;
CCSprite *selectedPowerUp2;


-(id)init
{
    self = [super init];
    waveClearedSprite = (WaveClearedBanner*)[CCBReader load:@"WaveCleared"];
    levelCoins = 0;
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [[[self door] animationManager] runAnimationsForSequenceNamed:@"Closed"];
    [[[self door] animationManager] runAnimationsForSequenceNamed:@"OpenDoor"];
    [self doLevelEnterAnimation];
    [self.gameHudSprite setTotalEnemies:self.levelEnemiesLeft];
    [self setupSpecificLevelArt];
    [[MetalAttackAnalytics sharedInstance] registerScreenAnalytics:@"LevelScene"];
    [self addObservers];
    [[GameSoundManager sharedInstance] resetBandGuysState];
    [[GameSoundManager sharedInstance] playBandInstrumentsEffects:@[bandSprite.drummer.name,bandSprite.leadGuitar.name, bandSprite.baseGuitar.name, bandSprite.vocal.name, bandSprite.bass.name]];
    [self calculateTotalPowerConsumption];
    [self calculateTotalPowerRestoration];
    
    if ([[UniversalInfo sharedInstance] isTesting]) {
        [self setupTestTimer];
    }
    
    //[self updateShoot];
}

-(void)setupTestTimer
{
    __weak LevelScene *weakSelf = self;
    [self scheduleBlock:^(CCTimer *timer) {
        weakSelf.levelEnemiesLeft = 0;
    } delay:2];
}

-(void)removePowerUPSprite
{
    [guita1PowerUp removeFromParent];
    [guita2PowerUp removeFromParent];
    [vocalPowerUp removeFromParent];
    [bassPowerUp removeFromParent];
    [drummerPowerUp removeFromParent];
    [allBandPowerUp removeFromParent];
}

-(void)calculateTotalPowerConsumption {
    float totalPowerConsuption = bandSprite.drummerPowerConsumption + bandSprite.guitar2PowerConsumption + bandSprite.guitar1PowerConsumption + bandSprite.vocalPowerConsumption + bandSprite.bassPowerConsumption;
    [gameHudSprite setTotalPowerConsumption:totalPowerConsuption];
}

-(void)calculateTotalPowerRestoration {
    float totalPowerRestoration = bandSprite.drummerPowerRestoration + bandSprite.guitar2PowerRestoration + bandSprite.guitar1PowerRestoration + bandSprite.bassPowerRestoration + bandSprite.vocalPowerRestoration;
    [gameHudSprite setTotalPowerRestoration:totalPowerRestoration];
}


-(void)setupLevelContext1 {
    
    [self.garagePartsGroup setZOrder:garagePartsGroupZindex];
    [self.garagePartsGroupUp setZOrder:garagePartsGroupUpZindex];
    NSArray *sprtLvArray = [[NSArray alloc] initWithObjects:_sprtLv1AddOn1,_sprtLv1AddOn2,_sprtLv1AddOn3,_sprtLv1AddOn4,_sprtLv1AddOn5,_sprtLv1AddOn6,_sprtLv1AddOn7,_sprtLv1AddOn8,_sprtLv1AddOn9,_sprtLv1AddOn10,_sprtLv1AddOn11,_sprtLv1AddOn12,_sprtLv1AddOn13,_sprtLv1AddOn14,_sprtLv1AddOn15,_sprtLv1AddOn16,_sprtLv1AddOn17,_sprtLv1AddOn18,_sprtLv1AddOn19, nil];
    [self randomSelectLevelAddOn:sprtLvArray];
}

-(void)setupLevelContext2 {
    
    [[self schoolFlagsGroup] setZOrder:schoolFlagsGroupZindex];
    [[self SchoolUpGroupParts] setZOrder:SchoolUpGroupPartsZindex];
    NSArray *sprtLvArray = [[NSArray alloc] initWithObjects:_sprtLv2AddOn1,_sprtLv2AddOn2,_sprtLv2AddOn3,_sprtLv2AddOn4,_sprtLv2AddOn5,_sprtLv2AddOn6,_sprtLv2AddOn7,_sprtLv2AddOn8,_sprtLv2AddOn9,_sprtLv2AddOn10,_sprtLv2AddOn11,_sprtLv2AddOn12,_sprtLv2AddOn13,_sprtLv2AddOn14,_sprtLv2AddOn15,_sprtLv2AddOn16,_sprtLv2AddOn17, nil];
    [self randomSelectLevelAddOn:sprtLvArray];
}

-(void)setupLevelContext3 {
    
    [[self barUpPartsContainer] setZOrder:barUpPartsContainerZindex];
    [[self barPoolContainer] setZOrder:barPoolContainerZindex];
    [[self barCounterContainer] setZOrder:barCounterContainerZindex];
    NSArray *sprtLvArray = [[NSArray alloc] initWithObjects:_sprtLv3AddOn1,_sprtLv3AddOn2,_sprtLv3AddOn3,_sprtLv3AddOn4,_sprtLv3AddOn5,_sprtLv3AddOn6,_sprtLv3AddOn7,_sprtLv3AddOn8,_sprtLv3AddOn9,_sprtLv3AddOn10,_sprtLv3AddOn11,_sprtLv3AddOn12,_sprtLv3AddOn13,_sprtLv3AddOn14,_sprtLv3AddOn15,_sprtLv3AddOn16,_sprtLv3AddOn17,_sprtLv3AddOn18, _sprtLv3AddOn19, _sprtLv3AddOn20, _sprtLv3AddOn21, nil];
    [self randomSelectLevelAddOn:sprtLvArray];
}

-(void)setupLevelContext4 {
    
    [[self prisionContainerUp] setZOrder:prisionContainerUpZindex];
    [[self prisionContainerLeft] setZOrder:prisionConteinerLeftZindex] ;
    [[self prisionContainerRight] setZOrder:prisionContainerRightZindex];
    
    NSArray *sprtLvArray = [[NSArray alloc] initWithObjects:_sprtLv4AddOn1,_sprtLv4AddOn2,_sprtLv4AddOn3,_sprtLv4AddOn4,_sprtLv4AddOn5,_sprtLv4AddOn6,_sprtLv4AddOn7,_sprtLv4AddOn8,_sprtLv4AddOn9,_sprtLv4AddOn10,_sprtLv4AddOn11,_sprtLv4AddOn12,_sprtLv4AddOn13, nil];
    [self randomSelectLevelAddOn:sprtLvArray];
}

-(void)setupLevelContext5 {
    [[self gasStationContainerRight] setZOrder:gasStationContainerRightZindex];
    [[self gasStationContainerUP] setZOrder:gasStationContainerUpZindex];
    NSArray *sprtLvArray = [[NSArray alloc] initWithObjects:_sprtLv5AddOn1,_sprtLv5AddOn2,_sprtLv5AddOn3,_sprtLv5AddOn4,_sprtLv5AddOn5,_sprtLv5AddOn6,_sprtLv5AddOn7,_sprtLv5AddOn8,_sprtLv5AddOn9,_sprtLv5AddOn10,_sprtLv5AddOn11,_sprtLv5AddOn12,_sprtLv5AddOn13,_sprtLv5AddOn14,_sprtLv5AddOn15, nil];
    [self randomSelectLevelAddOn:sprtLvArray];
}

-(void)randomSelectLevelAddOn:(NSArray *)sprtarray {
    NSArray* levelsPerContextArray = [[LevelSceneController sharedInstance] levelsPerLevelContext];
    NSArray* firstLevelPerContextArray = [[LevelSceneController sharedInstance] firstLevelPerContext];
    NSNumber *firstLevelPerContext =(NSNumber *) [firstLevelPerContextArray objectAtIndex:levelContext - 1];
    NSNumber *levelsPerContext = (NSNumber *) [levelsPerContextArray objectAtIndex:levelContext - 1];
    float deltaLevelPerContext = levelNumber - [firstLevelPerContext intValue];
    float completeness = deltaLevelPerContext/([levelsPerContext intValue] - [firstLevelPerContext intValue]);
    int itemsPerCompleteness = MIN([sprtarray count], completeness * [sprtarray count]);
    for (int i = 0; i < itemsPerCompleteness; i++) {
        int aleatoryIndex = arc4random() % [sprtarray count];
        CCSprite* addOnSprt = [sprtarray objectAtIndex:aleatoryIndex];
        [addOnSprt setVisible:YES];
    }
}

-(void)setupSpecificLevelArt {
    switch (levelContext) {
        case 1:
            [self setupLevelContext1];
            break;
        case 2:
            [self setupLevelContext2];
            break;
        case 3:
            [self setupLevelContext3];
            break;
        case 4:
            [self setupLevelContext4];
            break;
        case 5:
            [self setupLevelContext5];
            break;
        default:
            break;
    }
}

-(void)doLevelEnterAnimation
{
    [waveClearedSprite setPosition:ccp(screenSize.width * 0.5,screenSize.height * 0.77)];
    [waveClearedSprite setScale:0.82];
    [self addChild:waveClearedSprite];
    [waveClearedSprite setZOrder:waveClearedZindex];
    [[waveClearedSprite animationManager] setDelegate:self];
    [[waveClearedSprite animationManager] runAnimationsForSequenceNamed:@"Show"];
    
    int displayLevelNumber = levelNumber +1;
    //int displayLevelNumber = levelNumber;
    NSString *waveNumberStr = [NSString stringWithFormat:@"%d",displayLevelNumber];
    [[waveClearedSprite waveNumberText] setString:waveNumberStr];
    [[waveClearedSprite waveText] setString:@"Level"];
    [[waveClearedSprite waveClearedText] setString:@"GO !"];
    
    __weak LevelScene *weakSelf = self;
    
    [[waveClearedSprite animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        NSLog(@"Wave cleared animation ended");
        [weakSelf.waveClearedSprite removeFromParent];
        [weakSelf.gameHudSprite stopSnakeAnimation];
        [weakSelf activateEnemies];
    }];
}

-(void)activateEnemies
{
    LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
    [lvcontroller startEnemyMovment];
    //[self updateShoot];
}

-(void)clearEnemyFire
{
    for (int j = 0; j< [[self activeEnemies] count]; j++){
        [[[self activeEnemies] objectAtIndex:j] unscheduleAllSelectors];
    }
}

-(void)didLoadFromCCB
{
    
    [[self gamePause] setVisible:NO];
    [[self gamePause] setZOrder:99];
    [[self gamePause] setLevelDelegate:self];
    
    self.touchEnded = YES;
    BandVault *bandVault = [BandVault sharedInstance];
    
    CGPoint location;
    location.x = 0;
    location.y = 160;
    
    screenSize = [[CCDirector sharedDirector] viewSize];
    
    self.beginTouch = location;
    self.moved = NO;
    self.gameState = GAME_STARTED;
    
    self.userInteractionEnabled = TRUE;
    self.activeEnemies = [[NSMutableArray alloc] init];
    
    [bandSprite setZOrder:bandSpriteZorder];
    [door setZOrder:100];
    
    [bandSprite setBandCoins:[bandVault bandCoins]];
    [[self gameHudSprite] setDelegate:self];
    [self.gameHudSprite setZOrder:gameHudZindex];
    [self.menuItemRight setZOrder:gameHudZindex];
    [self.menuItemUp setZOrder:gameHudZindex];
    
    [[self frontAmp] setZOrder:frontAmpZindex];
    [[self backAmp] setZOrder:backAmpZindex];
    
    [self.itemBeltMenu setDelegate:self];
    [self.itemBeltButton setDelegate:self];
    
    
    self.killedEnemies = [[NSMutableArray alloc] init];
    
    #warning draw a grid to see the points remove it after debug
    //[self drawGrid];
    
    BandStoreItem *firstSelectedItem = nil;
    if (firstSelectedItem){
        
        if (![firstSelectedItem.itemSprt spriteFrame]){
            NSLog(@"Nil sprite frame");
        }
        
        [consumeItemButton setBackgroundSpriteFrame:[firstSelectedItem.itemSprt spriteFrame] forState:CCControlStateNormal];
    }else{
        [consumeItemButton setTitle:@"No Item"];
    }
    
    self.coinMultiplier = 1;
    
    [self.gameHudSprite setDrummerFig:self.bandSprite.drummerLife];
    [self.gameHudSprite setBaseGuitarFig:self.bandSprite.baseLife];
    [self.gameHudSprite setBassFig:self.bandSprite.bassLife];
    [self.gameHudSprite setVocalFig:self.bandSprite.vocalLife];
    [self.gameHudSprite setLeadGuitarFig:self.bandSprite.leadLife];
    
    self.noCoolDown = NO;

    [self setMultipleTouchEnabled:YES];
    [itemPackSprite loadItemButtonsWithDelegate:self];
    [itemPackSprite setZOrder:90];
    
    itemPackSprite = nil;
    
    self.activeShieldType = -1;
    
    self.invulnerabilityPowerUP = [[InvulnerabilityPowerUP alloc] init];
    [self.invulnerabilityPowerUP preparePowerUps];
    
    self.revivalPowerUp = [[RevivalPowerUp alloc] init];
    [self.revivalPowerUp preparePowerUps];
    
    self.coinMultiplierPowerUp = [[CoinMultiplierPowerUp alloc] init];
    [self.coinMultiplierPowerUp preparePowerUps:bandSprite.position];
    [self.coinMultiplierPowerUp setLevelDelegate:self];
    
    self.healthPowerUp = [[HealthPowerUp alloc] init];
    [self.healthPowerUp preparePowerUps:bandSprite.position];
    [self.healthPowerUp setLevelDelegate:self];
    
    
    [[self gamePause] loadItemSpriteFrame];
    [[self waveClearedSprite] removeFromParent];
    
    [self loadJewels];
    
    NSLog(@"Enemies Left %d",self.levelEnemiesLeft);

    [[CCTextureCache sharedTextureCache] dumpCachedTextureInfo];
    [self preloadBandGuysSound];
}

-(void)preloadBandGuysSound
{
    [[GameSoundManager sharedInstance] preloadBandGuySounds:@[bandSprite.drummer.name,bandSprite.leadGuitar.name, bandSprite.baseGuitar.name, bandSprite.vocal.name, bandSprite.bass.name]];
}

-(void)unloadBandGuysSound
{
    [[GameSoundManager sharedInstance] unloadBandGuySounds:@[bandSprite.drummer.name,bandSprite.leadGuitar.name, bandSprite.baseGuitar.name, bandSprite.vocal.name, bandSprite.bass.name]];
}

-(void)loadJewels
{
    jewelsArray = [[NSMutableArray alloc] initWithCapacity:9];
    for (int i = 1; i <= 9; i++) {
        NSString* jewelFileName = [NSString stringWithFormat:@"OtherSprites/jewels/jewel0%d",i];
        CCSprite* jewelSprite = (CCSprite *)[CCBReader load:jewelFileName];
        [jewelsArray addObject:jewelSprite];
    }
}

-(CCSprite *)selectRandomJewel
{
    int randomIndex = arc4random() % [jewelsArray count];
    return [jewelsArray objectAtIndex:randomIndex];
}

-(void)drawGrid{
    CCDrawNode *dot = [[CCDrawNode alloc] init];
    CCDrawNode *segment;
    
    for (int i = 0; i <= 31; i++) {
        segment = [[CCDrawNode alloc] init];
        [segment drawSegmentFrom:ccp(16*i, 0) to:ccp(16*i,512) radius:0.5 color:[CCColor magentaColor]];
        [self addChild:segment z:85];
    }
    
    [dot drawDot:ccp(171,247) radius:1 color:[CCColor greenColor]];
    [self addChild:dot z:85];
}

-(void)performPowerUp:(CGPoint)point coinDropLimits:(CGPoint)dropLimits;
{
    
    CCSprite *powerUp;
    int randomCoin = dropLimits.x + arc4random_uniform(dropLimits.y + 1);
    randomCoin = MIN(randomCoin, [jewelsArray count] -1);
    
    if (randomCoin == [jewelsArray count] - 1) {
        NSLog(@"max random coin");
    }
    
    CCSprite *randomJewel = [jewelsArray objectAtIndex:randomCoin];
    
    id jump;
    id fadeOut;
    id blink;
    id actionBlockSound;
    CCActionSequence *actionSequence;
    
    powerUp = [[CCSprite alloc] initWithSpriteFrame:[randomJewel spriteFrame]];
    [powerUp setScale:0.85];
    [powerUp setZOrder:gameHudZindex];
    powerUp.position = point;
    [self addChild:powerUp];
    jump = [CCActionJumpTo actionWithDuration:1 position:[[UniversalInfo sharedInstance] screenCenter] height:250 jumps:1];
    fadeOut = [CCActionFadeOut actionWithDuration:0.1];
    blink = [CCActionBlink actionWithDuration:0.5 blinks:10];
    actionBlockSound = [[CCActionCallBlock alloc] initWithBlock:^{
        NSLog(@"Play coin fx");
        NSArray *soundArray = @[@"_Soundfx046",@"_Soundfx047",@"_Soundfx048",@"_Soundfx049"];
        int randomIndex = arc4random() % [soundArray count];
        NSString *randomSound = [soundArray objectAtIndex:randomIndex];
        [[GameSoundManager sharedInstance] playSoundEffectByName:randomSound];
    }];
    
    actionSequence = [CCActionSequence actions:jump,actionBlockSound,blink,fadeOut, nil];
    
    [powerUp runAction:actionSequence];
    
    switch (levelContext) {
        case 1:
        randomCoin = randomCoin * 1;
        break;
        case 2:
        randomCoin = randomCoin * 1;
        break;
        case 3:
        randomCoin = randomCoin * 2;
        break;
        case 4:
        randomCoin = randomCoin * 2;
        break;
        case 5:
        randomCoin = randomCoin * 3;
        break;
        default:
        break;
    }
    
    self.bandSprite.bandCoins = self.bandSprite.bandCoins + randomCoin * coinMultiplier;
    self.levelCoins = self.levelCoins + randomCoin * coinMultiplier;
}

-(BOOL)detectMeeleImprovedColision:(CGRect)enemyRect playerSprite:(CCSprite *)playerSprt
{
    int index = 1;
    CCSprite *colSprite = (CCSprite *)[playerSprt getChildByName:@"ColisionRect1" recursively:NO];
    CGRect playerColRect = colSprite.boundingBox;
    CGRect playerRect = [playerSprt boundingBox];
    
    while (colSprite) {
        
        playerColRect.origin = ccpAdd([[self bandSprite] convertToWorldSpace:playerRect.origin], playerColRect.origin);
        if (!CGRectIsNull(CGRectIntersection(enemyRect, playerColRect))) {
            return YES;
        }
        
        index++;
        NSString *indexStr = [NSString stringWithFormat:@"ColisionRect%d",index];
        colSprite = (CCSprite *)[playerSprt getChildByName:indexStr recursively:NO];
        playerColRect = colSprite.boundingBox;
    }
    
    return NO;
}

-(void)checkEnemyHeroColision
{
    // this method will check if the a enemy has collided with each one of the heroes
    // this will only be valid for meele and autodestruction enemies
    // Maybe it will be required to parametrize the inset values.
    
    CGRect guita1Rect;
    CGRect guita2Rect;
    CGRect bassRect;
    CGRect drummerRect;
    CGRect vocalRectUp;
    CGRect vocalRectDn;
    
    CGRect guita1OrgRect;
    CGRect guita2OrgRect;
    CGRect bassOrgRect;
    CGRect drummerOrgRect;
    CGRect vocalOrgRectUp;
    CGRect vocalOrgRectDn;
    
    guita1Rect = [[[[self bandSprite] leadGuitar] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    guita2Rect = [[[[self bandSprite] baseGuitar] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    drummerRect = [[[[self bandSprite] drummer] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    bassRect = [[[[self bandSprite] bass] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    vocalRectDn = [[[[self bandSprite] vocal] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    vocalRectUp = CGRectMake(0, 0, 0, 0);

    guita1OrgRect = [[[self bandSprite] leadGuitar] boundingBox];
    guita2OrgRect = [[[self bandSprite] baseGuitar] boundingBox];
    bassOrgRect = [[[self bandSprite] bass] boundingBox];
    drummerOrgRect = [[[self bandSprite] drummer] boundingBox];
    vocalOrgRectDn = [[[self bandSprite] vocal] boundingBox];
    vocalOrgRectUp = [[[[[self bandSprite] vocal] children] firstObject] boundingBox];
    
    guita1Rect.origin = ccpAdd([[self bandSprite] convertToWorldSpace:guita1OrgRect.origin], guita1Rect.origin);
    guita2Rect.origin = ccpAdd([[self bandSprite] convertToWorldSpace:guita2OrgRect.origin], guita2Rect.origin);
    bassRect.origin = ccpAdd([[self bandSprite] convertToWorldSpace:bassOrgRect.origin], bassRect.origin);
    drummerRect.origin = ccpAdd([[self bandSprite] convertToWorldSpace:drummerOrgRect.origin], drummerRect.origin);
    vocalRectDn.origin = ccpAdd([[self bandSprite] convertToWorldSpace:vocalOrgRectDn.origin], vocalRectDn.origin);
    
    for (int i = 0; i< [[self activeEnemies] count]; i++){
        
        CGRect enemyBoundBox;
        
        if ([[[[self activeEnemies] objectAtIndex:i] enemySprite] getChildByName:@"meeleAttackRect" recursively:NO]){
            enemyBoundBox = [[[[[self activeEnemies] objectAtIndex:i] enemySprite] getChildByName:@"meeleAttackRect" recursively:NO] boundingBox];
            enemyBoundBox.origin = [[[[self activeEnemies] objectAtIndex:i] enemySprite] convertToWorldSpace:enemyBoundBox.origin];
            enemyBoundBox.size.width = 4;
            enemyBoundBox.size.height = 4;
        }else{
            enemyBoundBox = CGRectInset([[[[self activeEnemies] objectAtIndex:i] enemySprite] boundingBox],15, 15);
        }
    
        //if (!CGRectIsNull(CGRectIntersection(enemyBoundBox, guita1Rect)) && !([[self bandSprite] isGuita1Death])){
        if ([self detectMeeleImprovedColision:enemyBoundBox playerSprite:[bandSprite leadGuitar]] && !([[self bandSprite] isGuita1Death])){
            //NSLog(@"guita1Rect was hitted by enemy");
            [[self gameHudSprite] updateBandFaceState:GUITARRIST1_TYPE isDead:NO isRevival:NO withBandSprite:bandSprite];
            if ([[[self activeEnemies] objectAtIndex:i] attackType] == MEELEE){
                [[[self activeEnemies] objectAtIndex:i] meeleAttack:self.bandSprite bandComponent:GUITARRIST1_TYPE hitrect:CGRectIntersection(enemyBoundBox,guita1Rect)];
            }else if ([[[self activeEnemies] objectAtIndex:i] attackType] == AUTODESTRUCTION){
                float damagePoints = [[[self activeEnemies] objectAtIndex:i] autoDestruction];
                [[[self activeEnemies] objectAtIndex:i] setRemoveObject:YES];
                [[self bandSprite] hitByEnemy:damagePoints component:GUITARRIST1_TYPE rect:CGRectIntersection(enemyBoundBox, guita1Rect)];
            }
        }else if ([[self bandSprite] isGuita1Death]){
            [[self gameHudSprite] updateBandFaceState:GUITARRIST1_TYPE isDead:YES isRevival:NO withBandSprite:bandSprite];
            [[GameSoundManager sharedInstance] muteLeadGuitar];
        }
        
        //[self detectMeeleImprovedColision:enemyBoundBox playerSprite:[bandSprite baseGuitar]];
        if ([self detectMeeleImprovedColision:enemyBoundBox playerSprite:[bandSprite baseGuitar]] && !([[self bandSprite] isGuita2Death])){
        //if (!CGRectIsNull(CGRectIntersection(enemyBoundBox, guita2Rect)) && !([[self bandSprite] isGuita2Death])){
            //NSLog(@"guita2Rect was hitted by enemy");
            [[self gameHudSprite] updateBandFaceState:GUITARRIST2_TYPE isDead:NO isRevival:NO withBandSprite:bandSprite];
            if ([[[self activeEnemies] objectAtIndex:i] attackType] == MEELEE){
                [[[self activeEnemies] objectAtIndex:i] meeleAttack:self.bandSprite bandComponent:GUITARRIST2_TYPE hitrect:CGRectIntersection(enemyBoundBox,guita2Rect)];
            }else if ([[[self activeEnemies] objectAtIndex:i] attackType] == AUTODESTRUCTION){
                float damagePoints = [[[self activeEnemies] objectAtIndex:i] autoDestruction];
                [[[self activeEnemies] objectAtIndex:i] setRemoveObject:YES];
                [[self bandSprite] hitByEnemy:damagePoints component:GUITARRIST2_TYPE rect:CGRectIntersection(enemyBoundBox, guita2Rect)];
            }
        }else if ([[self bandSprite] isGuita2Death]){
            [[self gameHudSprite] updateBandFaceState:GUITARRIST2_TYPE isDead:YES isRevival:NO withBandSprite:bandSprite];
            [[GameSoundManager sharedInstance] muteBaseGuitar];
        }
        
        if ([self detectMeeleImprovedColision:enemyBoundBox playerSprite:[bandSprite bass]] && !([[self bandSprite] isBassDeath])){
        //if (!CGRectIsNull(CGRectIntersection(enemyBoundBox, bassRect)) && !([[self bandSprite] isBassDeath])){
            //NSLog(@"bassRect was hitted by enemy");
            [[self gameHudSprite] updateBandFaceState:BASS_TYPE isDead:NO isRevival:NO withBandSprite:bandSprite];
            if ([[[self activeEnemies] objectAtIndex:i] attackType] == MEELEE){
                [[[self activeEnemies] objectAtIndex:i] meeleAttack:self.bandSprite bandComponent:BASS_TYPE hitrect:CGRectIntersection(enemyBoundBox,bassRect)];
            }else if ([[[self activeEnemies] objectAtIndex:i] attackType] == AUTODESTRUCTION){
                float damagePoints = [[[self activeEnemies] objectAtIndex:i] autoDestruction];
                [[[self activeEnemies] objectAtIndex:i] setRemoveObject:YES];
                [[self bandSprite] hitByEnemy:damagePoints component:BASS_TYPE rect:CGRectIntersection(enemyBoundBox, bassRect)];
            }
        }else if ([[self bandSprite] isBassDeath]){
            [[self gameHudSprite] updateBandFaceState:BASS_TYPE isDead:YES isRevival:NO withBandSprite:bandSprite];
            [[GameSoundManager sharedInstance] muteBass];
        }
        
        if (!CGRectIsNull(CGRectIntersection(enemyBoundBox, drummerRect)) && !([[self bandSprite] isDrummerDeath])){
            //NSLog(@"drummerRect was hitted by enemy");
            [[self gameHudSprite] updateBandFaceState:DRUMMER_TYPE isDead:NO isRevival:NO withBandSprite:bandSprite];
            if ([[[self activeEnemies] objectAtIndex:i] attackType] == MEELEE){
                [[[self activeEnemies] objectAtIndex:i] meeleAttack:self.bandSprite bandComponent:DRUMMER_TYPE hitrect:CGRectIntersection(enemyBoundBox,drummerRect)];
            }else if ([[[self activeEnemies] objectAtIndex:i] attackType] == AUTODESTRUCTION){
                float damagePoints = [[[self activeEnemies] objectAtIndex:i] autoDestruction];
                [[[self activeEnemies] objectAtIndex:i] setRemoveObject:YES];
                [[self bandSprite] hitByEnemy:damagePoints component:DRUMMER_TYPE rect:CGRectIntersection(enemyBoundBox, drummerRect)];
            }
        }else if ([[self bandSprite] isDrummerDeath]){
            [[self gameHudSprite] updateBandFaceState:DRUMMER_TYPE isDead:YES isRevival:NO withBandSprite:bandSprite];
            [[GameSoundManager sharedInstance] muteDrummer];
        }
        
        if (!CGRectIsNull(CGRectIntersection(enemyBoundBox, vocalRectDn)) && !([[self bandSprite] isVocalDeath])){
            //NSLog(@"vocal was hitted by enemy");
            [[self gameHudSprite] updateBandFaceState:VOCALS_TYPE isDead:NO isRevival:NO withBandSprite:bandSprite];
            if ([[[self activeEnemies] objectAtIndex:i] attackType] == MEELEE){
                [[[self activeEnemies] objectAtIndex:i] meeleAttack:self.bandSprite bandComponent:VOCALS_TYPE hitrect:CGRectIntersection(enemyBoundBox,vocalRectDn)];
            }else if ([[[self activeEnemies] objectAtIndex:i] attackType] == AUTODESTRUCTION){
                float damagePoints = [[[self activeEnemies] objectAtIndex:i] autoDestruction];
                [[[self activeEnemies] objectAtIndex:i] setRemoveObject:YES];
                [[self bandSprite] hitByEnemy:damagePoints component:VOCALS_TYPE rect:CGRectIntersection(enemyBoundBox, vocalRectDn)];
            }
        }else if ([[self bandSprite] isVocalDeath]){
            [[self gameHudSprite] updateBandFaceState:VOCALS_TYPE isDead:YES isRevival:NO withBandSprite:bandSprite];
            [[GameSoundManager sharedInstance] muteVocal];
        }
    }
}
-(void)removeEnemyShootAfterColision:(CCNode *)shoot
{
    [shoot stopAllActions];
    [shoot removeFromParent];
}

-(BOOL)robotShootCollidedHero
{
    
    
    
    CGRect guita1Rect;
    CGRect guita2Rect;
    CGRect bassRect;
    CGRect drummerRect;
    CGRect vocalRectUp;
    CGRect vocalRectDn;
    
    CGRect guita1OrgRect;
    CGRect guita2OrgRect;
    CGRect bassOrgRect;
    CGRect drummerOrgRect;
    CGRect vocalOrgRectUp;
    CGRect vocalOrgRectDn;
    
    guita1Rect = [[[[self bandSprite] leadGuitar] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    guita2Rect = [[[[self bandSprite] baseGuitar] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    drummerRect = [[[[self bandSprite] drummer] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    bassRect = [[[[self bandSprite] bass] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    vocalRectDn = [[[[self bandSprite] vocal] getChildByName:@"ColisionRect" recursively:NO] boundingBox];
    vocalRectUp = CGRectMake(0, 0, 0, 0);
    
    guita1OrgRect = [[[self bandSprite] leadGuitar] boundingBox];
    guita2OrgRect = [[[self bandSprite] baseGuitar] boundingBox];
    bassOrgRect = [[[self bandSprite] bass] boundingBox];
    drummerOrgRect = [[[self bandSprite] drummer] boundingBox];
    vocalOrgRectDn = [[[self bandSprite] vocal] boundingBox];
    vocalOrgRectUp = [[[[[self bandSprite] vocal] children] firstObject] boundingBox];
    
    guita1Rect.origin = ccpAdd([[self bandSprite] convertToWorldSpace:guita1OrgRect.origin], guita1Rect.origin);
    guita2Rect.origin = ccpAdd([[self bandSprite] convertToWorldSpace:guita2OrgRect.origin], guita2Rect.origin);
    bassRect.origin = ccpAdd([[self bandSprite] convertToWorldSpace:bassOrgRect.origin], bassRect.origin);
    drummerRect.origin = ccpAdd([[self bandSprite] convertToWorldSpace:drummerOrgRect.origin], drummerRect.origin);
    vocalRectDn.origin = ccpAdd([[self bandSprite] convertToWorldSpace:vocalOrgRectDn.origin], vocalRectDn.origin);
    
    CGRect shootRect;
    
    /*
    
    CGRect shootRect;
    CGRect guita1Rect;
    CGRect guita2Rect;
    CGRect bassRect;
    CGRect drummerRect;
    CGRect vocalRectUp;
    CGRect vocalRectDn;
    
    guita1Rect = CGRectInset([[[self bandSprite] leadGuitar] boundingBox],5, 5);
    guita2Rect = CGRectInset([[[self bandSprite] baseGuitar] boundingBox],5, 5);
    bassRect = CGRectInset([[[self bandSprite] baseGuitar] boundingBox],5, 5);
    drummerRect = CGRectInset([[[self bandSprite] drummer] boundingBox],5, 5);
    vocalRectDn = CGRectInset([[[self bandSprite] vocal] boundingBox],5, 5);
    vocalRectUp = CGRectInset([[[[[self bandSprite] vocal] children] firstObject] boundingBox],5, 5);

    guita1Rect.origin = [[self bandSprite] convertToWorldSpace:guita1Rect.origin];
    guita2Rect.origin = [[self bandSprite] convertToWorldSpace:guita2Rect.origin];
    drummerRect.origin = [[self bandSprite] convertToWorldSpace:drummerRect.origin];
    vocalRectDn.origin = [[self bandSprite] convertToWorldSpace:vocalRectDn.origin];
    vocalRectUp.origin = [[self bandSprite] convertToWorldSpace:vocalRectUp.origin];
    bassRect.origin = [[self bandSprite] convertToWorldSpace:bassRect.origin];
    
     */

    CCNode *robotShoot;
    
    bool removeRobotShoot = NO;
    
    for (int j = 0; j< [[self activeEnemies] count]; j++){
        for (int i = 0; i < [[[[self activeEnemies] objectAtIndex:j] activeShoots] count] ; i++){
            
            removeRobotShoot = NO;
            
            robotShoot = [[[[self activeEnemies] objectAtIndex:j] activeShoots] objectAtIndex:i];
            shootRect = [robotShoot boundingBox];
            
            shootRect.origin = [self convertToWorldSpace:shootRect.origin];
            
            // if there is a collision remove the enemy shoot.
            // a good implementation is to not remove the shoot when the
            // hero is death.
            
            /*
            if (!CGRectIsNull(CGRectIntersection(shootRect, guita1Rect)) ||
                !CGRectIsNull(CGRectIntersection(shootRect, guita2Rect)) ||
                !CGRectIsNull(CGRectIntersection(shootRect, bassRect))   ||
                !CGRectIsNull(CGRectIntersection(shootRect, drummerRect))||
                !CGRectIsNull(CGRectIntersection(shootRect, vocalRectDn))||
                !CGRectIsNull(CGRectIntersection(shootRect, vocalRectUp))){
                [robotShoot stopAllActions];
                [self removeChild:robotShoot cleanup:YES];
                [[[[self activeEnemies] objectAtIndex:j] activeShoots] removeObjectAtIndex:i];
            }*/
        
            bool leadGuitarCollision = [self detectMeeleImprovedColision:shootRect playerSprite:[[self bandSprite] leadGuitar]];
            bool baseGuitarCollision = [self detectMeeleImprovedColision:shootRect playerSprite:[[self bandSprite] baseGuitar]];
            bool bassCollision = [self detectMeeleImprovedColision:shootRect playerSprite:[[self bandSprite] bass]];
            
            //[self detectMeeleImprovedColision:shootRect playerSprite:[[self bandSprite] drummer]];
            
            //[self detectMeeleImprovedColision:shootRect playerSprite:[[self bandSprite] vocal]];
            
#warning include here the invunerability if
            // check if the hero is dead.
            if (leadGuitarCollision && !([[self bandSprite] isGuita1Death])){
            
                //NSLog(@"guita1Rect was hitted by enemy");
                //change autoDestruction for blaster power
                float damagePoints = [[[self activeEnemies] objectAtIndex:j] weaponDamage];
                [[self bandSprite] hitByEnemy:damagePoints component:GUITARRIST1_TYPE rect:CGRectIntersection(shootRect, guita1Rect)];
                removeRobotShoot = YES;
                //[self removeEnemyShootAfterColision:robotShoot];
            
            }
            
            if (baseGuitarCollision && !([[self bandSprite] isGuita2Death])){
                //NSLog(@"guita2Rect was hitted by enemy");
                //change autoDestruction for blaster power
                float damagePoints = [[[self activeEnemies] objectAtIndex:j] weaponDamage];
                [[self bandSprite] hitByEnemy:damagePoints component:GUITARRIST2_TYPE rect:CGRectIntersection(shootRect, guita2Rect)];
                //[self removeEnemyShootAfterColision:robotShoot];
                removeRobotShoot = YES;
            }
            
            if (bassCollision && !([[self bandSprite] isBassDeath])){
                //NSLog(@"bassRect was hitted by enemy");
                //change autoDestruction for blaster power
                float damagePoints = [[[self activeEnemies] objectAtIndex:j] weaponDamage];
                [[self bandSprite] hitByEnemy:damagePoints component:BASS_TYPE rect:CGRectIntersection(shootRect, bassRect)];
                //[self removeEnemyShootAfterColision:robotShoot];
                removeRobotShoot = YES;
            }
            
            if (!CGRectIsNull(CGRectIntersection(shootRect, drummerRect)) && !([[self bandSprite] isDrummerDeath])){
                //NSLog(@"drummerRect was hitted by enemy");
                //change autoDestruction for blaster power
                float damagePoints = [[[self activeEnemies] objectAtIndex:j] weaponDamage];
                [[self bandSprite] hitByEnemy:damagePoints component:DRUMMER_TYPE rect:CGRectIntersection(shootRect, drummerRect)];
                //[self removeEnemyShootAfterColision:robotShoot];
                removeRobotShoot = YES;
            }
            
            if (!CGRectIsNull(CGRectIntersection(shootRect, vocalRectDn)) && !([[self bandSprite] isVocalDeath])){
                //NSLog(@"vocal was hitted by enemy");
                //change autoDestruction for blaster power
                float damagePoints = [[[self activeEnemies] objectAtIndex:j] weaponDamage];
                [[self bandSprite] hitByEnemy:damagePoints component:VOCALS_TYPE rect:CGRectIntersection(shootRect, vocalRectDn)];
                //[self removeEnemyShootAfterColision:robotShoot];
                removeRobotShoot = YES;
            }
            
            /*
            if (!CGRectIsNull(CGRectIntersection(shootRect, vocalRectUp)) && !([[self bandSprite] isVocalDeath])){
                //NSLog(@"vocal was hitted by enemy");
                //change autoDestruction for blaster power
                float damagePoints = [[[self activeEnemies] objectAtIndex:j] weaponDamage];
                [[self bandSprite] hitByEnemy:damagePoints component:VOCALS_TYPE rect:CGRectIntersection(shootRect, vocalRectUp)];
                //[self removeEnemyShootAfterColision:robotShoot];
                removeRobotShoot = YES;
            }*/
        
            if (removeRobotShoot) {
                
                [robotShoot stopAllActions];
                [self removeChild:robotShoot cleanup:YES];
                [[[[self activeEnemies] objectAtIndex:j] activeShoots] removeObjectAtIndex:i];
                
            }
        
        }
    }
    return YES;
}

-(BOOL)shootCollidedToTarget
{
    CGRect targetRect;
    CGRect shootRect;
    CGRect heroRect = [[self bandSprite] boundingBox];
    
    for (int j = 0; j< [[self activeEnemies] count]; j++){
        
        targetRect = [[[[self activeEnemies] objectAtIndex:j] enemySprite] boundingBox];
        for (int i = 0; i< [[bandSprite activeShoots] count]; i++){
            shootRect = [(CCNode *)[[bandSprite activeShoots] objectAtIndex:i] boundingBox];
            if (!CGRectIsNull(CGRectIntersection(shootRect, targetRect))){
                
                //first destroy the target.
                int enemyScore = [[[self activeEnemies] objectAtIndex:j]
                                  receiveHeroShoot:[[self bandSprite] shootPowerWithMegaShoot] killNow:NO shootRect:shootRect];
                
                self.scoreCount = self.scoreCount + enemyScore;
                [[[bandSprite activeShoots] objectAtIndex:i] setRemoveObject:YES];
                
                if ([[[self activeEnemies] objectAtIndex:j] armor] <= 0){
                    [[[self activeEnemies] objectAtIndex:j] setRemoveObject:YES];
                }
            }
        }
        
        //On this point the enemy reached the hero this will work for autodestruction
        //enemies not for all, it should be put on a separeted method.
        if (!CGRectIsNull(CGRectIntersection(heroRect, targetRect))){
            // The enemy gets the hero
            
            if ([[[self activeEnemies] objectAtIndex:j] attackType] == MEELEE){
                //[[[self activeEnemies] objectAtIndex:j] meeleAttack:self.bandSprite];
            }else{
                //float damagePoints = [[[self activeEnemies] objectAtIndex:j] autoDestruction];
                //[[[self activeEnemies] objectAtIndex:j] setRemoveObject:YES];
                //[[self bandSprite] hitByEnemy:damagePoints];
            }
        }
    }
    
    // clean fire and enemy arrays
    
    for (int j = 0; j< [[self activeEnemies] count]; j++){
        if ([[[self activeEnemies] objectAtIndex:j] removeObject] == YES) {
            // check if it is autodestruction enemy
            
            if ([[[self activeEnemies] objectAtIndex:j] attackType] == AUTODESTRUCTION) {
                [self.killedEnemies addObject:[[self activeEnemies] objectAtIndex:j]];
            }
            // Check for power up before remove the enemy. put this if again to include the coins
            //if ([[[self activeEnemies] objectAtIndex:j] typeOfPowerUp] != NOPOWERUP) {
            //[self performPowerUp:[[[self activeEnemies] objectAtIndex:j] typeOfPowerUp]
            //        initialPoint:[[[[self activeEnemies] objectAtIndex:j] enemySprite] position]];
            //}
            
            Enemy *enemy = [[self activeEnemies] objectAtIndex:j];
            [self performPowerUp:enemy.initialPosition coinDropLimits:ccp(enemy.dropCoinLowerLevel,enemy.dropCoinHighLevel)];
            
            
            [[self activeEnemies] removeObjectAtIndex:j];
            self.waveEnemiesLeft = self.waveEnemiesLeft - 1;
            self.levelEnemiesLeft = self.levelEnemiesLeft - 1;
            
            [[self gameHudSprite] updateSnakeBonesProgress:self.levelEnemiesLeft];
            
            //NSLog(@"Level Enemies left %d",self.levelEnemiesLeft);
            //NSLog(@"Wave Enemies left %d",self.levelEnemiesLeft);
        }
    }
    
    for (int i = 0; i< [[bandSprite activeShoots] count]; i++){
        if ([[[[self bandSprite] activeShoots] objectAtIndex:i] removeObject] == YES){
            [[[bandSprite activeShoots] objectAtIndex:i] stopAllActions];
            CCNode *shootParent = [[[bandSprite activeShoots] objectAtIndex:i] parent];
            [shootParent removeChild:[[bandSprite activeShoots] objectAtIndex:i]];
            [[bandSprite activeShoots] removeObjectAtIndex:i];
        }
    }
    
    return YES;
}

-(void)unscheduleAllEnemies {
    for (Enemy *enemy in [self activeEnemies]) {
        [enemy.enemyShootTimer invalidate];
        [enemy unscheduleAllSelectors];
    }
}

-(void)updateShoot:(CCTime)delta
{
    for (Enemy *enemy in [self activeEnemies]) {
        [enemy update:delta];
    }
}


-(void)updateBandCoins
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    [userdef setInteger:[[self bandSprite] bandCoins] forKey:@"coins"];
    int totalCoins = [[BandVault sharedInstance] totalBandCoins] + self.levelCoins;
    [[BandVault sharedInstance] setTotalBandCoins:totalCoins];
    [userdef setInteger:totalCoins forKey:@"totalcoins"];
    [userdef setInteger:[self scoreCount] forKey:@"highScore"];
    BandVault *vault = [BandVault sharedInstance];
    vault.bandCoins = [[self bandSprite] bandCoins];
    vault.highScore = scoreCount;
    [[BandVault sharedInstance] sendScoreToLeaderBoard];
}

-(void)update:(CCTime)delta
{
    if (([[CCDirector sharedDirector]isPaused] == NO) && (gameState != GAME_OVER)){
        [self shootCollidedToTarget];
        [self robotShootCollidedHero];
        [self checkEnemyHeroColision];
        [self updateShoot:delta];
        
        
        if ((!self.touchEnded) && ([[self bandSprite] bandReleaseFire] == YES)) {
            [self performBandShoot:self.playerTouch];
        }
        
        NSNumber *heroCoins = [[NSNumber alloc] initWithFloat:[[self bandSprite] bandCoins]];
        
        if ([[self bandSprite] getTotalArmor] <= 0) {
            [self setGameState:GAME_OVER];
            [self showGameOverInteristitalADS];
            NSString *levelStr = [NSString stringWithFormat:@"%d",levelNumber];
            [[MetalAttackAnalytics sharedInstance] registerLevelGameOver:levelStr];
            
            CCScene *gameOverScene = [CCBReader loadAsScene:@"GameOver"];
            GameOverScence *gameOver = (GameOverScence *)[gameOverScene getChildByName:@"GameOverScence" recursively:YES];
            
            [gameOver setLevelNumber:[self levelNumber]];
            
            [[[self door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
            
            [self updateBandCoins];
            
            NSLog(@"Retain count is %ld", CFGetRetainCount((__bridge CFTypeRef)self));
            [self unscheduleAllEnemies];
            [[CCDirector sharedDirector] replaceScene:gameOverScene];
            
        }

        if ((self.levelEnemiesLeft == 0) && (self.gameState == GAME_STARTED)){
            NSLog(@"Level cleared !!!");
            NSLog(@"Active Enemies count %lu",(unsigned long)[[[self bandSprite] activeShoots] count]);
            self.gameState = LEVEL_CLEARED;
            
            [self updateBandCoins];
            
            if ([self levelNumber] == totalLevel - 1) { //totalLevel - 1
                NSLog(@"You finished the game thanks for playing");
                
                //reset level cleared to the first one
                NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
                [userdef setInteger:0 forKey:@"Level"];
                
                CCScene *endGameScn = [CCBReader loadAsScene:@"FullEndScene"];
                FullEndScene *endGame = (FullEndScene*)[endGameScn getChildByName:@"FullEndScene" recursively:YES];
                
                endGame.drummerEndSprite = (CCSprite *)[CCBReader load:bandSprite.drummerEndFileName];
                endGame.baseEndSprite  = (CCSprite *)[CCBReader load:bandSprite.baseendFileName];
                endGame.leadEndSprite = (CCSprite *)[CCBReader load:bandSprite.leadEndFileName];
                endGame.bassEndSprite = (CCSprite *)[CCBReader load:bandSprite.bassEndFileName];
                endGame.vocalEndSprite = (CCSprite *)[CCBReader load:bandSprite.vocalEndFileName];
                
                endGame.drummerThinEndSprite = (CCSprite *)[CCBReader load:bandSprite.drummerEndFileName];
                endGame.baseThinEndSprite = (CCSprite *)[CCBReader load:bandSprite.baseendFileName];
                endGame.leadThinEndSprite = (CCSprite *)[CCBReader load:bandSprite.leadEndFileName];
                endGame.bassThinEndSprite = (CCSprite *)[CCBReader load:bandSprite.bassEndFileName];
                endGame.vocalThinEndSprite = (CCSprite *)[CCBReader load:bandSprite.vocalEndFileName];
                
                endGame.endSpritesArray = [NSArray arrayWithObjects: endGame.drummerEndSprite, endGame.baseEndSprite ,  endGame.leadEndSprite , endGame.bassEndSprite, endGame.vocalEndSprite, nil];
                
                endGame.endThinSpriteArray = [NSArray arrayWithObjects: endGame.drummerThinEndSprite, endGame.baseThinEndSprite, endGame.leadThinEndSprite, endGame.bassThinEndSprite, endGame.vocalThinEndSprite, nil];
                
                /*
                 Array band guys positions
                 
                 0 - Drummer
                 1 - Lead
                 2 - Base
                 3 - Vocal
                 4 - Bass
                 
                 */
                
                endGame.bandGuysItem = [[NSMutableArray alloc] initWithArray:@[bandSprite.drummer.name,bandSprite.baseGuitar.name,bandSprite.leadGuitar.name,bandSprite.bass.name,bandSprite.vocal.name]];
            
                endGame.bandGuysEndingText = [[NSMutableArray alloc] initWithArray:@[bandSprite.drummerEndText,bandSprite.baseEndText,bandSprite.leadEndText,bandSprite.bassEndText,bandSprite.vocalEndText]];
                
                [[CCDirector sharedDirector] replaceScene:endGameScn];
                
            }else{
                [self scheduleBlock:^(CCTimer *timer) {
                    CCScene *levelClearedScene = [CCBReader loadAsScene:@"LevelCleared"];
                    LevelCleared *lvclearscn = (LevelCleared*)[levelClearedScene getChildByName:@"LevelCleared" recursively:YES];
                    NSString *levelStr = [NSString stringWithFormat:@"%d",levelNumber];
                    [[MetalAttackAnalytics sharedInstance] registerLevelClear:levelStr];
                    [lvclearscn setDrummerPosterFileName:self.bandSprite.drummerPosterFileName];
                    [lvclearscn setVocalPosterFileName:self.bandSprite.vocalPosterFileName];
                    [lvclearscn setBassPosterFileName:self.bandSprite.bassPosterFileName];
                    [lvclearscn setLeadPosterFileName:self.bandSprite.leadPosterFileName];
                    [lvclearscn setBasePosterFileName:self.bandSprite.basePosterFileName];
                    [lvclearscn setLevelContext:levelContext];
                    [lvclearscn setNextLevel:self.levelNumber+1];
                    [lvclearscn setIsAllGuysLeft:[self checkAllGuysLived]];
                    
                    [[[self door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
                    [[[self door] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
                        [[CCDirector sharedDirector] replaceScene:levelClearedScene];
                    }];
                    
                } delay:1];
                
                
                [self unscheduleAllEnemies];
            }
            
        }else if ((self.waveEnemiesLeft == 0) && (self.gameState == GAME_STARTED)){
            NSLog(@"Wave cleared !");
            self.gameState = WAVE_CLEARED;
            self.waveNumber = self.waveNumber + 1;
            [self doWaveClearedAnimation:[self waveNumber]];
        }
        
        [self.gameHudSprite updateBandLife:self.bandSprite];
        [self.gameHudSprite updateCoinLabel:heroCoins];
        
        if (self.touchEnded) {
            [self.gameHudSprite raiseGuitarCooldownBar];
        }
    }else{
        [self unscheduleAllEnemies];
        [[[LevelSceneController sharedInstance] enemyArray] removeAllObjects];
    }
}

-(BOOL)checkAllGuysLived
{
    if ( [bandSprite drummerArmor] == 0 || [bandSprite bassArmor] == 0 || [bandSprite guitar1Armor] == 0
        || [bandSprite guitar2Armor] == 0 || [bandSprite vocalArmor] == 0){
        return NO;
    }
    return YES;
}


-(void)showGameOverInteristitalADS
{
    //if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
        
        
        //UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        
        /*[self.interstitial presentFromRootViewController:rootViewController];*/
        
    //    [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:rootViewController];
    //}
}

-(void)doWaveClearedAnimation:(int)waveNumberCleared
{
    [waveClearedSprite setPosition:ccp(screenSize.width * 0.5,screenSize.height * 0.77)];
    [waveClearedSprite setScale:0.82];
    [self addChild:waveClearedSprite];
    [waveClearedSprite setZOrder:waveClearedZindex];
    [[waveClearedSprite animationManager] setDelegate:self];
    [[waveClearedSprite animationManager] runAnimationsForSequenceNamed:@"Show"];
    [[waveClearedSprite waveText] setString:@"Wave"];
    [[waveClearedSprite waveClearedText] setString:@"Cleared"];
    
    NSString *waveNumberStr = [NSString stringWithFormat:@"%d",waveNumberCleared];
    [[waveClearedSprite waveNumberText] setString:waveNumberStr];
    //[waveClearedSprite setWaveNumberText:waveNumberStr];
    
    __weak LevelScene *weakSelf = self;
    
    [[waveClearedSprite animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        NSLog(@"Wave cleared animation ended");
        
        //LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
        //currentLevel = [lvcontroller loadLevelWave:[weakSelf levelNumber] waveNumber:[weakSelf waveNumber] delegate:weakSelf];
        //weakSelf.waveEnemiesLeft = [(NSNumber *)[[currentLevel waves] objectAtIndex:[weakSelf waveNumber]] intValue];
        //[self doWaveClearedAnimation:[self waveNumber]];
        //weakSelf.gameState = GAME_STARTED;
        
        [weakSelf.waveClearedSprite removeFromParent];
        [weakSelf releaseNewWave];
    }];
}

-(void)releaseNewWave
{
    LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
    currentLevel = [lvcontroller loadLevelWave:[self levelNumber] waveNumber:[self waveNumber] delegate:self];
    self.waveEnemiesLeft = [(NSNumber *)[[currentLevel waves] objectAtIndex:[self waveNumber]] intValue];
    //[self doWaveClearedAnimation:[self waveNumber]];
    self.gameState = GAME_STARTED;
}

/*
-(void)levelcleared
{
    NSLog(@"Level cleared called with delay outside update loop");
    NSLog(@"Outside ------ Retain count is %ld", CFGetRetainCount((__bridge CFTypeRef)self));
    CCScene *levelClearedScene = [CCBReader loadAsScene:@"LevelCleared"];
    LevelCleared *lvclearscn = (LevelCleared*)[levelClearedScene getChildByName:@"LevelCleared" recursively:YES];
    [lvclearscn setNextLevel:self.levelNumber+1];
    [[CCDirector sharedDirector] replaceScene:levelClearedScene];
}*/

-(float)convertDegreeToRad:(float)angle
{
    return (angle/180)*M_PI;
}

-(float)convertRadToDegree:(float)angle
{
    return (180/M_PI)*angle;
}

- (CGPoint)fireCirclePoint
{
    CGPoint circle;
    circle.x = [[UniversalInfo sharedInstance] screenCenter].x + 1000*sin([self convertDegreeToRad:[self rotationAngle]]);
    circle.y = [[UniversalInfo sharedInstance] screenCenter].y + 1000*cos([self convertDegreeToRad:[self rotationAngle]]);
    
    return circle;
}

-(void)doEndFire:(id)node
{
    [(BandShoot *)node setRemoveObject:YES];
    [self removeChild:node cleanup:YES];
    //[[self backgroundSprite] removeChild:node cleanup:YES];
    
    NSLog(@"Self childs %lu",(unsigned long)[[self children] count]);
}

-(void)fireLaser:(CGPoint)touchEndPoint
{
    if ([[CCDirector sharedDirector]isPaused] == NO){
        
        CGPoint shootPoint = [[UniversalInfo sharedInstance] screenCenter];
        CGPoint targetPoint;
        BandShoot *laserbean;
         
        targetPoint = [self fireCirclePoint];
        laserbean = [[self bandSprite] createBandShoot];
        [laserbean setZOrder:bandShootZindex];
        [[bandSprite activeShoots] addObject:laserbean];
        [laserbean setRotation:self.rotationAngle+90];
        
        float guitaRotation;
        
        if (([self rotationAngle] <= 360) && ([self rotationAngle] >= 180)) {
            // limit the guitarrist angle
            guitaRotation = 180 + 0.3333 * self.rotationAngle;
        }else{
            //
            guitaRotation = 60 + 0.3333 * self.rotationAngle;
        }
    
        
        [[[[self bandSprite] leadGuitar] getChildByName:@"GuitarFront" recursively:YES] setRotation:guitaRotation+270];
        [[[[self bandSprite] leadGuitar] getChildByName:@"GuitarBack" recursively:YES] setRotation:guitaRotation+270];
            
        [[[[self bandSprite] baseGuitar] getChildByName:@"GuitarFront" recursively:YES] setRotation:guitaRotation+270];
        [[[[self bandSprite] baseGuitar] getChildByName:@"GuitarBack" recursively:YES] setRotation:guitaRotation+270];
            
        [[[[self bandSprite] bass] getChildByName:@"GuitarFront" recursively:YES] setRotation:guitaRotation+270];
        [[[[self bandSprite] bass] getChildByName:@"GuitarBack" recursively:YES] setRotation:guitaRotation+270];
        
        if (![[self bandSprite] isVocalDeath]){
            float vocalRotation = self.rotationAngle/[[[self bandSprite] vocal] vocalRotFactor];
            [(CCNode *)[[[[self bandSprite] vocal] children] firstObject] setRotation:vocalRotation+270];
        }
        
        if (([self rotationAngle]/[[[self bandSprite] vocal] vocalRotFactor] <= 260) && ([self rotationAngle]/[[[self bandSprite] vocal] vocalRotFactor] >= 80)) {
                // blind spot for the vocalist
            if (![[self bandSprite] isVocalDeath]){
                CGPoint rightAnchorPoint = [[[self bandSprite] vocal] vocalRightAnchorPoint];
                CGPoint rightGuitarPosition = [[[self bandSprite] vocal] rightVocalPosition];
                [[[[[self bandSprite] vocal] children]firstObject] setFlipX:YES];
                [[[self bandSprite] vocal] setFlipX:YES];
                [(CCNode*)[[[[self bandSprite] vocal] children] firstObject] setPosition:rightGuitarPosition];
                [(CCNode*)[[[[self bandSprite] vocal] children] firstObject] setAnchorPoint:rightAnchorPoint];
            }
        }else{
            if (![[self bandSprite] isVocalDeath]){
                CGPoint leftAnchorPoint = [[[self bandSprite] vocal] vocalLeftAnchorPoint];
                CGPoint leftGuitarPosition = [[[self bandSprite] vocal] leftVocalPosition];
                [[[[[self bandSprite] vocal] children]firstObject] setFlipX:NO];
                [[[self bandSprite] vocal] setFlipX:NO];
                [(CCNode*)[[[[self bandSprite] vocal] children] firstObject] setPosition:leftGuitarPosition];
                [(CCNode*)[[[[self bandSprite] vocal] children] firstObject] setAnchorPoint:leftAnchorPoint];
            }
        }
        
        if (([self rotationAngle] <= 360) && ([self rotationAngle] >= 180)) {
                
            if (![[self bandSprite] isGuita1Death]){

                // New code for the back and front guitars
                
                CCSprite *guitarFront = (CCSprite *) [[[self bandSprite] leadGuitar]getChildByName:@"GuitarFront" recursively:YES];
                CCSprite *guitarBack = (CCSprite *)[[[self bandSprite] leadGuitar] getChildByName:@"GuitarBack" recursively:YES];
                CCSprite *guitarBody = (CCSprite *)[[[self bandSprite] leadGuitar] getChildByName:@"Body" recursively:YES];
                CCSprite *guita1ColRect = (CCSprite *)[[[self bandSprite] leadGuitar] getChildByName:@"ColisionRect" recursively:NO];
                
            
                [guita1ColRect setFlipX:YES];
                [guitarBody setFlipX:YES];
                [guitarBack setFlipY:YES];
                [guitarFront setFlipY:YES];
                
                CGPoint leftAnchorPoint = [[[self bandSprite] leadGuitar] leftAnchorPoint];
                CGPoint leftGuitarPosition = [[[self bandSprite] leadGuitar] leftGuitarPosition];
                    
                CGPoint leftBackAnchorPoint = [[[self bandSprite] leadGuitar] leftBackAnchorPoint];
                CGPoint leftBackGuitarPosition = [[[self bandSprite] leadGuitar] leftBackGuitarPosition];
                    
                [guitarFront setAnchorPoint:leftAnchorPoint];
                [guitarFront setPosition:leftGuitarPosition];
                    
                [guitarBack setAnchorPoint:leftBackAnchorPoint];
                [guitarBack setPosition:leftBackGuitarPosition];
            }
                
        }else{
                
            if (![[self bandSprite] isGuita1Death]){

                // New code for the back and front guitars
                    
                CCSprite *guitarFront = (CCSprite *) [[[self bandSprite] leadGuitar] getChildByName:@"GuitarFront" recursively:YES];
                CCSprite *guitarBack = (CCSprite *)[[[self bandSprite] leadGuitar] getChildByName:@"GuitarBack" recursively:YES];
                CCSprite *guitarBody = (CCSprite *)[[[self bandSprite] leadGuitar] getChildByName:@"Body" recursively:YES];
                CCSprite *guitarColRect = (CCSprite *)[[[self bandSprite] leadGuitar] getChildByName:@"ColisionRect" recursively:NO];
                
                [guitarColRect setFlipX:NO];
                [guitarBody setFlipX:NO];
                [guitarBack setFlipY:NO];
                [guitarFront setFlipY:NO];
                    
                CGPoint rightAnchorPoint = [[[self bandSprite] leadGuitar] rightAnchorPoint];
                CGPoint rightGuitarPosition = [[[self bandSprite] leadGuitar] rightGuitarPosition];
                    
                CGPoint rightBackAnchorPoint = [[[self bandSprite] leadGuitar] rightBackAnchorPoint];
                CGPoint rightBackGuitarPosition = [[[self bandSprite] leadGuitar] rightBackGuitarPosition];
                    
                [guitarFront setAnchorPoint:rightAnchorPoint];
                [guitarFront setPosition:rightGuitarPosition];
                    
                [guitarBack setAnchorPoint:rightBackAnchorPoint];
                [guitarBack setPosition:rightBackGuitarPosition];
                    
            }
                
        }
            
        if (([self rotationAngle] <= 360) && ([self rotationAngle] >= 180)) {
                
            if (![[self bandSprite] isBassDeath]){
                    
                    
                // New code for the back and front guitars
                    
                CCSprite *guitarFront = (CCSprite *) [[[self bandSprite] bass]getChildByName:@"GuitarFront" recursively:YES];
                CCSprite *guitarBack = (CCSprite *)[[[self bandSprite] bass] getChildByName:@"GuitarBack" recursively:YES];
                CCSprite *guitarBody = (CCSprite *)[[[self bandSprite] bass] getChildByName:@"Body" recursively:YES];
                CCSprite *guitarColRect = (CCSprite *)[[[self bandSprite] bass] getChildByName:@"ColisionRect" recursively:NO];
                    
                [guitarBody setFlipX:YES];
                [guitarBack setFlipY:YES];
                [guitarFront setFlipY:YES];
                [guitarColRect setFlipX:YES];
                    
                CGPoint leftAnchorPoint = [[[self bandSprite] bass] leftAnchorPoint];
                CGPoint leftGuitarPosition = [[[self bandSprite] bass] leftGuitarPosition];
                    
                CGPoint leftBackAnchorPoint = [[[self bandSprite] bass] leftBackAnchorPoint];
                CGPoint leftBackGuitarPosition = [[[self bandSprite] bass] leftBackGuitarPosition];
                    
                [guitarFront setAnchorPoint:leftAnchorPoint];
                [guitarFront setPosition:leftGuitarPosition];
                    
                [guitarBack setAnchorPoint:leftBackAnchorPoint];
                [guitarBack setPosition:leftBackGuitarPosition];
            }
                
        }else{
                
            if (![[self bandSprite] isBassDeath]){
                
                // New code for the back and front guitars
                
                CCSprite *guitarFront = (CCSprite *) [[[self bandSprite] bass] getChildByName:@"GuitarFront" recursively:YES];
                CCSprite *guitarBack = (CCSprite *)[[[self bandSprite] bass] getChildByName:@"GuitarBack" recursively:YES];
                CCSprite *guitarBody = (CCSprite *)[[[self bandSprite] bass] getChildByName:@"Body" recursively:YES];
                CCSprite *guitarColRect = (CCSprite *)[[[self bandSprite] bass] getChildByName:@"ColisionRect" recursively:NO];
                
                [guitarColRect setFlipX:NO];
                [guitarBody setFlipX:NO];
                [guitarBack setFlipY:NO];
                [guitarFront setFlipY:NO];
                    
                CGPoint rightAnchorPoint = [[[self bandSprite] bass] rightAnchorPoint];
                CGPoint rightGuitarPosition = [[[self bandSprite] bass] rightGuitarPosition];
                    
                CGPoint rightBackAnchorPoint = [[[self bandSprite] bass] rightBackAnchorPoint];
                CGPoint rightBackGuitarPosition = [[[self bandSprite] bass] rightBackGuitarPosition];
                    
                [guitarFront setAnchorPoint:rightAnchorPoint];
                [guitarFront setPosition:rightGuitarPosition];
                    
                [guitarBack setAnchorPoint:rightBackAnchorPoint];
                [guitarBack setPosition:rightBackGuitarPosition];
                    
            }
                
        }
    
        
        if (([self rotationAngle] <= 360) && ([self rotationAngle] >= 180)) {
                
            if (![[self bandSprite] isGuita2Death]){
                
                // New code for the back and front guitars
            
                CCSprite *guitarFront = (CCSprite *) [[[self bandSprite] baseGuitar]getChildByName:@"GuitarFront" recursively:YES];
                CCSprite *guitarBack = (CCSprite *)[[[self bandSprite] baseGuitar] getChildByName:@"GuitarBack" recursively:YES];
                CCSprite *guitarBody = (CCSprite *)[[[self bandSprite] baseGuitar] getChildByName:@"Body" recursively:YES];
                CCSprite *guitarColRect = (CCSprite *)[[[self bandSprite] baseGuitar] getChildByName:@"ColisionRect" recursively:NO];
                
                [guitarColRect setFlipX:YES];
                [guitarBody setFlipX:YES];
                [guitarBack setFlipY:YES];
                [guitarFront setFlipY:YES];
                
                CGPoint leftAnchorPoint = [[[self bandSprite] baseGuitar] leftAnchorPoint];
                CGPoint leftGuitarPosition = [[[self bandSprite] baseGuitar] leftGuitarPosition];
                
                CGPoint leftBackAnchorPoint = [[[self bandSprite] baseGuitar] leftBackAnchorPoint];
                CGPoint leftBackGuitarPosition = [[[self bandSprite] baseGuitar] leftBackGuitarPosition];
                
                [guitarFront setAnchorPoint:leftAnchorPoint];
                [guitarFront setPosition:leftGuitarPosition];
                
                [guitarBack setAnchorPoint:leftBackAnchorPoint];
                [guitarBack setPosition:leftBackGuitarPosition];
            }
            
        }else{
            
            if (![[self bandSprite] isGuita2Death]){
                
                // New code for the back and front guitars
                
                CCSprite *guitarFront = (CCSprite *) [[[self bandSprite] baseGuitar] getChildByName:@"GuitarFront" recursively:YES];
                CCSprite *guitarBack = (CCSprite *)[[[self bandSprite] baseGuitar] getChildByName:@"GuitarBack" recursively:YES];
                CCSprite *guitarBody = (CCSprite *)[[[self bandSprite] baseGuitar] getChildByName:@"Body" recursively:YES];
                CCSprite *guitarColRect = (CCSprite *)[[[self bandSprite] baseGuitar] getChildByName:@"ColisionRect" recursively:NO];
                
                [guitarColRect setFlipX:NO];
                [guitarBody setFlipX:NO];
                [guitarBack setFlipY:NO];
                [guitarFront setFlipY:NO];
                
                CGPoint rightAnchorPoint = [[[self bandSprite] baseGuitar] rightAnchorPoint];
                CGPoint rightGuitarPosition = [[[self bandSprite] baseGuitar] rightGuitarPosition];
        
                CGPoint rightBackAnchorPoint = [[[self bandSprite] baseGuitar] rightBackAnchorPoint];
                CGPoint rightBackGuitarPosition = [[[self bandSprite] baseGuitar] rightBackGuitarPosition];
                
                [guitarFront setAnchorPoint:rightAnchorPoint];
                [guitarFront setPosition:rightGuitarPosition];
                
                [guitarBack setAnchorPoint:rightBackAnchorPoint];
                [guitarBack setPosition:rightBackGuitarPosition];
                
            }
                
        }
        
        [laserbean setPosition:shootPoint];
        [self addChild:laserbean];
        //[[self backgroundSprite] addChild:laserbean];
        
#warning This can be faster or slow it can be parametrize
        
        
        
        
        id actionFire = [CCActionMoveTo actionWithDuration:1.2 position:targetPoint];
        id endFireCallBack = [CCActionCallBlock actionWithBlock:^{
                [self doEndFire:laserbean];
        }];
                
        id actionSequence = [CCActionSequence actions:actionFire, endFireCallBack, nil];
        
        [laserbean runAction:actionSequence];
    }
}

-(float)getHeroRotationAngle
{
    float normaBegin = sqrtf(powf(self.beginTouch.x,2)+powf(self.beginTouch.y,2));
    float normaEnd = sqrtf(powf(self.endTouch.x,2)+powf(self.endTouch.y,2));
    float dotProduct = self.beginTouch.x * self.endTouch.x + self.beginTouch.y * self.endTouch.y;
    float angleDegrees = [self convertRadToDegree:acosf(dotProduct/(normaEnd*normaBegin))];
    if (self.endTouch.x < 0) {
        return 360 - angleDegrees;
    }else{
        return angleDegrees;
    }
}

-(void)performBandShoot:(CCTouch *)touch
{
    CGPoint location = [touch locationInNode:self];
    
    location.x = location.x - (screenSize.width/2);
    location.y = location.y - (screenSize.height/2);
    
    self.endTouch = location;
    self.rotationAngle = self.getHeroRotationAngle;
    NSLog(@"RotationAngle %f",self.rotationAngle);
    self.moved = YES;
    
    if (([[self bandSprite] bandReleaseFire] == YES) && (gameHudSprite.guitarGaugeProgress.percentage > 0)){
        [[self bandSprite] setBandReleaseFire:NO];
        [[self bandSprite] scheduleReleaseFire];
        if (!self.noCoolDown){
            [self.gameHudSprite lowerGuitarCooldownBar];
        }
        [self fireLaser:[touch locationInNode:self]];
    }
}

-(void)touchCancelled:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    //NSLog(@"TouchCancelled");
}

-(void)touchBegan:(CCTouch *)touch withEvent:(UIEvent *)event
{
    //NSLog(@"TouchBegan");
    self.playerTouch = touch;
    self.touchEnded = NO;
    //[[GameSoundManager sharedInstance] raiseBandInstrumentsVolume];
    [[GameSoundManager sharedInstance] playVocalSound];
}

-(void)touchMoved:(CCTouch *)touch withEvent:(UIEvent *)event
{
    //NSLog(@"TouchMoved");
    self.playerTouch = touch;
}


-(void)touchEnded:(CCTouch *)touch withEvent:(UIEvent *)event
{
    //NSLog(@"TouchEnded");
    self.playerTouch = touch;
    [[self bandSprite] setBandReleaseFire:YES];
    self.touchEnded = YES;
    //[[GameSoundManager sharedInstance] lowerBandInstrumentsVolume];
}

-(void)removeEnemySprite:(CCSprite *)enemySprite AttackType:(AtackType)type
{
#warning remove it before or it will only be deallocated on the end of scence
    
    if (type == AUTODESTRUCTION) {
        [self.killedEnemies removeObject:enemySprite];
    }

    [[enemySprite parent] removeChild:enemySprite];
    [enemySprite unscheduleAllSelectors];
    
    if ([[self bandSprite] getTotalArmor] <= 0) {
        [self setGameState:GAME_OVER];
    }
}

-(void)scheduleFire
{
    [[[self activeEnemies] lastObject] fireWeapon];
}

-(void)addEnemySprite:(Enemy *)enemy;
{
    if ((enemy.definedPosition == POS3) || (enemy.definedPosition == POS4)
        || (enemy.definedPosition == POS5)){
        [self addChild:[enemy enemySprite] z:2];
        //[[self backgroundSprite] addChild:[enemy enemySprite] z:2];
        
    }else if((enemy.definedPosition == POS6) || (enemy.definedPosition == POS2)){
        [self addChild:[enemy enemySprite] z:25];
        //[[self backgroundSprite] addChild:[enemy enemySprite] z:25];
        
        //[self addChild:[enemy enemySprite] z:50];
    }else if ((enemy.definedPosition == POS1) || (enemy.definedPosition == POS8) ||
              (enemy.definedPosition == POS7)){
        [self addChild:[enemy enemySprite] z:50];
        //[[self backgroundSprite] addChild:[enemy enemySprite] z:50];
        
    }
    [[self activeEnemies] addObject:enemy];
}

-(BOOL)isGameOver;
{
    if (gameState == GAME_OVER) {
        return YES;
    }else{
        return FALSE;
    }
}

-(void)onExit
{
    [super onExit];
    NSLog(@"OnExit LeveScece");
    itemPackSprite = nil;
    [self.healthPowerUp setLevelDelegate:nil];
    [self.coinMultiplierPowerUp setLevelDelegate:nil];
    [self unscheduleAllEnemies];
    [self removePowerUPSprite];
    [self removePowerUPS];
    [[GameSoundManager sharedInstance] stopBandInstrumentsEffects];
    [self unloadBandGuysSound];
    [[self bandSprite] removeAllChildren];
    [self removeObservers];
    [[LevelSceneController sharedInstance] removeEnemiesSpriteSheet:levelContext];
    [[LevelSceneController sharedInstance] setScnDelegate:nil];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[[LevelSceneController sharedInstance] enemyArray] removeAllObjects];
    [[self activeEnemies] removeAllObjects];
    [self removeFromParentAndCleanup:YES];
}

-(void)removePowerUPS
{
    [[self invulnerabilityPowerUP] removePowerUps];
}

-(void)removeLevelTextures
{
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/hud/lifebar.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/hud/abahorintozal.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/hud/abavertical.png"];
}

-(void)doResumeGame
{
    [self setGameState:GAME_STARTED];
    [[self gamePause] setVisible:NO];
}

-(void)doPauseGame
{
    NSLog(@"doPauseGame from levelScene");
    if ([[CCDirector sharedDirector]isPaused] == YES){
        [self setGameState:GAME_STARTED];
        [[CCDirector sharedDirector] resume];
    }else{
        [[self gamePause] setVisible:YES];
        [[[self gamePause] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
            NSLog(@"Pause Game");
            [[CCDirector sharedDirector] pause];
        }];
        [[[self gamePause] animationManager] runAnimationsForSequenceNamed:@"DropPoster"];
    }
}

-(void)dealloc
{
    NSLog(@"dealloc LEVEL SCENE with total enemies !!!");
    [self unscheduleAllSelectors];
}

-(void)openItemBelt
{
    NSLog(@"openItemBelt");
}

-(void)runOpenAnimation
{
    //[[self animationManager] runAnimationsForSequenceNamed:@"showItemPack"];
}

-(void)triggerThePowerUp:(BandVaultItem *)item{
    
    if (item) {
        BandStoreItemsCtrl *bandStoreCtrl = [BandStoreItemsCtrl sharedInstance];
        
        NSArray *items = [[NSArray alloc] initWithArray:[bandStoreCtrl genericItemsId]] ;
        int itemPosition = [items indexOfObject:item.appId];
        BandStoreItem *bandStoreItem = [[bandStoreCtrl generalItems] objectAtIndex:itemPosition];
        
        BandVault *bandVault = [BandVault sharedInstance];
        //bool nomoreItensOfThisType =
        [bandVault consumeItem:item];
        
        //if (nomoreItensOfThisType) {
            [gamePause eraseItem:item.appId];
        //}
        
        switch (bandStoreItem.genericItemType) {
            case NO_COOL_DOWN:
                // put a blue tint on the dash
                [self performNoCoolDown:bandStoreItem.duration itemId:bandStoreItem.itemid];
                break;
            case COIN_MULTIPLIER:
                //effect put the botle on the side of the coin display with a multiplier
                [self performCoinMultiplier:bandStoreItem.duration multiplier:bandStoreItem.value itemId:bandStoreItem.itemid];
                break;
            case MEGA_SHOT:
                [self performMegaShoot:bandStoreItem.duration shootPower:bandStoreItem.value sprite:bandStoreItem.frame];
                break;
            case SHIELD:
                // give a percentage of armor by a determined time
                [self performShield:bandStoreItem.duration armor:bandStoreItem.value shieldType:bandStoreItem.frame];
                break;
            case REVIVAL_1:
            case REVIVAL_2:
                //duration from item will be used to indicate the band component that will be revived
                //value will indicate the percentage of armor that will be added when the component
                //revives
                //[self performRevival:bandStoreItem.duration percentageOfArmorRevival:bandStoreItem.value];
                
                [self performRevival:bandStoreItem.itemType bandItemType:bandStoreItem.genericItemType percentageOfArmorRevival:bandStoreItem.value duration:bandStoreItem.duration];
                break;
            case INVULNERABILITY_1:
            case INVULNERABILITY_2:
                [self performInvulnerability:bandStoreItem.duration bandComponent:bandStoreItem.genericItemType itemType:bandStoreItem.itemType];
                break;
            case HEALTH:
                [self performHealthPowerUP:bandStoreItem.value];
                break;
            default:
                break;
        }
    }
}

-(void)performNoCoolDown:(float)duration itemId:(int)idValue{
    NSLog(@"performNoCoolDown");
    
    NSString *NoCoolDownFX1 = @"PowerUpFX18";
    NSString *NoCoolDownFX2 = @"PowerUpFX19";
    NSString *NoCoolDownFX3 = @"PowerUpFX20";
    NSString *NoCoolDownFX4 = @"PowerUpFX21";
    
    GameSoundManager *gameSoundManager = [GameSoundManager sharedInstance];
    self.noCoolDown = YES;
    
    switch (idValue) {
        case 1:
            [gameSoundManager playSoundEffectByName:NoCoolDownFX1];
            [[[self gameHudSprite] animationManager] runAnimationsForSequenceNamed:@"Ice1Skew"];
            break;
        case 2:
            [gameSoundManager playSoundEffectByName:NoCoolDownFX2];
            [[[self gameHudSprite] animationManager] runAnimationsForSequenceNamed:@"Ice2Skew"];
            break;
        case 3:
            [gameSoundManager playSoundEffectByName:NoCoolDownFX3];
            [[[self gameHudSprite] animationManager] runAnimationsForSequenceNamed:@"Ice3Skew"];
            break;
        case 4:
            [gameSoundManager playSoundEffectByName:NoCoolDownFX4];
            [[[self gameHudSprite] animationManager] runAnimationsForSequenceNamed:@"Ice4Skew"];
            break;
        default:
            break;
    }
    
    
    if (duration > 0) {
        [self scheduleBlock:^(CCTimer *timer) {
            NSLog(@"Stop no cool down");
            self.noCoolDown = NO;
            
            switch (idValue) {
                case 1:
                    [[[self gameHudSprite] animationManager] runAnimationsForSequenceNamed:@"Ice1SkewRevert"];
                    break;
                case 2:
                    [[[self gameHudSprite] animationManager] runAnimationsForSequenceNamed:@"Ice2SkewRevert"];
                    break;
                case 3:
                    [[[self gameHudSprite] animationManager] runAnimationsForSequenceNamed:@"Ice3SkewRevert"];
                    break;
                case 4:
                    [[[self gameHudSprite] animationManager] runAnimationsForSequenceNamed:@"Ice4SkewRevert"];
                    break;
                default:
                    break;
            }
            
        } delay:duration/1000];
    }
}

-(void)performCoinMultiplier:(float)duration multiplier:(int)mult itemId:(int)idvalue{
    NSLog(@"performCoinMultiplier");
    
    [self.coinMultiplierPowerUp activateCoinPowerUp:idvalue];
    
    self.coinMultiplier = mult;
    [self scheduleBlock:^(CCTimer *timer) {
        NSLog(@"Stop coin multi %d",mult);
        self.coinMultiplier = 1;
    } delay:duration];
}

-(void)performHealthPowerUP:(float)value
{
    NSLog(@"Perform Health Power UP");
    
    if (value == 25) {
        [self.healthPowerUp activateHealthPowerUp:1];
    }else if (value == 50) {
        [self.healthPowerUp activateHealthPowerUp:2];
    }else if (value == 75) {
        [self.healthPowerUp activateHealthPowerUp:3];
    }
    
    [self healingWithArmor:value/100];
    [self.gameHudSprite updateBandLife:bandSprite];
   
}

-(void)healingWithArmor:(float)value {
    
    // never work with 100%
    float healingPoints = 0;
    
    if (!bandSprite.isGuita1Death) {
        healingPoints = self.bandSprite.totalGuitar1Armor * (value);
        self.bandSprite.guitar1Armor = MIN(healingPoints + self.bandSprite.guitar1Armor, self.bandSprite.totalGuitar1Armor);
    }

    if (!bandSprite.isGuita2Death) {
        healingPoints = self.bandSprite.totalGuitar2Armor * (value);
        self.bandSprite.guitar2Armor = MIN(healingPoints + self.bandSprite.guitar2Armor, self.bandSprite.totalGuitar2Armor);
    }
    
    if (!bandSprite.isDrummerDeath) {
        healingPoints = self.bandSprite.totalDrummerArmor * (value);
        self.bandSprite.drummerArmor = MIN(healingPoints + self.bandSprite.drummerArmor, self.bandSprite.totalDrummerArmor);
    }
    
    if (!bandSprite.isVocalDeath) {
        healingPoints = self.bandSprite.totalVocalArmor * (value);
        self.bandSprite.vocalArmor = MIN(healingPoints + self.bandSprite.vocalArmor, self.bandSprite.totalVocalArmor);
    }
    
    if (!bandSprite.isBassDeath) {
        healingPoints = self.bandSprite.totalBassArmor * (value);
        self.bandSprite.bassArmor = MIN(healingPoints + self.bandSprite.bassArmor, self.bandSprite.totalBassArmor);
    }
}

-(void)performInvulnerability:(float)duration bandComponent:(BandGenericItemType)powerUpType itemType:(BandItemType)bandComponent{
    NSLog(@"performInvulnerability");
    
    [self changeInvulnerability:bandComponent invulnerability:YES];
    [self animateInvulnerability:bandComponent bandItemType:powerUpType duration:duration];
}

-(void)animateInvulnerability:(BandItemType)bandComponent  bandItemType:(BandGenericItemType)genericType duration:(float)seconds{
    
    NSString *invulNerabilityAll01FX = @"PowerUpFX08";
    NSString *invulNerabilityAll02FX = @"PowerUpFX09";
    
    NSString *invulNerabilityBass01FX = @"PowerUpFX10";
    NSString *invulNerabilityBass02FX = @"PowerUpFX11";
    
    NSString *invulNerabilityDrummer01FX = @"PowerUpFX12";
    NSString *invulNerabilityDrummer02FX = @"PowerUpFX13";
    
    NSString *invulNerabilityGuitarBoth01FX = @"PowerUpFX14";
    NSString *invulNerabilityGuitarBoth02FX = @"PowerUpFX15";

    NSString *invulNerabilityVocal01FX = @"PowerUpFX16";
    NSString *invulNerabilityVocal02FX = @"PowerUpFX17";

    GameSoundManager *gameSoundManager = [GameSoundManager sharedInstance];
    
    BandPowerUp *pwdUP;
    
    __weak LevelScene *weakSelf = self;
    
    switch (bandComponent) {
        case GUITARRIST_BOTH:
        {
            if ((guita1PowerUp == nil) && (guita2PowerUp == nil)) {
                
                [guita1PowerUp removeFromParent];
                [guita2PowerUp removeFromParent];
                
                if (genericType == INVULNERABILITY_1) {
                    [gameSoundManager playSoundEffectByName:invulNerabilityGuitarBoth01FX];
                    guita1PowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulGuitar1"];
                    guita2PowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulGuitar1"];
                } else {
                    [gameSoundManager playSoundEffectByName:invulNerabilityGuitarBoth02FX];
                    guita1PowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulGuitar2"];
                    guita2PowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulGuitar2"];
                }
                
                CGRect leadBox = [bandSprite.leadGuitar boundingBox];
                CGRect baseBox = [bandSprite.baseGuitar boundingBox];
                
                CGPoint leadPosition = ccp(leadBox.size.width/2,leadBox.size.height/2);
                CGPoint basePosition = ccp(baseBox.size.width/2,baseBox.size.height/2);
                
                [guita1PowerUp setPosition:leadPosition];
                [guita2PowerUp setPosition:basePosition];
                
                
                [guita2PowerUp removeFromParent];
                [guita1PowerUp removeFromParent];
                
                [guita1PowerUp setZOrder:30];
                [guita2PowerUp setZOrder:30];
                
                [[guita1PowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                [[guita2PowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                
                [bandSprite.leadGuitar addChild:guita1PowerUp];
                [bandSprite.baseGuitar addChild:guita2PowerUp];
                
                [invulGuitarSchedule invalidate];
                
                
                invulGuitarSchedule = [self scheduleBlock:^(CCTimer *timer) {
                    NSLog(@"Stop invulnerability");
                    [weakSelf changeInvulnerability:bandComponent invulnerability:NO];
                    [weakSelf removeInvulnerabilityAnimation:bandComponent];
                    guita1PowerUp = nil;
                    guita2PowerUp = nil;
                } delay:seconds];
            }
        }
        break;
        case DRUMMER:
        {
            if (drummerPowerUp == nil) {
                [drummerPowerUp removeFromParent];
                if (genericType == INVULNERABILITY_1) {
                    [gameSoundManager playSoundEffectByName:invulNerabilityDrummer01FX];
                    drummerPowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulDrummer1"];
                } else {
                    [gameSoundManager playSoundEffectByName:invulNerabilityDrummer02FX];
                    drummerPowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulDrummer2"];
                }
                
                CGRect drummerRect = [bandSprite.drummer boundingBox];
                CGPoint drummerPwdPosition = ccp(drummerRect.size.width/2, drummerRect.size.height/2);
                
                [drummerPowerUp removeFromParent];
                [drummerPowerUp setPosition:drummerPwdPosition];
                
                [[drummerPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                [[bandSprite drummer] addChild:drummerPowerUp];
                
                [invulDrummerSchedule invalidate];
                
                invulDrummerSchedule = [self scheduleBlock:^(CCTimer *timer) {
                    NSLog(@"Stop invulnerability");
                    [weakSelf changeInvulnerability:bandComponent invulnerability:NO];
                    [weakSelf removeInvulnerabilityAnimation:bandComponent];
                    drummerPowerUp = nil;
                } delay:seconds];
            }
        }
        break;
        case BASSGUY:
        {
            if (bassPowerUp == nil){
                [bassPowerUp removeFromParent];
                if (genericType == INVULNERABILITY_1) {
                    [gameSoundManager playSoundEffectByName:invulNerabilityBass01FX];
                    bassPowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulBass1"];
                } else {
                    [gameSoundManager playSoundEffectByName:invulNerabilityBass02FX];
                    bassPowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulBass2"];
                }
                
                CGRect bassRect = [bandSprite.bass boundingBox];
                CGPoint bassPwdPosition = ccp(bassRect.size.width/2, bassRect.size.height/2);
                
                pwdUP = (BandPowerUp *) bassPowerUp;
                
                [bassPowerUp setScale:pwdUP.pwdScale];
                
                [bassPowerUp removeFromParent];
                [bassPowerUp setPosition:bassPwdPosition];
                [[bassPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                [bandSprite.bass addChild:bassPowerUp];
                
                [invulBassSchedule invalidate];
                
                invulBassSchedule = [self scheduleBlock:^(CCTimer *timer) {
                    NSLog(@"Stop invulnerability");
                    [weakSelf changeInvulnerability:bandComponent invulnerability:NO];
                    [weakSelf removeInvulnerabilityAnimation:bandComponent];
                    bassPowerUp = nil;
                } delay:seconds];
            }
        }
            
        break;
        case VOCALIST:
        {
            if (vocalPowerUp == nil) {
                [vocalPowerUp removeFromParent];
                
                if (genericType == INVULNERABILITY_1) {
                    [gameSoundManager playSoundEffectByName:invulNerabilityVocal01FX];
                    vocalPowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulVocal1"];
                } else {
                    [gameSoundManager playSoundEffectByName:invulNerabilityVocal02FX];
                    vocalPowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulVocal2"];
                }
                
                pwdUP = (BandPowerUp *) vocalPowerUp;
                
                CGRect vocalRect = [bandSprite.vocal boundingBox];
                CGPoint vocalPwdPosition = ccp(vocalRect.size.width/2 + pwdUP.posAdjustX, vocalRect.size.height/2 + pwdUP.posAdjustY);
                
                [vocalPowerUp setScale:pwdUP.pwdScale];
                [vocalPowerUp setPosition:vocalPwdPosition];
                [[vocalPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                
                [bandSprite.vocal addChild:vocalPowerUp];
                
                [invulVocalSchedule invalidate];
                
                invulVocalSchedule = [self scheduleBlock:^(CCTimer *timer) {
                    NSLog(@"Stop invulnerability");
                    [weakSelf changeInvulnerability:bandComponent invulnerability:NO];
                    [weakSelf removeInvulnerabilityAnimation:bandComponent];
                    vocalPowerUp = nil;
                } delay:seconds];
            }
        }
        break;
        case ALL_BAND:
        {
            if (allBandPowerUp == nil) {
                
                [allBandPowerUp removeFromParent];
                if (genericType == INVULNERABILITY_1) {
                    [gameSoundManager playSoundEffectByName:invulNerabilityAll01FX];
                    allBandPowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulAll1"];
                } else {
                    [gameSoundManager playSoundEffectByName:invulNerabilityAll02FX];
                    allBandPowerUp = (BandPowerUp *)[CCBReader load:@"invulnerability/InvulAll2"];
                }
                
                pwdUP = (BandPowerUp *) allBandPowerUp;
                
                [allBandPowerUp removeFromParent];
                [allBandPowerUp setPosition:ccp(bandSprite.position.x + pwdUP.posAdjustX, bandSprite.position.y + pwdUP.posAdjustY)];
                
                [allBandPowerUp setZOrder:30];
                [[allBandPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                [bandSprite addChild:allBandPowerUp];
                
                [invulAllSchedule invalidate];
                
                invulAllSchedule = [self scheduleBlock:^(CCTimer *timer) {
                    NSLog(@"Stop invulnerability");
                    [weakSelf changeInvulnerability:bandComponent invulnerability:NO];
                    [weakSelf removeInvulnerabilityAnimation:bandComponent];
                    allBandPowerUp = nil;
                } delay:seconds];
            }
        }
        break;
        default:
            break;
    }
    
}


-(void)removeInvulnerabilityAnimation:(BandItemType)bandComponent {
    
    switch (bandComponent) {
        case GUITARRIST_BOTH:
            [guita1PowerUp removeFromParent];
            [guita2PowerUp removeFromParent];
            break;
        case DRUMMER:
            [drummerPowerUp removeFromParent];
            break;
        case BASSGUY:
            [bassPowerUp removeFromParent];
            break;
        case VOCALIST:
            [vocalPowerUp removeFromParent];
            break;
        case ALL_BAND:
            [allBandPowerUp removeFromParent];
            break;
        default:
            break;
    }
    
}

-(void)changeInvulnerability:(BandItemType)bandComponent invulnerability:(BOOL)value
{
    switch (bandComponent) {
        case DRUMMER:
            [[self bandSprite] changeInvunerability:DRUMMER_TYPE invunerability:value];
            break;
        case GUITARRIST_BOTH:
            [[self bandSprite] changeInvunerability:ALL_GUITARS invunerability:value];
            break;
        case VOCALIST:
            [[self bandSprite] changeInvunerability:VOCALS_TYPE invunerability:value];
            break;
        case BASSGUY:
            [[self bandSprite] changeInvunerability:BASS_TYPE invunerability:value];
            break;
        case ALL_BAND:
            [[self bandSprite] changeInvunerability:ALL_GUYS invunerability:value];
            break;
        default:
            break;
    }
}

-(void)performMegaShoot:(float)duration shootPower:(int)power sprite:(int)position{
    NSLog(@"performMegaShoot");
    // it will increase the shoot power by an percentage
    __weak LevelScene *weakSelf = self;
    [[self bandSprite] selectMegaShotValue:power spritePosition:position];
    if (duration > 0) {
        [self scheduleBlock:^(CCTimer *timer) {
            NSLog(@"Stop megashot");
            [weakSelf disablePowerUpEffect];
            [[weakSelf bandSprite] setMegaShotValue:0];
        } delay:duration/1000];
    }
}

-(void)updateGameHudFaceWhenRevival:(BandItemType)bandComponent {
    
    switch (bandComponent) {
        case DRUMMER:
            [self.gameHudSprite updateBandFaceState:DRUMMER_TYPE isDead:false isRevival:YES withBandSprite:bandSprite];
            break;
        case GUITARRIST_BOTH:
            [self.gameHudSprite updateBandFaceState:GUITARRIST1_TYPE isDead:false isRevival:YES withBandSprite:bandSprite];
            [self.gameHudSprite updateBandFaceState:GUITARRIST2_TYPE isDead:false isRevival:YES withBandSprite:bandSprite];
            break;
        case VOCALIST:
            [self.gameHudSprite updateBandFaceState:VOCALS_TYPE isDead:false isRevival:YES withBandSprite:bandSprite];
            break;
        case BASSGUY:
            [self.gameHudSprite updateBandFaceState:BASS_TYPE isDead:false isRevival:YES withBandSprite:bandSprite];
            break;
        case ALL_BAND:
            [self.gameHudSprite updateBandFaceState:ALL_GUYS isDead:false isRevival:YES withBandSprite:bandSprite];
            break;
        default:
            break;
    }
}

-(void)performRevival:(BandItemType)bandComponent bandItemType:(BandGenericItemType)genericType percentageOfArmorRevival:(int)value duration:(int)seconds{
    NSLog(@"performRevival");
    [self animateRevival:bandComponent bandItemType:genericType percentageOfArmorRevival:value];
}

-(void)animateRevival:(BandItemType)bandComponent  bandItemType:(BandGenericItemType)genericType percentageOfArmorRevival:(int)value{
    
    BandPowerUp *pwdUP;
    CGPoint pwdPosition;
    
    NSString *revivalAll01FX = @"PowerUpFX22";
    NSString *revivalAll02FX = @"PowerUpFX23";
    
    NSString *revivalBass01FX = @"PowerUpFX24";
    NSString *revivalBass02FX = @"PowerUpFX25";
    
    NSString *revivalDrummer01FX = @"PowerUpFX26";
    NSString *revivalDrummer02FX = @"PowerUpFX27";
    
    NSString *revivalGuitarBoth01FX = @"PowerUpFX28";
    NSString *revivalGuitarBoth02FX = @"PowerUpFX29";
    
    NSString *revivalVocal01FX = @"PowerUpFX30";
    NSString *revivalVocal02FX = @"PowerUpFX31";
    
    GameSoundManager *gameSoundManager = [GameSoundManager sharedInstance];
    
    switch (bandComponent) {
        case GUITARRIST_BOTH:
        {
            if ((guita1RevivalPowerUp == nil) && (guita2RevivalPowerUp == nil)) {
                
                if (genericType == REVIVAL_1) {
                    [gameSoundManager playSoundEffectByName:revivalGuitarBoth01FX];
                    guita1RevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalGuitar1"];
                    guita2RevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalGuitar1"];
                } else {
                    [gameSoundManager playSoundEffectByName:revivalGuitarBoth02FX];
                    guita1RevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalGuitar2"];
                    guita2RevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalGuitar2"];
                }
                [guita1RevivalPowerUp setPosition:bandSprite.leadGuitar.position];
                [guita2RevivalPowerUp setPosition:bandSprite.baseGuitar.position];
                
                [guita1RevivalPowerUp removeFromParent];
                [guita2RevivalPowerUp removeFromParent];
                
                [guita1RevivalPowerUp setZOrder:30];
                [guita2RevivalPowerUp setZOrder:30];
                
                [bandSprite addChild:guita1RevivalPowerUp];
                [bandSprite addChild:guita2RevivalPowerUp];
                
                [[guita1RevivalPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                [[guita2RevivalPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                
                __weak LevelScene *weakSelf = self;
                
                [[guita1RevivalPowerUp animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
                    [weakSelf updateGameHudFaceWhenRevival:bandComponent];
                    [weakSelf.bandSprite revive:bandComponent withArmor:value];
                    guita1RevivalPowerUp = nil;
                    guita2RevivalPowerUp = nil;
                }];
            }
        }
        break;
        case DRUMMER:
            
        {
            if (drummerRevivalPowerUp == nil) {
                
                if (genericType == REVIVAL_1) {
                    [gameSoundManager playSoundEffectByName:revivalDrummer01FX];
                    drummerRevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalDrummer1"];
                } else {
                    [gameSoundManager playSoundEffectByName:revivalDrummer02FX];
                    drummerRevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalDrummer2"];
                }
                [drummerRevivalPowerUp removeFromParent];
                
                pwdUP = (BandPowerUp *) drummerRevivalPowerUp;
                pwdPosition = ccp(bandSprite.drummer.position.x + pwdUP.posAdjustX, bandSprite.drummer.position.y + pwdUP.posAdjustY);
                
                
                [drummerRevivalPowerUp setPosition:pwdPosition];
                [drummerRevivalPowerUp setScale:pwdUP.pwdScale];
                [drummerRevivalPowerUp setZOrder:30];
                [bandSprite addChild:drummerRevivalPowerUp];
                
                [[drummerRevivalPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                
                __weak LevelScene *weakSelf = self;
                
                [[drummerRevivalPowerUp animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
                    [weakSelf updateGameHudFaceWhenRevival:bandComponent];
                    [weakSelf.bandSprite revive:bandComponent withArmor:value];
                    drummerRevivalPowerUp = nil;
                }];
            }
        }
        break;
        case BASSGUY:
        {
            if (bassRevivalPowerUp == nil) {
                if (genericType == REVIVAL_1) {
                    [gameSoundManager playSoundEffectByName:revivalBass01FX];
                    bassRevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalBass1"];
                } else {
                    [gameSoundManager playSoundEffectByName:revivalBass02FX];
                    bassRevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalBass2"];
                }
                
                [bassRevivalPowerUp removeFromParent];
                
                pwdUP = (BandPowerUp *) bassRevivalPowerUp;
                pwdPosition = ccp(bandSprite.bass.position.x + pwdUP.posAdjustX, bandSprite.bass.position.y + pwdUP.posAdjustY);
                
                [bassRevivalPowerUp setPosition:pwdPosition];
                [bassRevivalPowerUp setZOrder:30];
                [bassRevivalPowerUp setScale:pwdUP.pwdScale];
                [bandSprite addChild:bassRevivalPowerUp];
                
                [[bassRevivalPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                
                __weak LevelScene *weakSelf = self;
                
                [[bassRevivalPowerUp animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
                    [weakSelf updateGameHudFaceWhenRevival:bandComponent];
                    [weakSelf.bandSprite revive:bandComponent withArmor:value];
                    bassRevivalPowerUp = nil;
                }];
            }
        }
        break;
        case VOCALIST:
        {
            
            if (self.vocalRevivalPowerUp == nil) {
                
                if (genericType == REVIVAL_1) {
                    [gameSoundManager playSoundEffectByName:revivalVocal01FX];
                    vocalRevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalVocal1"];
                } else {
                    [gameSoundManager playSoundEffectByName:revivalVocal02FX];
                    vocalRevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalVocal2"];
                }
                
                [vocalRevivalPowerUp removeFromParent];
                
                pwdUP = (BandPowerUp *) vocalRevivalPowerUp;
                pwdPosition = ccp(bandSprite.vocal.position.x + pwdUP.posAdjustX, bandSprite.vocal.position.y + pwdUP.posAdjustY);
                
                [vocalRevivalPowerUp setPosition:pwdPosition];
                [vocalRevivalPowerUp setZOrder:30];
                [vocalRevivalPowerUp setScale:pwdUP.pwdScale];
                
                [bandSprite addChild:vocalRevivalPowerUp];
                
                [vocalRevivalPowerUp stopAllActions];
                [[vocalRevivalPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                
                __weak LevelScene *weakSelf = self;
                
                [[vocalRevivalPowerUp animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
                    [weakSelf updateGameHudFaceWhenRevival:bandComponent];
                    [weakSelf.bandSprite revive:bandComponent withArmor:value];
                    [vocalRevivalPowerUp removeFromParent];
                    vocalRevivalPowerUp = nil;
                }];
                
            }
        }
        break;
        case ALL_BAND:
        {
            if (self.allBandRevivalPowerUp == nil) {
                if (genericType == REVIVAL_1) {
                    [gameSoundManager playSoundEffectByName:revivalAll01FX];
                    self.allBandRevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalAll1"];
                } else {
                    [gameSoundManager playSoundEffectByName:revivalAll02FX];
                    self.allBandRevivalPowerUp = (BandPowerUp *)[CCBReader load:@"revival/revivalAll2"];
                }
                
                [self.allBandRevivalPowerUp removeFromParent];
                
                pwdUP = (BandPowerUp *) self.allBandRevivalPowerUp;
                pwdPosition = ccp(bandSprite.revivalAll.position.x + pwdUP.posAdjustX, bandSprite.revivalAll.position.y + pwdUP.posAdjustY);
                
                [self.allBandRevivalPowerUp setPosition:pwdPosition];
                [self.allBandRevivalPowerUp setZOrder:30];
                [self.allBandRevivalPowerUp setScale:pwdUP.pwdScale];
                [self.bandSprite addChild:self.allBandRevivalPowerUp];
                
                [[self.allBandRevivalPowerUp animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
                
                __weak LevelScene *weakSelf = self;
                
                [[self.allBandRevivalPowerUp animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
                    [weakSelf updateGameHudFaceWhenRevival:bandComponent];
                    [weakSelf.bandSprite revive:bandComponent withArmor:value];
                    self.allBandRevivalPowerUp = nil;
                }];
            }
        }
        break;
        default:
        break;
    }

}

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    NSLog(@"Animation name %@",name);
}

-(void)animateShield:(BOOL)remove shieldType:(int)typevalue{
    // put the actions inside the array
    
    CCSprite *shieldToHandle = [self.shieldSpriteArray objectAtIndex:typevalue-1];
    ShieldPowerUp *shieldPowerUp = [self.shieldPositionArray objectAtIndex:typevalue-1];
    
    if (!remove) {
        [shieldToHandle runAction:shieldPowerUp.showShieldAction];
    }else{
        [shieldToHandle runAction:shieldPowerUp.hideShieldAction];
    }

}

-(void)performShield:(float)duration armor:(int)power shieldType:(int)typevalue{
    //it will be nice if we put the bottle near each guy on the band indicating that the
    //armor is increased
    NSLog(@"performShield");
    
    if (self.activeShieldType > 0) {
        [[self bandSprite] setMegaShieldValue:0];
        //NSString *removeShieldName = [NSString stringWithFormat:@"removeShieldAnimation%d",self.activeShieldType];
        //[[self animationManager] runAnimationsForSequenceNamed:removeShieldName];
        [self animateShield:YES shieldType:typevalue];
    }
    
    self.activeShieldType = typevalue;
    
    //NSString *activeShieldName = [NSString stringWithFormat:@"shieldAnimation%d",typevalue];
    //[[self animationManager] runAnimationsForSequenceNamed:activeShieldName];
    [self animateShield:NO shieldType:typevalue];
    
    
    // it will increase the shoot power by an percentagec
    [[self bandSprite] setMegaShieldValue:power];
    [self scheduleBlock:^(CCTimer *timer) {
        NSLog(@"Stop invulnerability");
        //disable shield with 0 value
        [[self bandSprite] setMegaShieldValue:0];
        //NSString *removeShieldName = [NSString stringWithFormat:@"removeShieldAnimation%d",typevalue];
        //[[self animationManager] runAnimationsForSequenceNamed:removeShieldName];
        [self animateShield:YES shieldType:typevalue];
    
    } delay:duration/1000];
    
}

-(void)disablePowerUpEffect
{
    CCActionBlink *blink = [CCActionBlink actionWithDuration:1 blinks:10];
    CCActionFadeOut *fadeOut = [CCActionFadeOut actionWithDuration:0.2];
    CCAction *disalbeSequence = [CCActionSequence actions:blink,fadeOut, nil];
    [drummerPowerUp runAction:disalbeSequence];
}

-(void)generalPowerUpEffect
{
    [drummerPowerUp removeFromParent];
    drummerPowerUp = [[CCSprite alloc] initWithSpriteFrame:[consumeItemButton backgroundSpriteFrameForState:CCControlStateNormal]];
    
    CGPoint powerUpPosition = ccp(screenSize.width * self.consumeItemButton.position.x ,
                                  screenSize.height * self.consumeItemButton.position.y);
    
    CGPoint drummerPosition = ccp((screenSize.width * [self.bandSprite.drummer parent].position.x) + self.bandSprite.drummer.boundingBox.size.width*0.4,
                                  (screenSize.height * [self.bandSprite.drummer parent].position.y) + self.bandSprite.drummer.boundingBox.size.height);

    [drummerPowerUp setPosition:powerUpPosition];
    [[self.consumeItemButton parent] addChild:drummerPowerUp];
    
    CCActionScaleTo *scaleTo = [CCActionScaleTo actionWithDuration:0.8 scale:0.50];
    CCActionJumpTo *jumpTo = [CCActionJumpTo actionWithDuration:0.2 position:drummerPosition height:100 jumps:1];
    
    [drummerPowerUp runAction:jumpTo];
    [drummerPowerUp runAction:scaleTo];
}

-(void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addRewardToBandCoins) name:@"Reward100Coins" object:nil];
}

-(void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Reward100Coins" object:nil];
}

- (void)addRewardToBandCoins {
#warning check adscolony
    self.bandSprite.bandCoins = self.bandSprite.bandCoins + 250;
}

@end
