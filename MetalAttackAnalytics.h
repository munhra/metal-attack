//
//  MetalAttackAnalytics.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 6/11/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GAITrackedViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@interface MetalAttackAnalytics : NSObject

+(MetalAttackAnalytics *)sharedInstance;
-(void)registerScreenAnalytics:(NSString *)screenName;
-(void)registerBandDrinkBuy:(NSString *)powerupName quantity:(NSString *)qtd;
-(void)registerBandGuyBuy:(NSString *)bandGuyName;
-(void)registerBandGuySelect:(NSString *)bandGuyName;
-(void)registerLevelGameOver:(NSString *)levelNumber;
-(void)registerLevelClear:(NSString *)levelNumber;

@end
