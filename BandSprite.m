//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  BandSprite.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 12/03/13.
//
//  All band guys position are oriented by the center of the screen

#import "BandSprite.h"
#import "UniversalInfo.h"
#import "CCAnimation.h"
#import "GameSoundManager.h"

@implementation BandSprite

@synthesize drummer;
@synthesize leadGuitar;
@synthesize baseGuitar;
@synthesize vocal;
@synthesize bass;

@synthesize isBassDeath;
@synthesize isGuita1Death;
@synthesize isGuita2Death;
@synthesize isVocalDeath;
@synthesize isDrummerDeath;

@synthesize activeShoots;
@synthesize bandReleaseFire;

@synthesize drummerArmor;
@synthesize guitar1Armor;
@synthesize guitar2Armor;   
@synthesize bassArmor;
@synthesize vocalArmor;
@synthesize delegate;

@synthesize guitar1ShootPower;
@synthesize guitar2ShootPower;
@synthesize bassShootPower;
@synthesize vocalsShootPower;
@synthesize drummerShootPower;
@synthesize shootPower;

@synthesize bandBlast;

@synthesize bandCoins;
@synthesize totalDrummerArmor;
@synthesize totalBassArmor;
@synthesize totalGuitar1Armor;
@synthesize totalGuitar2Armor;
@synthesize totalVocalArmor;

@synthesize invulnerability;
@synthesize drummerInvulnerabiliy;
@synthesize guitarBothInvulnerabiliy;
@synthesize bassInvulnerabiliy;
@synthesize vocalInvulnerabiliy;

@synthesize megaShotValue;
@synthesize megaShieldValue;

@synthesize bandShoot;
@synthesize maxBandLevel;

@synthesize basePoster;
@synthesize drummerPoster;
@synthesize vocalPoster;
@synthesize leadPoster;
@synthesize bassPoster;

@synthesize drummerPosterFileName;
@synthesize vocalPosterFileName;
@synthesize bassPosterFileName;
@synthesize leadPosterFileName;
@synthesize basePosterFileName;

@synthesize drummerEnd;
@synthesize leadEnd;
@synthesize baseend;
@synthesize bassEnd;
@synthesize vocalEnd;

@synthesize baseLife;
@synthesize drummerLife;
@synthesize vocalLife;
@synthesize leadLife;
@synthesize bassLife;

@synthesize revivalBass;
@synthesize revivalBase;
@synthesize revivalDrummer;
@synthesize revivalLead;
@synthesize revivalVocal;
@synthesize megaShoots;
@synthesize selectedMegaShoot;

@synthesize revivalAll;

@synthesize drummerPowerConsumption;
@synthesize drummerPowerRestoration;
@synthesize guitar1PowerConsumption;
@synthesize guitar1PowerRestoration;
@synthesize guitar2PowerConsumption;
@synthesize guitar2PowerRestoration;
@synthesize vocalPowerConsumption;
@synthesize vocalPowerRestoration;
@synthesize bassPowerConsumption;
@synthesize bassPowerRestoration;
    
@synthesize drummerEndText;
@synthesize bassEndText;
@synthesize vocalEndText;
@synthesize leadEndText;
@synthesize baseEndText;

const int drummerZindex = 20;
const int revivalDrummerZindex = 21;

const int leadGuitarZindex = 21;
const int revivalLeadZindex = 22;

const int bassZindex = 21;
const int revivalBassZindex = 22;

const int baseGuitarZindex = 22;
const int revivalBaseZindex = 23;

const int vocalZindex = 22;
const int revivalVocalZindex = 23;

const int bandHitZindex = 50;

-(void)createDrummer;
{
    NSLog(@"Create Drummer");
    BandStoreItem *itemDrummer;

    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    
    itemDrummer = [[storeCtrl drummers] objectAtIndex:vault.drummerIndex];
    
    CCNode *parent = [[self drummer] parent];
    CGPoint spritePosition = [[self drummer] position];
    [parent removeChild:self.drummer];
    
    self.drummer = (BandGuySprite *)[itemDrummer bandGuySprite];
    self.drummer.name = itemDrummer.itemName;
    self.drummerEndText = itemDrummer.endingText;
    
    [self.drummer setZOrder:drummerZindex];
    [self.drummer setPosition:spritePosition];
    
    [[[self drummer] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
    [parent addChild:self.drummer];
    
    self.drummerArmor = itemDrummer.armor;
    self.drummerShootPower = itemDrummer.shootPower;
    self.totalDrummerArmor = itemDrummer.armor;

    [self.bandShoot setDrummerShoot:itemDrummer.compShoot];
    self.drummerPoster = itemDrummer.compPoster;
    self.drummerPosterFileName = itemDrummer.posterFileName;
    self.drummerLife = itemDrummer.compLifeImage;
    self.drummerEnd = itemDrummer.bandGuyEndSprite;
    self.drummerEndFileName = itemDrummer.bandGuyEndFileName;
    
    self.drummerPowerRestoration = itemDrummer.powerRestoration;
    self.drummerPowerConsumption = itemDrummer.powerConsumption;
    
    if (itemDrummer.level > maxBandLevel) {
        maxBandLevel = itemDrummer.level;
    }

}

-(void)createLeadGuitar
{
    NSLog(@"CreateLeadGuitar");
    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    BandStoreItem *itemLeadGuitar;
    
    itemLeadGuitar = [[storeCtrl leadGuitars] objectAtIndex:vault.guitar1Index];
    CCNode *parent = [[self leadGuitar] parent];
    CGPoint spritePosition = [[self leadGuitar] position];
    [parent removeChild:self.leadGuitar];
    
    self.leadGuitar = (BandGuySprite *)[itemLeadGuitar bandGuySprite];
    [self.leadGuitar setZOrder:leadGuitarZindex];
    [self.leadGuitar restoreOriginalState];
    self.leadGuitar.name = itemLeadGuitar.itemName;
    self.leadEndText = itemLeadGuitar.endingText;
    
    [self.leadGuitar setPosition:spritePosition];
    [[[self leadGuitar] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
    [parent addChild:self.leadGuitar];
    
    self.guitar1Armor = itemLeadGuitar.armor;
    self.guitar1ShootPower = itemLeadGuitar.shootPower;
    self.totalGuitar1Armor = itemLeadGuitar.armor;
    
    [self.bandShoot setLeadShoot:itemLeadGuitar.compShoot];
    self.leadPoster = itemLeadGuitar.compPoster;
    self.leadPosterFileName = itemLeadGuitar.posterFileName;
    self.leadLife = itemLeadGuitar.compLifeImage;
    self.leadEnd = itemLeadGuitar.bandGuyEndSprite;
    self.leadEndFileName = itemLeadGuitar.bandGuyEndFileName;
    
    self.guitar1PowerRestoration = itemLeadGuitar.powerRestoration;
    self.guitar1PowerConsumption = itemLeadGuitar.powerConsumption;
    
    if (itemLeadGuitar.level > maxBandLevel) {
        maxBandLevel = itemLeadGuitar.level;
    }
}

-(void)createBass
{
    NSLog(@"CreateBassPlayer");
    BandStoreItem *itemBass;
    
    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    itemBass = [[storeCtrl basses] objectAtIndex:vault.bassIndex];
        
    CCNode *parent = [[self bass] parent];
    CGPoint spritePosition = [[self bass] position];
    [parent removeChild:self.bass];
            
    self.bass = (BandGuySprite *)[itemBass bandGuySprite];
    [self.bass setZOrder:bassZindex];
    self.bass.name = itemBass.itemName;
    self.bassEndText = itemBass.endingText;
    
    [self.bass restoreOriginalState];
    [self.bass setPosition:spritePosition];
    
    [[[self bass] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
    [parent addChild:self.bass];

    self.bassArmor = itemBass.armor;
    self.bassShootPower = itemBass.shootPower;
    self.totalBassArmor = itemBass.armor;
    
    [self.bandShoot setBassShoot:itemBass.compShoot];
    self.bassPoster = itemBass.compPoster;
    self.bassPosterFileName = itemBass.posterFileName;
    self.bassLife = itemBass.compLifeImage;
    self.bassEnd = itemBass.bandGuyEndSprite;
    self.bassEndFileName = itemBass.bandGuyEndFileName;
    
    self.bassPowerRestoration = itemBass.powerRestoration;
    self.bassPowerConsumption = itemBass.powerConsumption;
    
    if (itemBass.level > maxBandLevel) {
        maxBandLevel = itemBass.level;
    }
}

-(void)createBaseGuitar
{
    NSLog(@"createBaseGuitar");
    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    BandStoreItem *itemBaseGuitar;
    itemBaseGuitar = [[storeCtrl baseGuitars] objectAtIndex:vault.guitar2Index];
    
    CCNode *parent = [[self baseGuitar] parent];
    CGPoint spritePosition = [[self baseGuitar] position];
    [parent removeChild:self.baseGuitar];
    
    self.baseGuitar = (BandGuySprite *)[itemBaseGuitar bandGuySprite];
    [self.baseGuitar setZOrder:baseGuitarZindex];
    [self.baseGuitar restoreOriginalState];
    [self.baseGuitar setPosition:spritePosition];
    [[[self baseGuitar] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
    [parent addChild:self.baseGuitar];
    self.baseGuitar.name = itemBaseGuitar.itemName;
    self.baseEndText = itemBaseGuitar.endingText;
    
    self.guitar2Armor = itemBaseGuitar.armor;
    self.guitar2ShootPower = itemBaseGuitar.shootPower;
    self.totalGuitar2Armor = itemBaseGuitar.armor;
    
    [self.bandShoot setBaseShoot:itemBaseGuitar.compShoot];
    self.basePoster = itemBaseGuitar.compPoster;
    self.basePosterFileName = itemBaseGuitar.posterFileName;
    self.baseLife = itemBaseGuitar.compLifeImage;
    self.baseend = itemBaseGuitar.bandGuyEndSprite;
    self.baseendFileName = itemBaseGuitar.bandGuyEndFileName;
    
    self.guitar2PowerConsumption = itemBaseGuitar.powerConsumption;
    self.guitar2PowerRestoration = itemBaseGuitar.powerRestoration;
    
    if (itemBaseGuitar.level > maxBandLevel) {
        maxBandLevel = itemBaseGuitar.level;
    }

}

-(void)createVocal
{
    NSLog(@"createVocal");
    BandStoreItem *itemVocal;
    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    
    itemVocal = [[storeCtrl vocals] objectAtIndex:vault.vocalIndex];
    
    CCNode *parent = [[self vocal] parent];
    CGPoint spritePosition = [[self vocal] position];
    [parent removeChild:self.vocal];
    
    self.vocal = (BandGuySprite *)[itemVocal bandGuySprite];
    [self.vocal setZOrder:vocalZindex];
    [self.vocal restoreOriginalState];
    [self.vocal setPosition:spritePosition];
    [[[self vocal] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
    [parent addChild:self.vocal];
    self.vocal.name = itemVocal.itemName;
    self.vocalEndText = itemVocal.endingText;

    self.vocalArmor = itemVocal.armor;
    self.vocalsShootPower = itemVocal.shootPower;
    self.totalVocalArmor = itemVocal.armor;
    
    [self.bandShoot setVocalShoot:itemVocal.compShoot];
    self.vocalPoster = itemVocal.compPoster;
    self.vocalPosterFileName = itemVocal.posterFileName;
    self.vocalLife = itemVocal.compLifeImage;
    self.vocalEnd = itemVocal.bandGuyEndSprite;
    self.vocalEndFileName = itemVocal.bandGuyEndFileName;
    
    self.vocalPowerRestoration = itemVocal.powerRestoration;
    self.vocalPowerConsumption = itemVocal.powerConsumption;
    
    if (itemVocal.level > maxBandLevel) {
        maxBandLevel = itemVocal.level;
    }
}

-(BandShoot *)createBandShoot
{
    BandShoot *shoot = [[BandShoot alloc] initWithMaxBandLevel:maxBandLevel];
    
    [self.bandShoot.drummerShoot spriteFrame];
    
    shoot.drummerShoot = [[CCSprite alloc] initWithSpriteFrame:self.bandShoot.drummerShoot.spriteFrame];
    shoot.bassShoot = [[CCSprite alloc] initWithSpriteFrame:self.bandShoot.bassShoot.spriteFrame];
    shoot.baseShoot = [[CCSprite alloc] initWithSpriteFrame:self.bandShoot.baseShoot.spriteFrame];
    shoot.vocalShoot = [[CCSprite alloc] initWithSpriteFrame:self.bandShoot.vocalShoot.spriteFrame];
    shoot.leadShoot = [[CCSprite alloc] initWithSpriteFrame:self.bandShoot.leadShoot.spriteFrame];
    
    if (self.megaShotValue > 0){
        [shoot tintShoot];
        shoot.megaShoot = [[CCSprite alloc] initWithSpriteFrame:((CCSprite *) megaShoots[self.selectedMegaShoot]).spriteFrame];
    }else{
        [shoot.megaShoot setOpacity:0.0];
    }
    
    [shoot addAllShootComponents];
    [shoot enableMegaShootSprite];

    return shoot;
}

-(void)selectMegaShotValue:(float)power spritePosition:(int)position
{
    self.megaShotValue = power;
    self.selectedMegaShoot = position;
}

-(void)loadMegaShoots
{
    self.megaShoots = [[NSMutableArray alloc] init];
    for (int i = 1; i <= 11; i++) {
        NSString *megaShootFileName = [NSString stringWithFormat:@"BandGuys/shoot/Mega/Mega%d",i];
        CCSprite *megashoot = (CCSprite *)[CCBReader load:megaShootFileName];
        [megaShoots addObject:megashoot];
    }
}

-(void)didLoadFromCCB
{
    NSLog(@"didLoadFromCCB");
    maxBandLevel = 1;
    selectedMegaShoot = -1;

    self.bandShoot = (BandShoot *)[CCBReader load:@"BandShoot"];
    [self loadMegaShoots];
    
    [self createDrummer];
    [self createLeadGuitar];
    [self createBaseGuitar];
    [self createVocal];
    [self createBass];
    
    self.shootPower = self.guitar1ShootPower + self.guitar2ShootPower + self.bassShootPower + self.vocalsShootPower + self.drummerShootPower;

    [self setIsGuita1Death:NO];
    [self setIsGuita2Death:NO];
    [self setIsBassDeath:NO];
    [self setIsVocalDeath:NO];
    [self setIsDrummerDeath:NO];
    
    [[self revivalDrummer] setZOrder:revivalDrummerZindex];
    [[self revivalBass] setZOrder:revivalBassZindex];
    [[self revivalLead] setZOrder:revivalLeadZindex];
    [[self revivalVocal] setZOrder:revivalVocalZindex];
    [[self revivalBase] setZOrder:revivalBaseZindex];
     
    
}

-(id)init
{
    self = [super init];
    if(self) {
        NSLog(@"initBand");
        self.activeShoots = [[NSMutableArray alloc] init];
    }
    self.bandReleaseFire = YES;
    self.invulnerability = NO;
    self.drummerInvulnerabiliy = NO;
    self.guitarBothInvulnerabiliy = NO;
    self.vocalInvulnerabiliy = NO;
    self.bassInvulnerabiliy = NO;

    //[self schedule:@selector(doRelBandFire) interval:0.02];
    
    return self;
}

-(float)getTotalArmor
{
    float totalArmor = self.drummerArmor + self.guitar1Armor + self.guitar2Armor + self.bassArmor + self.vocalArmor;
    //totalArmor = totalVocalArmor * (1 + megaShieldValue/100);
    return totalArmor;
}

-(void)doRelBandFire
{
    bandReleaseFire = YES;
}

-(void)performBandDeath
{
    NSLog(@"The Band has been destroyed launch game over");
}

-(void)performGeneralHitAnimation:(CGPoint) hitPoint
{
    CCSprite *hitSprite = [[CCSprite alloc] init];
    hitSprite = (CCSprite *)[CCBReader load:@"OtherSprites/bandhit"];
    hitSprite.position = hitPoint;
    [hitSprite setZOrder:bandHitZindex];
    [[self parent] addChild:hitSprite];
}

-(void)performTintHit:(BandComponents)type{
    
    if (!self.invulnerability){
        switch (type) {
            case GUITARRIST1_TYPE:
            if (!self.guitarBothInvulnerabiliy) {
                [[self leadGuitar] performTintHit];
            }
            break;
            case GUITARRIST2_TYPE:
            if (!self.guitarBothInvulnerabiliy) {
                [[self baseGuitar] performTintHit];
            }
            break;
            case BASS_TYPE:
            if (!self.bassInvulnerabiliy) {
                [[self bass] performTintHit];
            }
            break;
            case DRUMMER_TYPE:
            if (!self.drummerInvulnerabiliy) {
                [[self drummer] performTintHit];
            }
            break;
            case VOCALS_TYPE:
            if (!self.vocalInvulnerabiliy) {
                [[self vocal] performTintHit];
            }
            break;
            default:
            break;
        }
    }

}

-(float)hitByEnemy:(float)damagePoints component:(BandComponents)type rect:(CGRect )collrect;
{
    [self performTintHit:type];
    [self performGeneralHitAnimation:collrect.origin];
    switch (type) {
        case GUITARRIST1_TYPE:
            if ((!self.isGuita1Death) && (!self.invulnerability) && (!self.guitarBothInvulnerabiliy)) {
                self.guitar1Armor = self.guitar1Armor - damagePoints;
                self.guitar1Armor = MAX(self.guitar1Armor, 0);
                if (guitar1Armor <= 0){
                    self.shootPower -= guitar1ShootPower;
                    [[[self leadGuitar] animationManager] runAnimationsForSequenceNamed:@"DieAnimation"];
                    [self playEnemyDieSound];
                    [self setIsGuita1Death:YES];
                    [self setGuitar1Armor:0.0];
                    [[self leadGuitar] prepareForDeath];
                }else{
                    if (![[[[self leadGuitar] animationManager] runningSequenceName] isEqualToString:@"HitAnimation"]) {
                        [[[self leadGuitar] animationManager] runAnimationsForSequenceNamed:@"HitAnimation"];
                        [self playGuitarBaseHitSound];
                    }
                }
            }
            return self.guitar1Armor;
            break;
        case GUITARRIST2_TYPE:
            if ((!self.isGuita2Death) && (!self.invulnerability) && (!self.guitarBothInvulnerabiliy)) {
                self.guitar2Armor = self.guitar2Armor - damagePoints;
                self.guitar2Armor = MAX(self.guitar2Armor, 0);
                if (guitar2Armor <= 0){
                    self.shootPower -= guitar2ShootPower;
                    [[[self baseGuitar] animationManager] runAnimationsForSequenceNamed:@"DieAnimation"];
                    [self playEnemyDieSound];
                    [self setIsGuita2Death:YES];
                    [self setGuitar2Armor:0.0];
                    [[self baseGuitar] prepareForDeath];
                }else{
                    if (![[[[self baseGuitar] animationManager] runningSequenceName] isEqualToString:@"HitAnimation"]) {
                        [[[self baseGuitar] animationManager] runAnimationsForSequenceNamed:@"HitAnimation"];
                        [self playGuitarLeadHitSound];
                    }
                }
            }
            return self.guitar2Armor;
            break;
        case BASS_TYPE:
            if ((!self.isBassDeath) && (!self.invulnerability) && (!self.bassInvulnerabiliy)) {
                self.bassArmor = self.bassArmor - damagePoints;
                self.bassArmor = MAX(self.bassArmor, 0);
                if (bassArmor <= 0){
                    self.shootPower -= bassShootPower;
                    [[[self bass] animationManager] runAnimationsForSequenceNamed:@"DieAnimation"];
                    [self playEnemyDieSound];
                    [self setIsBassDeath:YES];
                    [self setBassArmor:0.0];
                    [[self bass] prepareForDeath];
                }else{
                    if (![[[[self bass] animationManager] runningSequenceName] isEqualToString:@"HitAnimation"]) {
                        [[[self bass] animationManager] runAnimationsForSequenceNamed:@"HitAnimation"];
                        [self playBassHitSound];
                    }
                }
            }
            return self.bassArmor;
            break;
        case VOCALS_TYPE:
            if ((!self.isVocalDeath) && (!self.invulnerability) && (!self.vocalInvulnerabiliy)) {
                self.vocalArmor = self.vocalArmor - damagePoints;
                self.vocalArmor = MAX(self.vocalArmor, 0);
                if (vocalArmor <= 0){
                    self.shootPower -= vocalsShootPower;
                    [[[self vocal] animationManager] runAnimationsForSequenceNamed:@"DieAnimation"];
                    [self playEnemyDieSound];
                    [self setIsVocalDeath:YES];
                    [self setVocalArmor:0.0];
                    [[self vocal] prepareForDeath];
                }else{
                    if (![[[[self vocal] animationManager] runningSequenceName] isEqualToString:@"HitAnimation"]) {
                        [[[self vocal] animationManager] runAnimationsForSequenceNamed:@"HitAnimation"];
                        [self playVocalHitSound];
                    }
                }
            }
            return self.vocalArmor;
            break;
        case DRUMMER_TYPE:
            if ((!isDrummerDeath) && (!self.invulnerability) && (!self.drummerInvulnerabiliy)) {
                self.drummerArmor = self.drummerArmor - damagePoints;
                self.drummerArmor = MAX(self.drummerArmor, 0);
                if (drummerArmor <= 0){
                    self.shootPower -= drummerShootPower;
                    [[[self drummer] animationManager] runAnimationsForSequenceNamed:@"DieAnimation"];
                    [self playEnemyDieSound];
                    [self setIsDrummerDeath:YES];
                    [self setDrummerArmor:0.0];
                }else{
                    if (![[[[self drummer] animationManager] runningSequenceName] isEqualToString:@"HitAnimation"]) {
                        [[[self drummer] animationManager] runAnimationsForSequenceNamed:@"HitAnimation"];
                        [self playDrummerHitSound];
                    }
                 }
            }
            return self.drummerArmor;
            break;
        default:
            return -1;
            break;
    }
    
}

-(void)healingWithArmor:(float)value {
    
    // never work with 100%
    float healingPoints = 0;
    
    if (!isGuita1Death) {
        healingPoints = self.totalGuitar1Armor * (value);
        self.guitar1Armor = MIN(healingPoints + self.guitar1Armor, self.totalGuitar1Armor);
    }
    
    if (!isGuita2Death) {
        healingPoints = self.totalGuitar2Armor * (value);
        self.guitar1Armor = MIN(healingPoints + self.guitar1Armor, self.totalGuitar2Armor);
    }
    
    if (!isDrummerDeath) {
        healingPoints = self.totalDrummerArmor * (value);
        self.guitar1Armor = MIN(healingPoints + self.guitar1Armor, self.totalDrummerArmor);
    }
    
    if (!isVocalDeath) {
        healingPoints = self.totalVocalArmor * (value);
        self.guitar1Armor = MIN(healingPoints + self.guitar1Armor, self.totalVocalArmor);
    }
    
    if (!isBassDeath) {
        healingPoints = self.totalBassArmor * (value);
        self.guitar1Armor = MIN(healingPoints + self.guitar1Armor, self.totalBassArmor);
    }
}

-(void)revive:(BandItemType)type withArmor:(float)value
{
    
    switch (type) {
        case GUITARRIST_BOTH:
            if (isGuita1Death) {
                self.guitar1Armor = self.totalGuitar1Armor * (value/100);
                self.shootPower += guitar1ShootPower;
                isGuita1Death = NO;
                [[self leadGuitar] prepareForRevival];
                [[[self leadGuitar] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalLead] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            if (isGuita2Death) {
                self.shootPower += guitar2ShootPower;
                self.guitar2Armor = self.totalGuitar2Armor * (value/100);
                isGuita2Death = NO;
                [[self baseGuitar] prepareForRevival];
                [[[self baseGuitar] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalBase] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            break;
        case DRUMMER:
            if (isDrummerDeath) {
                self.shootPower += self.drummerShootPower;
                self.drummerArmor = self.totalDrummerArmor * (value/100);
                isDrummerDeath = NO;
                [[self drummer] prepareForRevival];
                [[[self drummer] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalDrummer] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            break;
        case BASSGUY:
            if (isBassDeath) {
                self.shootPower += self.bassShootPower;
                self.bassArmor = self.totalBassArmor * (value/100);
                isBassDeath = NO;
                [[self bass] prepareForRevival];
                [[[self bass] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalBass] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            break;
        case VOCALIST:
            if (isVocalDeath) {
                self.shootPower += self.vocalsShootPower;
                self.vocalArmor = self.totalVocalArmor * (value/100);
                isVocalDeath = NO;
                [[self vocal] prepareForRevival];
                [[[self vocal] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalVocal] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            break;
        case ALL_BAND:
            if (isGuita1Death){
                self.shootPower += guitar1ShootPower;
                self.guitar1Armor = self.totalGuitar1Armor * (value/100);
                isGuita1Death = NO;
                [[self leadGuitar] prepareForRevival];
                [[[self leadGuitar] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalLead] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            if (isGuita2Death){
                self.shootPower += guitar2ShootPower;
                self.guitar2Armor = self.totalGuitar2Armor * (value/100);
                isGuita2Death = NO;
                [[self baseGuitar] prepareForRevival];
                [[[self baseGuitar] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalBase] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            if (isVocalDeath) {
                self.shootPower += vocalsShootPower;
                self.vocalArmor = self.totalVocalArmor * (value/100);
                isVocalDeath = NO;
                [[self vocal] prepareForRevival];
                [[[self vocal] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalVocal] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            if (isBassDeath) {
                self.shootPower += bassShootPower;
                self.bassArmor = self.totalBassArmor * (value/100);
                isBassDeath = NO;
                [[self bass] prepareForRevival];
                [[[self bass] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalBass] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            if (isDrummerDeath) {
                self.shootPower += drummerShootPower;
                self.drummerArmor = self.totalDrummerArmor * (value/100);
                isDrummerDeath = NO;
                [[self drummer] prepareForRevival];
                [[[self drummer] animationManager] runAnimationsForSequenceNamed:@"BodyAnimation"];
                [[[self revivalDrummer] animationManager] runAnimationsForSequenceNamed:@"RevivalTimeLine"];
            }
            break;
        default:
            break;
    }
}

-(void)changeInvunerability:(BandComponents)type invunerability:(BOOL)value
{
    switch (type) {
        case DRUMMER_TYPE:
            self.drummerInvulnerabiliy = value;
            break;
        case ALL_GUITARS:
            self.guitarBothInvulnerabiliy = value;
            break;
        case BASS_TYPE:
            self.bassInvulnerabiliy = value;
            break;
        case VOCALS_TYPE:
            self.vocalInvulnerabiliy = value;
            break;
        case ALL_GUYS:
            self.invulnerability = value;
            break;
        default:
            break;
    }
}

-(float)shootPowerWithMegaShoot
{
    if (self.megaShotValue != 0) {
        return shootPower * (1 + self.megaShotValue/100);
    }else{
        return shootPower;
    }
}

-(void)playEnemyDieSound
{
    NSLog(@"playEnemyDieSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx052"];
}

-(void)playGuitarBaseHitSound
{
    NSLog(@"playEnemyDieSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx055"];
}

-(void)playGuitarLeadHitSound
{
    NSLog(@"playEnemyDieSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx056"];
}

-(void)playVocalHitSound
{
    NSLog(@"playEnemyDieSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx057"];
}

-(void)playDrummerHitSound
{
    NSLog(@"playEnemyDieSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx058"];
}

-(void)playBassHitSound
{
    NSLog(@"playEnemyDieSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx059"];
}

-(void)performBandBlast
{
    NSLog(@"Band Blast Explosion");
}

-(void)receiveHealthPowerUp:(float)value
{
    self.drummerArmor = self.drummerArmor + value;
    self.drummerArmor = MIN(self.drummerArmor, 100);
}

-(void)scheduleReleaseFire
{
    [self schedule:@selector(doRelBandFire) interval:0.09];
    
}

-(void)dealloc
{
    NSLog(@"Dealloc bandsprite");
}

@end
