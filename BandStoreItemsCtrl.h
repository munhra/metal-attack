//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//
//  BandStoreItemsCtrl.h
//  EightbitShooter
//
//  Created by Rafael Munhoz on 03/04/13.
//
//

#import <Foundation/Foundation.h>
#import "BandStoreItem.h"
#import "JSON.h"

@interface BandStoreItemsCtrl : NSObject

{
    NSDictionary *itemsDict;
    NSMutableArray *drummers;
    NSMutableArray *leadGuitars;
    NSMutableArray *baseGuitars;
    NSMutableArray *vocals;
    NSMutableArray *basses;
    NSMutableArray *coinpacks;
    NSMutableArray *coinpacksByid;
    NSMutableArray *generalItems;
    NSMutableArray *genericItemsId;

    int bandCoins;
}

@property (nonatomic, retain) NSDictionary *itemsDict;
@property (nonatomic, retain) NSMutableArray *drummers;
@property (nonatomic, retain) NSMutableArray *leadGuitars;
@property (nonatomic, retain) NSMutableArray *baseGuitars;
@property (nonatomic, retain) NSMutableArray *vocals;
@property (nonatomic, retain) NSMutableArray *basses;
@property (nonatomic, retain) NSMutableArray *coinpacks;
@property (nonatomic, retain) NSMutableArray *coinpacksByid;
@property (nonatomic, retain) NSMutableArray *generalItems;
@property (nonatomic, retain) NSMutableArray *genericItemsId;
@property (nonatomic) int bandCoins;

+(id)sharedInstance;
-(void)loadBandItemsFromJson;
-(void)restoreBandGuys;
@end
