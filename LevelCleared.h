//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  LevelCleared.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 08/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "InfinityBG.h"

@interface LevelCleared : CCNode
{
    int nextLevel;
    CCNode *door;
    CCSpriteFrame *drummerFrame;
    CCSpriteFrame *basePosterFrame;
    CCSpriteFrame *leadPosterFrame;
    CCSpriteFrame *bassPosterFrame;
    CCSpriteFrame *vocalPosterFrame;
}

@property (nonatomic) int nextLevel;

@property (nonatomic,retain) CCNode *door;
@property (nonatomic,retain) CCSprite *drummerPoster;
@property (nonatomic,weak) CCSprite *guitarBasePoster;
@property (nonatomic,weak) CCSprite *guitarLeadPoster;
@property (nonatomic,weak) CCSprite *bassPoster;
@property (nonatomic,weak) CCSprite *vocalPoster;

@property (nonatomic,weak) CCButton *facebookButton;
@property (nonatomic,weak) CCNode *buttonsLayoutBox;
@property (nonatomic,weak) CCNode *stageClearBase;
@property (nonatomic,weak) CCNode *stageClearMessage;
@property (nonatomic,weak) CCNode *stageClearText;
@property (nonatomic,weak) CCNode *metalLogo;

@property (nonatomic,retain) CCSpriteFrame *drummerPosterFrame;
@property (nonatomic,retain) CCSpriteFrame *basePosterFrame;
@property (nonatomic,retain) CCSpriteFrame *leadPosterFrame;
@property (nonatomic,retain) CCSpriteFrame *bassPosterFrame;
@property (nonatomic,retain) CCSpriteFrame *vocalPosterFrame;

@property (nonatomic,weak) NSString *drummerPosterFileName;
@property (nonatomic,weak) NSString *basePosterFileName;
@property (nonatomic,weak) NSString *leadPosterFileName;
@property (nonatomic,weak) NSString *bassPosterFileName;
@property (nonatomic,weak) NSString *vocalPosterFileName;

@property (nonatomic) int levelContext;
@property (nonatomic, weak) InfinityBG *endInfinityBG;

@property (nonatomic) BOOL isAllGuysLeft;

@end
