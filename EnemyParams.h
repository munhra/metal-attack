//
//  EnemyParams.h
//  EightbitShooter
//
//  Created by Rafael Munhoz on 3/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    GUNPOWER_1,
    GUNPOWER_2,
    HEALTHPOWER,
    RADIOPOWER,
    COIN,
    NOPOWERUP
} PowerUp;

typedef enum {
    MEELEE,
    AUTODESTRUCTION,
    WALK_SHOOT,
} AtackType;

typedef enum {
    SINGLE,
    ROTATION,
    JUMP,
} ShootStyle;

@interface EnemyParams : NSObject

{
    NSString *enemyName;
    float armor;
    float weaponDamage;
    float scorePoints;
    int numberOfFrames;
    double timeToReach;
    PowerUp typeOfPowerUp;
    AtackType atackType;
    CGPoint shootStartPosition;
    ShootStyle shootStyle;
    //Action point shoot and meele sprite relative point of action
    CGPoint upperAction;
    CGPoint sideAction;
    CGPoint lowerAction;
    CGPoint fUpperAction;
    CGPoint fSideAction;
    CGPoint fLowerAction;
    NSString *shootSound;
    NSString *shootHit;
    int dropCoinLowerLevel;
    int dropCoinHighLevel;
    
}

@property (nonatomic,retain) NSString *enemyName;
@property (nonatomic) float armor;
@property (nonatomic) float weaponDamage;
@property (nonatomic) float scorePoints;
@property (nonatomic) int numberOfFrames;
@property (nonatomic) double timeToReach;
@property (nonatomic) PowerUp typeOfPowerUp;
@property (nonatomic) AtackType atackType;
@property (nonatomic) CGPoint shootStartPosition;
@property (nonatomic) ShootStyle shootStyle;
@property (nonatomic) CGPoint upperAction;
@property (nonatomic) CGPoint sideAction;
@property (nonatomic) CGPoint lowerAction;
@property (nonatomic) CGPoint fUpperAction;
@property (nonatomic) CGPoint fSideAction;
@property (nonatomic) CGPoint fLowerAction;
@property (nonatomic) NSString *shootSound;
@property (nonatomic) NSString *shootHit;
@property (nonatomic) int dropCoinLowerLevel;
@property (nonatomic) int dropCoinHighLevel;




@end
