//
//  SplashScence.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 02/09/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "cocos2d.h"
#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface SplashScence : CCNode {
    
    MPMoviePlayerController *moviePlayer;
    MPMoviePlayerViewController *moviePlayerController;
    
}

@property(retain,nonatomic) MPMoviePlayerController *moviePlayer;
@property(retain,nonatomic) MPMoviePlayerViewController *moviePlayerController;

@end
