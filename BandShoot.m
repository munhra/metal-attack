//
//  BandShoot.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 09/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "BandShoot.h"


@implementation BandShoot
{
    NSArray *colorsArray;
    int colorCount;
}

@synthesize removeObject;
@synthesize baseShoot;
@synthesize drummerShoot;
@synthesize vocalShoot;
@synthesize bassShoot;
@synthesize leadShoot;
@synthesize level;
@synthesize megaShoot;

-(id)initWithMaxBandLevel:(int)maxLevel
{
    self = [super init];
    
    if (self) {
        self.removeObject = NO;
        level = maxLevel;
    }
    
    return self;
}

-(void)addAllShootComponents
{
    [self.drummerShoot setRotation:-90];
    [self.baseShoot setRotation:-90];
    [self.bassShoot setRotation:-90];
    [self.vocalShoot setRotation:-90];
    [self.leadShoot setRotation:-90];
    
    [self addChild:baseShoot];
    [self addChild:drummerShoot];
    [self addChild:vocalShoot];
    [self addChild:bassShoot];
    [self addChild:leadShoot];
    
    switch (level) {
        case 1:
            [self setScale:0.44];
            break;
        case 2:
        case 3:
            [self setScale:0.66];
            break;
        default:
            break;
    }
    
    colorsArray = [[NSArray alloc] initWithObjects:[CCColor redColor],[CCColor redColor],[CCColor blackColor],
                   [CCColor darkGrayColor],[CCColor lightGrayColor],[CCColor whiteColor],[CCColor grayColor],
                   [CCColor greenColor],[CCColor blueColor],[CCColor cyanColor],[CCColor yellowColor],[CCColor magentaColor],
                   [CCColor orangeColor], [CCColor purpleColor], [CCColor brownColor], [CCColor clearColor],nil];
    
   
    colorCount = 0;
}

-(void)enableMegaShootSprite
{
    if (megaShoot) {
        [[self megaShoot] setRotation:-90];
        [self addChild:megaShoot];
    }
 }

-(void)tintShoot
{
    [self schedule:@selector(blinkColor) interval:0.05];
}

-(void)blinkColor
{
#warning improve it using CCEffects
    NSLog(@"blink Color");
    int aleatoryIndex = arc4random() % [colorsArray count];
    [self.vocalShoot setColor:[colorsArray objectAtIndex:aleatoryIndex]];
    aleatoryIndex = arc4random() % [colorsArray count];
    [self.drummerShoot setColor:[colorsArray objectAtIndex:aleatoryIndex]];
    aleatoryIndex = arc4random() % [colorsArray count];
    [self.leadShoot setColor:[colorsArray objectAtIndex:aleatoryIndex]];
    aleatoryIndex = arc4random() % [colorsArray count];
    [self.baseShoot setColor:[colorsArray objectAtIndex:aleatoryIndex]];
    aleatoryIndex = arc4random() % [colorsArray count];
    [self.bassShoot setColor:[colorsArray objectAtIndex:aleatoryIndex]];
}

-(void)dealloc
{
    NSLog(@"Dealloc bandsprite");
}

@end
