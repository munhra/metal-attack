//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  BandBuyPoster.h
//  EightbitShooter
//
//  Created by Rafael Munhoz on 08/10/14.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface BandBuyPoster : CCSprite
{
    NSMutableArray *itemsToShow;
    id delegate;
}

@property (nonatomic, retain) NSMutableArray *itemsToShow;
@property (nonatomic, weak) CCButton *buyButton;
@property (nonatomic, weak) CCButton *selectButton;
@property (nonatomic, weak) CCButton *infoButton;
@property (nonatomic, weak) CCSprite *bandBuy;
@property (nonatomic, weak) CCSprite *info;
@property (nonatomic, weak) CCSprite *coinIcon;
@property (nonatomic, weak) CCSprite *bandGuyArt;
@property (nonatomic, weak) CCSprite *ownedTag;
@property (nonatomic, weak) CCSprite *damageIcon;
@property (nonatomic, weak) CCSprite *armorIcon;
@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) CCLabelTTF *costValue;
@property (nonatomic, retain) CCLabelTTF *armorValue;
@property (nonatomic, retain) CCLabelTTF *damageValue;
@property (nonatomic, retain) CCLabelTTF *title;
@property (nonatomic, retain) CCLabelTTF *costLabel;
@property (nonatomic, retain) CCLabelTTF *armorLabel;
@property (nonatomic, retain) CCLabelTTF *damageLabel;
@property (nonatomic, weak) CCButton *leftButton;
@property (nonatomic, weak) CCButton *rightButton;
@property (nonatomic, weak) CCLayoutBox *StarsDamageGroup1;
@property (nonatomic, weak) CCLayoutBox *StarsDamageGroup2;
@property (nonatomic, weak) CCLayoutBox *StarsArmorGroup1;
@property (nonatomic, weak) CCLayoutBox *StarsArmorGroup2;
@property (nonatomic, weak) CCSprite *guyInfo;
@property (retain,nonatomic) CCSprite *posterGuyPicture;
@property(nonatomic,weak) CCLabelTTF *bandGuyHistory;

@property (nonatomic, weak) CCSprite *orangeStar;
@property (nonatomic, weak) CCSprite *redStar;
@property (nonatomic, weak) CCSprite *blackStar;
@property (nonatomic, weak) NSString *currentSpriteSheet;

-(void)doInfo;
-(void)doScrRigh;
-(void)doScrLeft;
-(void)doShowLeadGuitar;
-(void)doShowBaseGuitar;
-(void)doShowVocalist:(CCButton *)menuItem;
-(void)doShowDrummer:(CCButton *)menuItem;
-(void)doShowBass:(CCButton *)menuItem;
-(void)closeBandPoster;

@end
