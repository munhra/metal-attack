//
//  LevelSlider.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 5/6/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LevelSlider : CCSlider

@property (nonatomic,retain) id delegate;

@end
