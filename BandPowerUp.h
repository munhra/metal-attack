//
//  PowerUp.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 12/21/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BandPowerUp : CCSprite

{
    int posAdjustX;
    int posAdjustY;
    float pwdScale;
}

@property (nonatomic) int posAdjustX;
@property (nonatomic) int posAdjustY;
@property (nonatomic) float pwdScale;

@end
