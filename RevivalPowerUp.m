//
//  RevivalPowerUp.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 25/11/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "RevivalPowerUp.h"
#import "BandVault.h"
#import "BandVaultItem.h"
#import "CCTextureCache.h"
#import "BandPowerUp.h"

@implementation RevivalPowerUp

@synthesize RevivalAllBand_LV1;
@synthesize RevivalAllBand_LV2;
@synthesize RevivalBass_LV1;
@synthesize RevivalBass_LV2;
@synthesize RevivalDrummer_LV1;
@synthesize RevivalDrummer_LV2;
@synthesize RevivalGuitar1_LV1;
@synthesize RevivalGuitar1_LV2;
@synthesize RevivalGuitar2_LV1;
@synthesize RevivalGuitar2_LV2;
@synthesize RevivalVocal_LV1;
@synthesize RevivalVocal_LV2;

-(void)removePowerUps
{
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/revival/bass.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/revival/drummer.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/revival/vocal.png"];
}

-(void)preparePowerUps
{
 
    BandVault *vault = [BandVault sharedInstance];
    for (BandVaultItem *vaultItem in [vault selectedItems]) {
        NSString *itemId = [vaultItem.appId substringFromIndex:4];
        
        switch ([itemId intValue]) {
            case 29:
                RevivalAllBand_LV1 = (BandPowerUp *)[CCBReader load:@"revival/revivalAll1"];
                break;
            case 30:
                RevivalAllBand_LV2 = (BandPowerUp *)[CCBReader load:@"revival/revivalAll2"];
                break;
            case 31:
                RevivalGuitar1_LV1 = (BandPowerUp *)[CCBReader load:@"revival/revivalGuitar1"];
                RevivalGuitar2_LV1 = (BandPowerUp *)[CCBReader load:@"revival/revivalGuitar1"];
                break;
            case 32:
                RevivalGuitar1_LV2 = (BandPowerUp *)[CCBReader load:@"revival/revivalGuitar2"];
                RevivalGuitar2_LV2 = (BandPowerUp *)[CCBReader load:@"revival/revivalGuitar2"];
                break;
            case 33:
                RevivalBass_LV1 = (BandPowerUp *)[CCBReader load:@"revival/revivalBass1"];
                break;
            case 34:
                RevivalBass_LV2 = (BandPowerUp *)[CCBReader load:@"revival/revivalBass2"];
                break;
            case 35:
                RevivalDrummer_LV1 = (BandPowerUp *)[CCBReader load:@"revival/revivalDrummer1"];
                break;
            case 36:
                RevivalDrummer_LV2 = (BandPowerUp *)[CCBReader load:@"revival/revivalDrummer2"];
                break;
            case 37:
                RevivalVocal_LV1 = (BandPowerUp *)[CCBReader load:@"revival/revivalVocal1"];
                break;
            case 38:
                RevivalVocal_LV2 = (BandPowerUp *)[CCBReader load:@"revival/revivalVocal2"];
                break;
            default:
                break;
        }
    }
}


@end
