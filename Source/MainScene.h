//
//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  MainScene.h
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "LevelSceneController.h"
//#import <Spotify/Spotify.h>
//#import "SessionFactory.h"
#import "InfinityBG.h"
#import "MenuGuysSprite.h"
//#import <GoogleSignIn/GoogleSignIn.h>

@interface MainScene : CCNode //<GIDSignInUIDelegate>

//@property (nonatomic, strong) SPTSession *session;
//@property (nonatomic, strong) SPTAudioStreamingController *player;
@property (nonatomic, weak) CCButton *continueButton;
@property (nonatomic, weak) CCTextField *levelField;
@property (nonatomic, weak) CCButton *handButton;
@property (nonatomic, weak) InfinityBG *infinityBG;
@property (nonatomic, weak) CCSprite *girlArms;
@property (nonatomic, weak) CCButton *storeVaultButton;
@property (nonatomic, weak) MenuGuysSprite *menuGuySprite;
@property (nonatomic, weak) CCSprite *girlBothArms;
@property (nonatomic, weak) CCSprite *girlLeftArm;

-(void)playGirlEnteringSound;

@end
