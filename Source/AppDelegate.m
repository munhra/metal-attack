/*
 * SpriteBuilder: http://www.spritebuilder.org
 *
 * Copyright (c) 2012 Zynga Inc.
 * Copyright (c) 2013 Apportable Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#import "cocos2d.h"
#import "AppDelegate.h"
#import "CCBuilderReader.h"
#import "BandStoreItemsCtrl.h"
#import "LevelSceneController.h"
#import "CCTextureCache.h"
#import "GameSoundManager.h"
#import "GameCenterManager.h"

@interface CCAppDelegate ()

//@property (nonatomic, retain) SPTSession *session;
//@property (nonatomic, retain) SPTAudioStreamingController *player;

@end

@implementation AppController


-(void)startGoogleAnalytics
{
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-49869258-4"];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Configure Cocos2d with the options set in SpriteBuilder
    NSString* configPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Published-iOS"]; // TODO: add support for Published-Android support
    configPath = [configPath stringByAppendingPathComponent:@"configCocos2d.plist"];
    
    NSMutableDictionary* cocos2dSetup = [NSMutableDictionary dictionaryWithContentsOfFile:configPath];
    
    // Note: this needs to happen before configureCCFileUtils is called, because we need apportable to correctly setup the screen scale factor.
#ifdef APPORTABLE
    if([cocos2dSetup[CCSetupScreenMode] isEqual:CCScreenModeFixed])
        [UIScreen mainScreen].currentMode = [UIScreenMode emulatedMode:UIScreenAspectFitEmulationMode];
    else
        [UIScreen mainScreen].currentMode = [UIScreenMode emulatedMode:UIScreenScaledAspectFitEmulationMode];
#endif
    
    // Configure CCFileUtils to work with SpriteBuilder
    [CCBReader configureCCFileUtils];
    
    // Do any extra configuration of Cocos2d here (the example line changes the pixel format for faster rendering, but with less colors)
    //[cocos2dSetup setObject:kEAGLColorFormatRGB565 forKey:CCConfigPixelFormat];
    
    [cocos2dSetup setObject:@GL_DEPTH24_STENCIL8_OES forKeyedSubscript:CCSetupDepthFormat];
    
    [self setupCocos2dWithOptions:cocos2dSetup];
    // show fps with this set to YES
    //[[CCDirector sharedDirector] setDisplayStats:YES];
    
    //#warning load items sheet this is a temporary place as it will waste memory
    
    //[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"bottles.plist"];
    /*
    [[SPTAuth defaultInstance] setClientID:@"f335d557b99845a5bb24cb2a5119c351"];
    [[SPTAuth defaultInstance] setRedirectURL:[NSURL URLWithString:@"vntradio-login://callback"]];
    [[SPTAuth defaultInstance] setRequestedScopes:@[SPTAuthStreamingScope]];
    
    // Construct a login URL and open it
    NSURL *loginURL = [[SPTAuth defaultInstance] loginURL];
    
    // Opening a URL in Safari close to application launch may trigger
    // an iOS bug, so we wait a bit before doing so.
    [application performSelector:@selector(openURL:)
                      withObject:loginURL afterDelay:0.1];
    
    */
    
    //[[GameSoundManager sharedInstance] playBackgroundSong];
    //[[GameSoundManager sharedInstance] preloadBandInstrumentsEffects];
    //[[GameSoundManager sharedInstance] preLoadAllSoundEffects];
    
    [self startGoogleAnalytics];
    [AdColony configureWithAppID:@"app785967c5c7664bd884" zoneIDs:@[@"vz548d673f185e4d4294",@"vz2b7ca1612b934349a8"] delegate:self logging:YES];
    [[GameCenterManager sharedInstance] authenticateLocalUser];
    //[[GameCenterManager sharedInstance] resetAchivments];
    [[GameCenterManager sharedInstance] downloadAchivmentsDescriptions];
    return YES;
}

/*
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    //[super application:application openURL:url sourceAplication:soureApplication annotation:annotation];
    
    //return [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation];
 
    if ([[SPTAuth defaultInstance] canHandleURL:url]) {
        [[SPTAuth defaultInstance] handleAuthCallbackWithTriggeredAuthURL:url callback:^(NSError *error, SPTSession *session) {
            
            if (error != nil) {
                NSLog(@"*** Auth error: %@", error);
                return;
            }
            
            [[SessionFactory sharedInstance] setSession:session];
            [[SessionFactory sharedInstance] playUsingSession];
            
        }];
        return YES;
    }
    
    //return NO;
}*/

/*
-(void)playUsingSession:(SPTSession *)session {
    
    // Create a new player if needed
    if (self.player == nil) {
        self.player = [[SPTAudioStreamingController alloc] initWithClientId:[SPTAuth defaultInstance].clientID];
    }
    
    [self.player loginWithSession:session callback:^(NSError *error) {
        if (error != nil) {
            NSLog(@"*** Logging in got error: %@", error);
            return;
        }
        
        NSURL *trackURI = [NSURL URLWithString:@"spotify:track:58s6EuEYJdlb0kO7awm3Vp"];
        [self.player playURIs:@[ trackURI ] fromIndex:0 callback:^(NSError *error) {
            if (error != nil) {
                NSLog(@"*** Starting playback got error: %@", error);
                return;
            }
        }];
    }];
}*/

-(void)onAdColonyV4VCReward:(BOOL)success currencyName:(NSString *)currencyName currencyAmount:(int)amount inZone:(NSString *)zoneID
{
    if (success) {
        NSLog(@"Reward the user !!");
        [[BandVault sharedInstance] updateBandCoins:250];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Reward100Coins" object:nil];
    }else{
        NSLog(@"Not Reward the user !!");
    }
}


-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    NSLog(@"******* Application received a memory warning ! ******");
    //[[CCTextureCache sharedTextureCache] dumpCachedTextureInfo];
}

-(void)dealloc{
    NSLog(@"Dealloc appdelegate");
}

- (CCScene*) startScene
{
    [[BandStoreItemsCtrl sharedInstance] loadBandItemsFromJson];
    LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
    [lvcontroller loadLevelJson];
    return [CCBReader loadAsScene:@"MainMenu"];
}

@end
