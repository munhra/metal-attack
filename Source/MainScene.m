//
//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "MainScene.h"
#import "CCTextureCache.h"
#import "ItemSelection.h"
#import "GameSoundManager.h"
#import "LevelSelector.h"
#import "MetalAttackAnalytics.h"
#import "TutorialScene.h"
#import "FullEndScene.h"
#import "GameCenterManager.h"
#import <AdColony/AdColony.h>
//#import <gpg/GooglePlayGames.h>

@implementation MainScene

//@synthesize player;
//@synthesize session;
@synthesize levelField;
@synthesize continueButton;
@synthesize handButton;
@synthesize infinityBG;
@synthesize girlArms;
@synthesize storeVaultButton;
@synthesize menuGuySprite;
@synthesize girlBothArms;
@synthesize girlLeftArm;

bool rightMoved;

static NSString *const kClientID = @"355413642212-i2u2f2vr4t3ton9l1crl7ei7hia6m6hq.apps.googleusercontent.com";

-(id)init{
    
    self = [super init];
    
    if (self) {
        NSLog(@"MainScene init called !!!");
        rightMoved = NO;
    }
    return self;
}

-(void)runHandButtonAnimation {
    //[[[self handButton] animationManager] runAnimationsForSequenceNamed:@"HandAnimation"];

}

-(void)onExit {
    [[GameSoundManager sharedInstance] stopBackgroundSong];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}
    
-(void)onEnter {
    [super onEnter];
    [[GameSoundManager sharedInstance] playMainMenuSong];
    
    [[self infinityBG] setBGColor:[[UniversalInfo sharedInstance] generateRandomColor]];
    
    self.levelField.string = @"0";
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    int levelToLoad = [userdef integerForKey:@"Level"];
    /*
    if (levelToLoad == 0) {
        [[self continueButton] setVisible:NO];
    }else{
        [[self continueButton] setVisible:YES];
    }*/
    [self runHandButtonAnimation];
    [[MetalAttackAnalytics sharedInstance] registerScreenAnalytics:@"MainMenu"];
    [self hideGirlStoreButton:NO];
    [[CCTextureCache sharedTextureCache] dumpCachedTextureInfo];
    if ([[UniversalInfo sharedInstance] isTesting]) {
        [self setupTestTimer];
    }
    
    //CCAppDelegate* app = (CCAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //[GIDSignIn sharedInstance].uiDelegate = app.navController;
    //[GIDSignIn sharedInstance].uiDelegate = self;
    //[[GPGManager sharedInstance] signInWithClientID:kClientID silently:YES];
}
/*
-(void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    NSLog(@"presentViewController");
    CCAppDelegate* app = (CCAppDelegate*)[[UIApplication sharedApplication] delegate];
    [app.navController presentViewController:viewController animated:YES completion:nil];
}*/

-(void)setupTestTimer
{
    //__weak LevelCleared *weakSelf = self;
    [self scheduleBlock:^(CCTimer *timer) {
        //weakSelf.levelEnemiesLeft = 0;
        //[self doNextLevel];
        //[self doMenu:nil];
        if ([[UniversalInfo sharedInstance] gotoStore]) {
            [self doStore];
            [[UniversalInfo sharedInstance] setGotoStore:NO];
        }else{
            [self doNewGame];
            [[UniversalInfo sharedInstance] setGotoStore:YES];
        }
    } delay:5];
}

-(void)doRight
{
    NSLog(@"doRight");
    if (rightMoved) {
        [[self animationManager] runAnimationsForSequenceNamed:@"MoveMenuLeft"];
        rightMoved = NO;
    }else{
        [[self animationManager] runAnimationsForSequenceNamed:@"MoveMenuRight"];
        rightMoved = YES;
        [[GameCenterManager sharedInstance] achivmentUpdate:@"GreenHandMate" withCallBack:^(AchievimentBanner *achievimentBanner) {
            if (achievimentBanner) {
                [self addChild:achievimentBanner];
                [achievimentBanner animateMessageInAndOut];
            }
        }];
    }
}

-(void)doNewGame
{
    NSLog(@"doNewGame");
    
    CCNode *door = [self getChildByName:@"Door" recursively:YES];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        Boolean isTutorialPlayed = [userdef boolForKey:@"TUTORIAL_PLAYED"];
        
        if (isTutorialPlayed) {
            NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
#warning set level reached
            //[userdef setInteger:99 forKey:@"Level"];
            CCScene *levelSelectionScence = [CCBReader loadAsScene:@"LevelSelector"];
            [levelSelectionScence getChildByName:@"LevelSelector" recursively:NO];
            LevelSelector *levelSelector = (LevelSelector*)[levelSelectionScence getChildByName:@"LevelSelector" recursively:NO];
            [[CCDirector sharedDirector] replaceScene:levelSelectionScence];
        }else{
            CCScene *scene = [CCBReader loadAsScene:@"TutorialScene"];
            TutorialScene *tutorialScene = (TutorialScene *)[scene getChildByName:@"TutorialScene" recursively:YES];
            [tutorialScene setCalledFromGamePlay:YES];
            [[CCDirector sharedDirector] replaceScene:scene];
        }

    }];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
}

-(void)doStore
{
    NSLog(@"doStore");
    CCNode *door = [self getChildByName:@"Door" recursively:YES];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"Store"]];
    }];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
}

-(void)doBestiary
{
 
    //[[UniversalInfo sharedInstance] playInterstitialVideo:100];
    
    //[[GPGManager sharedInstance] signInWithClientID:kClientID silently:NO];
    
    /*
    NSLog(@"doBestiary");
    
    BandStoreItemsCtrl *storeCtrl = [BandStoreItemsCtrl sharedInstance];
    BandVault *vault = [BandVault sharedInstance];
    
    BandStoreItem *itemDrummer = [[storeCtrl drummers] objectAtIndex:vault.drummerIndex];
    BandStoreItem *itemVocal = [[storeCtrl vocals] objectAtIndex:vault.vocalIndex];
    BandStoreItem *itemBase = [[storeCtrl leadGuitars] objectAtIndex:vault.guitar2Index];
    BandStoreItem *itemLead = [[storeCtrl baseGuitars] objectAtIndex:vault.guitar1Index];
    BandStoreItem *itemBass = [[storeCtrl basses] objectAtIndex:vault.bassIndex];
    
    //[[UniversalInfo sharedInstance] playVideoForReward:90];
    CCScene *endGameScn = [CCBReader loadAsScene:@"FullEndScene"];
    FullEndScene *endGame = (FullEndScene*)[endGameScn getChildByName:@"FullEndScene" recursively:YES];

    NSString *drummerPath = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",itemDrummer.level, itemDrummer.itemName];
    NSString *basePath = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",itemBase.level, itemBase.itemName];
    NSString *bassPath = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",itemBass.level, itemBass.itemName];
    NSString *leadPath = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",itemLead.level, itemLead.itemName];
    NSString *vocalPath = [NSString stringWithFormat:@"BandGuys/end/Level%d/%@End",itemVocal.level, itemVocal.itemName];
    
    endGame.drummerEndSprite = (CCSprite *)[CCBReader load:drummerPath];
    endGame.baseEndSprite  = (CCSprite *)[CCBReader load:basePath];
    endGame.leadEndSprite = (CCSprite *)[CCBReader load:leadPath];
    endGame.bassEndSprite = (CCSprite *)[CCBReader load:bassPath];
    endGame.vocalEndSprite = (CCSprite *)[CCBReader load:vocalPath];
    
    endGame.drummerThinEndSprite = (CCSprite *)[CCBReader load:drummerPath];
    endGame.baseThinEndSprite = (CCSprite *)[CCBReader load:basePath];
    endGame.leadThinEndSprite = (CCSprite *)[CCBReader load:leadPath];
    endGame.bassThinEndSprite = (CCSprite *)[CCBReader load:bassPath];
    endGame.vocalThinEndSprite = (CCSprite *)[CCBReader load:vocalPath];
    
    endGame.endSpritesArray = [NSArray arrayWithObjects: endGame.drummerEndSprite, endGame.baseEndSprite ,  endGame.leadEndSprite , endGame.bassEndSprite, endGame.vocalEndSprite, nil];
    
    endGame.endThinSpriteArray = [NSArray arrayWithObjects: endGame.drummerThinEndSprite, endGame.baseThinEndSprite, endGame.leadThinEndSprite, endGame.bassThinEndSprite, endGame.vocalThinEndSprite, nil];
    
    endGame.bandGuysItem = [[NSMutableArray alloc] initWithArray:@[itemDrummer.itemName,itemBase.itemName,itemLead.itemName,itemBass.itemName,itemVocal.itemName]];
    
    endGame.bandGuysEndingText = [[NSMutableArray alloc] initWithArray:@[itemDrummer.endingText,itemBase.endingText,itemLead.endingText,itemBass.endingText,itemVocal.endingText]];

    [[CCDirector sharedDirector] replaceScene:endGameScn withTransition:[CCTransition transitionFadeWithDuration:2]];
    */
    
}

-(void)doTutorial
{
    NSLog(@"doTutorial");
    CCNode *door = [self getChildByName:@"Door" recursively:YES];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        CCScene *scene = [CCBReader loadAsScene:@"TutorialScene"];
        TutorialScene *tutorialScene = (TutorialScene *)[tutorialScene getChildByName:@"TutorialScene" recursively:YES];
        [tutorialScene setCalledFromGamePlay:NO];
        [[CCDirector sharedDirector] replaceScene:scene];
    }];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
}

-(void)doGameCenter
{
    NSLog(@"doGameCenter");
    [[GameCenterManager sharedInstance] showLeaderBoard:[UIApplication sharedApplication].keyWindow.rootViewController];
}

-(void)doCredits
{
    NSLog(@"doCredits");
    CCNode *door = [self getChildByName:@"Door" recursively:YES];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"Credits"]];
    }];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    
}

-(void)doContinue
{
    NSLog(@"doContinue");
    
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    int levelToLoad = [userdef integerForKey:@"Level"];

    

    CCNode *door = [self getChildByName:@"Door" recursively:YES];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        /*
        CCScene *itemSelectionScence = [CCBReader loadAsScene:@"ItemSelection"];
        [itemSelectionScence getChildByName:@"ItemSelection" recursively:NO];
        ItemSelection *itemSelection = (ItemSelection*)[itemSelectionScence getChildByName:@"ItemSelection" recursively:NO];
        
        [itemSelection setNextLevel:levelToLoad];
        [[CCDirector sharedDirector] replaceScene:itemSelectionScence];
         */
        
        CCScene *levelSelectionScence = [CCBReader loadAsScene:@"LevelSelector"];
        [levelSelectionScence getChildByName:@"LevelSelector" recursively:NO];
        LevelSelector *levelSelector = (LevelSelector*)[levelSelectionScence getChildByName:@"LevelSelector" recursively:NO];
        
        //[itemSelection setNextLevel:levelToLoad];
        [[CCDirector sharedDirector] replaceScene:levelSelectionScence];
        
        
        
    }];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];

}

-(void)hideGirlStoreButton:(BOOL)hide
{
    [[self girlBothArms] setVisible:hide];
    [[self girlLeftArm] setVisible:!hide];
    [[self storeVaultButton] setVisible:!hide];
}

-(void)playGirlEnteringSound
{
    NSLog(@"playGirlEnteringSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}

-(void)playGuyEnteringSound
{
    NSLog(@"playGuyEnteringSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}

-(void)playGameCenterEnteringSound
{
    NSLog(@"playGameCenterEnteringSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx030"];
}

-(void)playMenuTransitionSound
{
    NSLog(@"playMenuTransitionSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx041"];
}

-(void)playLogoVibrationSound
{
    NSLog(@"playLogoVibrationSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx009"];
}

-(void)playWallCrackSound
{
    NSLog(@"playWallCrackSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx050"];
}

-(void)dealloc
{
    NSLog(@"Dealloc maiscene");
    //[self removeAllChildren];
    //[[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[CCTextureCache sharedTextureCache] dumpCachedTextureInfo];
    
}

@end
