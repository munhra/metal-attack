//
//  AchievimentBanner.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 08/09/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface AchievimentBanner : CCNode {
    CCLabelTTF *messageText;
    //CCSprite *achievimentCoin;
}

@property(nonatomic,retain) CCLabelTTF *messageText;
@property(nonatomic,weak) CCSprite *achievimentCoin;
@property(nonatomic,weak) CCSprite *banner;

//+(id)sharedInstance;
-(void)setBannerPosition;
-(AchievimentBanner *)createAchievmentBanner;
-(void)setTextMessage:(NSString *)text;
-(void)setCoinSpriteFrame:(CCSpriteFrame *)spriteFrame;
-(void)animateMessageInAndOut;

@end
