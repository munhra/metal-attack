//
//  EndGame.h
//  EightbitShooter
//
//  Created by Rafael Munhoz on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface EndGame : CCNode
{
    int nextLevel;
}

@property(nonatomic) int nextLevel;

@end
