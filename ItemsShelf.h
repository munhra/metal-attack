//
//  ItemsShelf.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 25/04/15.
//  Copyright 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface ItemsShelf : CCNode {
    id delegate;
}

@property(nonatomic,retain) id delegate;

-(void)showItemSummary:(CCButton *)sender;

@end
