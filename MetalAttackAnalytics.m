//
//  MetalAttackAnalytics.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 6/11/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "MetalAttackAnalytics.h"
#import "GAI.h"
//#import <gpg/GooglePlayGames.h>


//#define EVENT_BUY_050_HEALING_01 @"CgkI5Je3gqwKEAIQAA"

@implementation MetalAttackAnalytics

+ (MetalAttackAnalytics *)sharedInstance
{
    static MetalAttackAnalytics *myInstance = nil;
    if (nil == myInstance) {
        myInstance  = [[[self class] alloc] init];
    }
    return myInstance;
}

-(void)registerScreenAnalytics:(NSString *)screenName
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)registerBandDrinkBuy:(NSString *)powerupName quantity:(NSString *)qtd
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"STORE"
                                                          action:@"BUY"
                                                           label:powerupName
                                                           value:nil] build]];
}

-(void)registerBandGuyBuy:(NSString *)bandGuyName
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"STORE"
                                                          action:@"BUY"
                                                           label:bandGuyName
                                                           value:nil] build]];
}

-(void)registerBandGuySelect:(NSString *)bandGuyName
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"STORE"
                                                          action:@"SELECT"
                                                           label:bandGuyName
                                                           value:nil] build]];

}

-(void)registerLevelGameOver:(NSString *)levelNumber
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"LEVEL"
                                                          action:@"GAMEOVER"
                                                           label:levelNumber
                                                           value:nil] build]];
}

-(void)registerLevelClear:(NSString *)levelNumber
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"LEVEL"
                                                          action:@"LEVELCLEARED"
                                                           label:levelNumber
                                                           value:nil] build]];
}

// Google play game services
/*
-(void)registerEvent:(NSString *)eventID withQTD:(int)qtd
{
    [GPGEvent eventForId:EVENT_BUY_050_HEALING_01 completionHandler:^(GPGEvent *event, NSError *error) {
        if (event) {
            [event incrementBy:qtd];
            NSLog(@"increment event count now %d",event.count);
        }else{
            NSLog(@"Error while incrementing %@",error);
        }
    }];
}*/

@end
