//
//  EndSprite.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 7/9/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "EndSprite.h"

@implementation EndSprite

@synthesize endingClipMask;
@synthesize endClippingNode;

/*
-(void)visit
{
    if (!self.visible) {
        return;
    }
    
    glEnable(GL_SCISSOR_TEST);
    glScissor(10,10,50,50);
    [super visit];
    glDisable(GL_SCISSOR_TEST);
}
*/

@end
