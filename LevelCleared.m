//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//
//  LevelCleared.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 08/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "LevelCleared.h"
#import "LevelSceneController.h"
#import "CCTextureCache.h"
#import "ItemSelection.h"
#import "MetalAttackAnalytics.h"
#import "GameSoundManager.h"
#import "UniversalInfo.h"
#import <AdColony/AdColony.h>
#import "GameCenterManager.h"

@implementation LevelCleared

@synthesize nextLevel;
@synthesize door;
@synthesize drummerPosterFrame;
@synthesize vocalPosterFrame;
@synthesize leadPosterFrame;
@synthesize basePosterFrame;
@synthesize bassPosterFrame;
@synthesize facebookButton;
@synthesize buttonsLayoutBox;
@synthesize stageClearBase;
@synthesize stageClearText;
@synthesize stageClearMessage;
@synthesize metalLogo;
@synthesize endInfinityBG;
@synthesize basePosterFileName;
@synthesize bassPosterFileName;
@synthesize leadPosterFileName;
@synthesize vocalPosterFileName;
@synthesize drummerPosterFileName;
@synthesize levelContext;
@synthesize isAllGuysLeft;

-(void)didLoadFromCCB
{
    NSLog(@"Level cleared did load");
}

-(void)onEnter
{
    [super onEnter];
    [[door animationManager] runAnimationsForSequenceNamed:@"Closed"];
    [[door animationManager] runAnimationsForSequenceNamed:@"OpenDoor"];
    
    CCSprite *drummerPosterSprite = (CCSprite *)[CCBReader load:self.drummerPosterFileName];
    CCSprite *vocalPosterSprite = (CCSprite *)[CCBReader load:self.vocalPosterFileName];
    CCSprite *bassPosterSprite = (CCSprite *)[CCBReader load:self.bassPosterFileName];
    CCSprite *guitarLeadPosterSprite = (CCSprite *)[CCBReader load:self.leadPosterFileName];
    CCSprite *guitarBasePosterSprite = (CCSprite *)[CCBReader load:self.basePosterFileName];
    
    [self.drummerPoster setSpriteFrame:[drummerPosterSprite spriteFrame]];
    [self.vocalPoster setSpriteFrame:[vocalPosterSprite spriteFrame]];
    [self.bassPoster setSpriteFrame:[bassPosterSprite spriteFrame]];
    [self.guitarLeadPoster setSpriteFrame:[guitarLeadPosterSprite spriteFrame]];
    [self.guitarBasePoster setSpriteFrame:[guitarBasePosterSprite spriteFrame]];
    
    
    [[MetalAttackAnalytics sharedInstance] registerScreenAnalytics:@"LevelCleared"];
    [self doRandomTintBackground];
    [[GameSoundManager sharedInstance] playLevelClearedSong];
    
    if ([[UniversalInfo sharedInstance] isTesting]){
        [self setupTestTimer];
    }
    
    [self performAchivment];
    
    if ([self isAllGuysLeft]) {
        [self peformAllGuysLeftAchivment];
    }
    
    [[UniversalInfo sharedInstance] playInterstitialVideo:2];
}

-(void)peformAllGuysLeftAchivment
{
    NSString *achivmentName = @"";
    
    switch (levelContext) {
        case 1:
            achivmentName = @"GarageBandEndurace";
            break;
        case 2:
            achivmentName = @"SchoolSurvival";
            break;
        case 3:
            achivmentName = @"NoDieAtBar";
            break;
        case 4:
            achivmentName = @"FlawlessPrisionBreak";
            break;
        case 5:
            achivmentName = @"GasStationRuler";
            break;
        default:
            return;
    }
    
    if (![[GameCenterManager sharedInstance] checkAchivmentCompleteByKey:achivmentName]) {
        [[GameCenterManager sharedInstance] achivmentUpdate:achivmentName withCallBack:^(AchievimentBanner *achievimentBanner) {
            if (achievimentBanner) {
                [self addChild:achievimentBanner];
                [achievimentBanner animateMessageInAndOut];
                [[GameCenterManager sharedInstance] setAchivmentCompleteByKey:achivmentName];
            }
        }];
    }
}

-(void)performAchivment
{
    NSString *achivmentName = @"";
    
    switch (levelContext) {
        case 2:
            achivmentName = @"GarageFlawless";
            break;
        case 3:
            achivmentName = @"SchoolsOut";
            break;
        case 4:
            achivmentName = @"FromDusk";
            break;
        case 5:
            achivmentName = @"PrisonBreak";
            break;
        //case 5:
        //    achivmentName = @"BrickOctan";
        //    break;
        default:
            
            return;
    }
    
    if (![[GameCenterManager sharedInstance] checkAchivmentCompleteByKey:achivmentName]) {
        [[GameCenterManager sharedInstance] achivmentUpdate:achivmentName withCallBack:^(AchievimentBanner *achievimentBanner) {
            if (achievimentBanner) {
                [self addChild:achievimentBanner];
                [achievimentBanner animateMessageInAndOut];
                [[GameCenterManager sharedInstance] setAchivmentCompleteByKey:achivmentName];
            }
        }];
    }
}

-(void)setupTestTimer
{
    //__weak LevelCleared *weakSelf = self;
    [self scheduleBlock:^(CCTimer *timer) {
        
        if ([[UniversalInfo sharedInstance] gotoNextLevel]){
            [self doNextLevel];
            [[UniversalInfo sharedInstance] setGotoNextLevel:NO];
        }else{
            [self doMenu:nil];
            [[UniversalInfo sharedInstance] setGotoNextLevel:YES];
        }
        //weakSelf.levelEnemiesLeft = 0;
        //[self doNextLevel];
        
    } delay:5];
}


-(void)doRandomTintBackground
{
    [[self endInfinityBG] setBGColor:[[UniversalInfo sharedInstance] generateRandomColor]];
}

-(void)onExit
{
    [super onExit];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    [[UniversalInfo sharedInstance] playVideoForReward:10];
    [[GameSoundManager sharedInstance] stopBackgroundSong];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [self removeLevelClearedTextures];
}

-(void)removeLevelClearedTextures
{
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/LevelCleared/posters/drumms.png"];
}

-(void)setNextLevel:(int)level
{
    NSLog(@"Save the next level");
    nextLevel = level;
    [[[self door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    [[[self door] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[LevelSceneController sharedInstance] saveLastReachedLevel:level];
    }];
}

-(void)postOnFacebook
{
 
    [[GameCenterManager sharedInstance] achivmentUpdate:@"SocialStalker" withCallBack:^(AchievimentBanner *achievimentBanner) {
        if (achievimentBanner) {
            [self addChild:achievimentBanner];
            [achievimentBanner animateMessageInAndOut];
        }
    }];
    
    [[self stageClearMessage] setVisible:NO];
    [[self stageClearText] setVisible:NO];
    [[self stageClearBase] setVisible:NO];
    [[self metalLogo] setVisible:YES];
    [[self buttonsLayoutBox] setVisible:NO];
    [[self facebookButton] setVisible:NO];
    
    UIImage *scrShot = [[UniversalInfo sharedInstance] screenshot];
    
    [[self facebookButton] setVisible:YES];
    [[self stageClearMessage] setVisible:YES];
    [[self stageClearText] setVisible:YES];
    [[self stageClearBase] setVisible:YES];
    [[self metalLogo] setVisible:NO];
    [[self buttonsLayoutBox] setVisible:YES];
    

    NSLog(@"Screen shot !!!");
    //NSURL *gamelink = [[NSURL alloc] initWithString:@"http://8bs.com.br/"];
    NSArray *items = @[scrShot];
    UIActivityViewController *shareViewController =[[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
    [[CCDirector sharedDirector] presentViewController:shareViewController animated:YES completion:nil];
}

-(void)doMenu:(CCNode *)sender
{
    if ([[self animationManager] runningSequenceName] == nil) {
        NSLog(@"Goto Menu");
        [[[self door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
        [[[self door] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
            [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
        }];
    }
}

-(void)doNextLevel
{
    NSLog(@"doGamePlay");
    __weak LevelCleared *weakSelf = self;
    [[[self door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    [[[self door] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
        [[CCDirector sharedDirector] replaceScene:[lvcontroller loadLevelScene:[weakSelf nextLevel] waveNumber:0]];
    }];
    //LevelSceneController *lvcontroller = [LevelSceneController sharedInstance];
    //[[CCDirector sharedDirector] replaceScene:[lvcontroller loadLevelScene:nextLevel waveNumber:0] withTransition:[CCTransition transitionCrossFadeWithDuration:0.3]];
}

-(void)doPreviousLevel
{
    NSLog(@"Goto PreviousLevel");
    [[[self door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    __weak LevelCleared *weakSelf = self;
    [[[self door] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[[LevelSceneController sharedInstance] loadLevelScene:[weakSelf nextLevel] - 1 waveNumber:0]];
    }];
}

-(void)doCallVan
{
    NSLog(@"Goto Do Call van");
    
    
    NSLog(@"Goto NextLevel %d",self.nextLevel);
    //[[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[[self door] animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    [[[self door] animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
#warning removed the line bellow to test the solutino for the non deallocation problem
        //[[CCDirector sharedDirector] replaceScene:[[LevelSceneController sharedInstance] loadLevelScene:nextLevel waveNumber:0] withTransition:[CCTransition transitionCrossFadeWithDuration:0.3]];
        //[[CCDirector sharedDirector] replaceScene:[lvcontroller loadLevelScene:0 waveNumber:0] withTransition:[CCTransition transitionCrossFadeWithDuration:0.3]];
    }];
    
    [self scheduleBlock:^(CCTimer *timer) {
        CCScene *itemSelectionScence = [CCBReader loadAsScene:@"ItemSelection"];
        [itemSelectionScence getChildByName:@"ItemSelection" recursively:NO];
        ItemSelection *itemSelection = (ItemSelection*)[itemSelectionScence getChildByName:@"ItemSelection" recursively:NO];
        
        [itemSelection setNextLevel:nextLevel];
        [[CCDirector sharedDirector] replaceScene:itemSelectionScence];
    } delay:0.5];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    
}


- (UIImage *)renderUIImageFromSprite:(CCSprite *)sprite {
    int tx = sprite.contentSize.width;
    int ty = sprite.contentSize.height;
    CCRenderTexture *renderer = [CCRenderTexture renderTextureWithWidth:tx height:ty];
    const CGPoint ANCHORBEFORE = sprite.anchorPoint;
    self.anchorPoint = CGPointZero;
    sprite.anchorPoint  = CGPointZero;
    [renderer begin];
    [sprite visit];
    [renderer end];
    sprite.anchorPoint = ANCHORBEFORE;
    return [renderer getUIImage];
}

-(void)tintBackGround:(CCSprite *)sprite {
    UIImage *spriteImage = [[UniversalInfo sharedInstance] screenshot];
    UIColor *bgColor = [[UniversalInfo sharedInstance] averageColor:spriteImage];
    CCColor *bgccColor = [CCColor colorWithUIColor:bgColor];
    [[self endInfinityBG] setBGColor:bgccColor];
}

-(void)playBandGuysShow
{
    NSLog(@"playBandGuysShow");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx025"];
}

-(void)playLevelClearedMessage
{
    NSLog(@"playLevelClearedMessage");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx068"];
}

-(void)playButtonAppearSound
{
    NSLog(@"playHudAppearSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx031"];
}

-(void)playLettersShowSound
{
    NSLog(@"playLettersShowSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx067"];
}

-(void)dealloc
{
    NSLog(@"***** Dealloc clear level *****");
}


@end
