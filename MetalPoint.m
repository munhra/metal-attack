//
//  MetalPoint.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 27/03/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

#import "MetalPoint.h"

@implementation MetalPoint

@synthesize point;

-(id)initWithPoint:(CGPoint)newPoint {
    self = [super init];
    if (self){
        self.point = newPoint;
    }
    return self;
}

@end
