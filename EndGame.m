//
//  EndGame.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EndGame.h"
#import "LevelSceneController.h"

@implementation EndGame

@synthesize nextLevel;

-(void)doMenu
{
    NSLog(@"doMenu");
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
    
}

-(void)doPreviousLevel
{
    NSLog(@"doPreviousLevel");
    NSLog(@"Goto PreviousLevel");
    [[CCDirector sharedDirector] replaceScene:[[LevelSceneController sharedInstance] loadLevelScene:nextLevel waveNumber:0]];

}

@end
