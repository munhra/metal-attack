//
//  BandGuySprite.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 01/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface BandGuySprite : CCSprite {
    
    CGPoint rightAnchorPoint;
    CGPoint leftAnchorPoint;
    CGPoint leftGuitarPosition;
    CGPoint rightGuitarPosition;
    
    CGPoint rightBackAnchorPoint;
    CGPoint leftBackAnchorPoint;
    CGPoint leftBackGuitarPosition;
    CGPoint rightBackGuitarPosition;
    
    CGPoint vocalRightAnchorPoint;
    CGPoint vocalLeftAnchorPoint;
    CGPoint leftVocalPosition;
    CGPoint rightVocalPosition;
    
    NSString *itemID;
    float vocalRotFactor;
    float guitarRotFactor;
    
}

@property(nonatomic) CGPoint rightAnchorPoint;
@property(nonatomic) CGPoint leftAnchorPoint;
@property(nonatomic) CGPoint leftGuitarPosition;
@property(nonatomic) CGPoint rightGuitarPosition;

@property(nonatomic) CGPoint rightBackAnchorPoint;
@property(nonatomic) CGPoint leftBackAnchorPoint;
@property(nonatomic) CGPoint leftBackGuitarPosition;
@property(nonatomic) CGPoint rightBackGuitarPosition;

@property(nonatomic) CGPoint vocalRightAnchorPoint;
@property(nonatomic) CGPoint vocalLeftAnchorPoint;
@property(nonatomic) CGPoint leftVocalPosition;
@property(nonatomic) CGPoint rightVocalPosition;
@property(nonatomic) float vocalRotFactor;
@property(nonatomic) float guitarRotFactor;

-(void)restoreOriginalState;
-(void)prepareForDeath;
-(void)performTintHit;
-(void)prepareForRevival;

@end
