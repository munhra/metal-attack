//
//  RewardVideoDelegate.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 6/15/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "RewardVideoDelegate.h"

@implementation RewardVideoDelegate
/*
/// Tells the delegate that the reward based video ad has been received.
- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"rewardBasedVideoAdDidReceiveAd");
}

/// Tells the delegate that the reward based video ad is opened.
- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"rewardBasedVideoAdDidOpen");
}

/// Tells the delegate that the reward based video ad has started playing.
- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"rewardBasedVideoAdDidStartPlaying");
}

/// Tells the delegate that the reward based video ad is closed.
- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"rewardBasedVideoAdDidClose");
}

/// Tells the delegate that the reward based video ad will leave the application.
- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    NSLog(@"rewardBasedVideoAdWillLeaveApplication");
}

/// Tells the delegate that the reward based video ad has rewarded the user.
- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward
{
    NSLog(@"rewardBasedVideoAd");
}

/// Tells the delegate that the reward based video ad has failed to load.
- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error
{
    NSLog(@"rewardBasedVideoAd");
}

- (void)dealloc
{
    NSLog(@"dealloc RewardVideoDelegate");
}*/

@end
