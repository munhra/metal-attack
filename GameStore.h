//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  GameStore.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 16/11/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameStoreObserver.h"
#import "BandBuyPoster.h"
#import "ItemSummary.h"
#import "ItemsShelf.h"
#import "MessagesSprite.h"
#import <StoreKit/StoreKit.h>

typedef enum {
    
    NOFILTER = 0,
    GUITARFILTER = 1,
    BASSFILTER = 2,
    VOCALFILTER = 3,
    DRUMMEFILTER = 4,
    COINSFILTER = 5,
    BANDFILTER = 6,
    COOLDOWNFILTER = 7,
    HEALINGFILTER = 8,
    MEGASHOOTFILTER = 9,
    SHIELDFILTER = 10
    
} ItemFilter;

@interface GameStore : CCNode <SKProductsRequestDelegate,SKPaymentTransactionObserver>{
    
    //BandBuyPoster *bandBuyPoster;
    NSArray *displayItems;
    //CCNode *door;
    //ItemSummary *itemSummary;
    BandStoreItem *selectedItem;
    CCLabelTTF *bandCoinsLabel;
    CCNode *itemsNode1;
    CCNode *itemsNode2;
    //ItemsShelf *itemsShelf;
    NSMutableArray *itemButtons;
    int nextItemCounter;
    int megaShootItemCounter;
    int shieldItemCounter;
    //CCSprite *punk;
    CCButton *moreItems;
    MessagesSprite *message;
    MessagesSprite *storeWaitMessage;
    ItemFilter activeItemFilter;
    bool waitForAppleStore;
}

@property(weak,nonatomic) BandBuyPoster *bandBuyPoster;
@property(retain,nonatomic) NSArray *displayItems;
@property(weak,nonatomic) CCNode *door;
@property(weak,nonatomic) ItemSummary *itemSummary;
@property(retain,nonatomic) BandStoreItem *selectedItem;
@property(weak,nonatomic) CCSprite *punk;
@property(retain,nonatomic) CCLabelTTF *bandCoinsLabel;
@property(retain,nonatomic) CCButton *moreItems;
@property(weak,nonatomic) CCLabelTTF *price1;
@property(weak,nonatomic) CCLabelTTF *price2;
@property(weak,nonatomic) CCLabelTTF *price3;
@property(weak,nonatomic) CCLabelTTF *price4;
@property(weak,nonatomic) CCLabelTTF *price5;
@property(retain,nonatomic) CCNode *itemsNode1;
@property(retain,nonatomic) CCNode *itemsNode2;
@property(weak,nonatomic) ItemsShelf *itemsShelf;
@property(nonatomic) int nextItemCounter;
@property(nonatomic) int megaShootItemCounter;
@property(nonatomic) int shieldItemCounter;
@property(retain, nonatomic) NSMutableArray *itemButtons;
@property(retain, nonatomic) MessagesSprite *message;
@property(retain, nonatomic) MessagesSprite *storeWaitMessage;
@property(weak,nonatomic) CCButton *drummerButton;
@property(weak,nonatomic) CCButton *bassButton;
@property(weak,nonatomic) CCButton *baseButton;
@property(weak,nonatomic) CCButton *leadButton;
@property(weak,nonatomic) CCButton *vocalButton;
@property(weak,nonatomic) CCSprite *guyInfo;
@property(weak,nonatomic) CCSprite *posterInfoGuyPicture;
@property(weak,nonatomic) CCSprite *filterDrinksMenu;
@property(weak,nonatomic) CCSprite *moreDrinks;
@property (nonatomic,retain) CCLayoutBox *bandDamageStarsGroup1;
@property (nonatomic,retain) CCLayoutBox *bandDamageStarsGroup2;
@property (nonatomic,retain) CCLayoutBox *bandArmorStarsGroup1;
@property (nonatomic,retain) CCLayoutBox *bandArmorStarsGroup2;
@property (nonatomic,weak) CCLabelTTF *moneyBox;
@property (nonatomic) bool waitForAppleStore;
@property (nonatomic) bool purchaseCanceled;
@property (nonatomic) ItemFilter activeItemFilter;
//@property (nonatomic,retain) SKProductsRequest *prequest;

//Store
@property (nonatomic,retain) SKProduct *coinPack;
@property (nonatomic,retain) SKPayment *coinPackPayment;
@property (nonatomic,retain) SKPaymentQueue *paymentQueue;


-(void)doBuyBandGuy;
-(void)doBuyItem:(int)itemQtd;
-(void)moveItemSummaryOut;
-(void)closeBandPoster;

@end
