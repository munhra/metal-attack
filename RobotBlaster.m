//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  RobotBlaster.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RobotBlaster.h"
#import "UniversalInfo.h"
#import "CCAnimation.h"
#import "LevelScene.h"
#import "CCTextureCache.h"
#import "GameSoundManager.h"
#import "MetalPoint.h"

/*
    Enemy variable parameters
    Enemy name
    Armor
    WeaponDamage
    Score Points
    Time to reach hero
    Number of frames

    By default all the enemies animations will have 3 frames and will be divided into 
    two groups, the normal walking animation that will be present on frames 1 to 2 and the 
    attack animation that will be on frames 2 to 3. 

    Otimization initialize all animation frames on the creation of the enemy
 
 */

@implementation RobotBlaster

/*
 * Divides the animation into walking animation and attack animation
 */
-(void)divideAnimations:(CCSpriteFrame *)frame animFrames:(NSMutableArray *)animFrames
           attackFrames:(NSMutableArray *)attackFrames frameidx:(int)i{
    
    // this method will check too if the type of enemy is autodestruction, if it is the case
    // the logic below will change with the frames of the auto destruction for attachframes.

    if (self.attackType == AUTODESTRUCTION){
        //NSLog(@"Autodestruction");
        if (i <= 2){
            // moviment animation
            [animFrames addObject:frame];
        }else{
            // autodestruction animation
            [attackFrames addObject:frame];
        }
    }else{
        switch (i) {
            case 1:
                [animFrames addObject:frame];
                break;
            case 2:
                [animFrames addObject:frame];
                [attackFrames addObject:frame];
                break;
            case 3:
                [attackFrames addObject:frame];
                break;
            default:
                break;
        }
    }

}

-(id)initwithStartPosition:(CGPoint)position actionTime:(double)time delegate:(id)scnDelegate enemyParams:(EnemyParams *)params enemyPosition:(EnemyPositions)defPosition shootEndPos:(CGPoint)shootEnd;
{
    id robotBlaster = [super init];
    //self = [super init];
    //if(self) {
    
    CGRect frameRect;
    CGPoint meeleAttackPos;
    self.enemyShootTimer = [[CCTimer alloc] init];
    [self.enemyShootTimer invalidate];
    
    
    self.activeShoots = [[NSMutableArray alloc] init];
    self.shootPosition = shootEnd;
    self.initialPosition = position;
    self.armor = params.armor;
    self.weaponDamage = params.weaponDamage;
    self.scorePoints = params.scorePoints;
    self.timeToReach = params.timeToReach;
    self.typeOfPowerUp = params.typeOfPowerUp;
    self.name = params.enemyName;
    self.frames = params.numberOfFrames;
    self.definedPosition = defPosition;
    self.attackType = params.atackType;
    self.isMeeleAttacking = false;
    self.isInMovement = false;
    self.shootStyle = params.shootStyle;
    self.shootStartPosition = params.shootStartPosition;
    self.timeToStart = time;
    self.dropCoinLowerLevel = params.dropCoinLowerLevel;
    self.dropCoinHighLevel = params.dropCoinHighLevel;
    
    id attackAnim;
    id endAnimationCallback;
    
    NSMutableArray *animFrames = [[NSMutableArray alloc] init];
    NSMutableArray *attackFrames = [[NSMutableArray alloc] init];
    
    self.enemySprite = [[CCSprite alloc] init];
    
    
    if ((defPosition == POS8) || (defPosition == POS7) || (defPosition == POS1)){
        self.shootZIndex = 49;
        for(int i = 1; i <= [self frames]; i++) {
            NSString *frameName = [[self name] stringByAppendingString:@"_back_%d.png"];
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:frameName,i]];
            frameRect = [frame rect];
            meeleAttackPos = ccp(params.lowerAction.x*frameRect.size.width, frameRect.size.height*params.lowerAction.y);
            [[CCSprite alloc] initWithSpriteFrame:frame];
            [self divideAnimations:frame animFrames:animFrames attackFrames:attackFrames frameidx:i];
        }
        if (defPosition == POS7) {
            self.enemySprite.flipX = YES;
            meeleAttackPos = ccp(params.fLowerAction.x*frameRect.size.width, frameRect.size.height*params.fLowerAction.y);
            
        }
    }else if ((defPosition == POS3) || (defPosition == POS4) || (defPosition == POS5)) {
        self.shootZIndex = 30;
        for(int i = 1; i <= [self frames]; i++) {
            NSString *frameName = [[self name] stringByAppendingString:@"_front_%d.png"];
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:frameName,i]];
            frameRect = [frame rect];
            meeleAttackPos = ccp(params.upperAction.x*frameRect.size.width, frameRect.size.height*params.upperAction.y);
            [[CCSprite alloc] initWithSpriteFrame:frame];
            [self divideAnimations:frame animFrames:animFrames attackFrames:attackFrames frameidx:i];
        }
        if (defPosition == POS5) {
            self.enemySprite.flipX = YES;
            meeleAttackPos = ccp(params.fUpperAction.x*frameRect.size.width, frameRect.size.height*params.fUpperAction.y);
        }
    }else{
        self.shootZIndex = 30;
        for(int i = 1; i <= [self frames]; i++) {
            NSString *frameName = [[self name] stringByAppendingString:@"_left_%d.png"];
            CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:frameName,i]];
            frameRect = [frame rect];
            meeleAttackPos = ccp(params.sideAction.x*frameRect.size.width, frameRect.size.height*params.sideAction.y);
            [[CCSprite alloc] initWithSpriteFrame:frame];
            [self divideAnimations:frame animFrames:animFrames attackFrames:attackFrames frameidx:i];
        }
        self.enemySprite.flipX = YES;
        if (defPosition == POS2) {
            self.enemySprite.flipX = NO;
            meeleAttackPos = ccp(params.fSideAction.x*frameRect.size.width, frameRect.size.height*params.fSideAction.y);
        }
    }
    //scaling required according spritebuilder
    enemySprite.scaleX = 0.25;
    enemySprite.scaleY = 0.25;
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:animFrames delay:0.09f];
    CCAnimation *attackAnimation = [CCAnimation animationWithSpriteFrames:attackFrames delay:0.09f];
    
    self.enemySprite.position = position;
    self.enemyAction = [CCActionRepeatForever actionWithAction:[CCActionAnimate actionWithAnimation:animation]];
    
    if (self.attackType == MEELEE){
        
        attackAnim = [CCActionAnimate actionWithAnimation: attackAnimation];
        endAnimationCallback = [CCActionCallFunc actionWithTarget:self selector: @selector(doEndMeeleAttackAnim)];
        self.enemyAttack = [CCActionSequence actions: attackAnim, endAnimationCallback, nil];
        
    }else{
        
        attackAnim = [CCActionAnimate actionWithAnimation: attackAnimation];
        endAnimationCallback = [CCActionCallFunc actionWithTarget:self selector: @selector(doEndAttackAnim)];
        self.enemyAttack = [CCActionSequence actions: attackAnim, endAnimationCallback, nil];
    }
    
    // include here the attackAction animation with the previous selected frames
    
    [[self enemySprite] runAction:[self enemyAction]];
    self.delegate = scnDelegate;
    
    
#warning just a study to improve the meele enemey colision when attacking remove this square
    
    [[self.meeleAttackRect spriteFrame] rect];
    self.meeleAttackRect = [[CCSprite alloc] initWithImageNamed:@"testsprt.png"];
    [self.meeleAttackRect setScale:0.25];
    [self.meeleAttackRect setPosition:meeleAttackPos];
    [meeleAttackRect setName:@"meeleAttackRect"];
    [meeleAttackRect setOpacity:0];
    [self.enemySprite addChild:self.meeleAttackRect];
    
    if (defPosition == POS2 || defPosition == POS6) {
        self.shootInterval = 0.8 + arc4random() % 11 * 0.1;
    }else{
        self.shootInterval = 1.1 + arc4random() % 3 * 0.1;
    }
    self.enemyUpdateSeconds = 0;
    return robotBlaster;
}

-(void)restartMovement
{
    NSLog(@"Restart Movement");
    [[self enemySprite] runAction:[self enemyAction]];
    [[self enemySprite] runAction:[CCActionMoveTo actionWithDuration:[self timeToReach]
                                                      position:[[UniversalInfo sharedInstance] screenCenter]]];
}

-(void)startMovement
{
    if ([(LevelScene *)[self delegate] isGameOver] == NO) {
        [[self delegate] addEnemySprite:self];
        
        CGPoint bandRelPosition = ((LevelScene *)[self delegate]).bandSprite.position;
        CGPoint bassRelPos = ((LevelScene *)[self delegate]).bandSprite.bass.position;
        CGPoint leadRelPos = ((LevelScene *)[self delegate]).bandSprite.leadGuitar.position;
        
        CGSize screnSize = [[CCDirector sharedDirector] viewSize];
        CGPoint bandSprtAbsPos = ccp(bandRelPosition.x * screnSize.width, bandRelPosition.y * screnSize.height);
        
        CGPoint bassPosition = ccpAdd(bandSprtAbsPos,bassRelPos );
        CGPoint leadGuitarPosition = ccpAdd(bandSprtAbsPos, leadRelPos);
        
        if (self.definedPosition == POS2){
            [[self enemySprite] runAction:[CCActionMoveTo actionWithDuration:[self timeToReach]
                                                                    position:leadGuitarPosition]];
        }else if (self.definedPosition == POS6){
            [[self enemySprite] runAction:[CCActionMoveTo actionWithDuration:[self timeToReach]
                                                                    position:bassPosition]];
        }else{
            [[self enemySprite] runAction:[CCActionMoveTo actionWithDuration:[self timeToReach]
                                                                    position:[[UniversalInfo sharedInstance] screenCenter]]];
        }
        
        self.isInMovement = YES;
    }
}


-(void)update:(CCTime)delta
{
    if (self.enemyUpdateSeconds >= self.shootInterval) {
        [self fireWeapon];
        self.enemyUpdateSeconds = 0;
    }else{
        self.enemyUpdateSeconds += 0.0166;
    }
}

-(void)performGeneralHitAnimation:(CGPoint) hitPoint
{
#warning runtime Couldn't create texture for file: in CCTextureCache
    /*
    CCSprite *hitSprite = [[CCSprite alloc] init];
    hitSprite = (CCSprite *)[CCBReader load:@"Published-iOS/enemyhit.ccbi"];
    hitSprite.position = hitPoint;
    [[self delegate] addChild:hitSprite z:50];
    hitSprite.scaleX = 0.75;
    hitSprite.scaleY = 0.75;
     */
}

-(void)meeleAttack:(BandSprite *)bandsprite bandComponent:(BandComponents)component hitrect:(CGRect)rect;
{
    if (!self.isMeeleAttacking){
        NSLog(@"Meele Attack");
        self.bandsprite = bandsprite;
        self.hitRect = rect;
        self.hitComponent = component;
        self.isMeeleAttacking = YES;
        self.isInMovement = NO;
        [[self enemySprite] stopAllActions];
        [[self enemySprite] runAction:self.enemyAttack];
    }
}

-(void)performAutoDestruction
{
    if ([(LevelScene *)[self delegate] isGameOver] == NO){
        [[self enemySprite] stopAllActions];
        [[self enemySprite] runAction:self.enemyAttack];
    }
}

-(void)performDeath
{
    NSLog(@"performDeath");
    [self unscheduleAllSelectors];
    CCSprite *dieSpriteAnimation = (CCSprite *)[CCBReader load:@"OtherSprites/enemydie"];
    dieSpriteAnimation.position = enemySprite.position;
    [[self delegate] addChild:dieSpriteAnimation z:[[self enemySprite] zOrder]];
    [[self delegate] removeEnemySprite:[self enemySprite] AttackType:self.attackType];
    [self playEnemyDieSound];
}

-(void)playEnemyDieSound
{
    NSLog(@"playEnemyDieSound");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx060"];
}

-(void)doEndMeeleAttackAnim
{
    float updatedArmor = [self.bandsprite hitByEnemy:self.weaponDamage component:self.hitComponent rect:self.hitRect];
    self.isMeeleAttacking = false;
    if (updatedArmor <= 0) {
        [self restartMovement];
    }
}

/*
-(CGPoint)improvedAttack
{
    CGPoint enemyPosition;
    
    int randomIndex = arc4random() % 5;
    
    switch (randomIndex) {
        case 0:
            enemyPosition = self.leadGuitarPosition;
            break;
        case 1:
            enemyPosition = self.baseGuitarPosition;
            break;
            
        case 2:
            enemyPosition = self.bassPosition;
            break;
            
        case 3:
            enemyPosition = self.vocalPosition;
            break;
        case 4:
            enemyPosition = self.drummerPosition;
            break;
        
        default:
            break;
    }

    return enemyPosition;
}*/

-(void)doEndAttackAnim
{
    GameSoundManager *gameSoundManager = [GameSoundManager sharedInstance];
    
    if (self.attackType == MEELEE){
        //NSLog(@"Do end meele attack!");
        [gameSoundManager playSoundEffectByName:self.shootSound];
    }else if (self.attackType == AUTODESTRUCTION){
        [[self delegate] removeEnemySprite:[self enemySprite] AttackType:self.attackType];
        [gameSoundManager playSoundEffectByName:self.shootSound];
    }else{
        [gameSoundManager playSoundEffectByName:self.shootSound];
        NSString *bulletName = [[self name] stringByAppendingString:@"_bullet.png"];
        CCSprite *robotShootSprite = [[CCSprite alloc] initWithImageNamed:bulletName];
        
        if (([[self name] isEqualToString:@"skate_guy"]) || ([[self name] isEqualToString:@"Bully"])){
            robotShootSprite.scaleX = 0.50; // too large reduce to 0.5
            robotShootSprite.scaleY = 0.50;
        }else{
            robotShootSprite.scaleX = 0.25;
            robotShootSprite.scaleY = 0.25;
            
        }
        
        CGRect shootOrigRect = [[self.enemySprite getChildByName:@"meeleAttackRect" recursively:NO] boundingBox];
        CGPoint shtOrigPosition = [self.enemySprite convertToWorldSpace:shootOrigRect.origin];
        [robotShootSprite setPosition:shtOrigPosition];
        
        CGPoint improvedTarget = [self calculateBandGuysPosition];
        CGPoint lowAccyrancyPoint = CGPointMake(improvedTarget.x, improvedTarget.y);
        
        CCActionRotateBy *rot = [CCActionRepeatForever actionWithAction:[CCActionRotateBy actionWithDuration:0.1 angle: 360]];
        
        id actionRobotFire;
        actionRobotFire = [CCActionMoveTo actionWithDuration:0.5 position:lowAccyrancyPoint];
      
        id endFireRobotCallBack = [CCActionCallBlock actionWithBlock:^{
            [self doEndRobotFire:robotShootSprite];
        }];
        
        id actionSequence = [CCActionSequence actions:actionRobotFire, endFireRobotCallBack, nil];
        
        [[self activeShoots] addObject:robotShootSprite];
        [[self delegate] addChild:robotShootSprite z:self.shootZIndex];
        
        if (self.shootStyle == ROTATION) {
            [robotShootSprite runAction:rot];
        }
    
        [robotShootSprite runAction:actionSequence];
    }
}


-(CGPoint)calculateBandGuysPosition
{
    
    NSLog(@"calculateBandGuysPosition");
    CGSize screnSize = [[CCDirector sharedDirector] viewSize];
    CGPoint bandRelPosition = ((LevelScene *)[self delegate]).bandSprite.position;
    CGPoint bandSprtAbsPos = ccp(bandRelPosition.x * screnSize.width, bandRelPosition.y * screnSize.height);
    
    
    NSNumber *bassArmor = [[NSNumber alloc] initWithFloat:((LevelScene *)[self delegate]).bandSprite.bassArmor];
    NSNumber *leadArmor = [[NSNumber alloc] initWithFloat:((LevelScene *)[self delegate]).bandSprite.guitar1Armor];
    NSNumber *baseArmor = [[NSNumber alloc] initWithFloat:((LevelScene *)[self delegate]).bandSprite.guitar2Armor];
    NSNumber *drummerArmor = [[NSNumber alloc] initWithFloat:((LevelScene *)[self delegate]).bandSprite.drummerArmor];
    NSNumber *vocalArmor = [[NSNumber alloc] initWithFloat:((LevelScene *)[self delegate]).bandSprite.vocalArmor];
    
    NSArray *bandGuysArmor = [[NSArray alloc] initWithObjects:bassArmor, leadArmor, baseArmor, drummerArmor, vocalArmor, nil];
    
    [bandGuysArmor valueForKeyPath:@"@max.floatValue"];
    
    int maxArmorPosition = [bandGuysArmor indexOfObject:[bandGuysArmor valueForKeyPath:@"@max.floatValue"]];
    
    
    CGPoint bassRelPos = ((LevelScene *)[self delegate]).bandSprite.bass.position;
    CGPoint leadRelPos = ((LevelScene *)[self delegate]).bandSprite.leadGuitar.position;
    CGPoint baseRelPos = ((LevelScene *)[self delegate]).bandSprite.baseGuitar.position;
    CGPoint drummerRelPos = ((LevelScene *)[self delegate]).bandSprite.drummer.position;
    CGPoint vocalRelPos = ((LevelScene *)[self delegate]).bandSprite.vocal.position;

    MetalPoint *bassMetalPosition = [[MetalPoint alloc] initWithPoint:ccpAdd(bandSprtAbsPos,bassRelPos)];
    MetalPoint *leadMetalPosition = [[MetalPoint alloc] initWithPoint:ccpAdd(bandSprtAbsPos,leadRelPos)];
    MetalPoint *baseMetalPosition = [[MetalPoint alloc] initWithPoint:ccpAdd(bandSprtAbsPos,baseRelPos)];
    MetalPoint *drummerMetalPosition = [[MetalPoint alloc] initWithPoint:ccpAdd(bandSprtAbsPos,drummerRelPos)];
    MetalPoint *vocalMetalPosition = [[MetalPoint alloc] initWithPoint:ccpAdd(bandSprtAbsPos,vocalRelPos)];
 
    NSArray *pointsArray = [[NSArray alloc] initWithObjects:bassMetalPosition,leadMetalPosition,baseMetalPosition,drummerMetalPosition, vocalMetalPosition, nil];

    return ((MetalPoint *)[pointsArray objectAtIndex:maxArmorPosition]).point;
}

-(void)testSchedule
{
    NSLog(@"test schedule");
}

-(void)fireWeapon
{
    if (self.attackType == WALK_SHOOT){
    
        if ([(LevelScene *)[self delegate] isGameOver] == NO){
            //include a random factor maybe the robot cant reach the hero, if we dont put
            //this factor the hit will happen every time
            //NSLog(@"Fire weapon in RobotBlaster fireeee");
            //NSLog(@"Game State %ld",[[self delegate] state]);
            // put a parameter here to check if the robot stops before shoot
            // to stop the movement of the enemy it is required to ensure that
            // the enemy is on the screen, I will comment this for a while
            // another problem is that stopAllActions stops the animation too
            // so it will be nice if we play another animation here
            [[self enemySprite] stopAllActions];
            [[self enemySprite] runAction:self.enemyAttack];
        }else{
            NSLog(@"Turn of all timers for fire robot laser");
            [self unscheduleAllSelectors];
            //[[CCScheduler sharedScheduler] unscheduleAllSelectorsForTarget:self];
        }
    }

}

-(void)removeEnemyFire:(id)node
{
    CCNode *parent = [node parent];
    [parent removeChild:node];
}

-(void)doEndRobotFire:(id)node
{
    NSLog(@"End robot fire with some effect can be a fade");
    
    id actionFade = [CCActionFadeTo actionWithDuration:1.0f opacity:0];
    id endFireFadeCallBack = [CCActionCallBlock actionWithBlock:^{
        [self removeEnemyFire:node];
    }];
    
    id actionSequence = [CCActionSequence actions:actionFade, endFireFadeCallBack, nil];
    [node runAction:actionSequence];
    //removes the shoot as it will be disapear and will not be considered on the colision
    [[self activeShoots] removeObject:node];
}

-(int)receiveHeroShoot:(float)damage killNow:(BOOL)value shootRect:(CGRect)rect;
{
    if (value == NO) {
        self.armor = self.armor - damage;
        [self performGeneralHitAnimation:rect.origin];
    }else{
        self.armor = 0;
    }
    if (self.armor <= 0) {
        [self performDeath];
        return [self scorePoints];
    }else{
        return 0;
    }
}

-(float)autoDestruction
{
    [self setArmor:-1];
    if (self.attackType == AUTODESTRUCTION)
    {
        [self performAutoDestruction];
    }else{
        [self performDeath];
    }
    return [self weaponDamage];
}

-(void)dealloc
{
    NSLog(@"Dealloc enemy");
}

@end
