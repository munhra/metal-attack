//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  FullEndScene.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 6/25/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InfinityBG.h"
#import "MessagesSprite.h"
#import "EndSprite.h"
#import "BandStoreItem.h"

@interface FullEndScene : CCNode <CCBAnimationManagerDelegate>

@property (nonatomic, retain) CCSprite *drummerEndSprite;
@property (nonatomic, weak) CCEffectNode *drummerEndSpriteEffect;
@property (nonatomic, weak) CCSprite *baseEndSprite;
@property (nonatomic, weak) CCSprite *leadEndSprite;
@property (nonatomic, weak) CCSprite *bassEndSprite;
@property (nonatomic, weak) CCSprite *vocalEndSprite;

@property (nonatomic, weak) CCSprite *drummerThinEndSprite;
@property (nonatomic, weak) CCSprite *baseThinEndSprite;
@property (nonatomic, weak) CCSprite *leadThinEndSprite;
@property (nonatomic, weak) CCSprite *bassThinEndSprite;
@property (nonatomic, weak) CCSprite *vocalThinEndSprite;

@property (nonatomic, retain) EndSprite *endStripe1;
@property (nonatomic, weak) CCNode *endStripe2;
@property (nonatomic, weak) CCNode *endStripe3;
@property (nonatomic, weak) CCNode *endStripe4;
@property (nonatomic, weak) CCNode *endStripe5;
@property (nonatomic, weak) CCSprite *endAreaNode;
@property (nonatomic, weak) CCSprite *logo;
@property (nonatomic, weak) InfinityBG *endInfinityBG;
@property (nonatomic,retain) MessagesSprite *message;

@property (nonatomic, weak) CCSprite *punkSprite;
@property (nonatomic, weak) CCSprite *endMessageSprite;
@property (nonatomic, weak) CCSprite *logoWater;

@property (nonatomic, retain) NSArray *endSpritesArray;
@property (nonatomic, retain) NSArray *endStripeArray;
@property (nonatomic, weak) NSArray *endThinSpriteArray;

@property (nonatomic, retain) CCSprite *stripeBorder;
@property (nonatomic, retain) NSMutableArray *bandGuysItem;
@property (nonatomic, retain) NSMutableArray *bandGuysEndingText;

@property (nonatomic) int endIndex;
@property (nonatomic) bool updateEnd;
@property (nonatomic) float previousPixelBlockSize;
@property (nonatomic) float currentPixelBlockSize;

@end
