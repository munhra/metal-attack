//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  ReloadLevel.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 06/12/14.
//  Copyright 2014 Apportable. All rights reserved.
//

#import "ReloadLevel.h"
#import "LevelSceneController.h"
#import "CCTextureCache.h"


@implementation ReloadLevel
@synthesize levelToReload;
@synthesize endInfinityBG;

-(id)init
{
    self = [super init];
    NSLog(@"Init ReloadLevel");
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self doRandomTintBackground];
}

-(void)onExit
{
    [super onExit];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}

-(void)doRandomTintBackground
{
    [[self endInfinityBG] setBGColor:[[UniversalInfo sharedInstance] generateRandomColor]];
}

-(void)didLoadFromCCB
{
    NSLog(@"ReloadLevel didLoadFromCCB");
    [self schedule:@selector(reloadScene) interval:1.5];
}

-(void)doClick
{
    NSLog(@"doClick");
    [[CCDirector sharedDirector] replaceScene:[[LevelSceneController sharedInstance] loadLevelScene:0 waveNumber:0]];
}

-(void)reloadScene
{
    [[CCDirector sharedDirector] replaceScene:[[LevelSceneController sharedInstance] loadLevelScene:levelToReload waveNumber:0]];
}

-(void)dealloc
{
    NSLog(@"***** Dealloc ReloadLevelScreen *****");
}

@end
