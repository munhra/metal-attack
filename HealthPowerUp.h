//
//  HealthPowerUp.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 16/01/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HealthPowerUp : NSObject

{
    CCSprite *healthPwd1;
    CCSprite *healthPwd2;
    CCSprite *healthPwd3;
}

@property(nonatomic,retain) CCSprite *healthPwd1;
@property(nonatomic,retain) CCSprite *healthPwd2;
@property(nonatomic,retain) CCSprite *healthPwd3;
@property(nonatomic,weak) id levelDelegate;

//+(id)sharedInstance;
-(void)preparePowerUps:(CGPoint)bandposition;
-(void)activateHealthPowerUp:(int)healthMultiplierId;
-(void)removePowerUps;

@end
