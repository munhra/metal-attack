//
//  DoorSprite.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 14/01/17.
//  Copyright © 2017 Apportable. All rights reserved.
//

#import "DoorSprite.h"
#import "GameSoundManager.h"

@implementation DoorSprite

-(void)playOpenCloseDoorSound
{
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx030"];
}

@end
