//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  BandBuyPoster.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 08/10/14.
//
//

#import "BandBuyPoster.h"
#import "BandStoreItemsCtrl.h"
#import "BandStoreItem.h"
#import "CCAnimation.h"
#import "GameStore.h"
#import "GameSoundManager.h"
#import "CCTextureCache.h"

@implementation BandBuyPoster
@synthesize itemsToShow;
@synthesize selectButton;
@synthesize buyButton;
@synthesize infoButton;
@synthesize info;
@synthesize coinIcon;
@synthesize bandBuy;
@synthesize delegate;
@synthesize ownedTag;
@synthesize costValue;
@synthesize armorValue;
@synthesize damageValue;
@synthesize armorIcon;
@synthesize damageIcon;
@synthesize title;
@synthesize armorLabel;
@synthesize costLabel;
@synthesize damageLabel;
@synthesize bandGuyArt;
@synthesize leftButton;
@synthesize rightButton;
@synthesize StarsDamageGroup1;
@synthesize StarsDamageGroup2;
@synthesize StarsArmorGroup1;
@synthesize StarsArmorGroup2;
@synthesize guyInfo;
@synthesize posterGuyPicture;
@synthesize bandGuyHistory;
@synthesize orangeStar;
@synthesize redStar;
@synthesize blackStar;
@synthesize currentSpriteSheet;

int itemCount;
int labelfontSize;
const int bandPosterZindex = 0;

//CCSprite *orangeStar;
//CCSprite *redStar;
//CCSprite *blackStar;

-(id)init
{
    self = [super init];
    NSLog(@"Init BandBuyPoster");
    ownedTag = [[CCSprite alloc] initWithImageNamed:@"owned.png"];
    
    orangeStar = (CCSprite *)[CCBReader load:@"store/OrangeStar"];
    redStar = (CCSprite *)[CCBReader load:@"store/RedStar"];
    blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
    
    [[BandStoreItemsCtrl sharedInstance] restoreBandGuys];
    
    return self;
}

- (void)dealloc
{
    NSLog(@"Dealloc band buy poster");
}

-(void)didLoadFromCCB
{
    [self.rightButton setZOrder:20];
    [self.leftButton setZOrder:20];
    [self.selectButton setZOrder:20];
    [self.buyButton setZOrder:20];
    [self.title setZOrder:20];
    [self.StarsArmorGroup1 setZOrder:20];
    [self.StarsArmorGroup2 setZOrder:20];
    [self.StarsDamageGroup1 setZOrder:20];
    [self.StarsDamageGroup2 setZOrder:20];
    [self.info setZOrder:20];
    [self.infoButton setZOrder:20];
    [self.coinIcon setZOrder:20];
    [self.guyInfo setZOrder:30];
}

-(void)setBandBuy:(CCSprite *)bandBuySprite
{
    NSLog(@"setBandBuy");
    bandBuy = bandBuySprite;
    [ownedTag setPosition:ccp(420, 530)];
    [ownedTag removeFromParent];
    [bandBuy addChild:ownedTag z:30];
}

-(void)doCloseInfo
{
    NSLog(@"doCloseInfo");
    [self.guyInfo setVisible:false];
    [self.leftButton setEnabled:true];
    [self.rightButton setEnabled:true];
    [self.infoButton setEnabled:true];
    [self.selectButton setEnabled:true];
    [self.buyButton setEnabled:true];
}

-(void)doInfo;
{
    NSLog(@"doInfo");
    [self.guyInfo setVisible:true];
    [self.leftButton setEnabled:false];
    [self.rightButton setEnabled:false];
    [self.infoButton setEnabled:false];
    [self.selectButton setEnabled:false];
    [self.buyButton setEnabled:false];
}

-(void)doScrRigh;
{
    NSLog(@"doScrRigh");
    itemCount++;
    if (itemCount < [itemsToShow count]) {
        CCNode *parent = [bandGuyArt parent];
        CGPoint spritePosition = [bandGuyArt position];
        [parent removeChild:bandGuyArt];
        bandGuyArt = (CCSprite *)[CCBReader load: [[itemsToShow objectAtIndex:itemCount] storePosterFileName]];
        //bandGuyArt = [[itemsToShow objectAtIndex:itemCount] compStorePosterImage];
        ownedTag.visible = [[itemsToShow objectAtIndex:itemCount] ownedBandItem];
        [ownedTag setZOrder:40];
        [buyButton setVisible:![[itemsToShow objectAtIndex:itemCount] ownedBandItem]];
        [selectButton setVisible:[[itemsToShow objectAtIndex:itemCount] ownedBandItem]];
        [bandGuyArt setPosition:spritePosition];
        [bandGuyArt removeFromParent];
        [parent addChild:bandGuyArt];
        [self updateText:[itemsToShow objectAtIndex:itemCount]];
        [self updateInfoPicture:[itemsToShow objectAtIndex:itemCount]];
        
        NSString *bandGuyName = [[itemsToShow objectAtIndex:itemCount] itemName];
        
        [[GameSoundManager sharedInstance] stopOneBandGuySound];
        BandStoreItem *bandStoreItem = [itemsToShow objectAtIndex:itemCount];
        if ([bandStoreItem itemType] == VOCALIST) {
            [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:NO];
        }else{
            [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:YES];
        }
        
    }else{
        itemCount = [itemsToShow count] - 1;
    }
    NSLog(@"doScrRigh %d",itemCount);
}

-(void)doScrLeft;
{
    NSLog(@"doScrLeft");
    itemCount--;
    if (itemCount < [itemsToShow count]) {
        CCNode *parent = [bandGuyArt parent];
        CGPoint spritePosition = [bandGuyArt position];
        [parent removeChild:bandGuyArt];
        bandGuyArt = (CCSprite *)[CCBReader load: [[itemsToShow objectAtIndex:itemCount] storePosterFileName]];
        ownedTag.visible = [[itemsToShow objectAtIndex:itemCount] ownedBandItem];
        [ownedTag setZOrder:40];
        [buyButton setVisible:![[itemsToShow objectAtIndex:itemCount] ownedBandItem]];
        [selectButton setVisible:[[itemsToShow objectAtIndex:itemCount] ownedBandItem]];
        [bandGuyArt setPosition:spritePosition];
        [bandGuyArt removeFromParent];
        [parent addChild:bandGuyArt z:bandPosterZindex];
        [self updateText:[itemsToShow objectAtIndex:itemCount]];
        [self updateInfoPicture:[itemsToShow objectAtIndex:itemCount]];
        NSString *bandGuyName = [[itemsToShow objectAtIndex:itemCount] itemName];
        [[GameSoundManager sharedInstance] stopOneBandGuySound];
        
        BandStoreItem *bandStoreItem = [itemsToShow objectAtIndex:itemCount];
        if ([bandStoreItem itemType] == VOCALIST) {
            [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:NO];
        }else{
            [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:YES];
        }
    }else{
        itemCount = 0;
    }
    NSLog(@"doScrLeft %d", itemCount);
}

-(void)doShowLeadGuitar
{
    NSLog(@"doShowLeadGuitar");
    CGPoint spritePosition = [bandGuyArt position];
    CCNode *parent = [bandGuyArt parent];
    [parent removeChild:bandGuyArt cleanup:YES];
    itemCount = 0;
    BandStoreItemsCtrl *itemsCtrl = [BandStoreItemsCtrl sharedInstance];
    itemsToShow = [itemsCtrl leadGuitars];
    [self preloadItemsToShowSound];
    //currentSpriteSheet = (NSString *)[[itemsToShow objectAtIndex:0] storePosterFileName];
    bandGuyArt = (CCSprite *)[CCBReader load: [[itemsToShow objectAtIndex:0] storePosterFileName]];
    ownedTag.visible = [[itemsToShow objectAtIndex:0] ownedBandItem];
    [ownedTag setZOrder:40];
    [buyButton setVisible:![[itemsToShow objectAtIndex:0] ownedBandItem]];
    [selectButton setVisible:[[itemsToShow objectAtIndex:0] ownedBandItem]];
    [bandGuyArt setPosition:spritePosition];
    [bandGuyArt removeFromParent];
    [parent addChild:bandGuyArt];
    [self updateText:[itemsToShow objectAtIndex:0]];
    [self updateInfoPicture:[itemsToShow objectAtIndex:0]];
    [[GameSoundManager sharedInstance] stopOneBandGuySound];
    NSString *bandGuyName = [[itemsToShow objectAtIndex:0] itemName];
    [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:YES];
}

-(void)doShowBaseGuitar
{
    NSLog(@"doShowBaseGuitar");
    CGPoint spritePosition = [bandGuyArt position];
    CCNode *parent = [bandGuyArt parent];
    [parent removeChild:bandGuyArt cleanup:YES];
    itemCount = 0;
    BandStoreItemsCtrl *itemsCtrl = [BandStoreItemsCtrl sharedInstance];
    itemsToShow = [itemsCtrl baseGuitars];
    [self preloadItemsToShowSound];
    //currentSpriteSheet = (NSString *)[[itemsToShow objectAtIndex:0] storePosterFileName];
    bandGuyArt = (CCSprite *)[CCBReader load: [[itemsToShow objectAtIndex:0] storePosterFileName]];
    ownedTag.visible = [[itemsToShow objectAtIndex:0] ownedBandItem];
    [ownedTag setZOrder:40];
    [buyButton setVisible:![[itemsToShow objectAtIndex:0] ownedBandItem]];
    [selectButton setVisible:[[itemsToShow objectAtIndex:0] ownedBandItem]];
    [bandGuyArt setPosition:spritePosition];
    [bandGuyArt removeFromParent];
    [parent addChild:bandGuyArt];
    [self updateText:[itemsToShow objectAtIndex:0]];
    [self updateInfoPicture:[itemsToShow objectAtIndex:0]];
    [[GameSoundManager sharedInstance] stopOneBandGuySound];
    NSString *bandGuyName = [[itemsToShow objectAtIndex:0] itemName];
    [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:YES];
}

-(void)doShowVocalist:(CCButton *)menuItem
{
    NSLog(@"Do show vocalist");
    CGPoint spritePosition = [bandGuyArt position];
    CCNode *parent = [bandGuyArt parent];
    [parent removeChild:bandGuyArt cleanup:YES];
    itemCount = 0;
    BandStoreItemsCtrl *itemsCtrl = [BandStoreItemsCtrl sharedInstance];
    itemsToShow = [itemsCtrl vocals];
    [self preloadItemsToShowSound];
    currentSpriteSheet = (NSString *)[[itemsToShow objectAtIndex:0] storePosterFileName];
    bandGuyArt = (CCSprite *)[CCBReader load: [[itemsToShow objectAtIndex:0] storePosterFileName]];
    ownedTag.visible = [[itemsToShow objectAtIndex:0] ownedBandItem];
    [ownedTag setZOrder:40];
    [buyButton setVisible:![[itemsToShow objectAtIndex:0] ownedBandItem]];
    [selectButton setVisible:[[itemsToShow objectAtIndex:0] ownedBandItem]];
    [bandGuyArt setPosition:spritePosition];
    [bandGuyArt removeFromParent];
    [parent addChild:bandGuyArt];
    [self updateText:[itemsToShow objectAtIndex:0]];
    [self updateInfoPicture:[itemsToShow objectAtIndex:0]];
    [[GameSoundManager sharedInstance] stopOneBandGuySound];
    NSString *bandGuyName = [[itemsToShow objectAtIndex:0] itemName];
    [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:NO];
}

-(void)doShowDrummer:(CCButton *)menuItem
{
    NSLog(@"Do show drummer");
    CGPoint spritePosition = [bandGuyArt position];
    CCNode *parent = [bandGuyArt parent];
    [parent removeChild:bandGuyArt cleanup:YES];
    itemCount = 0;
    BandStoreItemsCtrl *itemsCtrl = [BandStoreItemsCtrl sharedInstance];
    itemsToShow = [itemsCtrl drummers];
    [self preloadItemsToShowSound];
    //currentSpriteSheet = (NSString *)[[itemsToShow objectAtIndex:0] storePosterFileName];
    bandGuyArt = (CCSprite *)[CCBReader load: [[itemsToShow objectAtIndex:0] storePosterFileName]];
    ownedTag.visible = [[itemsToShow objectAtIndex:0] ownedBandItem];
    [ownedTag setZOrder:40];
    [buyButton setVisible:![[itemsToShow objectAtIndex:0] ownedBandItem]];
    [selectButton setVisible:[[itemsToShow objectAtIndex:0] ownedBandItem]];
    [bandGuyArt setPosition:spritePosition];
    [bandGuyArt removeFromParent];
    [parent addChild:bandGuyArt];
    [self updateText:[itemsToShow objectAtIndex:0]];
    [self updateInfoPicture:[itemsToShow objectAtIndex:0]];
    [[GameSoundManager sharedInstance] stopOneBandGuySound];
    NSString *bandGuyName = [[itemsToShow objectAtIndex:0] itemName];
    [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:YES];
}

-(void)doShowBass:(CCButton *)menuItem;
{
    NSLog(@"Do show BASS");
    CGPoint spritePosition = [bandGuyArt position];
    CCNode *parent = [bandGuyArt parent];
    [parent removeChild:bandGuyArt cleanup:YES];
    itemCount = 0;
    BandStoreItemsCtrl *itemsCtrl = [BandStoreItemsCtrl sharedInstance];
    itemsToShow = [itemsCtrl basses];
    [self preloadItemsToShowSound];
    //currentSpriteSheet = (NSString *)[[itemsToShow objectAtIndex:0] storePosterFileName];
    bandGuyArt = (CCSprite *)[CCBReader load: [[itemsToShow objectAtIndex:0] storePosterFileName]];
    ownedTag.visible = [[itemsToShow objectAtIndex:0] ownedBandItem];
    [ownedTag setZOrder:40];
    [buyButton setVisible:![[itemsToShow objectAtIndex:0] ownedBandItem]];
    [selectButton setVisible:[[itemsToShow objectAtIndex:0] ownedBandItem]];
    [bandGuyArt setPosition:spritePosition];
    [bandGuyArt removeFromParent];
    [parent addChild:bandGuyArt];
    [self updateText:[itemsToShow objectAtIndex:0]];
    [self updateInfoPicture:[itemsToShow objectAtIndex:0]];
    [[GameSoundManager sharedInstance] stopOneBandGuySound];
    NSString *bandGuyName = [[itemsToShow objectAtIndex:0] itemName];
    [[GameSoundManager sharedInstance] playOneBandGuySound:bandGuyName withLoop:YES];
}

-(void)preloadItemsToShowSound
{
    NSMutableArray *bandGuysName = [[NSMutableArray alloc] initWithCapacity:[itemsToShow count]];
    for (BandStoreItem *bandItem in itemsToShow) {
        [bandGuysName addObject:[bandItem itemName]];
    }
    [[GameSoundManager sharedInstance] preloadBandGuySounds:bandGuysName];
}

-(void)unloadItemsSound
{
    NSMutableArray *bandGuysName = [[NSMutableArray alloc] initWithCapacity:[itemsToShow count]];
    for (BandStoreItem *bandItem in itemsToShow) {
        [bandGuysName addObject:[bandItem itemName]];
    }
    [[GameSoundManager sharedInstance] unloadBandGuySounds:bandGuysName];
}

-(void)resetArmorStars:(BandStoreItem *)item
{
    //CCSprite *blackStar;
    
    //MAX 100
    //(ItemArmor / 100) * 10
    
    int armorPoints = (int) (item.armor / 13);
    
    [StarsArmorGroup1 removeAllChildren];
    [StarsArmorGroup2 removeAllChildren];
    
    
    for (int i = 0; i < armorPoints ; i++) {
        if (i < 5){
            orangeStar = (CCSprite *)[CCBReader load:@"store/OrangeStar"];
            [StarsArmorGroup1 addChild:orangeStar];
        }else{
            orangeStar = (CCSprite *)[CCBReader load:@"store/OrangeStar"];
            [StarsArmorGroup2 addChild:orangeStar];
        }
    }
    
    int blackCount = 0;
    
    if ([[StarsArmorGroup1 children] count] < 5) {
        //complete first 1 with black stars
        blackCount = 5 - armorPoints;
        for (int i = 0; i < blackCount ; i++) {
            self.blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [StarsArmorGroup1 addChild:self.blackStar];
        }
        for (int i = 0; i < 5 ; i++) {
            self.blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [StarsArmorGroup2 addChild:self.blackStar];
        }
    }else{
        //First one is complete
        blackCount = 10 - armorPoints;
        for (int i = 0; i < blackCount ; i++) {
            self.blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [StarsArmorGroup2 addChild:self.blackStar];
        }
    }

}

-(void)resetDamageStars:(BandStoreItem *)item
{
    
    //CCSprite *blackStar;
    
    int damagePoints = (int) (item.shootPower/13);
    
    [StarsDamageGroup1 removeAllChildren];
    [StarsDamageGroup2 removeAllChildren];
    
    for (int i = 0; i < damagePoints ; i++) {
        if (i < 5){
            redStar = (CCSprite *)[CCBReader load:@"store/RedStar"];
            [StarsDamageGroup1 addChild:redStar];
        }else{
            redStar = (CCSprite *)[CCBReader load:@"store/RedStar"];
            [StarsDamageGroup2 addChild:redStar];
        }
    }

    int blackCount = 0;

    if ([[StarsDamageGroup1 children] count] < 5) {
        //complete first 1 with black stars
        blackCount = 5 - damagePoints;
        for (int i = 0; i < blackCount ; i++) {
            self.blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [StarsDamageGroup1 addChild:self.blackStar];
        }
        for (int i = 0; i < 5 ; i++) {
            self.blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [StarsDamageGroup2 addChild:self.blackStar];
        }
    }else{
        //First one is complete
        blackCount = 10 - damagePoints;
        for (int i = 0; i < blackCount ; i++) {
            self.blackStar = (CCSprite *)[CCBReader load:@"store/BlackStar"];
            [StarsDamageGroup2 addChild:self.blackStar];
        }
    }
    
}

-(void)updateStars:(BandStoreItem *)item
{
    [self resetDamageStars:item];
    [self resetArmorStars:item];
}

-(void)updateInfoPicture:(BandStoreItem *)item {
    CCSprite *bandGuyInfoPosterArt = (CCSprite *) [CCBReader load:[item posterFileName]];
    [[self posterGuyPicture] setSpriteFrame:[bandGuyInfoPosterArt spriteFrame]];
}

-(void)updateText:(BandStoreItem *)item
{
    // there is a problem with the setString method of CCLabelTTF it reduces the fps
    // a workaround is to remove the label and add it again with the correct string
    // this can be a good post on cocos forum.
    
    [self updateStars:item];
    
    CGPoint titlePosition = [[self title] position];
    CGPoint costPosition = [[self costValue] position];

    CCNode *textParent = [armorValue parent];
    [textParent removeChild:costValue];
    
    [self removeChild:title];
    title = [CCLabelTTF labelWithString:item.itemName fontName:@"Rockwell-CondensedBold" fontSize:30];
    [title setZOrder:20];
    [title setPosition:titlePosition];
    CGSize titleSize = CGSizeMake(300, 0);
    [title setDimensions:titleSize];

    costValue = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",item.credits] fontName:@"Rockwell-CondensedBold" fontSize:25];
    [costValue setPosition:costPosition];
    [costLabel setColor:[CCColor colorWithRed:0.3960 green:0.1372 blue:0.0274]];
    
    [self addChild:title];
    [textParent addChild:costValue];
    
    [bandGuyHistory setString:item.description];
}

-(void)onExit
{
    NSLog(@"Remove All Children from bandbuy");
    [super onExit];
    [self removeAllChildren];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"store/RedStar"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"store/BlackStar"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"store/OrangeStar"];
}

-(void)doBuy
{
    NSLog(@"doBuy from BandBuyPoster call delegate");
    [[self delegate] doBuyBandGuy];
}

-(void)closeBandPoster
{
    NSLog(@"Close Band Poster");
    [[GameSoundManager sharedInstance] stopOneBandGuySound];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/bandArt/lead.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/bandArt/bass.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/bandArt/drummer.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/bandArt/solo.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/Store/bandposter/bandArt/vocal.png"];
    [self unloadItemsSound];
    [[self delegate] closeBandPoster];
}



@end
