//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//
//  BandVault.m
//  EightbitShooter
//
//  Created by Rafael Munhoz on 12/03/13.
//
//

#import "BandVault.h"
#import "BandStoreItemsCtrl.h"
#import "GameCenterManager.h"

@implementation BandVault

@synthesize drummerId;
@synthesize guitar1Id;
@synthesize guitar2Id;
@synthesize bassId;
@synthesize vocalId;
@synthesize bandCoins;
@synthesize highScore;
@synthesize drummerIndex;
@synthesize guitar1Index;
@synthesize guitar2Index;
@synthesize bassIndex;
@synthesize vocalIndex;
@synthesize genericItems;
@synthesize firstRun;
@synthesize selectedItems;
@synthesize selectedItem;
@synthesize totalBandCoins;

+(id)sharedInstance
{
	//Singleton Implementation
    //We can initialize the selected band guys at this point, setting the index position of each one
    static id master = nil;
	
	@synchronized(self)
	{
		if (master == nil){
            master = [self new];
            [master setDrummerIndex:0];
            [master setGuitar1Index:0];
            [master setGuitar2Index:1];
            [master setVocalIndex:0];
            [master setBassIndex:0];
            NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];

            [master setBandCoins:[userdef integerForKey:@"coins"]];
            int totalCoins = [userdef integerForKey:@"totalcoins"];
            if (!totalCoins) {
                [master setTotalBandCoins:0];
            }else{
                [master setTotalBandCoins:totalCoins];
            }
            
            
            [master setSelectedItems:[[NSMutableArray alloc] init]];
            NSLog(@"Read coins from vault %d",[master bandCoins]);
            
        }
	}
    return master;
}

//include a method here to return all owned items
//BandVaultItem

-(void)consumeItem:(BandVaultItem *)itemToConsume
{
    NSString *itemid = itemToConsume.appId;
    
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSData *loadEncodedObject = [userdef objectForKey:itemid];
    BandVaultItem *vaultItem = [NSKeyedUnarchiver unarchiveObjectWithData:loadEncodedObject];
    
    vaultItem.qtd = @([vaultItem.qtd integerValue] - 1);
    
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:vaultItem];
    [userdef setObject:encodedObject forKey:itemid];
    
    [selectedItems removeObject:itemToConsume];
    
    
    //bool nomoreItemsOfThisType = YES;
    /*
    for (BandVaultItem *vaultItem in selectedItems) {
        if ([vaultItem.appId isEqualToString:itemToConsume.appId]) {
            nomoreItemsOfThisType = NO;
        }
    }*/
    
    //return nomoreItemsOfThisType;
    
}

-(void)storeItem:(NSString *)itemid itemQtd:(int)qtd
{
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSData *loadEncodedObject = [userdef objectForKey:itemid];
    BandVaultItem *vaultItem = [NSKeyedUnarchiver unarchiveObjectWithData:loadEncodedObject];
    
    if (vaultItem) {
        int value =  [vaultItem.qtd intValue];
        value = value + qtd;
        vaultItem.qtd = [[NSNumber alloc] initWithInt:value];
    }else{
        vaultItem = [[BandVaultItem alloc] init];
        vaultItem.appId = itemid;
        vaultItem.ownedVaultItem = [[NSNumber alloc] initWithBool:YES];
        vaultItem.selectedVaultItem = [[NSNumber alloc] initWithBool:NO];
        vaultItem.qtd = [[NSNumber alloc] initWithInt:qtd];
    }
    
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:vaultItem];
    [userdef setObject:encodedObject forKey:itemid];
}

-(void)selectBandGuy:(NSString *)itemid bandGuyType:(int)bandType oldSelectedGuy:(NSString *)itemId;
{
    // this method will be called when the player selects a new band guy
    // the other selected band guy needs to be unselected
    // maybe it will be good to include a new parameter on the method regarding
    // to the id of the other selected item
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSData *loadEncodedObject = [userdef objectForKey:itemid];
    BandVaultItem *vaultItem = [NSKeyedUnarchiver unarchiveObjectWithData:loadEncodedObject];

    vaultItem.selectedVaultItem = [[NSNumber alloc] initWithBool:YES];
    
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:vaultItem];
    [userdef setObject:encodedObject forKey:itemid];
    
    switch (bandType) {
        case 0:
            //guitarrist1 check if it works
            [self setGuitar1Index:vaultItem.indexPosition];
            break;
        case 1:
            //guitarrist2 check if it works
            [self setGuitar2Index:vaultItem.indexPosition];
            
            // and so on, I will do for this two first and change it again
            
        default:
            break;
    }
    
}

-(void)storeBandGuy:(NSString *)itemid indexPosition:(int)pos
{
    //this method will be called when the player buys a new band guy
    //or on the first time to store the default band guys
    
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
   
    BandVaultItem *vaultItem = [[BandVaultItem alloc] init];
    vaultItem = [[BandVaultItem alloc] init];
    vaultItem.appId = itemid;
    vaultItem.ownedVaultItem = [[NSNumber alloc] initWithBool:YES];
    vaultItem.selectedVaultItem = [[NSNumber alloc] initWithBool:NO];
    
    vaultItem.qtd = [[NSNumber alloc] initWithInt:1];
    vaultItem.indexPosition = [[NSNumber alloc] initWithInt:pos];
    
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:vaultItem];
    [userdef setObject:encodedObject forKey:itemid];
    

}

-(void)updateBandCoins:(int)coins
{
    // this method will update the band coins
    // to subtract a quantity pass the value with signal -
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    
    
    if (coins > 0) {
        self.totalBandCoins = self.totalBandCoins + coins;
        [userdef setInteger:self.totalBandCoins forKey:@"totalcoins"];
    }
    
    self.bandCoins = self.bandCoins + coins;
    [userdef setInteger:self.bandCoins forKey:@"coins"];
}

-(void)loadAllOwnedBandGuys
{
    //????
}

-(BandVaultItem *)getBandVaultByItemId:(NSString *)itemid
{
    // this method will return the bandvault item by itemid
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSData *loadEncodedObject = [userdef objectForKey:itemid];
    if (loadEncodedObject != nil) {
        BandVaultItem *vaultItem = [NSKeyedUnarchiver unarchiveObjectWithData:loadEncodedObject];
        return vaultItem;
    }else{
        return nil;
    }
}

-(int)getAllitemsQtd
{
    NSArray *ownedItems = [[NSArray alloc] initWithArray:[self loadAllOwnedGenericItems]];
    int itemQtd = 0;
    for (BandVaultItem *ownedItem in ownedItems) {
        itemQtd += [ownedItem.qtd intValue];
    }
    return itemQtd;
}

-(int)getItemQuantity:(NSString *)itemid
{
    NSArray *ownedItems = [[NSArray alloc] initWithArray:[self loadAllOwnedGenericItems]];
    for (BandVaultItem *ownedItem in ownedItems) {
        if ([ownedItem.appId isEqualToString:itemid]) {
            return [ownedItem.qtd intValue];
        }
    }
    return 0;
}

-(NSMutableArray*)loadAllOwnedGenericItems
{
    NSMutableArray *ownedItems = [[NSMutableArray alloc] init];
    BandStoreItemsCtrl *bandStoreItemsCtrl = [BandStoreItemsCtrl sharedInstance];
    
    for (BandStoreItem *genericItem in [bandStoreItemsCtrl generalItems]) {
        BandVaultItem *bandVaultItem = [[BandVaultItem alloc] init];
        bandVaultItem = [self getBandVaultByItemId:genericItem.appStoreId];
        
        if (bandVaultItem) {
            bandVaultItem.itemDescription = genericItem.description;
            [ownedItems addObject:bandVaultItem];
        }
    }
    
    return ownedItems;
}

-(int)addItemSelected:(BandVaultItem *)item
{
    [[self selectedItems] addObject:item];
    return [[self selectedItems] indexOfObject:item];
}

-(void)clearSelectedItems
{
    [[self selectedItems] removeAllObjects];
}

-(BandVaultItem *)copyBandVaultItem:(BandVaultItem *)item
{
    BandVaultItem *newItem = [[BandVaultItem alloc] init];
    [newItem setAppId:item.appId];
    [newItem setOwnedVaultItem:item.ownedVaultItem];
    [newItem setSelectedVaultItem:item.selectedVaultItem];
    [newItem setQtd:item.qtd];
    [newItem setIndexPosition:item.indexPosition];
    [newItem setSelected:item.selected];
    [newItem setItemDescription:item.itemDescription];
    
    return newItem;
}

-(int)removeItemSelected:(BandVaultItem *)item
{
    int removedPosition = [[self selectedItems] indexOfObject:item];
    [[self selectedItems] removeObject:item];
    return removedPosition;
}

-(void)sendScoreToLeaderBoard
{
    [[GameCenterManager sharedInstance] updateLeaderBoard:[self totalBandCoins]];
}

-(BandStoreItem *)getNextSelectedStrItem:(BOOL)remove;
{
 
    if ([selectedItems count] > 0) {
        
        BandStoreItemsCtrl *bandStoreItemsCtrl = [BandStoreItemsCtrl sharedInstance];
        //BandVaultItem *firstItem = [selectedItems firstObject];
        self.selectedItem = [selectedItems firstObject];
        
        if (remove) {
            NSLog(@"Remove the item and consume it !");
            self.selectedItem = [selectedItems firstObject];
            [selectedItems removeObjectAtIndex:0];
#warning dont consume yet just for debug purposes
            //[self consumeItem:self.selectedItem.appId];
        }
        
        for (BandStoreItem *genericItem in [bandStoreItemsCtrl generalItems]) {
            NSLog(@"genericItem id %@ firstItem %@",genericItem.appStoreId, self.selectedItem.appId);
            if ([genericItem.appStoreId isEqualToString:self.selectedItem.appId]) {
                return genericItem;
            }
        }
    }else{
        return nil;
    }
        
    //it is supposed to never reach here.
    return nil;
}




@end
