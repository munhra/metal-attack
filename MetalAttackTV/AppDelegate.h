//
//  AppDelegate.h
//  MetalAttackTV
//
//  Created by Rafael M. A. da Silva on 4/15/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface AppDelegate : CCAppDelegate <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

