//
//  ViewController.m
//  MetalAttackTV
//
//  Created by Rafael M. A. da Silva on 4/15/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
