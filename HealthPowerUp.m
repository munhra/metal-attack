//
//  HealthPowerUp.m
//  MetalAttack
//
//  Created by Rafael Munhoz on 16/01/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "HealthPowerUp.h"
#import "GameSoundManager.h"
#import "BandVault.h"
#import "BandVaultItem.h"
#import "CCTextureCache.h"
#import "BandPowerUp.h"

@implementation HealthPowerUp

@synthesize healthPwd1;
@synthesize healthPwd2;
@synthesize healthPwd3;
@synthesize levelDelegate;


CGSize screenSize;
CGPoint bandPosition;
//const int powerUPZorder = 90;

/*
+(id)sharedInstance
{
    static id master = nil;
    @synchronized(self)
    {
        if (master == nil){
            master = [self new];
        }
    }
    return master;
}*/

-(void)removePowerUps
{
    //[[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/revival/bass.png"];
    //[[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/revival/drummer.png"];
    //[[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/revival/vocal.png"];
}

-(void)preparePowerUps:(CGPoint)bandposition
{
    BandVault *vault = [BandVault sharedInstance];
    for (BandVaultItem *vaultItem in [vault selectedItems]) {
        NSString *itemId = [vaultItem.appId substringFromIndex:4];
        
        switch ([itemId intValue]) {
            case 50:
                healthPwd1 = (BandPowerUp *) [CCBReader load:@"health/Health1"];
                break;
            case 51:
                healthPwd2 = (BandPowerUp *) [CCBReader load:@"health/Health2"];
                break;
            case 52:
                healthPwd3 = (BandPowerUp *) [CCBReader load:@"health/Health3"];
                break;
            default:
                break;
        }
    }
    
    /*
    healthPwd1 = (CCSprite *) [CCBReader load:@"health/Health1"];
    healthPwd2 = (CCSprite *) [CCBReader load:@"health/Health2"];
    healthPwd3 = (CCSprite *) [CCBReader load:@"health/Health3"];
     */
    bandPosition = bandposition;
    screenSize = [[CCDirector sharedDirector] viewSize];
}

-(void)activateHealthPowerUp:(int)healthMultiplierId
{
    GameSoundManager *gameSoundManager = [GameSoundManager sharedInstance];
    
    switch (healthMultiplierId) {
        case 1:
            [healthPwd1 removeFromParent];
            [healthPwd1 setPosition:ccp(screenSize.width*0.5,screenSize.height*0.5)];
            [levelDelegate addChild:healthPwd1 z:90];
            [[healthPwd1 animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
            [gameSoundManager playSoundEffectByName:@"PowerUpFX05"];
            break;
        case 2:
            [healthPwd2 removeFromParent];
            [healthPwd2 setPosition:ccp(screenSize.width*0.5,screenSize.height*0.48)];
            [levelDelegate addChild:healthPwd2 z:90];
            [[healthPwd2 animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
            [gameSoundManager playSoundEffectByName:@"PowerUpFX06"];
            break;
        case 3:
            [healthPwd3 removeFromParent];
            [healthPwd3 setPosition:ccp(screenSize.width*0.5,screenSize.height*0.65)];
            [levelDelegate addChild:healthPwd3 z:90];
            [[healthPwd3 animationManager] runAnimationsForSequenceNamed:@"Default Timeline"];
            [gameSoundManager playSoundEffectByName:@"PowerUpFX07"];
            break;
        default:
            break;
    }
}



@end
