//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  BandSprite.h
//  EightbitShooter
//
//  Created by Rafael Munhoz on 12/03/13.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "BandVault.h"
#import "BandStoreItemsCtrl.h"
#import "BandGuySprite.h"
#import "BandShoot.h"

typedef enum {
    GUITARRIST1_TYPE = 0,
    GUITARRIST2_TYPE = 1,
    BASS_TYPE = 2,
    VOCALS_TYPE = 3,
    DRUMMER_TYPE = 4,
    ALL_GUITARS = 5,
    ALL_GUYS = 6
} BandComponents;

//const int invulPwupZindex = 30;

@interface BandSprite : CCSprite
{
    NSMutableArray *activeShoots;
    
    float drummerArmor;
    float guitar1Armor;
    float guitar2Armor;
    float vocalArmor;
    float bassArmor;
    
    float totalDrummerArmor;
    float totalGuitar1Armor;
    float totalGuitar2Armor;
    float totalVocalArmor;
    float totalBassArmor;

    float drummerPowerConsumption;
    float drummerPowerRestoration;
    float guitar1PowerConsumption;
    float guitar1PowerRestoration;
    float guitar2PowerConsumption;
    float guitar2PowerRestoration;
    float vocalPowerConsumption;
    float vocalPowerRestoration;
    float bassPowerConsumption;
    float bassPowerRestoration;
    
    BOOL bandReleaseFire;
    BOOL isGuita1Death;
    BOOL isGuita2Death;
    BOOL isBassDeath;
    BOOL isDrummerDeath;
    BOOL isVocalDeath;
    
    CCParticleSystem *bandParticle;
    CCParticleSystem *bandParticleBlaster;
    
    float guitar1ShootPower;
    float guitar2ShootPower;
    float bassShootPower;
    float vocalsShootPower;
    float drummerShootPower;
    float shootPower;
    
    int bandBlast;
    int bandCoins;
    int maxBandLevel;
    
    BandShoot *bandShoot;
    
    NSMutableArray *megaShoots;
    
    CCSprite *drummerPoster;
    CCSprite *bassPoster;
    CCSprite *vocalPoster;
    CCSprite *leadPoster;
    CCSprite *basePoster;
    
    /*
    NSString *drummerPosterFileName;
    NSString *bassPosterFileName;
    NSString *vocalPosterFileName;
    NSString *leadPosterFileName;
    NSString *basePosterFileName;
    */
    
    CCSprite *drummerEnd;
    CCSprite *bassEnd;
    CCSprite *vocalEnd;
    CCSprite *leadEnd;
    CCSprite *baseEnd;
    
    NSString *drummerEndingText;
    NSString *bassEndText;
    NSString *vocalEndText;
    NSString *leadEndText;
    NSString *baseEndText;
    
    NSString *drummerEndFileName;
    NSString *bassEndFileName;
    NSString *vocalEndFileName;
    NSString *leadEndFileName;
    NSString *baseEndFileName;

    CCSprite *drummerLife;
    CCSprite *bassLife;
    CCSprite *vocalLife;
    CCSprite *leadLife;
    CCSprite *baseLife;
    
    CCNode *revivalAll;
    
    BOOL invulnerability;
    BOOL drummerInvulnerabiliy;
    BOOL bassInvulnerabiliy;
    BOOL guitarBothInvulnerabiliy;
    BOOL vocalInvulnerabiliy;
    
    // if diferent from 0 it means that megashot is enabled
    float megaShotValue;
    int megaShieldValue;
    int selectedMegaShoot;
    
    //float totalPowerConsumption;
    //float totalPowerRestoration;
    
}

@property (nonatomic,weak) BandGuySprite *drummer;
@property (nonatomic,weak) BandGuySprite *leadGuitar;
@property (nonatomic,weak) BandGuySprite *baseGuitar;
@property (nonatomic,weak) BandGuySprite *vocal;
@property (nonatomic,weak) BandGuySprite *bass;

@property (nonatomic,weak) CCSprite *revivalBass;
@property (nonatomic,weak) CCSprite *revivalDrummer;
@property (nonatomic,weak) CCSprite *revivalBase;
@property (nonatomic,weak) CCSprite *revivalLead;
@property (nonatomic,weak) CCSprite *revivalVocal;

@property (nonatomic,retain) NSMutableArray *activeShoots;
@property (nonatomic) BOOL bandReleaseFire;

@property (nonatomic) float drummerArmor;
@property (nonatomic) float guitar1Armor;
@property (nonatomic) float guitar2Armor;
@property (nonatomic) float vocalArmor;
@property (nonatomic) float bassArmor;
@property (nonatomic, weak) id delegate;

@property (nonatomic) float guitar1ShootPower;
@property (nonatomic) float guitar2ShootPower;
@property (nonatomic) float bassShootPower;
@property (nonatomic) float vocalsShootPower;
@property (nonatomic) float drummerShootPower;
@property (nonatomic) float shootPower; // Total shoot power, it will be replaced

@property (nonatomic) int bandBlast;

@property (nonatomic) BOOL isGuita1Death;
@property (nonatomic) BOOL isGuita2Death;
@property (nonatomic) BOOL isBassDeath;
@property (nonatomic) BOOL isDrummerDeath;
@property (nonatomic) BOOL isVocalDeath;

@property (nonatomic) int bandCoins;

@property (nonatomic) float totalDrummerArmor;
@property (nonatomic) float totalGuitar1Armor;
@property (nonatomic) float totalGuitar2Armor;
@property (nonatomic) float totalVocalArmor;
@property (nonatomic) float totalBassArmor;

@property (nonatomic) BOOL invulnerability;
@property (nonatomic) BOOL drummerInvulnerabiliy;
@property (nonatomic) BOOL bassInvulnerabiliy;
@property (nonatomic) BOOL guitarBothInvulnerabiliy;
@property (nonatomic) BOOL vocalInvulnerabiliy;


// percentage of shoot increase it can go from 10% to 100%
@property (nonatomic) float megaShotValue;
@property (nonatomic) int megaShieldValue;
@property (nonatomic,retain) BandShoot *bandShoot;

@property (nonatomic) int maxBandLevel;

@property (nonatomic, retain) CCSprite *drummerPoster;
@property (nonatomic, retain) CCSprite *bassPoster;
@property (nonatomic, retain) CCSprite *vocalPoster;
@property (nonatomic, retain) CCSprite *leadPoster;
@property (nonatomic, retain) CCSprite *basePoster;

@property (nonatomic, weak) NSString *drummerPosterFileName;
@property (nonatomic, weak) NSString *bassPosterFileName;
@property (nonatomic, weak) NSString *vocalPosterFileName;
@property (nonatomic, weak) NSString *leadPosterFileName;
@property (nonatomic, weak) NSString *basePosterFileName;

@property (nonatomic, retain) CCSprite *drummerEnd;
@property (nonatomic, retain) CCSprite *bassEnd;
@property (nonatomic, retain) CCSprite *vocalEnd;
@property (nonatomic, retain) CCSprite *leadEnd;
@property (nonatomic, retain) CCSprite *baseend;

@property (nonatomic, retain) NSString *drummerEndFileName;
@property (nonatomic, retain) NSString *bassEndFileName;
@property (nonatomic, retain) NSString *vocalEndFileName;
@property (nonatomic, retain) NSString *leadEndFileName;
@property (nonatomic, retain) NSString *baseendFileName;

@property (nonatomic, retain) CCSprite *drummerLife;
@property (nonatomic, retain) CCSprite *bassLife;
@property (nonatomic, retain) CCSprite *vocalLife;
@property (nonatomic, retain) CCSprite *leadLife;
@property (nonatomic, retain) CCSprite *baseLife;
    
@property (nonatomic, retain) NSString *drummerEndText;
@property (nonatomic, retain) NSString *bassEndText;
@property (nonatomic, retain) NSString *vocalEndText;
@property (nonatomic, retain) NSString *leadEndText;
@property (nonatomic, retain) NSString *baseEndText;
    
@property (nonatomic, retain) CCNode *revivalAll;
@property (nonatomic, retain) NSMutableArray *megaShoots;
@property (nonatomic) int selectedMegaShoot;

@property (nonatomic) float drummerPowerConsumption;
@property (nonatomic) float drummerPowerRestoration;
@property (nonatomic) float guitar1PowerConsumption;
@property (nonatomic) float guitar1PowerRestoration;
@property (nonatomic) float guitar2PowerConsumption;
@property (nonatomic) float guitar2PowerRestoration;
@property (nonatomic) float vocalPowerConsumption;
@property (nonatomic) float vocalPowerRestoration;
@property (nonatomic) float bassPowerConsumption;
@property (nonatomic) float bassPowerRestoration;

-(void)performBandDeath;
-(float)hitByEnemy:(float)damagePoints component:(BandComponents)type rect:(CGRect )collrect;
-(void)performBandBlast;
-(void)receiveHealthPowerUp:(float)value;
-(float)getTotalArmor;
-(BandShoot *)createBandShoot;
-(void)scheduleReleaseFire;
-(void)revive:(BandItemType)type withArmor:(float)value;
-(void)changeInvunerability:(BandComponents)type invunerability:(BOOL)value;
-(void)selectMegaShotValue:(float)power spritePosition:(int)position;
-(void)healingWithArmor:(float)value;
-(float)shootPowerWithMegaShoot;

@end


