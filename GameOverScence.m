//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//
//  GameOverScence.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 3/10/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "GameOverScence.h"
#import "LevelScene.h"
#import "LevelSceneController.h"
#import "ReloadLevel.h"
#import "MetalAttackAnalytics.h"
#import "GameSoundManager.h"
#import <AdColony/AdColony.h>
#import "CCTextureCache.h"

@implementation GameOverScence
@synthesize door;
@synthesize levelNumber;

-(void)onEnter
{
    [super onEnter];
    [[GameSoundManager sharedInstance] playGameOverSong];
    [[door animationManager] runAnimationsForSequenceNamed:@"Closed"];
    [[door animationManager] runAnimationsForSequenceNamed:@"OpenDoor"];
    [[self animationManager] runAnimationsForSequenceNamed:@"GameOverSequence"];
    [[MetalAttackAnalytics sharedInstance] registerScreenAnalytics:@"GameOverScene"];
    [[UniversalInfo sharedInstance] playVideoForReward:30];
}

-(void)onExit
{
    [super onExit];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    [[GameSoundManager sharedInstance] stopBackgroundSong];
    [self removeAllChildren];
    [self removeGameOverTextures];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}

-(void)removeGameOverTextures
{
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/GameOver/lightning.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/GameOver/pano 01.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/GameOver/pano 02.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/GameOver/pano 03.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/GameOver/guitar.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"ccbResources/GameOver/lightning.png"];
}

-(void)doMenu
{
    NSLog(@"doMenu");
    [[CCDirector sharedDirector] resume];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainMenu"]];
    }];
}

-(void)doRestartLevel
{
    NSLog(@"restartLevel");
    
    [[CCDirector sharedDirector] resume];
    [[door animationManager] runAnimationsForSequenceNamed:@"CloseDoor"];
    __weak GameOverScence *weakSelf = self;
    [[door animationManager] setCompletedAnimationCallbackBlock:^(id sender) {
        CCScene *relScene =  [CCBReader loadAsScene:@"ReloadLevel"];
        ReloadLevel *reloadLevel = (ReloadLevel *)[relScene getChildByName:@"ReloadLevel" recursively:YES];
        [reloadLevel setLevelToReload:[weakSelf levelNumber]];
        [[CCDirector sharedDirector] replaceScene:relScene];
    }];
}
    
-(void)playGameOverFlash
{
    NSLog(@"playGameOverFlash");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx053"];
}

-(void)playGameOverWord
{
    NSLog(@"playGameOverFlash");
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx054"];
}

-(void)dealloc {
    NSLog(@"dealloc gameover");
}

@end

