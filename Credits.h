//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//  Credits.h
//  HelloWorldCocos
//
//  Created by Rafael Munhoz on 23/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Credits : CCNode
{
    CCNode *door;
    CCSprite *lifeBar;
    NSArray *shirtStamps;
    int currentStampIndex;
    int previousStampIndex;
    float previousPixelBlockSize;
    float currentPixelBlockSize;
    bool updateStamp;
    bool achivmentShowed;
}

@property(retain,nonatomic) CCNode *door;
@property(retain,nonatomic) CCSprite *lifeBar;

@property(weak,nonatomic) CCEffectNode *leadProgrammerEffects;
@property(weak,nonatomic) CCEffectNode *artDesignEffectsNode;
@property(weak,nonatomic) CCEffectNode *thanksEffectsNode;

@property(weak,nonatomic) CCEffectNode *friend1;
@property(weak,nonatomic) CCEffectNode *friend2;
@property(weak,nonatomic) CCEffectNode *friend3;
@property(weak,nonatomic) CCEffectNode *friend4;
@property(weak,nonatomic) CCEffectNode *friend5;
@property(weak,nonatomic) CCEffectNode *friend6;

@property(weak,nonatomic) CCNode *girlButt;
@property(retain,nonatomic) NSArray *shirtStamps;
@property(nonatomic) int currentStampIndex;
@property(nonatomic) int previousStampIndex;
@property(nonatomic) float previousPixelBlockSize;
@property(nonatomic) float currentPixelBlockSize;
@property(nonatomic) bool updateStamp;
@property(nonatomic) bool achivmentShowed;

@end
