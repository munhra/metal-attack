//
//  ███╗   ███╗███████╗████████╗ █████╗ ██╗          █████╗ ████████╗████████╗ █████╗  ██████╗██╗  ██╗
//  ████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║         ██╔══██╗╚══██╔══╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
//  ██╔████╔██║█████╗     ██║   ███████║██║         ███████║   ██║      ██║   ███████║██║     █████╔╝
//  ██║╚██╔╝██║██╔══╝     ██║   ██╔══██║██║         ██╔══██║   ██║      ██║   ██╔══██║██║     ██╔═██╗
//  ██║ ╚═╝ ██║███████╗   ██║   ██║  ██║███████╗    ██║  ██║   ██║      ██║   ██║  ██║╚██████╗██║  ██╗
//  ╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
//
//
//  BandStoreItem.h
//  EightbitShooter
//
//  Created by Rafael Munhoz on 03/04/13.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "BandGuySprite.h"

typedef enum {
    DRUMMER = 0,
    GUITARRIST_1 = 1,
    GUITARRIST_2 = 2,
    VOCALIST = 3,
    BASSGUY = 4,
    BAND_COIN_PACK = 5,
    BAND_SKIN = 6,
    GENERIC_ITEM = 7,
    GUITARRIST_BOTH = 8,
    ALL_BAND = 9
} BandItemType;

typedef enum {
    NO_COOL_DOWN = 0,
    COIN_MULTIPLIER = 1,
    MEGA_SHOT = 2,
    SHIELD = 3,
    REVIVAL_1 = 4,
    INVULNERABILITY_1 = 5,
    INVULNERABILITY_2 = 6,
    REVIVAL_2 = 7,
    HEALTH = 8
    
} BandGenericItemType;

@interface BandStoreItem : NSObject

{
    // id of the generic item
    int itemid;
    // it will be used inside the game as key for the item.
    NSString *appStoreId;
    // Name of the item to be showed on at the game store
    NSString *itemName;
    // Price of the item in game credits
    int credits;
    // points of the item that will affect the game play, lets say for a weapon power up
    // how many shoot points it will add the same for armor.
    int value;
    // how many levels the item will be avaliable.
    int duration;
    // the type of the item, it can be ARMMOR, GUN, COIN_PACK or SKIN
    BandItemType itemType;
    // fantasy description of the item.
    NSString *description;
    // Ending bandguy text
    NSString *endingText;
    // Name of the file that represents the item. Each item will have a 2 frames
    // animation. The name of the asset shall fowllow the patter:
    // body1_frm, where body1 indentify the type of the item and
    // frm is the frame
    NSString *assetName;
    // flag to show if the item is owned or not
    bool ownedBandItem;
    // flag that indicates if the item is selected
    bool selectedBandItem;
    // this attribute represents how much of the item the user has
    int qtd;
    // sprite that represents the item
    CCSprite *itemSprt;
    // represents the band component when this item is a band component
    BandGuySprite *bandGuySprite;
    float armor;
    // shoot power valid for all guys from the band exluding the drummer
    float shootPower;
    //represents the level of the band component
    int level;
    //store the asset of the band component
    CCSprite *compShoot;
    //store the poster of the band component
    CCSprite *compPoster;
    //posterfile
    NSString *posterFileName;
    //hudlife image
    CCSprite *compLifeImage;
    //banner image
    CCSprite *compBannerImage;
    //poster store band guy
    CCSprite *compStorePosterImage;
    //poster store file name band guy
    NSString *storePosterFileName;
    //generic item type
    BandGenericItemType genericItemType;
    //band guy end sprite
    CCSprite *bandGuyEndSprite;
    //end file name
    NSString *bandGuyEndFileName;
    //frame
    int frame;
    //powerConsumption
    float powerConsumption;
    //powerRestoration
    float powerRestoration;
}

@property (nonatomic,retain) NSString *appStoreId;
@property (nonatomic,retain) NSString *itemName;
@property (nonatomic) int credits;
@property (nonatomic) int value;
@property (nonatomic) BandItemType itemType;
@property (nonatomic) BandGenericItemType genericItemType;
@property (nonatomic, retain) NSString *endingText;
@property (nonatomic, retain) NSString *description;
@property (nonatomic) int duration;
@property (nonatomic, retain) NSString *assetName;
@property (nonatomic) bool ownedBandItem;
@property (nonatomic) bool selectedBandItem;
@property (nonatomic) int qtd;
@property (nonatomic,retain) CCSprite *itemSprt;
@property (nonatomic, retain) BandGuySprite *bandGuySprite;
@property (nonatomic) float armor;
@property (nonatomic) float shootPower;
@property (nonatomic) int level;
@property (nonatomic,retain) CCSprite *compShoot;
@property (nonatomic,retain) CCSprite *compPoster;
@property (nonatomic,retain) NSString *posterFileName;
@property (nonatomic,retain) CCSprite *compLifeImage;
@property (nonatomic,retain) CCSprite *compBannerImage;
@property (nonatomic,retain) CCSprite *compStorePosterImage;
@property (nonatomic,retain) NSString *storePosterFileName;
@property (nonatomic,retain) CCSprite *bandGuyEndSprite;
@property (nonatomic,retain) NSString *bandGuyEndFileName;
@property (nonatomic) int itemid;
@property (nonatomic) int frame;
@property (nonatomic) float powerConsumption;
@property (nonatomic) float powerRestoration;

@end
