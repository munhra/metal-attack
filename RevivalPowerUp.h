//
//  RevivalPowerUp.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 25/11/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BandPowerUp.h"

@interface RevivalPowerUp : NSObject
{
    CCSprite *RevivalBass_LV1;
    CCSprite *RevivalBass_LV2;
    CCSprite *RevivalDrummer_LV1;
    CCSprite *RevivalDrummer_LV2;
    CCSprite *RevivalGuitar1_LV1;
    CCSprite *RevivalGuitar1_LV2;
    CCSprite *RevivalGuitar2_LV1;
    CCSprite *RevivalGuitar2_LV2;
    CCSprite *RevivalVocal_LV1;
    CCSprite *RevivalVocal_LV2;
    BandPowerUp *RevivalAllBand_LV1;
    BandPowerUp *RevivalAllBand_LV2;
}

@property(retain,nonatomic) CCSprite *RevivalBass_LV1;
@property(retain,nonatomic) CCSprite *RevivalBass_LV2;
@property(retain,nonatomic) CCSprite *RevivalDrummer_LV1;
@property(retain,nonatomic) CCSprite *RevivalDrummer_LV2;
@property(retain,nonatomic) CCSprite *RevivalGuitar1_LV1;
@property(retain,nonatomic) CCSprite *RevivalGuitar1_LV2;
@property(retain,nonatomic) CCSprite *RevivalGuitar2_LV1;
@property(retain,nonatomic) CCSprite *RevivalGuitar2_LV2;
@property(retain,nonatomic) CCSprite *RevivalVocal_LV1;
@property(retain,nonatomic) CCSprite *RevivalVocal_LV2;
@property(retain,nonatomic) BandPowerUp *RevivalAllBand_LV1;
@property(retain,nonatomic) BandPowerUp *RevivalAllBand_LV2;

//+(id)sharedInstance;
-(void)preparePowerUps;
-(void)removePowerUps;

@end
