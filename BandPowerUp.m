//
//  PowerUp.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 12/21/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "BandPowerUp.h"

@implementation BandPowerUp

@synthesize posAdjustX;
@synthesize posAdjustY;
@synthesize pwdScale;

-(void)onExit
{
    [super onExit];
    [self removeAllChildren];
}

- (void)dealloc
{
    NSLog(@"dealloc Band Power up ");
}

@end
