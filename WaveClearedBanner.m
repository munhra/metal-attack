//
//  WaveClearedBanner.m
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 4/23/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import "WaveClearedBanner.h"
#import "GameSoundManager.h"

@implementation WaveClearedBanner

@synthesize waveText;
@synthesize waveClearedText;
@synthesize waveNumberText;
@synthesize image;
@synthesize backgroundNine;
@synthesize hideWithPixelation;

-(void)waveClearedPlaySound
{
    [[GameSoundManager sharedInstance] playSoundEffectByName:@"_Soundfx064"];
}

- (void)dealloc
{
    NSLog(@"dealloc wavecleared");
}

@end
