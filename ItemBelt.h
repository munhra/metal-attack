//
//  ItemBelt.h
//  MetalAttack
//
//  Created by Rafael Munhoz on 10/10/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface ItemBelt : CCNode

@property(nonatomic,weak) CCButton *item1Button;
@property(nonatomic,weak) CCButton *item2Button;
@property(nonatomic,weak) CCButton *item3Button;
@property(nonatomic,weak) CCButton *item4Button;
@property(nonatomic,weak) CCButton *item5Button;
@property(nonatomic,weak) CCButton *item6Button;
@property(nonatomic,weak) CCButton *item7Button;
@property(nonatomic,weak) CCButton *item8Button;
@property(nonatomic,retain) NSArray *buttonArray;
@property(nonatomic,retain) NSMutableArray *buttonItemArray;
@property(nonatomic, weak) id delegate;
@property(nonatomic) BOOL hidden;

-(void)loadSelectedItems;
-(void)activateItem:(CCButton*)sender;
-(void)enableUserInteraction;
-(void)disableUserInteraction;

@end
