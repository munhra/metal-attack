//
//  LevelSelector.h
//  MetalAttack
//
//  Created by Rafael M. A. da Silva on 29/04/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LevelSlider.h"

@interface LevelSelector : CCNode

@property (nonatomic, weak) CCButton *level1Button;
@property (nonatomic, weak) CCButton *level2Button;
@property (nonatomic, weak) CCButton *level3Button;
@property (nonatomic, weak) CCButton *level4Button;
@property (nonatomic, weak) CCButton *level5Button;
@property (nonatomic, weak) LevelSlider *levelSlider;
@property (nonatomic, weak) CCSprite *lockedLevel;
@property (nonatomic, weak) CCLabelTTF *levelNumber;
@property (nonatomic) int selectedLevelNumber;
@property (nonatomic, retain) NSMutableArray *levelsPerContext;
@property (nonatomic, retain) NSMutableArray *firstLevelPerContext;
@property (nonatomic, retain) NSArray *buttonArray;
@property (nonatomic) int reachedContext;
@property (nonatomic) int reachedLevel;

-(void)doLevelSelection;
-(void)cancelLevelSelection;
-(void)selectLevel:(id)sender;
-(void)levelSlideChange:(id)sender;
-(void)doMenu;
-(void)addLevel;
-(void)subtractLevel;
-(void)changeSlideValue:(float)sliderValue;

@end
